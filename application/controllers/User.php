<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    var $content = "";
	var $addHeader = array();
    function index($dok = "") {
        if($this->newsession->userdata('LOGGED')){
			$this->load->model('main');
			$this->main->get_index($dok,$this->addHeader);	
		}else{
			$this->newsession->sess_destroy();		
			redirect(base_url());
		}
    }

    function profil($tipe = "") {
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        if ($tipe == "lihat")
            $aksi = "view";
        if ($tipe == "ubah")
            $aksi = "edit";

        $this->load->model("usertrader_act");
        $data = $this->usertrader_act->get_profil($aksi);
        $this->content = $this->load->view('user/profil', $data, true);
        $this->index();
    }
    function edit() {
        $func = get_instance();
        $func->load->model("main", "main", true);
        $this->load->model("usertrader_act");
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $data = $this->usertrader_act->proses_form('profil');
            echo $data;
        }
    }

    function userlist($tipe = "") {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
        if ($this->newsession->userdata('LOGGED') && $this->newsession->userdata('KODE_ROLE') == 3) {
            $this->load->model("usertrader_act");
            $arrdata = $this->usertrader_act->daftar($tipe);
            $data = $this->content = $this->load->view('list', $arrdata, true);
            if ($this->input->post("ajax")) {
                echo $arrdata;
            } else {
                $this->content = $data;
                $this->index();
            }
        } else {
            $this->newsession->sess_destroy();
            $this->index();
            return;
        }
    }

    function set_user($tipe = "") {
        $this->load->model("usertrader_act");
        $ret = $this->usertrader_act->set_user($this->input->post("act"), $tipe);
    }

    function user_dok($tipe = "") {
        $this->load->model("usertrader_act");
        $arrdata = $this->usertrader_act->daftar($tipe);
        echo $this->load->view('user/inv', $arrdata, true);
    }

    function password() {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'font_path' => '../sys/fonts/texb.ttf',
            'img_height' => 35,
            'img_width' => 110,
            'expiration' => 120
        );
        $cap = create_captcha($vals);
        $this->newsession->set_userdata('capt_login', $cap['word']);


        $this->load->model("usertrader_act");
        $data = $this->usertrader_act->get_password();
        $data['cap'] = $cap['image'];
        $this->content = $this->load->view('user/password', $data, true);
        $this->index();
    }

    public function reload_captcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/',
            'font_path' => '../sys/fonts/texb.ttf',
            'img_height' => 35,
            'img_width' => 110,
            'expiration' => 120
        );
        $cap = create_captcha($vals);
        $this->newsession->set_userdata('capt_login', $cap['word']);
        echo $cap['image'];
    }

    function updatePass() {
        $this->load->model("usertrader_act");
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $data = $this->usertrader_act->updatePass();
            echo $data;
        }
    }

    /////////////////////////// NEW ///////////////////////////

    function add($tipe = "") {
        $func = get_instance();
        $func->load->model("main", "main", true);
        $this->load->model("usertrader_act");
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["ui"] = 1;
        if ($tipe == "user") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->usertrader_act->proses_form($tipe);
                echo $data;
            }
            $judul = "Tambah Data User";
        }
        $menu_management = $this->usertrader_act->menu_management();
        $data = array("menu" => $menu_management,
            "judul" => $judul,
            "act" => "save",
            "dokumen" => $dok,
            'kode_role' => $func->main->get_combobox("SELECT KODE_ROLE, NAMA FROM M_ROLE 
																			   WHERE KODE_ROLE IN('3','4','5') 
																			   ORDER BY KODE_ROLE", "KODE_ROLE", "NAMA", TRUE),
            'status_user' => $func->main->get_mtabel('STATUS_USER', '', true, ' AND KODE <> 0'));
        $this->content = $this->load->view('user/form', $data, true);
        $this->index($tipe);
    }

    function getmenu() {
        $this->load->model("usertrader_act");
        echo $this->usertrader_act->menu_management();
    }

    function editUser($tipe = "", $id = "") {
        $func = get_instance();
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["ui"] = 1;
        $func->load->model("main", "main", true);
        $this->load->model("usertrader_act");
        if ($tipe == "user") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $id = $this->input->post('idUser');
                $data = $this->usertrader_act->proses_form($tipe, $id);
                echo $data;
            }
            $judul = "Ubah Data User";
        }
        $menu_management = $this->usertrader_act->menu_management($id);
        $resultdata = $this->usertrader_act->getData($tipe, $id);
        $data = array("menu" => $menu_management,
            "judul" => $judul,
            "act" => "update",
            "dokumen" => $dok,
            "sess" => $resultdata,
            'kode_role' => $func->main->get_combobox("SELECT KODE_ROLE, NAMA FROM m_role 
																			   WHERE KODE_ROLE IN('3','4','5') 
																			   ORDER BY KODE_ROLE", "KODE_ROLE", "NAMA", TRUE),
            'status_user' => $func->main->get_mtabel('STATUS_USER'));
        $this->content = $this->load->view('user/form', $data, true);
        $this->index($tipe);
    }

    function activitylog() {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
        if ($this->newsession->userdata('LOGGED') && ($this->newsession->userdata('KODE_ROLE') == 3 or $this->newsession->userdata('KODE_ROLE') == 5)) {
            $this->load->model("user_act");
            $arrdata = $this->user_act->activitylog();
            $data = $this->load->view('list', $arrdata, true);
            if ($this->input->post("ajax")) {
                echo $arrdata;
            } else {
                $this->content = $data;
                $this->index();
            }
        } else {
            $this->newsession->sess_destroy();
            $this->index();
            return;
        }
    }

    function revisilog() {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
        if ($this->newsession->userdata('LOGGED') && $this->newsession->userdata('KODE_ROLE') == 3) {
            $this->load->model("user_act");
            $arrdata = $this->user_act->revisilog();
            $data = $this->load->view('list', $arrdata, true);
            if ($this->input->post("ajax")) {
                echo $arrdata;
            } else {
                $this->content = $data;
                $this->index();
            }
        } else {
            $this->newsession->sess_destroy();
            $this->index();
            return;
        }
    }

}

?>
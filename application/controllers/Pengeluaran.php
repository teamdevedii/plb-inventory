<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran extends CI_Controller{
	var $content = "";
	var $addHeader = array();
	public function index($dok = "") {
        if($this->newsession->userdata('LOGGED')){
			$this->load->model('main');
			$this->main->get_index($dok,$this->addHeader);	
		}else{
			$this->newsession->sess_destroy();		
			redirect(base_url());
		}
    }
	
	public function create($dok=""){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		if(in_array(strtoupper($dok),array('BC282','BC27','BC30','BC41','PPB')) && $dok){
			$this->pabean($dok);
		}else{
			$this->content = $this->load->view('pengeluaran/Create', '', true);		
			$this->index();
		}
	}

	public function daftar($tipe = "", $dokumen = "", $aju = "") {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;		
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		if ($tipe == "priview") {
			$this->priview($dokumen,$aju);
		} else {
			$this->load->model("pengeluaran_act");
			$arrdata = $this->pengeluaran_act->daftar($tipe, $dokumen, $aju);		
			$data = $this->load->view('box-pabean', $arrdata, true);
			if ($this->input->post("ajax")) {
				echo  $arrdata;
			} else {
				$this->content = $data;
				$this->index();
			}
		}
	}
	
	public function pabean($dok="",$asal="",$jns=""){
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		$dokname = str_replace("bc","",$dok);
		$subdok="";
		for($a=0; $a<strlen($dokname); $a++){
			$subdok .= substr($dokname,$a,1).".";
		}
		$dokumen = "BC ".substr($subdok,0,strlen($subdok)-1);		
		$data = array("judul"=>"Data Pengeluaran $dokumen",
					  "dokumen"=>$dok);	
		$this->content = $this->load->view('pengeluaran/'.$dok.'/Tabs', $data, true);    	
		$this->index();	
	}
	function set_ppb($act="",$id=""){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}		
		$this->load->model("pengeluaran_act");
		if(strtolower($_SERVER['REQUEST_METHOD'])=="post"){	
			echo $this->inventory_act->set_ppbkb($act);
		}else{			
			//$mdl = $this->inventory_act->get_ppbkb($act,$id);
			if($act=="view"){
				echo $this->load->view("pengeluaran/ppb",'',true);
			}else{
				$this->content = $this->load->view("pengeluaran/ppb/ppb", "",true);
				$this->index(); 
			}
		}
	}

	public function bc282($isajax = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post"){
            redirect(base_url());
            exit();
        }else{
            $this->load->model("bc282/header_act");
            $this->header_act->set_header($this->input->post("act"), $isajax);
        }
        if($isajax != "ajax"){
            redirect(base_url());
            exit();
        }
    }
	
}
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemasukan extends CI_Controller{
	var $content = "";
	var $dokumen = "";
	var $addHeader = array();
	function index($dok = "") {
        if($this->newsession->userdata('LOGGED')){
			$this->load->model('main');
			$this->dokumen = $dok;
			$this->main->get_index($dok,$this->addHeader);	
		}else{
			$this->newsession->sess_destroy();		
			redirect(base_url());
		}
    }
	
	function create($dok=""){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		if(in_array(strtoupper($dok),array('BC281','BC27','BC40','BC24','BC30'))&&$dok){
			$this->pabean($dok);		
		}else{
			$this->content = $this->load->view('pemasukan/Create', '', true);		
			$this->index();
		}
	}
	
	function pabean($dok="",$asal="",$jns=""){
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		$dokname = str_replace("bc","",$dok);
		$subdok="";
		for($a=0;$a<strlen($dokname);$a++){
			$subdok .= substr($dokname,$a,1).".";
		}
		$dokumen = "BC ".substr($subdok,0,strlen($subdok)-1);		
		$data = array("judul"=>"Data Pemasukan $dokumen",
					  "dokumen"=>$dok);	
		$this->content = $this->load->view('pemasukan/'.$dok.'/Tabs', $data, true);    	
		$this->index();	
	}
	
	function draft($tipe="draft",$dokumen="",$aju=""){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["ui"] = 1;

		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		if($tipe=="priview"){
			$this->priview($dokumen,$aju);
		}		
		else{
			$this->load->model("pemasukan_act");
			$arrdata = $this->pemasukan_act->daftar($tipe,$dokumen,$aju);		
			$data = $this->load->view('box-pabean', $arrdata, true);
			if($this->input->post("ajax")){
				echo  $arrdata;
			}else{
				$this->content = $data;
				$this->index();
			}
		}
	}
	
	function approved($tipe="approved",$dokumen="",$aju=""){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["ui"]    = 1;
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}	
		if($tipe=="priview"){
			$this->priview($dokumen,$aju);
		}		
		else{
			$this->load->model("pemasukan_act");
			$arrdata = $this->pemasukan_act->daftar($tipe,$dokumen,$aju);		
			$data = $this->load->view('box-pabean', $arrdata, true);
			if($this->input->post("ajax")){
				echo  $arrdata;
			}else{
				$this->content = $data;
				$this->index();
			}
		}
	}
	
	function hitungan($id = "", $data = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        $this->load->model('main');
        $data = array('data' => $data,
            		  'kode_harga' => $this->main->get_mtabel('KODE_HARGA'),
            		  'kode_asuransi' => $this->main->get_mtabel('KODE_ASURANSI', 1, FALSE));
        $this->load->view('pemasukan/'.$id.'/Hitungan', $data);
    }
	
	function bc281($isajax = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post"){
            redirect(base_url());
            exit();
        }else{
            $this->load->model("bc281/header_act");
            $this->header_act->set_header($this->input->post("act"), $isajax);
        }
        if($isajax != "ajax"){
            redirect(base_url());
            exit();
        }
    }
	
	function bc27($isajax = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post"){
            redirect(base_url());
            exit();
        }else{
            $this->load->model("bc27/header_act");
            $this->header_act->set_header($this->input->post("act"), $isajax);
        }
        if($isajax != "ajax"){
            redirect(base_url());
            exit();
        }
    }
	
	function bc40($isajax = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post"){
            redirect(base_url());
            exit();
        }else{
            $this->load->model("bc40/header_act");
            $this->header_act->set_header($this->input->post("act"), $isajax);
        }
        if($isajax != "ajax"){
            redirect(base_url());
            exit();
        }
    }
	
	function bc30($isajax = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post"){
            redirect(base_url());
            exit();
        }else{
            $this->load->model("bc30/header_act");
            $this->header_act->set_header($this->input->post("act"), $isajax);
        }
        if($isajax != "ajax"){
            redirect(base_url());
            exit();
        }
    }
	
	function sstb($tipe="",$id="",$sub=false){
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
		if(in_array($tipe,array("save","update","delete"))){ 
			$this->set_sstb($tipe,$id,$sub);   
		}else{
			$this->load->model('pemasukan_act');
			$table = $this->pemasukan_act->sstb($tipe,$sub);  
			$data = $this->load->view('list', $table, true);
			if($this->input->post("ajax")||$tipe=="ajax"){
				echo $table;
			}else{
				$this->content = $data;
				$this->index();
			}
		}
	}
	
	function set_sstb($tipe="",$id=""){ 
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
		$generate = $this->input->post("generate");
		if(strtolower($_SERVER['REQUEST_METHOD'])=="post" && $generate!="formjs"){ 
			$this->load->model('sstb/header_act');
			$this->header_act->set_sstb($tipe,$id);
		}else{
			if($generate=="formjs"){
				$id = $this->input->post("id1");
				$tipe = $this->input->post("action");
			}
			$this->load->model('pemasukan_act');
			$arrdata = $this->pemasukan_act->get_sstb($tipe,$id);
			$this->content = $this->load->view('pemasukan/sstb/Tabs',$arrdata,true);
			$this->index();
		}
	}
	
	function detil_sstb($act="",$IDSSTB=""){  
		if($act=='save' || $act=='update'){
			$this->load->model("sstb/barang_act"); 
			$mdldata = $this->barang_act->get_barang($act,$IDSSTB);   
			echo $this->load->view('pemasukan/sstb/Barang', $mdldata, true);	
		}else{
			$this->load->model("sstb/detil_act");	
			$modeldata = $this->detil_act->detil($act,$IDSSTB);  
			$data = $this->load->view('list', $modeldata, true);	
			if($this->input->post("ajax")||$act=="ajax"){
				echo $modeldata;
			} else {
				echo $data;
			}
		}
	}
	
	function setujui($data="",$div=""){
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
		$this->load->model("pemasukan_act");
		if(strtolower($_SERVER['REQUEST_METHOD'])=="post"){
			echo $this->pemasukan_act->setujui();
		}else{
			if($data){
				$data = explode(",",$data); 
				$dokumen = $data[0];
				$noaju = $data[1];
				$arrdata = $this->pemasukan_act->getInformasiBC($dokumen,$noaju);
				$data = array("dokumen"=>$dokumen,"noaju"=>$noaju,"DATA"=>$arrdata,"div"=>$div);
			}
			echo $this->load->view("pemasukan/Persetujuan", $data); 
		}
	}
	
	function LoadSSTB($type = "", $id = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        $this->load->model("sstb/header_act");
        $data = $this->header_act->get_sstb($type,$id);
        echo $this->load->view("pemasukan/sstb/Header", $data,true);
    }
	
	function LoadHeader($type = "", $aju = "") {
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
        $this->load->model("".$type."/header_act");
        $data = $this->header_act->get_header($aju);
        echo $this->load->view("pemasukan/".$type."/Header", $data);
    }
	
	function getBm() {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("bc281/barang_act");
        $data = $this->barang_act->get_barang();
        echo $this->load->view("pemasukan/bc281/Bm", $data);
    }
	
	function barang($type = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            #call form barang for edit	(from table)		
            if ($this->input->post("edit") == "edit") {
                foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                }
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                (strtolower($type) == "bc27") ? $kdbarang = str_replace("^", ".", $arrchk[2]) : $kdbarang = str_replace("^", ".", $arrchk[3]);
                $this->load->model($type . "/barang_act");
                $data = $this->barang_act->get_barang($aju, $seri, $kdbarang);
                $data = array_merge($data, array('edit' => "edit"));
                return $this->load->view("pemasukan/" . $type . "/Barang", $data);
            }
            #call form barang for add	(from table)
            elseif ($this->input->post("edit") == "add") {
                $aju = $isajax;
                $this->load->model($type . "/barang_act");
                $data = $this->barang_act->get_barang($aju, $seri);
                $data = array_merge($data, array('edit' => "add"));
                return $this->load->view("pemasukan/" . $type . "/Barang", $data);
            } else {
                #save barang	
                $this->load->model("" . $type . "/barang_act");
                $data = $this->barang_act->set_barang($this->input->post("act"), $isajax);
            }
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
    }
	
	function kemasan($type = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            #call form kemasan for edit (from table)		
            if ($this->input->post("edit") == "edit") {
                foreach ($this->input->post('tb_chkfkemasan') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                }
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $this->load->model($type . "/kemasan_act");
                $data = $this->kemasan_act->get_kemasan($aju, $seri);
                $data = array_merge($data, array('edit' => "edit"));
                return $this->load->view("pemasukan/" . $type . "/Kemasan", $data);
            }
			#all form kemasan for add	(from table)
            elseif ($this->input->post("edit") == "add") {
                $aju = $isajax;
                $this->load->model($type . "/kemasan_act");
                $data = $this->kemasan_act->get_kemasan($aju, $seri);
                $data = array_merge($data, array('edit' => "add"));
                return $this->load->view("pemasukan/" . $type . "/Kemasan", $data);
            } else {
                #save & delete kemasan
                $this->load->model("" . $type . "/kemasan_act");
                $this->kemasan_act->set_kemasan($this->input->post("act"), $isajax);
            }
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
    }
	
	function dokumen($type = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            #call form dokumen for edit (from table)		
            if ($this->input->post("edit") == "edit") {
                foreach ($this->input->post('tb_chkfdokumen') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                }
                $aju = $arrchk[0];
                $nomor = str_replace("~", "/",$arrchk[1]);
                $kddoc = $arrchk[2];

                $this->load->model($type . "/dokumen_act");
                $data = $this->dokumen_act->get_dokumen($aju, $nomor, $kddoc);
                $data = array_merge($data, array('edit' => "edit"));
                return $this->load->view("pemasukan/" . $type . "/Dokumen", $data);
            }
			#call form kontainer for add	(from table)
            elseif ($this->input->post("edit") == "add") {
                $aju = $isajax;
                $this->load->model($type . "/dokumen_act");
                $data = $this->dokumen_act->get_dokumen($aju, $seri);
                $data = array_merge($data, array('edit' => "add"));
                return $this->load->view("pemasukan/" . $type . "/Dokumen", $data);
            } else {
                #save kontainer
                $this->load->model("" . $type . "/dokumen_act");
                $this->dokumen_act->set_dokumen($this->input->post("act"), $isajax);
            }
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
    }
	
	function kontainer($type = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            #call form kontainer for edit (from table)		
            if ($this->input->post("edit") == "edit") {
                foreach ($this->input->post('tb_chkfkontainer') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                }
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $this->load->model($type . "/kontainer_act");
                $data = $this->kontainer_act->get_kontainer($aju, $seri);
                $data = array_merge($data, array('edit' => "edit"));
                return $this->load->view("pemasukan/" . $type . "/Kontainer", $data);
            }
			#call form kontainer for add	(from table)
            elseif ($this->input->post("edit") == "add") {
                $aju = $isajax;
                $this->load->model($type . "/kontainer_act");
                $data = $this->kontainer_act->get_kontainer($aju, $seri);
                $data = array_merge($data, array('edit' => "add"));
                return $this->load->view("pemasukan/" . $type . "/Kontainer", $data);
            } else {
                #save kontainer
                $this->load->model("" . $type . "/kontainer_act");
                $this->kontainer_act->set_kontainer($this->input->post("act"), $isajax);
            }
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
    }
	
	function edit($dok = "", $aju = "") {
		$this->addHeader["newtable"]    = 1;
		$this->addHeader["ui"]    		= 1;
		$this->addHeader["alert"]    	= 1;
		$this->addHeader["autocomplete"]    	= 1;
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
		if(strtolower($_SERVER['REQUEST_METHOD']) == "post"){
			$generate = $this->input->post("generate");
			if($generate=="formjs"){
				$dok = strtolower($this->input->post("id1"));
				$aju = $this->input->post("id2");
			}
			$dokname = str_replace("bc", "", $dok);
			$subdok = "";
			for ($a = 0; $a < strlen($dokname); $a++) {
				$subdok .= substr($dokname, $a, 1) . ".";
			}
			$dokumen = "BC " . substr($subdok, 0, strlen($subdok) - 1);
			$this->load->model("pemasukan_act");
			$surat = $this->pemasukan_act->get_pemasukan("ceksurat", $aju);
			$this->load->model('main');
			$data = array("surat" => $surat,"dokumen" => $dok,"aju" => $aju,"tipeproses" => "Updatesurat",
						  "judul" => "Data Pemasukan ".strtoupper($dokumen));
			$form = $this->load->view('pemasukan/' . $dok . '/Tabs', $data, true);
			$view = array("tabel" => $form);
			$this->content = $this->load->view('view', $view, true);
			$this->index($dok);
		}else{
			redirect(base_url());
		}
    }
	
	function detil($type = "", $dokumen = "", $aju = "") {
        $this->load->model("" . $dokumen . "/detil_act");
        $arrdata = $this->detil_act->detil($type, $dokumen, $aju);
        if ($this->input->post("ajax")) {
            echo $arrdata;
        } else {
            echo $this->load->view('view', $arrdata, true);
        }
    }
	
	function cekstatus($aju = "", $type = "") {
        $this->load->model("main");
        $KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		if($type=="bc281") $type = "bc23";
        if($aju && $type){
            if($type == "bc27" || $type=="bc40"){
                $status = $this->main->get_uraian("SELECT f_status_".$type."('" . $aju . "','3','" . $KODE_TRADER . "') AS STAT FROM DUAL", "STAT");
            }else{
                $status = $this->main->get_uraian("SELECT f_status_" . $type . "('" . $aju . "','3') AS STAT FROM DUAL", "STAT");
            }
        }else{
            $status = 'Cek status Error';
        }
        echo $status;
    }
	
	function barang_jadi($type = "", $isajax = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            #call form kemasan for edit (from table)		
            if ($this->input->post("edit") == "edit") {
                foreach ($this->input->post('tb_chkfbarang_jadi') as $chkitem) {
                    $arrchk = explode("|", $chkitem);
                }
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $this->load->model($type . "/barang_jadi_act");
                $data = $this->barang_jadi_act->get_barang_jadi($aju, $seri);		
                $data = array_merge($data, array('edit' => "edit", 'seriBJ' => $seri));
                return $this->load->view("pemasukan/" . $type . "/Barang_jadi", $data);
            }
			#call form kemasan for add	(from table)
            elseif ($this->input->post("edit") == "add") {
                $aju = $isajax;
                $this->load->model($type . "/barang_jadi_act");
                $data = $this->barang_jadi_act->get_barang_jadi($aju, $seri);
                $data = array_merge($data, array('edit' => "add", 'seriBJ' => $seri));
                return $this->load->view("pemasukan/" . $type . "/Barang_jadi", $data);
            } else {
                #save & delete kemasan
                $this->load->model("" . $type . "/barang_jadi_act");
                $this->barang_jadi_act->set_barang_jadi($this->input->post("act"), $isajax);
            }
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
    }
	
	function list_BB($type = "", $dokumen = "", $aju = "", $seri = "") {
        $this->load->model("" . $dokumen . "/detil_act");
        $arrdata = $this->detil_act->detil($type, $dokumen, $aju);
        $list = $this->load->view('list', $arrdata, true);
        if ($this->input->post("ajax")) {
            echo $arrdata;
        } else {
            echo $list;
        }
    }
	
	function bahan_baku($type = "", $jenis = "", $key1 = "", $key2 = "") {
        $this->load->model("" . $type . "/bahan_baku_act");
        if ($jenis == 'addBB') {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $this->bahan_baku_act->addBB();
            }
        } elseif ($jenis == 'editBB') {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $this->bahan_baku_act->editBB();
            }
        } elseif ($jenis == 'delete') {
            $this->bahan_baku_act->deleteBB();
        } elseif ($jenis == 'edit') {
            $aju = $this->uri->segment(5);
            $seri = $this->uri->segment(6);
            $seriBB = $this->uri->segment(7);
            $resultBB = $this->bahan_baku_act->listDataBB($aju, $seri, $seriBB);
        }
        $data = array("aju" => $key1,
            "seri" => $key2,
            "sess" => $resultBB);
        $this->load->view("pemasukan/" . $type . "/Bahan_baku", $data);
    }
	
	function load_list_BB($type = "", $dokumen = "", $aju = "", $seri = "") {
        $this->load->model("" . $dokumen . "/detil_act");
        $arrdata = $this->detil_act->detil($type, $dokumen, $aju);
        $list = $this->load->view('view', $arrdata, true);
        if ($this->input->post("ajax")) {
            echo $arrdata;
        } else {
            echo $list;
        }
    }
	
	function daftar_dok($tipe="",$dokumen="",$aju=""){
		$this->load->model("pemasukan_act");
		$arrdata = $this->pemasukan_act->daftar($tipe,$dokumen,$aju);
		echo $this->load->view('view', $arrdata, true);
	}
	
	function popup($type,$jenis){
		$this->load->model('main');
        if ($type == 'bc30') {
            $this->load->model("bc30/header_act");
            if ($jenis == 'QQ') {
                $kode_id_qq = $this->input->post('kode_id_qq');
                $id_qq = $this->input->post('id_qq');
                $nama_qq = $this->input->post('nama_qq');
                $alamat_qq = $this->input->post('alamat_qq');
                $niper_qq = $this->input->post('niper_qq');

                $data = array('kode_id_qq' => $kode_id_qq, 'id_qq' => $id_qq, 'nama_qq' => $nama_qq,
                    		  'alamat_qq' => $alamat_qq, 'niper_qq' => $niper_qq,
                    		  'kode_id' => $this->main->get_mtabel('kode_id'));

                $this->load->view('pemasukan/' . $type . '/QQ', $data);
            } elseif ($jenis == 'PKB') {
                if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                    $data = $this->header_act->set_pkb($this->input->post('act'));
                    echo $data;
                }
                $data = $this->header_act->get_pkb();
                $this->load->view('pemasukan/' . $type . '/PKB', $data);
            } elseif ($jenis == 'hitungan') {
                $asuransi = $this->uri->segment(5);
                $freight = $this->uri->segment(6);
                $kode_harga = $this->uri->segment(7);
                $fob = $this->uri->segment(8);
                $cif = $this->uri->segment(9);
                $invoice = $this->uri->segment(10);
                $data = array('asuransi' => $asuransi,'freight' => $freight,'kode_harga' => $kode_harga,
                    		  'fob' => $fob, 'cif' => $cif, 'invoice' => $invoice,
                    		  'kode_asuransi' => $this->main->get_mtabel('KODE_ASURANSI'));
				$this->load->view('pemasukan/' . $type . '/Hitungan', $data);
            }
        }
	}
	
	function getTarif($type, $jenis) {
        if ($type == 'bc30'){
            if ($jenis == 'spesifik') {
                $this->load->view('pemasukan/' . $type . '/Bea_spesifik');
            } else if ($jenis == 'advolarum') {
                $this->load->view('pemasukan/' . $type . '/Bea_advolarum');
            }
		}
    }
	
	function set_pemasukan($tipe = "") {
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            redirect(base_url());
            exit();
        } else {
            $this->load->model("pemasukan_act");
            $ret = $this->pemasukan_act->set_pemasukan($this->input->post("act"), $tipe);
        }
        echo $ret;
    }
	
	function breakdown($dokumen = "", $aju = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
		$this->addHeader["newtable"] = 1;
		$this->addHeader["alert"]    = 1;
        $this->load->model("pemasukan_act");
        if ($dokumen == "priview") {
            $data = explode("|", $aju);
            $noaju = $data[0];
            $jnsdok = $data[1];
            $detilbarang = $this->pemasukan_act->detil_breakdown($noaju, $jnsdok);
            $arrdata = array("detilbarang" => $detilbarang);
            echo $this->load->view('pemasukan/breakdown_priview', $arrdata, true);
        } else {
            $arrdata = $this->pemasukan_act->daftar_breakdown($dokumen, $aju);
            $data = $this->load->view('box-pabean', $arrdata, true);
            if ($this->input->post("ajax")) {
                echo $arrdata;
            } else {
                $this->content = $data;
                $this->index();
            }
        }
    }

	function priview(){
		if(!$this->newsession->userdata('LOGGED')){
			$this->index();
			return;
		}
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
		if(strtolower($_SERVER['REQUEST_METHOD']) == "post"){
			$dok = strtolower($this->input->post("id1"));
			$aju = $this->input->post("id2");
			
			if(!in_array($dok,array('lokal','subkon'))){
				$dokname = str_replace("bc","",$dok);
				$subdok="";
				for($a=0;$a<strlen($dokname);$a++){
					$subdok .= substr($dokname,$a,1).".";
				}
				$dokumen = "BC ".substr($subdok,0,strlen($subdok)-1); 
			}
			$this->load->model($dok."/header_act");
			$DATAHEADER = $this->header_act->get_header($aju); 
			$this->load->model($dok."/detil_act");		
			$DATADETIL = $this->detil_act->list_tab($aju); 
			$arrdata = array_merge($DATAHEADER,array("priview"=>true,"readonly"=>"readonly","disabled"=>"disabled",
											   "display"=>'style="display:none"',"width"=>'style="width:100%"',
											   "DETILPRIVIEW"=>$DATADETIL));	
			$content = '<span class="view">';
			$content .= '<a href="javascript:void(0);" class="btn btn-sm btn-danger"';
			$content .= "onclick=\"print_('".site_url()."/pemasukan/print_dok/".$dok."','".$aju."');\" ";
			$content .= 'style="float:right;margin:4px 4px 0px 0px;"><i class="icon-print"></i>PDF</a>';			
			$content .= '<a href="javascript: window.history.go(-1)" class="btn btn-sm btn-success" ';
			$content .= 'style="float:right;margin:4px 4px 0px 0px;"><i class="icon-arrow-left"></i>Back</a>';
			$content .= '<div class="header">';
			$content .= '<h3 class="green"><strong>Priview Data '.$dokumen.'</strong></h3>';
			$content .= '</div>';
			$content .= '<div class="content"><div class="span12">';	
			$content .= $this->load->view('pemasukan/'.$dok.'/Header', $arrdata, true);				  	
			$content .= '<div></div></span>';		
			$this->content = $content;	
			$this->index($dok);	
		}else{
			redirect(site_url()."/pemasukan/draft");
		}
	}
}
?>
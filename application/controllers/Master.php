<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

    var $content = "";
    var $addHeader = array();
    function index($dok = "") {
        if($this->newsession->userdata('LOGGED')){
			$this->load->model('main');
			$this->main->get_index($dok,$this->addHeader);	
		}else{
			$this->newsession->sess_destroy();		
			redirect(base_url());
		}
    }

    function add($tipe = "", $id = "") {
        $func = get_instance();
        $func->load->model("main", "main", true);
        $this->load->model("master_act");
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
        if ($tipe == "pemasok") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
                die();
            }
            $judul = "Tambah Data Pemasok atau Pembeli/Penerima";
            $act = "save";
            $data = array("judul" => $judul,
                "act" => $act,
                "kode_id" => $func->main->get_mtabel('KODE_ID'),
                "status_partner" => $func->main->get_combobox("SELECT KODE , CONCAT(URAIAN,' ','(',KODE,')') 
                                                                AS URAIAN FROM m_tabel 
							 									WHERE JENIS='TEMPAT' ORDER BY KODE", "KODE", "URAIAN", TRUE),
                "jenis_partner" => $func->main->get_mtabel('JENIS_PERUSAHAAN'),
            );
            $this->content = $this->load->view('master/pemasok/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "mapping") {
            $namaPart = "";
            $kodePart = "";
            if ($id != "") {
                $sql = "SELECT KODE_PARTNER, NAMA_PARTNER FROM m_trader_partner 
						WHERE KODE_PARTNER='" . $id . "' AND KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'";
                $resultNomor = $this->db->query($sql);
                $rowNomor = $resultNomor->row();
                $kodePart = $rowNomor->KODE_PARTNER;
                $namaPart = $rowNomor->NAMA_PARTNER;
                if ($resultNomor->num_rows() == 0) {
					$nm = str_replace("%20"," ",$nm);
                    $kodePart = $id;
                    $namaPart = $nm;
                }
            }
            $arrdata = $this->master_act->detil($tipe, $id);
            $list = $this->load->view('list', $arrdata, true);
            $judul = "Tambah Data Mapping";
            $act = "save";
            $data = array("judul" => $judul,
                "act" => $act,
                "list" => $list,
                "dokumen" => $dok,
                "kodePart" => $kodePart,
                "namaPart" => $namaPart,
                "tipe" => $tipe
            );
            $this->content = $this->load->view('master/mapping/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "mappingPOPN") {
            $idTrader = $this->uri->segment(5);
            $tgl = $this->uri->segment(5);
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                echo $this->master_act->proses_form('mapping');
            } else {
                $getKODE = $dataarray['KODE_TRADER'];
                $data = array("judul" => "Entry Data Kode Barang",
                    "act" => "Save",
                    "idPartner" => $id
                );
                $this->load->view('master/mapping/form_mapping', $data);
            }
        } elseif ($tipe == "gudang") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }
            #$arrdata = $this->master_act->detil($tipe, $id);
            #$list = $this->load->view('list', $arrdata, true);
            $judul = "Tambah Data Gudang";
            $act = "save";
            $data = array("judul" => $judul,
                "act" => $act,
                "tipe" => $tipe
            );
            $this->content = $this->load->view('master/gudang/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "rak") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }
            $arrdata = $this->master_act->detil($tipe, $id);
            $list = $this->load->view('list', $arrdata, true);
            $judul = "Tambah Data Rak";
            $act = "save";
            $data = array("judul" => $judul,
                "act" => $act,
                "tipe" => $tipe,
		"kode_gudang" => $func->main->get_combobox("SELECT KODE_GUDANG , NAMA_GUDANG FROM m_trader_gudang 
		WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'", "KODE_GUDANG", "KODE_GUDANG", TRUE)
            );
            $this->content = $this->load->view('master/rak/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "sub_rak") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }
            $arrdata = $this->master_act->detil($tipe, $id);
            $list = $this->load->view('list', $arrdata, true);
            $judul = "Tambah Data Sub Rak";
            $act = "save";
            $data = array("judul" => $judul,
                "act" => $act,
                "tipe" => $tipe,
		"kode_gudang" => $func->main->get_combobox("SELECT KODE_GUDANG , NAMA_GUDANG FROM m_trader_gudang 
				WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'  ORDER BY KODE_GUDANG ASC", "KODE_GUDANG", "KODE_GUDANG", TRUE),
		"kode_rak" => $func->main->get_combobox("SELECT KODE_RAK , NAMA_RAK FROM m_trader_rak 
				WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'  ORDER BY KODE_RAK ASC", "KODE_RAK", "NAMA_RAK", TRUE)
            );
            $this->content = $this->load->view('master/sub_rak/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "penyelenggara") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }     
			$judul 	= "Tambah Data Penyelenggara";
        	$act 	= "save";
        	$data 	= array('act' => $act,
							'judul' => $judul,
            				'kode_id_trader' => $func->main->get_mtabel('KODE_ID'),
            				'kode_api_trader' => $func->main->get_mtabel('API'),
            				'JENIS_TPB' => $func->main->get_mtabel('JENIS_TPB'),
            				'TIPE_TRADER' => $func->main->get_mtabel('JENIS_EKSPORTIR'),
							'PROPINSI_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'KOTA_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)!='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'JENIS_PLB' => $func->main->get_combobox("SELECT KODE, URAIAN FROM M_TABEL WHERE JENIS='JENIS_PLB' AND KODE<>'01' ORDER BY KODE ","KODE","URAIAN", TRUE),
							'tipe' => $tipe
							);        
            $this->content = $this->load->view('master/penyelenggara/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "anggota") {
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }     
			$judul 	= "Tambah Data Anggota";
        	$act 	= "save";
        	$data 	= array('act' => $act,
							'judul' => $judul,
            				'kode_id_trader' => $func->main->get_mtabel('KODE_ID'),
            				'kode_api_trader' => $func->main->get_mtabel('API'),
            				'JENIS_TPB' => $func->main->get_mtabel('JENIS_TPB'),
            				'TIPE_TRADER' => $func->main->get_mtabel('JENIS_EKSPORTIR'),
							'PROPINSI_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'KOTA_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)!='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'tipe' => $tipe
							);        
            $this->content = $this->load->view('master/anggota/form', $data, true);
            $this->index($tipe);
        }
    }

    function edit($tipe = "", $id= "", $id2= "" , $id3="") {
        $func = get_instance();
        $func->load->model("main", "main", true);
        $this->load->model("master_act");
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
        if ($tipe == "profil") {
              if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form($tipe);
                echo $data;
            }
        } elseif ($tipe == "pemasok") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
			#start by pass untuk edit formjs
	    	$generate = $this->input->post('generate');
	    	if($generate == "formjs"){
				$kode_partner = $this->input->post('id1');
			}
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_PARTNER');
                $data = $this->master_act->proses_form($tipe, $id);
                echo $data;
                die();
            }
            $judul = "Ubah Data Pemasok atau Pembeli/Penerima";
            $resultdata = $this->master_act->getData($tipe, $kode_partner);
            $kode_id = $func->main->get_mtabel('KODE_ID', 1, TRUE);
            $status_partner = $func->main->get_combobox("SELECT KODE , CONCAT(URAIAN,' ','(',KODE,')') 
                                                        AS URAIAN FROM m_tabel 
                                                        WHERE JENIS='TEMPAT' ORDER BY KODE", "KODE", "URAIAN", TRUE);
            $jenis_partner = $func->main->get_mtabel('JENIS_PERUSAHAAN');
            $data = array("judul" => $judul,
            "act" => "update",
            "dokumen" => $dok,
            "kode_id" => $kode_id,
            "sess" => $resultdata,
            "status_partner" => $status_partner,
            "jenis_partner" => $jenis_partner);
            $this->content = $this->load->view('master/pemasok/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "mappingPOPN") {
            $tipe = "mappingPOPget";
            $id = $this->uri->segment(4);
            $idPartner = $this->uri->segment(5);
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $data = $this->master_act->proses_form("mapping");
                echo $data;
            } else {
                $resultdata = $this->master_act->getData($tipe, $id);
                $data = array("judul" => "Ubah Data Bahan Baku",
                    "act" => "update",
                    "idPartner" => $idPartner,
                    "idMapping" => $id,
                    "sess" => $resultdata,
                    "JENIS_BRG" => $func->main->get_mtabel('ASAL_JENIS_BARANG'));
                $this->load->view('master/mapping/form_mapping', $data);
            }
        } elseif ($tipe == "mapping") {
			$kodePart = "";
			$namaPart = "";
			if ($id != "") {
            	$sql = "SELECT KODE_PARTNER, NAMA_PARTNER FROM m_trader_partner 
						WHERE KODE_PARTNER='" . $id . "' AND KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'";
            	$resultNomor = $this->db->query($sql);
            	$rowNomor = $resultNomor->row();
            	$kodePart = $rowNomor->KODE_PARTNER;
            	$namaPart = $rowNomor->NAMA_PARTNER;
        	}
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $id = $this->input->post('idPemasok');
                $data = $this->master_act->proses_form($tipe, $id);
                echo $data;
            }
			//echo $sql;
			//die();
            $judul = "Ubah Data Mapping";
            $arrdata = $this->master_act->detil($tipe, $id);
            $list = $this->load->view('list', $arrdata, true);
            $act = "update";
			$data = array("judul" => $judul,
            "act" => $act,
            "list" => $list,
            "dokumen" => $dok,
            "kodePart" => $kodePart,
            "namaPart" => $namaPart,
            "tipe" => $tipe,
            "JENIS_BRG" => $func->main->get_mtabel('ASAL_JENIS_BARANG'));
        	$this->content = $this->load->view('master/' . $tipe . '/form', $data, true);
        	$this->index($tipe);
        } elseif ($tipe == "gudang") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
	    	#start by pass untuk edit formjs
	    	$generate = $this->input->post('generate');
	    	if($generate == "formjs"){
				$kode_gudang = $this->input->post('id1');
			//$jns_barang  = $this->input->post('id2');
	    	}
	    #end by pass untuk edit formjs
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_GUDANG');
                $data = $this->master_act->proses_form($tipe, $id);
                echo $data;
                die();
            }
            $judul = "Ubah Data Gudang";
            $resultdata = $this->master_act->getData($tipe, $kode_gudang);            
            $data = array("judul" => $judul,
            "act" => "update",
            "sess" => $resultdata);
            $this->content = $this->load->view('master/gudang/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "rak") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
	    	#start by pass untuk edit formjs
	    	$generate = $this->input->post('generate');
	    	if($generate == "formjs"){
				$kode_gudang = $this->input->post('id1');
				$kode_rak  = $this->input->post('id2');
	    	}
	    	#end by pass untuk edit formjs

            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_GUDANG');
				$id2 = $this->input->post('KODE_RAK');
                $data = $this->master_act->proses_form($tipe, $id , $id2);
                echo $data;
                die();
            }
            $judul = "Ubah Data Rak";
            $resultdata = $this->master_act->getData($tipe, $kode_gudang, $kode_rak);
            $kode_gudang = $func->main->get_combobox("SELECT KODE_GUDANG , NAMA_GUDANG FROM m_trader_gudang 
			WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."' ORDER BY KODE_GUDANG ASC", "KODE_GUDANG", "KODE_GUDANG", TRUE);
            $data = array("judul" => $judul,
            		"act" => "update",
            		"sess" => $resultdata,
            		"kode_gudang" =>  $kode_gudang
			);
            $this->content = $this->load->view('master/rak/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "sub_rak") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
	   		$id2 = str_replace("%5Bspasi%5D", " ", $id2);
	    	$id3 = str_replace("%5Bspasi%5D", " ", $id3);
	    	#start by pass untuk edit formjs
	    	$generate = $this->input->post('generate');
	    	if($generate == "formjs"){
				$kode_gudang 	= $this->input->post('id1');
				$kode_rak  	= $this->input->post('id2');
				$kode_sub_rak 	= $this->input->post('id3');
	    	}
	    	#end by pass untuk edit formjs

            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_GUDANG');
		$id2 = $this->input->post('KODE_RAK');
		$id3 = $this->input->post('KODE_SUB_RAK');				
                $data = $this->master_act->proses_form($tipe, $id , $id2 , $id3);
                echo $data;
                die();
            }
            $judul 		= "Ubah Data Rak";
            $resultdata = $this->master_act->getData($tipe, $kode_gudang, $kode_rak, $kode_sub_rak);
            $kode_gudang = $func->main->get_combobox("SELECT KODE_GUDANG , NAMA_GUDANG FROM m_trader_gudang 
			WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'  ORDER BY KODE_GUDANG ASC", "KODE_GUDANG", "KODE_GUDANG", TRUE);
			$kode_rak = $func->main->get_combobox("SELECT KODE_RAK , NAMA_RAK FROM m_trader_rak 
			WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'  ORDER BY KODE_RAK ASC", "KODE_RAK", "NAMA_RAK", TRUE);
            $data = array("judul" => $judul,
            		"act" => "update",
            		"sess" => $resultdata
			);
            $this->content = $this->load->view('master/sub_rak/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "penyelenggara") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
	    	$id = str_replace("%20", " ", $id);
	    	#start by pass untuk edit formjs
	    	$generate = $this->input->post('generate');
	    	if($generate == "formjs"){
				$kode_penyelenggara 	= $this->input->post('id1');
	    	}
	    	#end by pass untuk edit formjs
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_TRADER');
                $data = $this->master_act->proses_form($tipe, $id);
                return $data;
                die();
            }
            $judul = "Ubah Data Penyelenggara";
            $resultdata = $this->master_act->getData($tipe, $kode_penyelenggara);            
            $act 	= "update";
        	$data 	= array('act' => $act,
						'judul' => $judul,
            			'kode_id_trader' => $func->main->get_mtabel('KODE_ID'),
            			'kode_api_trader' => $func->main->get_mtabel('API'),
            			'JENIS_TPB' => $func->main->get_mtabel('JENIS_TPB'),
            			'TIPE_TRADER' => $func->main->get_mtabel('JENIS_EKSPORTIR'),
						'PROPINSI_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
						'KOTA_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)!='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
						'JENIS_PLB' => $func->main->get_combobox("SELECT KODE, URAIAN FROM M_TABEL WHERE JENIS='JENIS_PLB' AND KODE<>'01' ORDER BY KODE ","KODE","URAIAN", TRUE),
						'tipe' => $tipe,
						'sess' => $resultdata
						);
            $this->content = $this->load->view('master/penyelenggara/form', $data, true);
            $this->index($tipe);
        } elseif ($tipe == "anggota") {
            $id = str_replace("%5Bspasi%5D", " ", $id);
	    $id = str_replace("%20", " ", $id);
	    #start by pass untuk edit formjs
	    $generate = $this->input->post('generate');
	    if($generate == "formjs"){
		$kode_anggota 	= $this->input->post('id1');
	    }
	    #end by pass untuk edit formjs
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post" && ($generate != "formjs")) {
                $id = $this->input->post('KODE_TRADER');
                $data = $this->master_act->proses_form($tipe, $id);
                echo $data;
                die();
            }
            $judul = "Ubah Data Anggota";
            $resultdata = $this->master_act->getData($tipe, $kode_anggota);            
            $act 	= "update";
        	$data 	= array('act' => $act,
				'judul' => $judul,
            			'kode_id_trader' => $func->main->get_mtabel('KODE_ID'),
            			'kode_api_trader' => $func->main->get_mtabel('API'),
            			'JENIS_TPB' => $func->main->get_mtabel('JENIS_TPB'),
            			'TIPE_TRADER' => $func->main->get_mtabel('JENIS_EKSPORTIR'),
				'PROPINSI_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
				'KOTA_GB' => $func->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)!='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
				'tipe' => $tipe,
				'sess' => $resultdata
				);
            $this->content = $this->load->view('master/anggota/form', $data, true);
            $this->index($tipe);
        }
    }

    function delete($tipe = "") {
        $this->load->model("master_act");
        $ret = $this->master_act->proses_form($tipe);
    }

    function daftar($tipe = "") {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("master_act");
        $arrdata = $this->master_act->daftar($tipe);
        $data = $this->load->view('list', $arrdata, true);
        if ($this->input->post("ajax")) {
            echo $arrdata;
        } else {
            $this->content = $data;
            $this->index();
        }
    }

    #untuk preview mapping dll
    function preview($tipe = "", $id = "") {
        $func = get_instance();
        $func->load->model("main", "main", true);
        $this->load->model("master_act");      
        if ($tipe == "mapping") {
			$namaPart = "";
        	$kodePart = "";
        	if ($id != "") {
            	$sql = "SELECT KODE_PARTNER, NAMA_PARTNER FROM m_trader_partner 
						WHERE KODE_PARTNER='" . $id . "' AND KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'";
            	$resultNomor = $this->db->query($sql);
            	$rowNomor = $resultNomor->row();
            	$kodePart = $rowNomor->KODE_PARTNER;
            	$namaPart = $rowNomor->NAMA_PARTNER;
        	}
            if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
                $id = $this->input->post('idPemasok');
                $data = $this->master_act->proses_form($tipe, $id);
                echo $data;
            }
            $tipe = "mapping";
            $judul = "Preview Data Mapping";
            $act = "preview";
            $arrdata = $this->master_act->detil($tipe, $id, $act);
            $list = $this->load->view('list', $arrdata, true);
			$data = array("judul" => $judul,
            "act" => $act,
            "list" => $list,
            "dokumen" => $dok,
            "kodePart" => $kodePart,
            "namaPart" => $namaPart,
            "tipe" => $tipe,
            "JENIS_BRG" => $func->main->get_mtabel('ASAL_JENIS_BARANG')
        	);
        	$this->content = $this->load->view('master/' . $tipe . '/form', $data, true);
        	$this->index($tipe);
        }        
    }
    #end preview

    #untuk set nomer aju
   function setnomor() {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            $this->load->model("master_act");
            $this->master_act->setnomor();
        } else {
            $KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
            $SQL = "SELECT DOKNAME, NO_URUT, SEGMEN1, SEGMEN2 FROM m_trader_no_urut WHERE KODE_TRADER='" . $KODE_TRADER . "' 
                    AND DOKNAME <> 'PRODUKSI'";
            $rs = $this->db->query($SQL);
            $DATA = array();
            if ($rs->num_rows() > 0) {
                foreach ($rs->result_array() as $a => $b) {
                    $DATA[$a] = $b;
                }
            }
            echo $this->load->view('master/setnomor', array("DATA" => $DATA), true);
        }
    }    
    #end aju  

    #untuk lihat profil
	function profil($tipe = "") {
		$this->addHeader["newtable"] = 1;
		$this->addHeader["ui"] = 1;
		$this->addHeader["alert"]    = 1;
		$this->addHeader["autocomplete"] = 1;
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        if ($tipe == "lihat") $aksi = "view";
        if ($tipe == "ubah") $aksi = "edit";

        $this->load->model("master_act");
        $data = $this->master_act->get_profil($aksi);
        $this->content = $this->load->view('master/profil', $data, true);
        $this->index('master');
    }
    #end profil
    
    #untuk pop up
    function listdatapopup($tipe = "") {
	$this->addHeader["newtable"] = 1;
	$this->addHeader["ui"] = 1;
	$this->addHeader["alert"]    = 1;
	$this->addHeader["autocomplete"] = 1;
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        $this->load->model("master_act");
        if ($this->input->post("edit") == "add") {
            $data = $this->master_act->get_datapopup($tipe);
            $data = array_merge($data, array('edit' => "add"));
            return $this->load->view("master/" . $tipe, $data);
        } elseif ($this->input->post("edit") == "edit") {
            $checkbox = $this->input->post('tb_chkf' . $tipe);
            foreach ($checkbox as $chkitem) {
                $arrchk = explode("|", $chkitem);
            }
	    $seri = $arrchk[1];
            $id = $arrchk[0];;
            $data = $this->master_act->get_datapopup($tipe,$seri);
            $data = array_merge($data, array('edit' => "edit"));
            return $this->load->view("master/" . $tipe, $data);
        } else {
            if ($this->input->post("act")) {
                $this->master_act->set_datapopup($tipe, $this->input->post("act"));
            } else {
                $arrdata = $this->master_act->listdatapopup($tipe);
                $data = $this->load->view("view", $arrdata, true);
                if ($this->input->post("ajax")) {
                    echo $arrdata;
                } else {
                    echo $data;
                }
            }
        }
    }
    #end pop up

    #untuk mapping
    function partner($tipe = "") {
        if (!$this->newsession->userdata('LOGGED')) {
            $this->index();
            return;
        }
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            echo $this->load->view('master/mapping/partner', '', true);
        } else {
            $this->load->model("master_act");
            $this->master_act->cekpartner();
        }
    }

    function mapping_dok($tipe = "", $idPartner = "", $act = "") {
        $this->load->model("master_act");
        $arrdata = $this->master_act->detil($tipe, $idPartner, $act);
        $list = $this->load->view('list', $arrdata, true);
        if ($this->input->post("ajax") || $act == "delete") {
            echo $arrdata;
        } else {
            echo $list;
        }
    }
    #end mapping
}

?>
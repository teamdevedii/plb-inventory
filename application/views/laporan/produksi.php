<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>
<div class="content_luar">
<div class="content_dalam">
<h4><span class="info_">&nbsp;</span><?= $judul; ?></h4>
<form name="frmLaporanPros" id="frmLaporanPros">
<table class="normal" cellpadding="2" width="100%">
<tr>
    <td colspan="2"><b>LAPORAN PENGERJAAN SEDERHANA</b></td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td width="5%">Periode</td>
	<td width="95%">: 
	  <input type="text" name="TANGGAL_AWAL" id="TANGGAL_AWAL" onFocus="ShowDP('TANGGAL_AWAL');" wajib="yes" class="stext date">&nbsp;s/d&nbsp;<input type="text" name="TANGGAL_AKHIR" id="TANGGAL_AKHIR" onFocus="ShowDP('TANGGAL_AKHIR');" wajib="yes" class="stext date">
    </td>
</tr>
<tr>
    <td>Jenis</td>
    <td>: <?= form_dropdown('JENIS', array(""=>"","PROCESS_IN"=>"Barang yang diproses [Input]","PROCESS_OUT"=>"Hasil Pengerjaan [Output]","SCRAP"=>"Sisa Pengerjaan/Scrap"), '', 'id="JENIS" class="dtext" wajib="yes"'); ?>
		 &nbsp; <a href="javascript:void(0);" class="button next" onclick="Laporan('frmLaporanPros','msg_laporan','divLapProses','<?= base_url()."index.php/laporan/daftar_dok/produksi";?>','laporan');"><span><span class="icon"></span>&nbsp;OK&nbsp;</span></a>
	</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td colspan="2"><div id="divLapProses"><span class="msg_laporan" style="margin-left:70px"><?= $tabel; ?></span></div></td>
</tr>
</table>
</form>
</div>
</div>

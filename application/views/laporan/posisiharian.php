<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>
<div class="content_luar">
<div class="content_dalam">
<h4><span class="info_">&nbsp;</span><?= $judul; ?></h4>
<form name="frmLaporanPros" id="frmLaporanPros">
<table class="normal" cellpadding="2" width="100%">
<tr>
    <td colspan="3"><b>LAPORAN POSISI BARANG PER DOKUMEN PABEAN HARIAN</b></td>
</tr>
<tr>
    <td colspan="3">&nbsp;</td>
</tr>
<tr>
    <td width="10%">Jenis Dokumen</td>
    <td width="1%">:</td>
    <td width="89%"><?= form_dropdown('KODE_DOKUMEN', array("BC23"=>"BC 2.3","BC27"=>"BC 2.7"),'', 'id="KODE_DOKUMEN" class="text"'); ?></td>
</tr>
<tr>
    <td>Nomor Pendaftaran</td>
    <td>:</td>
    <td><input type="text" name="NOMOR_DAFTAR" id="NOMOR_DAFTAR" wajib="yes" class="text">&nbsp;
    <input type="button" name="cari" id="cari" class="button" onclick="tb_search('after_realisasi_in','NOMOR_DAFTAR;TANGGAL_DAFTAR;KODE_DOKUMEN','DOKUMEN PEMASUKAN',this.form.id,650,445)" value="..."></td>
</tr>
<tr>
    <td>Tanggal Pendaftaran</td>
    <td>:</td>
    <td><input type="text" name="TANGGAL_DAFTAR" id="TANGGAL_DAFTAR" onFocus="ShowDP('TANGGAL_DAFTAR');" wajib="yes" class="stext date">&nbsp; <a href="javascript:void(0);" class="button next" onclick="Laporan('frmLaporanPros','msg_laporan','divLapProses','<?= site_url()."/laporan/daftar_dok/posisiharian";?>','laporan');"><span><span class="icon"></span>&nbsp;OK&nbsp;</span></a></td>
</tr>
<tr>
    <td colspan="3">&nbsp;</td>
</tr>
<tr>
    <td colspan="3">&nbsp;</td>
</tr>
<tr>
    <td  colspan="3"><div id="divLapProses"><span class="msg_laporan" style="margin-left:50px"><?= $tabel; ?></span></div></td>
</tr>
</table>
</form>
</div>
</div>

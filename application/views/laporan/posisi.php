<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>

<div class="content_luar">
  <div class="content_dalam">
    <h4><span class="info_">&nbsp;</span>
      <?= $judul; ?>
    </h4>
    <form name="frmLaporanPros" id="frmLaporanPros">
      <table class="normal" cellpadding="2" width="100%">
        <tr>
          <td colspan="2"><b>LAPORAN POSISI BARANG PER DOKUMEN PABEAN</b></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="6%"> Periode :</td>
          <td width="94%">
          	<?=form_dropdown('TIPE_PERIODE',array('IN'=>'PEMASUKAN','OUT'=>'PENGELUARAN','INOUT'=>'PEMASUKAN DAN PENGELUARAN'),'','id="TIPE_PERIODE" class="text"'); ?>
            <input type="text" name="TANGGAL_AWAL" id="TANGGAL_AWAL" onFocus="ShowDP('TANGGAL_AWAL');" wajib="yes" class="stext date">
            &nbsp;s/d&nbsp;
            <input type="text" name="TANGGAL_AKHIR" id="TANGGAL_AKHIR" onFocus="ShowDP('TANGGAL_AKHIR');" wajib="yes" class="stext date">
            &nbsp; <a href="javascript:void(0);" class="button next" onclick="Laporan('frmLaporanPros','msg_laporan','divLapProses','<?= base_url()."index.php/laporan/daftar_dok/posisi";?>','laporan');"><span><span class="icon"></span>&nbsp;OK&nbsp;</span></a></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td><input type="checkbox" name="ALL_SALDO" id="ALL_SALDO" value="1" />
            Tampilkan pemasukan yang masih memiliki saldo
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><div id="divLapProses"><?= $tabel; ?></div></td>
        </tr>
      </table>
    </form>
  </div>
</div>

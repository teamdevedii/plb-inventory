<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>

<div class="content_luar">
  <div class="content_dalam"> <span id="btnExitBB"> <a href="javascript:void(0)" onclick="window.location.href='<?= site_url()."/laporan/tipe_laporan/mutasi"?>'" style="float:right;margin:-5px 0px 0px 0px" class="button prev" id="ok_"><span><span class="icon"></span>&nbsp;Selesai&nbsp;</span></a></span>
    <h4><span class="info_">&nbsp;</span>
      <?= $judul; ?>
    </h4>
    <form name="frmLaporanMutBB" id="frmLaporanMutBB">
      <table class="normal" cellpadding="2" width="100%">
        <tr>
          <td colspan="2"><b>LAPORAN PERTANGGUNGJAWABAN MUTASI BARANG</b></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="5%"> Periode </td>
          <td width="95%">:
            <input type="text" name="TANGGAL_AWAL" id="TANGGAL_AWAL" onFocus="ShowDP('TANGGAL_AWAL');" wajib="yes" class="stext date">
            &nbsp;s/d&nbsp;
            <input type="text" name="TANGGAL_AKHIR" id="TANGGAL_AKHIR" onFocus="ShowDP('TANGGAL_AKHIR');" wajib="yes" class="stext date">
            &nbsp; <a href="javascript:void(0);" class="button next" onclick="LaporanList('frmLaporanMutBB','msg_laporan','divLapMutasiBB','divListMutasiBB','btnExitBB','<?= base_url()."index.php/laporan/daftar_dok/mutasi";?>','laporan');"><span><span class="icon"></span>&nbsp;OK&nbsp;</span></a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;
            <input  type="checkbox" name="all" id="all" value="1"/>
            &nbsp;Tampilkan semua daftar barang </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><div id="divLapMutasiBB" style="display:none"><span class="msg_laporan" style="margin-left:50px"></span></div></td>
        </tr>
        <tr>
          <td colspan="2"><div id="divListMutasiBB" style="display:none">
              <?= $list;?>
            </div></td>
        </tr>
      </table>
    </form>
  </div>
</div>
<script>
$(document).ready(function(){
	$('#divListMutasiBB').show();
	$('#divLapMutasiBB').hide();
	$('#btnExitBB').hide();
});
</script>
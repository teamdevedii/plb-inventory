<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>
<div class="content_luar">
<div class="content_dalam">
<h4><span class="info_">&nbsp;</span><?= $judul; ?></h4>
<form name="frmLaporaninout" id="frmLaporaninout">
<table class="normal" cellpadding="2" width="100%">
<tr>
    <td colspan="2"><b>LAPORAN KELUAR / MASUK BARANG PER DOKUMEN PABEAN</b></td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td width="9%">Periode Tgl.</td>
	<td width="91%">: <?= form_dropdown('TIPE_TANGGAL', array("TGL_MASUK"=>"REALISASI","TGL_DOK"=>"DOKUMEN"), '', 'id="TIPE_TANGGAL" class="sstext"'); ?> <input type="text" name="TANGGAL_AWAL" id="TANGGAL_AWAL" onFocus="ShowDP('TANGGAL_AWAL');" wajib="yes" class="sstext date">&nbsp;s/d&nbsp;<input type="text" name="TANGGAL_AKHIR" id="TANGGAL_AKHIR" onFocus="ShowDP('TANGGAL_AKHIR');" wajib="yes" class="sstext date">
    </td>
</tr>
<tr>
    <td>Jenis Dokumen</td>
    <td>: <?= form_dropdown('JENIS', array(""=>"","MASUK"=>"PEMASUKAN","KELUAR"=>"PENGELUARAN"), '', 'id="JENIS" class="mtext" wajib="yes" style="width:293px"'); ?>
		 &nbsp; <a href="javascript:void(0);" class="button next" onclick="Laporan('frmLaporaninout','msg_laporan','divLapinout','<?= base_url()."index.php/laporan/daftar_dok/inout";?>','laporan');"><span><span class="icon"></span>&nbsp;OK&nbsp;</span></a>
	</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td colspan="2"><div id="divLapinout"><span class="msg_laporan" style="margin-left:9em"><?= $tabel; ?></span></div></td>
</tr>
</table>
</form>
</div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);//echo $tipe;
$kodeTrader = $this->newsession->userdata('KODE_TRADER');

function getKeterangan($jml){
	if($jml<0) return "SELISIH KURANG";
	if($jml>0) return "SELISIH LEBIH";
	if($jml==0) return "SESUAI";
}

function FormatDate($vardate)
{
	$pecah1 = explode("-", $vardate);
	$tanggal = intval($pecah1[2]);
	$arrayBulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
						"Agustus", "September", "Oktober", "November", "Desember");
	$bulan = $arrayBulan[intval($pecah1[1])];
	$tahun = intval($pecah1[0]);
	$balik = $tanggal." ".$bulan." ".$tahun;
	return $balik;
}
if($tipe=="posisi"){
function contentProses(){
	$content  ='<thead><tr>
					<th rowspan="2" align="center">No</th>
					<th colspan="10">Dokumen Pemasukan</th>
					<th colspan="8">Dokumen Pengeluaran</th>
					<th colspan="3">Saldo Barang</th>
				</tr>    
				<tr>    
					<th>Jenis</th>
					<th>NO</th>
					<th>Tgl</th>
					<th>Tgl<br>Masuk</th>
					<th>Kode<br>Barang</th>
					<th>Seri<br>Barang</th>
					<th>Nama<br>Barang</th>
					<th>Sat</th>
					<th>Jmlh</th>
					<th>Nilai<br>Pabean</th>
					<th>Jenis</th>
					<th>NO</th>
					<th>Tgl</th>
					<th>Tgl<br>Keluar</th>
					<th>Nama<br>Barang</th>
					<th>Sat</th>
					<th>Jmlh</th>
					<th>Nilai<br>Pabean</th>
					<th>Jmlh</th>
					<th>Sat</th>
					<th>Nilai<br>Pabean</th>
				</tr></thead>';
return $content;	
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo contentProses();
	echo "<tbody>";
    $banyakData=count($resultData);
	if($banyakData>0){
		$no=1; 
		$kodeTrader = $this->newsession->userdata('KODE_TRADER');
		$sqlin = "SELECT LOGID, JENIS_DOK, NO_DOK, TGL_DOK, TGL_MASUK, KODE_TRADER, KODE_BARANG, JNS_BARANG, SERI_BARANG, NAMA_BARANG,
  				  SATUAN, JUMLAH, NILAI_PABEAN, FLAG_TUTUP, SALDO FROM t_logbook_pemasukan WHERE KODE_TRADER='".$kodeTrader."'				  
				  AND TGL_DOK BETWEEN '".$tglAwal."' AND '". $tglAkhir."' ORDER BY TGL_DOK, TGL_MASUK, SERI_BARANG ASC";	
		$rsmasuk = $this->db->query($sqlin);
		if($rsmasuk->num_rows()>0){
			$JUMLAHPEMASUKAN=0;
			$NILAIPABEANMASUK=0;
			foreach($rsmasuk->result_array() as $rowmasuk){
				$JUMLAHPEMASUKAN = $rowmasuk["JUMLAH"];
				$NILAIPABEANMASUK = $rowmasuk["NILAI_PABEAN"];
				$SATUAN = $rowmasuk["SATUAN"];
				
				#sementara AND NILAI_PABEAN='".$rowmasuk["NILAI_PABEAN"]."'
				if($kodeTrader=='EDIICITR0000' && $rowmasuk["KODE_BARANG"]=='09023B'){
					$sqlout = "SELECT LOGID, JENIS_DOK, NO_DOK, TGL_DOK, TGL_MASUK, KODE_TRADER, KODE_BARANG, JNS_BARANG, SERI_BARANG,
						   NAMA_BARANG, SATUAN, JUMLAH, NILAI_PABEAN, NO_DOK_MASUK, TGL_DOK_MASUK, JENIS_DOK_MASUK
						   FROM t_logbook_pengeluaran WHERE NO_DOK_MASUK = '".$rowmasuk["NO_DOK"]."' 
						   AND TGL_DOK_MASUK = '".$rowmasuk["TGL_DOK"]."' AND JENIS_DOK_MASUK = '".$rowmasuk["JENIS_DOK"]."'
						   AND KODE_BARANG='".$rowmasuk["KODE_BARANG"]."' AND JNS_BARANG='".$rowmasuk["JNS_BARANG"]."'
						   AND KODE_TRADER='".$kodeTrader."' AND JUMLAH='".$rowmasuk["JUMLAH"]."' 
						    AND SERI_BARANG='".$rowmasuk["SERI_BARANG"]."'
						   ORDER BY TGL_MASUK ASC";

				}else{
				
					$sqlout = "SELECT LOGID, JENIS_DOK, NO_DOK, TGL_DOK, TGL_MASUK, KODE_TRADER, KODE_BARANG, JNS_BARANG, SERI_BARANG,
						   NAMA_BARANG, SATUAN, JUMLAH, NILAI_PABEAN, NO_DOK_MASUK, TGL_DOK_MASUK, JENIS_DOK_MASUK
						   FROM t_logbook_pengeluaran WHERE NO_DOK_MASUK = '".$rowmasuk["NO_DOK"]."' 
						   AND TGL_DOK_MASUK = '".$rowmasuk["TGL_DOK"]."' AND JENIS_DOK_MASUK = '".$rowmasuk["JENIS_DOK"]."'
						   AND KODE_BARANG='".$rowmasuk["KODE_BARANG"]."' AND JNS_BARANG='".$rowmasuk["JNS_BARANG"]."'
						   AND KODE_TRADER='".$kodeTrader."' 
						   AND SERI_BARANG='".$rowmasuk["SERI_BARANG"]."'
						   ORDER BY TGL_MASUK ASC";
				}

				$rskeluar = $this->db->query($sqlout);
				$jumkeluar="";
				if($rskeluar->num_rows()>0){
					$jumkeluar = $rskeluar->num_rows();
				}
				$JUMLAHKELUARAN=0;
				$NILAIPABEANKELUAR=0;
				foreach($rskeluar->result_array() as $ROWSS){
					$JUMLAHKELUARAN = $JUMLAHKELUARAN+$ROWSS["JUMLAH"];	
					$NILAIPABEANKELUAR = $NILAIPABEANKELUAR+$ROWSS["NILAI_PABEAN"];	
				}
	?>
    			<tr>    
                    <td align="center" rowspan="<?=$jumkeluar?>"><?= $no;?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["JENIS_DOK"]?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["NO_DOK"]?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$this->fungsi->dateFormat($rowmasuk["TGL_DOK"])?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$this->fungsi->dateFormat($rowmasuk["TGL_MASUK"])?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["KODE_BARANG"]?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["SERI_BARANG"]?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["NAMA_BARANG"]?></td>
                    <td rowspan="<?=$jumkeluar?>"><?=$rowmasuk["SATUAN"]?></td>
                    <td rowspan="<?=$jumkeluar?>" align="right"><?=$this->fungsi->FormatRupiah($rowmasuk["JUMLAH"],2)?></td>
                    <td rowspan="<?=$jumkeluar?>" align="right"><?=$this->fungsi->FormatRupiah($rowmasuk["NILAI_PABEAN"],2)?></td>
                    
                    
    <?						
				if($rskeluar->num_rows()>0){
					$kel = 1;
					foreach($rskeluar->result_array() as $rowkeluar){	
						if($kel>1){
							echo "<tr>";	
						}	
	?>
    					<td><?=$rowkeluar["JENIS_DOK"]?></td>
                        <td><?=$rowkeluar["NO_DOK"]?></td>
                        <td><?=$this->fungsi->dateFormat($rowkeluar["TGL_DOK"])?></td>
                        <td><?=$this->fungsi->dateFormat($rowkeluar["TGL_MASUK"])?></td>
                        <td><?=$rowkeluar["NAMA_BARANG"]?></td>
                        <td><?=$rowkeluar["SATUAN"]?></td>
                        <td align="right"><?=$this->fungsi->FormatRupiah($rowkeluar["JUMLAH"],2)?></td>
                        <td align="right"><?=$this->fungsi->FormatRupiah($rowkeluar["NILAI_PABEAN"],2)?></td>
    <?                    
    	if($kel==1){  
	?>
            <td rowspan="<?=$jumkeluar?>" align="right"><?=$this->fungsi->FormatRupiah($JUMLAHPEMASUKAN-$JUMLAHKELUARAN,2)?></td>
            <td rowspan="<?=$jumkeluar?>"><?=$SATUAN?></td>
            <td rowspan="<?=$jumkeluar?>" align="right"><?=$this->fungsi->FormatRupiah($NILAIPABEANMASUK-$NILAIPABEANKELUAR,2)?></td>  
    <?     					
		}                    
	?>					
          
                            	
    <?					if($kel>1){
							echo "</tr>";	
						}
						$kel++;				
					}
				}else{
	?>    					
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><?=$this->fungsi->FormatRupiah($JUMLAHPEMASUKAN-$JUMLAHKELUARAN,2)?></td>
                    <td><?=$SATUAN?></td>
                    <td align="right"><?=$this->fungsi->FormatRupiah($NILAIPABEANMASUK-$NILAIPABEANKELUAR,2)?></td>  
    <?				
				}
			$no++;}
		}		
	?>
    	 
    </tr> 
    </tbody>
	<? }else{?>
    <tr>
    	<td colspan="23">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
</table>
<? }elseif($tipe=='mutasi'){
function contentMutasiBB($tglAwal,$tglAkhir,$TGLSTOCK){
	if(empty($tglAwal))$VALIDTGLAWAL="";else $VALIDTGLAWAL=FormatDate($tglAwal);
	if(empty($tglAkhir))$VALIDTGLAKHIR="";else $VALIDTGLAKHIR=FormatDate($tglAkhir);
	if(empty($TGLSTOCK))$VALIDTGLSTOCK="";else $VALIDTGLSTOCK=FormatDate($TGLSTOCK);	
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th rowspan=\"2\" width=\"1%\">No</th>";
	$content .="<th rowspan=\"2\" width=\"7%\">Kode&nbsp;Barang</th>";
	$content .="<th rowspan=\"2\" width=\"22%\">Nama&nbsp;Barang</th>";
	$content .="<th rowspan=\"2\" width=\"4%\">Satuan</th>";
	$content .="<th width=\"8%\">Saldo&nbsp;Awal</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Pemasukan</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Pengeluaran</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Penyesuaian (Adjusment)</th>";
	$content .="<th width=\"8%\">Saldo&nbsp;Akhir</th>";
	$content .="<th width=\"8%\">Stock&nbsp;Opname</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Selisih</th>";
	$content .="<th rowspan=\"2\" width=\"10%\">Keterangan</th>";
	$content .="</tr>";
	$content .="<tr>";
	$content .="<th>".$VALIDTGLAWAL."</th>";
	$content .="<th>".$VALIDTGLAKHIR."</th>";
	$content .="<th>".$VALIDTGLSTOCK."</th>";
	$content .="</tr>";	
	$content .="</thead>";
	return $content;
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo contentMutasiBB($tglAwal,$tglAkhir,$TGLSTOCK);
    $banyakData=count($resultData);
	
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$SaldoAwl=0;
		foreach($resultData as $listData){ 			
			#KOLOM PEMASUKAN
			if($listData['PEMASUKAN']==''){
				$sqlGetMasuk ="SELECT SUM(JUMLAH) AS PEMASUKAN
						   FROM m_trader_barang_inout
						   WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."' 
						   AND KODE_TRADER = '".$kodeTrader."'
						   AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
						   AND JNS_BARANG ='".$listData['JNS_BARANG']."'
						   AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP')"; 
				$MASUK=$this->db->query($sqlGetMasuk)->row();
				$MASUK = $MASUK->PEMASUKAN;
			}else{ 
				$MASUK = $listData['PEMASUKAN'];
			}			
			//CEK DARI DATA MUTASI YG DISIMPAN
			$sqlGetSaldoAwl="SELECT JUMLAH_SALDO_AKHIR
							 FROM r_trader_mutasi
							 WHERE TANGGAL_AWAL > '1999-01-01'
							 AND TANGGAL_AKHIR = DATE_SUB('".$tglAwal."',INTERVAL 1 DAY) 
							 AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
							 AND JNS_BARANG = '".$listData['JNS_BARANG']."'
							 AND KODE_TRADER = '".$kodeTrader."'
							 ORDER BY TANGGAL_AKHIR DESC"; 
		   	$query=$this->db->query($sqlGetSaldoAwl);
			$SALDOAWLGET = 0;
		   	if($query->num_rows() == 0){
				//KOLOM SALDO AWAL
				if($listData['JUMLAH_SALDO_AKHIR']==''){ 
					$tglAwalInOut=date('Y-m-d',strtotime($TGLSTOCK."+1 day")); 
					$tglAkhirInOut=date('Y-m-d',strtotime($tglAwal."-1 day"));
					$sqlGetSaldoStock ="SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
										FROM m_trader_stockopname
										WHERE KODE_TRADER ='".$kodeTrader."' 
										AND TANGGAL_STOCK <= '".$tglAwal."'
										AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
										AND JNS_BARANG ='".$listData['JNS_BARANG']."'
										ORDER BY TANGGAL_STOCK DESC LIMIT 1"; 
					$RSSTOCKOPNAME=$this->db->query($sqlGetSaldoStock)->row(); 
					$GETSALDOAWALSTOCK=$RSSTOCKOPNAME->JUMLAH_STOCK;
								 
					$sqlGetSaldoIn = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_IN', STR_TO_DATE(TANGGAL,'%Y-%m-%d') 'TGL_IN'
									  FROM m_trader_barang_inout
									  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$RSSTOCKOPNAME->TANGGAL_STOCK."' AND '". $tglAkhirInOut."'
									  AND KODE_TRADER = '".$kodeTrader."'
									  AND KODE_BARANG ='".$listData['KODE_BARANG']."' AND JNS_BARANG ='".$listData['JNS_BARANG']."'				  
									  AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP')";
									  
					$sqlGetSaldoOut ="SELECT SUM(JUMLAH) AS 'AWAL_SALDO_OUT', STR_TO_DATE(TANGGAL,'%Y-%m-%d') 'TGL_OUT'
									  FROM m_trader_barang_inout
									  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$RSSTOCKOPNAME->TANGGAL_STOCK."' AND '". $tglAkhirInOut."'
									  AND KODE_TRADER = '".$kodeTrader."'								  
									  AND KODE_BARANG ='".$listData['KODE_BARANG']."' AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									  AND TIPE IN ('GATE-OUT','PROCESS_IN')";
					
					$RSGETSALDOAWALIN=$this->db->query($sqlGetSaldoIn)->row(); 
					$GETSALDOAWALIN=$RSGETSALDOAWALIN->AWAL_SALDO_IN;
					$RSGETSALDOAWALOUT=$this->db->query($sqlGetSaldoOut)->row(); 
					$GETSALDOAWALOUT=$RSGETSALDOAWALOUT->AWAL_SALDO_OUT;
	
	
					if($GETSALDOAWALSTOCK==""){ 
						$SALDOAWLGET = $GETSALDOAWALSTOCK+$GETSALDOAWALIN-$GETSALDOAWALOUT;
					}else{
						if($RSSTOCKOPNAME->TANGGAL_STOCK==$tglAkhirInOut){
							$SALDOAWLGET = $GETSALDOAWALSTOCK;
						}else{
							if($RSSTOCKOPNAME->TANGGAL_STOCK==$RSGETSALDOAWALIN->TGL_IN||$RSSTOCKOPNAME->TANGGAL_STOCK==$RSGETSALDOAWALOUT->TGL_OUT){
								$SALDOAWLGET = $GETSALDOAWALSTOCK;
							}else{
								$SALDOAWLGET = $GETSALDOAWALSTOCK+$GETSALDOAWALIN-$GETSALDOAWALOUT;
							}
						}
					}
					$SALDOAWL = $SALDOAWLGET;
				}else{
					$SALDOAWL = $listData['JUMLAH_SALDO_AWAL'];
				}						   			 	   

	   		}else{
				$sqlGetSaldoStock ="SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
									FROM m_trader_stockopname
									WHERE TANGGAL_STOCK <= '".$tglAwal."'
									AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
									AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									AND KODE_TRADER ='".$kodeTrader."' ORDER BY TANGGAL_STOCK DESC LIMIT 1";
				$RSSTOCKOPNAME=$this->db->query($sqlGetSaldoStock)->row(); 
				$GETSALDOAWALSTOCK=$RSSTOCKOPNAME->JUMLAH_STOCK;
				if($GETSALDOAWALSTOCK!=""){
					if($RSSTOCKOPNAME->TANGGAL_STOCK==$tglAkhirInOut){
						$SALDOAWL = $GETSALDOAWALSTOCK;
					}else{
						if($listData['JUMLAH_SALDO_AKHIR']==''){
							$SALDOAWL = $query->row(); 
							$SALDOAWL = $SALDOAWL->JUMLAH_SALDO_AKHIR? $SALDOAWL->JUMLAH_SALDO_AKHIR:$SALDOAWLGET;
						}else{							
							$SALDOAWL=$listData['JUMLAH_SALDO_AWAL'];
						}
					}
				}else{
					if($listData['JUMLAH_SALDO_AKHIR']==''){
						$SALDOAWL = $query->row(); 
						$SALDOAWL = $SALDOAWL->JUMLAH_SALDO_AKHIR? $SALDOAWL->JUMLAH_SALDO_AKHIR:$SALDOAWLGET;
					}else{
						$SALDOAWL=$listData['JUMLAH_SALDO_AWAL'];
					}
				}			
			}
				   		
			#KOLOM PENGELUARAN		  
			if($listData['PENGELUARAN']==''){
				$sqlGetKeluar = "SELECT SUM(JUMLAH) AS PENGELUARAN
						FROM m_trader_barang_inout
						WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."'
						AND KODE_TRADER = '".$kodeTrader."'
						AND KODE_BARANG = '".$listData['KODE_BARANG']."' AND JNS_BARANG = '".$listData['JNS_BARANG']."'
						AND TIPE IN ('GATE-OUT','MUSNAH','RUSAK','PROCESS_IN')";
				$KELUAR=$this->db->query($sqlGetKeluar)->row(); 
				$KELUAR = $KELUAR->PENGELUARAN;
			}else{ 
				$KELUAR=$listData['PENGELUARAN'];
			}				   			
			#KOLOM PENYESUAIAN
			if($listData['PENYESUAIAN']==''){
				$PENYESUAIAN = 0;
			}else{ 
				$PENYESUAIAN = $listData['PENYESUAIAN'];
			}
			#KOLOM SALDO AKHIR
			if($listData['JUMLAH_SALDO_AKHIR']==''){
				$SALDOAKHR = $SALDOAWL + $MASUK - $KELUAR + $PENYESUAIAN;
			}else{
				$SALDOAKHR=$listData['JUMLAH_SALDO_AKHIR']; 
			}
			#KOLOM STOCKOPNAME
			if($listData['STOCK_OPNAME']==''){
				$sqlGetStock ="SELECT JUMLAH
								FROM m_trader_stockopname
								WHERE TANGGAL_STOCK='".$TGLSTOCK."' 
								AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
								AND JNS_BARANG = '".$listData['JNS_BARANG']."'
								AND KODE_TRADER = '".$kodeTrader."'";
				$STOCK=$this->db->query($sqlGetStock);
				if($STOCK->num_rows() > 0){
					$STOCK = $STOCK->row(); 
					$STOCK = $STOCK->JUMLAH;
				}else{
					$STOCK="";
				}
			}else{ 
				$STOCK=$listData['STOCK_OPNAME'];
			}	
			#UNTUK MENAMPILKAN SEMUA BARANG BAIK YG DIMUTASI ATAU TIDAK
			if($ALL){
				if(!in_array($listData['KODE_BARANG'],$INARRAY)){
				
				$sqlGetSaldoAwl="SELECT JUMLAH_SALDO_AKHIR
						 FROM r_trader_mutasi
						 WHERE TANGGAL_AWAL > '1999-01-01'
						 AND TANGGAL_AKHIR = DATE_SUB('".$tglAwal."',INTERVAL 1 DAY) 
						 AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
						 AND JNS_BARANG = '".$listData['JNS_BARANG']."'
						 AND KODE_TRADER = '".$kodeTrader."'
						 ORDER BY TANGGAL_AKHIR DESC";
		  		$query=$this->db->query($sqlGetSaldoAwl);
				if($query->num_rows() == 0){			

					$tglAkhirInOut=date('Y-m-d',strtotime($tglAwal."-1 day"));
					$sqlGetSaldoStock ="SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
									FROM m_trader_stockopname
									WHERE KODE_TRADER ='".$kodeTrader."' 
									AND TANGGAL_STOCK <= '".$tglAwal."'
									AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
									AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									ORDER BY TANGGAL_STOCK DESC LIMIT 1";
					$RSSTOCKOPNAME=$this->db->query($sqlGetSaldoStock)->row(); 
					$GETSALDOAWALSTOCK=$RSSTOCKOPNAME->JUMLAH_STOCK;
							 
					$sqlGetSaldoIn = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_IN', STR_TO_DATE(TANGGAL,'%Y-%m-%d') 'TGL_IN'
									FROM m_trader_barang_inout
									WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$RSSTOCKOPNAME->TANGGAL_STOCK."' AND '". $tglAkhirInOut."'
									AND KODE_TRADER = '".$kodeTrader."'
									AND KODE_BARANG ='".$listData['KODE_BARANG']."' AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP')";
								  
					$sqlGetSaldoOut ="SELECT SUM(JUMLAH) AS 'AWAL_SALDO_OUT', STR_TO_DATE(TANGGAL,'%Y-%m-%d') 'TGL_OUT'
									  FROM m_trader_barang_inout
									  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$RSSTOCKOPNAME->TANGGAL_STOCK."' AND '". $tglAkhirInOut."'
									  AND KODE_TRADER = '".$kodeTrader."'
									  AND KODE_BARANG ='".$listData['KODE_BARANG']."' AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									  AND TIPE IN ('GATE-OUT','PROCESS_IN')";
					
					$RSGETSALDOAWALIN=$this->db->query($sqlGetSaldoIn)->row(); 
					$GETSALDOAWALIN=$RSGETSALDOAWALIN->AWAL_SALDO_IN;
					$RSGETSALDOAWALOUT=$this->db->query($sqlGetSaldoOut)->row(); 
					$GETSALDOAWALOUT=$RSGETSALDOAWALOUT->AWAL_SALDO_OUT;
						
					if($GETSALDOAWALSTOCK==""){
						$SALDOAWLGET = $GETSALDOAWALSTOCK+$GETSALDOAWALIN-$GETSALDOAWALOUT; 
					}else{
						if($RSSTOCKOPNAME->TANGGAL_STOCK==$tglAkhirInOut){ 
							$SALDOAWLGET = $GETSALDOAWALSTOCK;
						}else{
							if($RSSTOCKOPNAME->TANGGAL_STOCK==$RSGETSALDOAWALIN->TGL_IN||$RSSTOCKOPNAME->TANGGAL_STOCK==$RSGETSALDOAWALOUT->TGL_OUT){
								$SALDOAWLGET = $GETSALDOAWALSTOCK; 
							}else{
								$SALDOAWLGET = $GETSALDOAWALSTOCK+$GETSALDOAWALIN-$GETSALDOAWALOUT;
							}
						}
					}
				}else{
					$tglAkhirInOut=date('Y-m-d',strtotime($tglAwal."-1 day"));
					$sqlGetSaldoStock ="SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
									FROM m_trader_stockopname
									WHERE KODE_TRADER ='".$kodeTrader."' 
									AND TANGGAL_STOCK <= '".$tglAwal."'
									AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
									AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									ORDER BY TANGGAL_STOCK DESC LIMIT 1"; 
					$RSSTOCKOPNAME=$this->db->query($sqlGetSaldoStock)->row(); 
					$GETSALDOAWALSTOCK=$RSSTOCKOPNAME->JUMLAH_STOCK;
					if($GETSALDOAWALSTOCK!=""){					
						if($RSSTOCKOPNAME->TANGGAL_STOCK==$tglAkhirInOut){
							$SALDOAWLGET= $GETSALDOAWALSTOCK;
						}else{
							if($listData['JUMLAH_SALDO_AKHIR']==''){
								$SALDOAWLGET = $query->row(); 
								$SALDOAWLGET = $SALDOAWLGET->JUMLAH_SALDO_AKHIR?$SALDOAWLGET->JUMLAH_SALDO_AKHIR:0;
							}else{
								$SALDOAWLGET=$listData['JUMLAH_SALDO_AWAL'];
							}
						}
					}else{
						if($listData['JUMLAH_SALDO_AKHIR']==''){
							$SALDOAWLGET = $query->row(); 
							$SALDOAWLGET = $SALDOAWLGET->JUMLAH_SALDO_AKHIR? $SALDOAWLGET->JUMLAH_SALDO_AKHIR:0;
						}else{
							$SALDOAWLGET=$listData['JUMLAH_SALDO_AWAL'];
						}
					} 			
				}
					
					$sqlGetMasuk ="SELECT SUM(JUMLAH) AS PEMASUKAN
						        FROM m_trader_barang_inout
						        WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."'
						        AND KODE_TRADER = '".$kodeTrader."'
						        AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
						        AND JNS_BARANG ='".$listData['JNS_BARANG']."'
						        AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP')";
					$MASUK=$this->db->query($sqlGetMasuk)->row();
				       $MASUK = $MASUK->PEMASUKAN;
				
					#KOLOM PENGELUARAN
					if($listData['PENGELUARAN']==''){						
						$sqlGetKeluar = "SELECT SUM(JUMLAH) AS PENGELUARAN
								   FROM m_trader_barang_inout
								   WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."'
								   AND KODE_TRADER = '".$kodeTrader."'
								   AND KODE_BARANG = '".$listData['KODE_BARANG']."' AND JNS_BARANG = '".$listData['JNS_BARANG']."'
								   AND TIPE IN ('GATE-OUT','MUSNAH','RUSAK','PROCESS_IN')";
						$KELUAR=$this->db->query($sqlGetKeluar)->row(); 
						$KELUAR = $KELUAR->PENGELUARAN;
					}else{ 
						$KELUAR=$listData['PENGELUARAN'];
					}
					
					$SALDOAWL = $SALDOAWLGET ;
					$SALDOAKHR = $SALDOAWL + $MASUK - $KELUAR + 0;

					#KOLOM STOCKOPNAME
					if($listData['STOCK_OPNAME']==''){
						$sqlGetStock ="SELECT JUMLAH
								FROM m_trader_stockopname
								WHERE TANGGAL_STOCK='".$TGLSTOCK."'
								AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
								AND JNS_BARANG = '".$listData['JNS_BARANG']."'
								AND KODE_TRADER = '".$kodeTrader."'";
						$STOCK=$this->db->query($sqlGetStock);
						if($STOCK->num_rows() > 0){
							$STOCK = $STOCK->row(); 
							$STOCK = $STOCK->JUMLAH;
						}else{
							$STOCK="";
						}
					}else{ 
						$STOCK=$listData['STOCK_OPNAME'];
					}
				}
			}

			#KOLOM SELISIH
			$SELISIH="";
			if($listData['SELISIH']==''){
				if ( (is_array($STOCK) && empty($STOCK)) || strlen($STOCK) === 0 ) { 
					$SELISIH ="";
				}else{
					$SELISIH = ($STOCK - $SALDOAKHR) + $PENYESUAIAN;
				}
			}else{
				$SELISIH=$listData['SELISIH'];
			}	

			if($SELISIH){
				if(number_format($SELISIH,2)=='-0.00'){
					$SELISIH = 0;
				}
			}	
	?>
		
    <tr>
    	<td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap" align="right"><?= number_format($SALDOAWL,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($MASUK,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($KELUAR,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($PENYESUAIAN,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($SALDOAKHR,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($STOCK,2);?></td>
        <td nowrap="nowrap" align="right"><?= number_format($SELISIH,2);?></td>
        <td nowrap="nowrap"><?=$listData['KETERANGAN']?$listData['KETERANGAN']:getKeterangan($SELISIH)?></td>
    </tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="pengrusakan"||$tipe=="pemusnahan"){
function content(){
	$content  ="<tr align=\"left\">";
	$content .="<th>No</th>";
	$content .="<th>Tanggal ".ucwords(strtolower($tipe))."</th>";
	$content .="<th>Kode Barang</th>";
	$content .="<th>Uraian Barang</th>";
	$content .="<th>Uraian Jenis</th>";
	$content .="<th>Jumlah</th>";
	$content .="<th>Uraian Satuan</th>";
	$content .="<th>Keterangan</th>";
	$content .="</tr>";
return $content;	
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo content();
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		$no=1;
		foreach($resultData as $listData){
		if($no%21==0){
			echo content();	
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['TGL'];?></td>
        <td nowrap="nowrap"><?= $listData['KDBARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIBARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIJENIS'];?></td>
        <td nowrap="nowrap"><?= $listData['JUMLAH'];?></td>
        <td nowrap="nowrap"><?= $listData['SATUAN'];?></td>
        <td nowrap="nowrap"><?= $listData['KETERANGAN'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="8">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
</table>

<? }elseif($tipe=="barang"){
function content(){
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th>No</th>";
	$content .="<th>KODE PERUSAHAAN</th>";
	$content .="<th>KODE BARANG</th>";
	$content .="<th>NAMA BARANG</th>";
	$content .="<th>JENIS BARANG</th>";
	$content .="<th>KODE HS</th>";
	$content .="<th>MERK</th>";
	$content .="<th>TIPE</th>";
	$content .="<th>UKURAN</th>";
	$content .="<th>SATUAN TERBESAR</th>";
	$content .="<th>SATUAN TERKECIL</th>";
	$content .="<th>KONVERSI SATUAN</th>";
	$content .="<th>STOCK GUDANG</th>";
	$content .="</tr>";
	$content .="</thead>";
	return $content;	
}	
?>
<table class="tabelPopUp" width="100%" border="0">
    <?
	echo content();
    $banyakData=count($resultData);
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$jm=21;
		foreach($resultData as $listData){
		if(($no/$jm)==1){
			 $jm=$jm+20;
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['KODE_PARTNER'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['JNS_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_HS'];?></td>
        <td nowrap="nowrap"><?= $listData['MERK'];?></td>
        <td nowrap="nowrap"><?= $listData['TIPE'];?></td>
        <td nowrap="nowrap"><?= $listData['UKURAN'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN_TERKECIL'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['JML_SATUAN_TERKECIL'];?></td>
        <td nowrap="nowrap" align="right"><?= $listData['STOCK_AKHIR'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="stock_opname"){ ?>
<table class="tabelPopUp" width="100%" border="0">
		<thead>
		<tr>    
	        <th>No</th>
	        <th>Kode Perusahaan</th>
	        <th>Kode Barang</th>
	        <th>Jenis Barang</th>
	        <th>Nama Barang</th>
	        <th>Kode Satuan</th>
	        <th>Jumlah Stock Opname</th>
	        <th>Kode Dok. Masuk</th>
	        <th>Nomor Dok. Masuk</th>
	        <th>Tanggal Dok. Masuk</th>
	        <th>Jumlah</th>
	        <th>Keterangan</th>
	    </tr>   
	    </thead>
		<tbody>
	    <? 	$banyakData=count($list['header']);
			if($banyakData>0){
				$no=1;$s=0;$rowspan="";
				foreach($list['header'] as $data){
					$SQL = "SELECT IDHDR, JENIS_DOK_MASUK, NO_DOK_MASUK, TGL_DOK_MASUK, JUMLAH 
						    FROM M_TRADER_STOCKOPNAME_DTL WHERE IDHDR = '".$data['ID']."'
						    ORDER BY TGL_DOK_MASUK";
					$rs = $this->db->query($SQL);
					$s = $rs->num_rows();
					if($s!=0) $rowspan =  'rowspan="'.$s.'"';
				?>
					<tr>    
		                <td align="center" <?=$rowspan;?>><?= $no;?></td>
		                <td <?=$rowspan;?>><?=$data["KODE_PARTNER"]?></td>
		                <td <?=$rowspan;?>><?=$data["KODE_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["JENIS_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["URAIAN_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["KODE_SATUAN"]?></td>
		                <td <?=$rowspan;?>><?=$data["JUMLAH"]?></td>
				<?
					$no++;											
					if($rs->num_rows()>0){
						$x=1;
						foreach($rs->result_array() as $dataDetil){
							if($x>1){
								echo '<tr>';
								echo '<td>'.$dataDetil["JENIS_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["NO_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["TGL_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["JUMLAH"].'</td>';
							}else{
								echo '<td>'.$dataDetil["JENIS_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["NO_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["TGL_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["JUMLAH"].'</td>
									  <td '.$rowspan.'>'.$data["KETERANGAN"].'</td>';
							}	  
							$x++;	  
						}	
					}else{
						echo '<td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>'.$data["KETERANGAN"].'</td>';
					}					
				}            
			}else{?>
	    <tr>
	    	<td colspan="12">Maaf Belum Terdapat Data</td>
	    </tr>
	    <? }?>
		
		</tbody>
	</table>
<? }elseif($tipe=="floating_uplift"){
		function content(){
			$content  ="<tr align=\"left\">
							<th>NAMA AIRLINES</th>
							<th>NO. PENERBANGAN</th>
							<th>TANGGAL BERANGKAT</th>
							<th>TANGGAL KEMBALI</th>
							<th>CATATAN</th>
						</tr>";
			return $content;	
		}	
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th></th>
							<th>No</th>
							<th>KODE AIRLINES</th>
							<th>KODE BARANG</th>
							<th>NAMA BARANG</th>
							<th>JENIS BARANG</th>
							<th>UNIT</th>
							<th>STANDAR UPLIFT QTY</th>
							<th>RETURN GOOD</th>
							<th>RETURN NG</th>
							<th>CONSUMPTION</th>
							<th>REMARKS</th>
						</tr>";
			return $content;	
		}	
?>
<table class="tabelPopUp" width="100%" ="1">
    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
			echo content();
	?>
  	<tr>
        <td nowrap="nowrap" width="10%"><?= $listData['NAMA_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['NO_FLIGHT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_BERANGKAT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_KEMBALI'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['CATATAN'];?></td>
   	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['UPLIFT_ID']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
        <td nowrap="nowrap"></td>
        <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['STANDAR_UPLIFT_QTY'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_BAIK'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_NG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KONSUMSI'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['REMARKS'];?></td>
   	</tr>
	<? $no++;}
	echo "<tr><td colspan='12'>&nbsp;</td></tr>";
	}}} else{?>
    <tr>
    	<td colspan="12">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
</table>
<? }elseif($tipe=="floating_pdf"){
		
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th>No</th>
							<th width=\"30%\">ITEMS</th>
							<th width=\"10%\">UNIT</th>
							<th width=\"15%\">STANDAR UPLIFT</th>
							<th width=\"15%\">RETURN GOOD</th>
							<th width=\"15%\">RETURN NG</th>
							<th>CONSUMPTION</th>
						</tr>";
			return $content;	
		}
		
		function contentBawah(){
			$content  ="<tr align=\"left\">
							<th colspan=\"2\">Delivered By :<br/><br/><br/><br/>(Operation Handling) </th>
							<th colspan=\"3\">Received By :<br/><br/><br/><br/>(Inflight Crew) </th>
							<th colspan=\"2\">Acknowledged By :<br/><br/><br/><br/>(Costums) </th>
						</tr>";
			return $content;	
		}
?>

    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
	?>
	<table class="tabelPopUp" width="100%" border="0">
	<tr>
		<th align="left">TO </th><th align="left">: IGC</th><th rowspan="2" colspan="3" style="font-size:20px;border:0;border-width:0" nowrap="nowrap"><?= $listData['NAMA_PARTNER'];?></th>
	</tr>
	<tr>
		<th align="left">FROM </th><th align="left">: OPERATION</th>
	</tr>
  	<tr>
		<th align="left">DATE</th> <th align="left">: <?= $listData['TGL_BERANGKAT'];?></th><th colspan="3"></th><th>JAL-<?= $listData['NO_FLIGHT'];?></th>
	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['UPLIFT_ID']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
       <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
       <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
       <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['STANDAR_UPLIFT_QTY'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_BAIK'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_NG'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['KONSUMSI'];?></td>
   	</tr>
	<? $no++;
	}
	echo "<tr><td colspan='7'>&nbsp;</td></tr>";
	echo contentBawah();
	?>
	<tr>
		<th align="left" colspan="2">Nama &nbsp;:&nbsp;<?= $bawah['DELIVERED_NAME']; ?></th>
		<th align="left" colspan="3">Nama &nbsp;:</th>
		<th align="left" colspan="2">Nama &nbsp;:</th>
	</tr>
	<tr>
		<th align="left" colspan="2">Nopeg :&nbsp;<?= $bawah['DELIVERED_NO']; ?></th>
		<th align="left" colspan="3">Nopeg :</th>
		<th align="left" colspan="2">Nopeg :</th>
	</tr>
	
	<?
	echo "<tr><td colspan='7'>&nbsp;</td></tr>";
	echo "</table><br/>";
	echo "<pagebreak>";
	}}
	//echo "</table><br/>";
	
	} else{?>
    <tr>
    	<td colspan="7">Maaf Belum Terdapat Data</td>
    </tr>
	</table>
    <? }?>
<? }elseif($tipe=="floating_store"){
		function content(){
			$content  ="<tr align=\"left\">
							<th>NOMOR PERMOHONAN</th>
							<th>TANGGAL PERMOHONAN</th>
							<th>NAMA AIRLINES</th>
							<th>JUMLAH ITEM BARANG</th>
							<th>STATUS</th>
						</tr>";
			return $content;	
		}	
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th></th>
							<th>No</th>
							<th>KODE BARANG</th>
							<th>NAMA BARANG</th>
							<th>JENIS BARANG</th>
							<th>UNIT</th>
							<th>SATUAN</th>
							<th>JUMLAH</th>
						</tr>";
			return $content;	
		}	
?>
<table class="tabelPopUp" width="100%" border="1">
    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
			echo content();
	?>
  	<tr>
        <td nowrap="nowrap" width="10%"><?= $listData['NO_SRR'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_SRR'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['NAMA_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['JML_BARANG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['STATUS'];?></td>
   	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['NO_SRR']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
        <td nowrap="nowrap"></td>
        <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SATUAN'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['JUMLAH'];?></td>
   	</tr>
	<? $no++;}
	echo "<tr><td colspan='12'>&nbsp;</td></tr>";
	}}} else{?>
    <tr>
    	<td colspan="12">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
</table>
<? }elseif($tipe=="store_pdf"){
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
	?>
	<div id="myDIV"><strong>DATE : <?= $resultData[0]['TGL_SRR'];?></strong></div>
	<table class="tabelPopUp" width="100%" border="0">
	<tr>
		<th>No</th>
		<th>KODE BARANG</th>
		<th>NAMA BARANG</th>
		<th>SIZE</th>
		<th>UNIT</th>
	</tr>
	<?
	//echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		foreach($resultDataDetil[$listData['NO_SRR']] as $listDataDetil){
	?>
	<tr>
        <td nowrap="nowrap"  align="center" width="3%"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SATUAN'];?></td>
   	</tr>
	<? $no++;
	}
	echo "";
	?>
</table>
<tr><td>&nbsp;</td></tr>
<table>
	<tr>
		<th nowrap="nowrap" width="8%">Requested By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Acknowledged By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Delivered By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Received By :<br/><br/><br/><br/><br/><br/></th>
	</tr>
	<tr>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $bawah['DELIVERED_NAME']; ?> </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
	</tr>
	<tr>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :&nbsp;<?= $bawah['DELIVERED_NO']; ?></th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
	</tr>
	<?
	echo "</table><br/>";
	echo "<pagebreak>";
	}}} else{?>
    <tr>
    	<td colspan="5">Maaf Belum Terdapat Data</td>
    </tr>
	</table>
    <? }?>
<? }elseif($tipe=="produksi"){	
function content(){
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th width=\"10px\">No</th>";
	$content .="<th>KODE BARANG</th>";
	$content .="<th>NAMA BARANG</th>";
	$content .="<th>JENIS BARANG</th>";
	$content .="<th>JUMLAH</th>";
	$content .="<th>SATUAN</th>";
	$content .="<th>TANGGAL</th>";
	$content .="</tr>";
	$content .="</thead>";
	return $content;	
}	
?>
<table class="tabelPopUp" width="100%" border="0">
    <?
	echo content();
    $banyakData=count($resultData);
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$jm=21;
		foreach($resultData as $listData){
		if(($no/$jm)==1){
			 $jm=$jm+20;
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right"><?= $listData['JUMLAH'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap"><?= $listData['TANGGAL'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="inout"){
	$no=1;		
?>  
 
<table class="tabelPopUp" width="100%" border="0">
	<thead>
	<tr>
    	<th rowspan="2" align="center">No</th>
        <th colspan="10">Dokumen <?=($jenis=="MASUK")?"Pemasukan":"Pengeluaran"?></th>
    </tr>    
    <tr>    
        <th>Jenis</th>
        <th>No. Dokumen</th>
        <th>Tgl. Dokumen</th>
        <th>Tgl. <?=ucwords(strtolower($jenis))?></th>
        <th>Kode Barang</th>
        <th>Seri Barang</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Jumlah</th>
        <th>Nilai Pabean</th>
    </tr>   
    </thead>
    <tbody>
    <? $banyakData=count($resultData);	
	   if($banyakData>0){
		$no=1; 	
		foreach($resultData as $rowmasuk){ 	
	?>
			<tr>    
				<td align="center"><?= $no;?></td>
				<td><?=$rowmasuk["JENIS_DOK"]?></td>
				<td><?=$rowmasuk["NO_DOK"]?></td>
				<td><?=$this->fungsi->dateFormat($rowmasuk["TGL_DOK"])?></td>
				<td><?=$this->fungsi->dateFormat($rowmasuk["TGL_MASUK"])?></td>
				<td><?=$rowmasuk["KODE_BARANG"]?></td>
				<td><?=$rowmasuk["SERI_BARANG"]?></td>
				<td><?=$rowmasuk["NAMA_BARANG"]?></td>
				<td><?=$rowmasuk["SATUAN"]?></td>
				<td align="right"><?=$this->fungsi->FormatRupiah($rowmasuk["JUMLAH"],2)?></td>
				<td align="right"><?=$this->fungsi->FormatRupiah($rowmasuk["NILAI_PABEAN"],2)?></td>
			</tr>    
    <? $no++; } ?>                  	 
    </tbody>
	<? }else{?>
    <tr>
    	<td colspan="23" align="center">Maaf Belum Terdapat Data</td>
    </tr>
    <? }?>
</table>
<? } ?>



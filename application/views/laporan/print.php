<? if($EXCEL=="excel"){?>
<style>
.fnumber{mso-number-format:"0\.00"}
.ftext{mso-number-format:"\@"}
</style>	
<? } ?>	
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);//echo $tipe;
$kodeTrader = $this->newsession->userdata('KODE_TRADER');

function getKeterangan($jml){
	if($jml<0) return "SELISIH KURANG";
	if($jml>0) return "SELISIH LEBIH";
	if($jml==0) return "SESUAI";
}

function FormatDate($vardate)
{
	$pecah1 = explode("-", $vardate);
	$tanggal = intval($pecah1[2]);
	$arrayBulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
						"Agustus", "September", "Oktober", "November", "Desember");
	$bulan = $arrayBulan[intval($pecah1[1])];
	$tahun = intval($pecah1[0]);
	$balik = $tanggal." ".$bulan." ".$tahun;
	return $balik;
}
if($tipe=="posisi"){
function contentProses(){
	$content  ='<thead><tr>
					<th rowspan="2" align="center">No</th>
					<th colspan="10">Dokumen Pemasukan</th>
					<th colspan="8">Dokumen Pengeluaran</th>
					<th colspan="3">Saldo Barang</th>
				</tr>    
				<tr>    
					<th>Jenis</th>
					<th>NO</th>
					<th>Tgl</th>
					<th>Tgl<br>Masuk</th>
					<th>Kode<br>Barang</th>
					<th>Seri<br>Barang</th>
					<th>Nama<br>Barang</th>
					<th>Sat</th>
					<th>Jmlh</th>
					<th>Nilai<br>Pabean</th>
					<th>Jenis</th>
					<th>NO</th>
					<th>Tgl</th>
					<th>Tgl<br>Keluar</th>
					<th>Nama<br>Barang</th>
					<th>Sat</th>
					<th>Jmlh</th>
					<th>Nilai<br>Pabean</th>
					<th>Jmlh</th>
					<th>Sat</th>
					<th>Nilai<br>Pabean</th>
				</tr></thead>';
return $content;	
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo contentProses();
	echo "<tbody>";
    #sementara kasih kondisi peruser :D
	if($kodeTrader=='EDITCTI00019XX'){		
	
	  $banyakData=count($resultData);	
	   if($banyakData>0){ 
			$no=0; 		
			$TOTALKELUAR = array();
			$TOTALNILAIP = array();
			$LOGID_1 = array();
			$LOGID_2 = array();
			$JUMARRAY = array();
			$NILARRAY = array();
			$OUTDOKARRAY = array();
			foreach($resultData as $row){	
				$JUMARRAY[$row["LOGID_IN"]][] = $row["JUMLAH_OUT"];					
				$TOTALKELUAR[$row["LOGID_IN"]] = array_sum($JUMARRAY[$row["LOGID_IN"]]);
				$NILARRAY[$row["LOGID_IN"]][] = $row["NILAI_PABEAN_OUT"];					
				$TOTALNILAIP[$row["LOGID_IN"]] = array_sum($NILARRAY[$row["LOGID_IN"]]);
				
				$VAR = trim(str_replace(' ','',$row["JENIS_DOK_OUT"].$row["NO_DOK_OUT"].$row["TGL_DOK_OUT"].$row["TGL_MASUK_OUT"].$row["KODE_BARANG_OUT"].$row["JNS_BARANG_OUT"].$row["SERI_BARANG_OUT"].$row["JUMLAH_OUT"].$row["NILAI_PABEAN_OUT"]));											
				if($VAR){	
					$JUM_OUT_ARRAY[$VAR][] = $row["JUMLAH_IN"];	
					$TOT_JUM_OUT_ARRAY[$VAR] = array_sum($JUM_OUT_ARRAY[$VAR]);					
					$NILAI_OUT_ARRAY[$VAR][] = $row["NILAI_PABEAN_IN"];					
					$TOT_NILAI_OUT_ARRAY[$VAR] = array_sum($NILAI_OUT_ARRAY[$VAR]);	
					
					$JUMCOLSPAN[$VAR] = count($JUM_OUT_ARRAY[$VAR]);	
				}
								
			}
			foreach($resultData as $row){				
				if(!in_array($row["LOGID_IN"],$LOGID_1)){
					$LOGID_1[] = $row["LOGID_IN"];$no++;
	?>
				<tr>    
					<td align="center"><?= $no;?></td>
					<td><?=$row["JENIS_DOK_IN"]?></td>
					<td><?=$row["NO_DOK_IN"]?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_DOK_IN"])?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_MASUK_IN"])?></td>
					<td><?=$row["KODE_BARANG_IN"]?></td>
					<td><?=$row["SERI_BARANG_IN"]?></td>
					<td><?=$row["NAMA_BARANG_IN"]?></td>
					<td><?=$row["SATUAN_IN"]?></td>
					<td align="right"><?=$this->fungsi->FormatRupiah($row["JUMLAH_IN"],2)?></td>
					<td align="right"><?=$this->fungsi->FormatRupiah($row["NILAI_PABEAN_IN"],2)?></td>
	<?
				}else{									
	?>				
    				<tr>    
					<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
     <?
				}	
				
				$VAR = trim(str_replace(' ','',$row["JENIS_DOK_OUT"].$row["NO_DOK_OUT"].$row["TGL_DOK_OUT"].$row["TGL_MASUK_OUT"].$row["KODE_BARANG_OUT"].$row["JNS_BARANG_OUT"].$row["SERI_BARANG_OUT"].$row["JUMLAH_OUT"].$row["NILAI_PABEAN_OUT"]));	
				if((!in_array($VAR,$OUTDOKARRAY) && $row["NO_DOK_OUT"]!="")){				
					$OUTDOKARRAY[] = $VAR;
	 ?>               
					<td><?=$row["JENIS_DOK_OUT"]?></td>
					<td><?=$row["NO_DOK_OUT"]?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_DOK_OUT"])?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_MASUK_OUT"])?></td>
					<td><?=$row["NAMA_BARANG_OUT"]?></td>
					<td><?=$row["SATUAN_OUT"]?></td>
					<td align="right"><?=$row["JUMLAH_OUT"]?$this->fungsi->FormatRupiah($row["JUMLAH_OUT"],2):''?></td>
					<td align="right"><?=$row["NILAI_PABEAN_OUT"]?$this->fungsi->FormatRupiah($row["NILAI_PABEAN_OUT"],2):''?></td>
	<?
				}else{		
				
				if( ($row["NO_DOK_MASUK_OUT"]==$row["NO_DOK_IN"]) 
						and ($row["TGL_DOK_MASUK_OUT"]==$row["TGL_DOK_IN"])
						and ($row["KODE_BARANG_OUT"]==$row["KODE_BARANG_IN"])
						and ($row["JNS_BARANG_OUT"]==$row["JNS_BARANG_IN"])
						)
				{
					$tes = "y";
				}else{					
					$tes = "n";
				}						
	?>				 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>	
                    <td>&nbsp;</td>                    
     <?
				}	
				
    			if(!in_array($row["LOGID_IN"],$LOGID_2)&&$tes=="n"){
					$LOGID_2[] = $row["LOGID_IN"]; 
					
					if($JUMCOLSPAN[$VAR]>1){
	?>				    				
					<td align="right">
					<?=$this->fungsi->FormatRupiah($TOT_JUM_OUT_ARRAY[$VAR]-$row["JUMLAH_OUT"],2)?></td>
					<td><?=$row["SATUAN_OUT"]?></td>
					<td align="right"><?=$this->fungsi->FormatRupiah($TOT_NILAI_OUT_ARRAY[$VAR]-$row["NILAI_PABEAN_OUT"],2)?></td>  
    <?								
					}else{
	?>				
					<td align="right"><?=$this->fungsi->FormatRupiah($row["JUMLAH_IN"]-$TOTALKELUAR[$row["LOGID_IN"]],2)?></td>
					<td><?=$row["SATUAN_IN"]?></td>
					<td align="right"><?=$this->fungsi->FormatRupiah($row["NILAI_PABEAN_IN"]-$TOTALNILAIP[$row["LOGID_IN"]],2)?></td>  
    <?
					}
				}else{
	?>                    				
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
    <?
				}
	?>                
				</tr>
	<?											
		}	
	?>
    	 
    </tr> 
    </tbody>
	<? }else{?>
    <tr>
    	<td colspan="23" align="center">Nihil</td>
    </tr>
    <? }?>
  
  <? 
		
	}
	else{
		$banyakData=count($resultData);	
	   if($banyakData>0){ 
			$no=0; 		
			$TOTALKELUAR = array();
			$TOTALNILAIP = array();
			$LOGID_1 = array();
			$LOGID_2 = array();
			$JUMARRAY = array();
			$NILARRAY = array();
			foreach($resultData as $row){	
				$JUMARRAY[$row["LOGID_IN"]][] = $row["JUMLAH_OUT"];					
				$TOTALKELUAR[$row["LOGID_IN"]] = array_sum($JUMARRAY[$row["LOGID_IN"]]);
				$NILARRAY[$row["LOGID_IN"]][] = $row["NILAI_PABEAN_OUT"];					
				$TOTALNILAIP[$row["LOGID_IN"]] = array_sum($NILARRAY[$row["LOGID_IN"]]);
			}
			foreach($resultData as $row){
				if(!in_array($row["LOGID_IN"],$LOGID_1)){
					$LOGID_1[] = $row["LOGID_IN"];$no++;
					
					if($TIPE_PERIODE!="IN"){
						$SQLOUT ="SELECT IFNULL(SUM(JUMLAH),0) AS JUMKELUAR_SEBELUMNYA, 
								  IFNULL(SUM(NILAI_PABEAN),0) NILAI_PABEAN_SEBELUMNYA
								   FROM t_logbook_pengeluaran
								   WHERE NO_DOK_MASUK = '".$row["NO_DOK_IN"]."' AND TGL_DOK_MASUK = '".$row["TGL_DOK_IN"]."'
								   AND KODE_BARANG='".$row["KODE_BARANG_IN"]."' AND JNS_BARANG='".$row["JNS_BARANG_IN"]."'"; 
						$SQLOUT.=" AND TGL_DOK < '".$tglAwal."'	 AND LOGID != '".$row["LOGID_OUT"]."'";		   
						$RSS = $this->db->query($SQLOUT)->row();
						$JUMKELUAR_SEBELUMNYA = $RSS->JUMKELUAR_SEBELUMNYA;
						$NILAI_PABEAN_SEBELUMNYA = $RSS->NILAI_PABEAN_SEBELUMNYA;				
						if($JUMKELUAR_SEBELUMNYA>0) $row["JUMLAH_IN"] = $row["JUMLAH_IN"]-$JUMKELUAR_SEBELUMNYA;
						if($NILAI_PABEAN_SEBELUMNYA>0) $row["NILAI_PABEAN_IN"] = $row["NILAI_PABEAN_IN"]-$NILAI_PABEAN_SEBELUMNYA;
					}
					//if($row["NO_DOK_IN"]=='002937') echo $JUMKELUAR_SEBELUMNYA;
					
	?>
				<tr>    
					<td align="center"><?= $no;?></td>
					<td><?=$row["JENIS_DOK_IN"]?></td>
					<td class="ftext"><?=$row["NO_DOK_IN"]?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_DOK_IN"])?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_MASUK_IN"])?></td>
					<td class="ftext"><?=$row["KODE_BARANG_IN"]?></td>
					<td><?=$row["SERI_BARANG_IN"]?></td>
					<td class="ftext"><?=$row["NAMA_BARANG_IN"]?></td>
					<td><?=$row["SATUAN_IN"]?></td>
					<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($row["JUMLAH_IN"],2)?></td>
					<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($row["NILAI_PABEAN_IN"],2)?></td>
	<?
				}else{									
	?>				
    				<tr>    
					<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
     <?
				}	
	 ?>               
					<td><?=$row["JENIS_DOK_OUT"]?></td>
					<td class="ftext"><?=$row["NO_DOK_OUT"]?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_DOK_OUT"])?></td>
					<td><?=$this->fungsi->dateFormat($row["TGL_MASUK_OUT"])?></td>
					<td class="ftext"><?=$row["NAMA_BARANG_OUT"]?></td>
					<td><?=$row["SATUAN_OUT"]?></td>
					<td align="right" class="fnumber"><?=$row["JUMLAH_OUT"]?$this->fungsi->FormatRupiah($row["JUMLAH_OUT"],2):''?></td>
					<td align="right" class="fnumber"><?=$row["NILAI_PABEAN_OUT"]?$this->fungsi->FormatRupiah($row["NILAI_PABEAN_OUT"],2):''?></td>
	<?
    			if(!in_array($row["LOGID_IN"],$LOGID_2)){
					$LOGID_2[] = $row["LOGID_IN"]; 
					$TOTSELISIH = $row["JUMLAH_IN"]-$TOTALKELUAR[$row["LOGID_IN"]];
	?>				
					<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($TOTSELISIH<0?0:$TOTSELISIH,2)?></td>
					<td><?=$row["SATUAN_IN"]?></td>
					<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($row["NILAI_PABEAN_IN"]-$TOTALNILAIP[$row["LOGID_IN"]],2)?></td>  
    <?
				}else{
	?>                    				
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
    <?
				}
	?>                
				</tr>
	<?											
		}	
	?>
    	 
    </tr> 
    </tbody>
	<? }else{?>
    <tr>
    	<td colspan="23" align="center">Nihil</td>
    </tr>
    <? }} ?>  
</table>
<? }elseif($tipe=='mutasi'){
function contentMutasiBB($tglAwal,$tglAkhir,$TGLSTOCK){
	if(empty($tglAwal))$VALIDTGLAWAL="";else $VALIDTGLAWAL=FormatDate($tglAwal);
	if(empty($tglAkhir))$VALIDTGLAKHIR="";else $VALIDTGLAKHIR=FormatDate($tglAkhir);
	if(empty($TGLSTOCK))$VALIDTGLSTOCK="";else $VALIDTGLSTOCK=FormatDate($TGLSTOCK);	
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th rowspan=\"2\" width=\"1%\">No</th>";
	$content .="<th rowspan=\"2\" width=\"7%\">Kode&nbsp;Barang</th>";
	$content .="<th rowspan=\"2\" width=\"22%\">Nama&nbsp;Barang</th>";
	$content .="<th rowspan=\"2\" width=\"4%\">Satuan</th>";
	$content .="<th width=\"8%\">Saldo&nbsp;Awal</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Pemasukan</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Pengeluaran</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Penyesuaian (Adjusment)</th>";
	$content .="<th width=\"8%\">Saldo&nbsp;Akhir</th>";
	$content .="<th width=\"8%\">Stock&nbsp;Opname</th>";
	$content .="<th rowspan=\"2\" width=\"8%\">Selisih</th>";
	$content .="<th rowspan=\"2\" width=\"10%\">Keterangan</th>";
	$content .="</tr>";
	$content .="<tr>";
	$content .="<th>".$VALIDTGLAWAL."</th>";
	$content .="<th>".$VALIDTGLAKHIR."</th>";
	$content .="<th>".$VALIDTGLSTOCK."</th>";
	$content .="</tr>";	
	$content .="</thead>";
	return $content;
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo contentMutasiBB($tglAwal,$tglAkhir,$TGLSTOCK);
    $banyakData=count($resultData);
	
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$SaldoAwl=0;
		foreach($resultData as $listData){ 	
			#KOLOM PEMASUKAN
			if($listData['PEMASUKAN']==''){
				$sqlGetMasuk ="SELECT SUM(JUMLAH) AS PEMASUKAN
						   FROM m_trader_barang_inout
						   WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."' 
						   AND KODE_TRADER = '".$kodeTrader."'
						   AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
						   AND JNS_BARANG ='".$listData['JNS_BARANG']."'
						   AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP')"; 
				
				$sqlGetMasuk .= " GROUP BY KODE_BARANG, JNS_BARANG";
						   
				$MASUK=$this->db->query($sqlGetMasuk)->row();
				$MASUK = $MASUK->PEMASUKAN;
			}else{ 
				$MASUK = $listData['PEMASUKAN'];
			}			
				
			//CEK DARI DATA MUTASI YG DISIMPAN
			$sqlGetSaldoAwl="SELECT JUMLAH_SALDO_AKHIR
							 FROM r_trader_mutasi
							 WHERE TANGGAL_AWAL > '1999-01-01'
							 AND TANGGAL_AKHIR = DATE_SUB('".$tglAwal."',INTERVAL 1 DAY) 
							 AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
							 AND JNS_BARANG = '".$listData['JNS_BARANG']."'
							 AND KODE_TRADER = '".$kodeTrader."'"; 
			
			$sqlGetSaldoAwl .= " ORDER BY TANGGAL_AKHIR DESC";				 
		   	$query=$this->db->query($sqlGetSaldoAwl);
			$SALDOAWLGET = 0;
		   	if($query->num_rows() == 0){
				//KOLOM SALDO AWAL
				if($listData['JUMLAH_SALDO_AKHIR']==''){ 
					 $tglAwalInOut = date('Y-m-d', strtotime($TGLSTOCK . "+1 day"));
					$tglAkhirInOut = date('Y-m-d', strtotime($tglAwal . "-1 day"));
					$sqlGetSaldoStock = "SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
									FROM m_trader_stockopname
									WHERE KODE_TRADER ='" . $kodeTrader . "' 
									AND TANGGAL_STOCK <= '" . $tglAwal . "'
									AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' 
									AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'";
					
					$sqlGetSaldoStock .= " ORDER BY TANGGAL_STOCK DESC LIMIT 1";

					$RSSTOCKOPNAME = $this->db->query($sqlGetSaldoStock)->row();
					$GETSALDOAWALSTOCK = $RSSTOCKOPNAME->JUMLAH_STOCK;

					$TGSTK = "";
					if ($RSSTOCKOPNAME->TANGGAL_STOCK != "") {
						$TGSTK = " BETWEEN '" . date('Y-m-d', strtotime($RSSTOCKOPNAME->TANGGAL_STOCK . "+1 day")) . "' AND '" . $tglAkhirInOut . "'";
					} else {
						$TGSTK = " <= '" . $tglAkhirInOut . "'";
					}
								 
					 $sqlGetSaldoIn = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_IN', STR_TO_DATE(MAX(TANGGAL),'%Y-%m-%d') 'TGL_IN'
									  FROM m_trader_barang_inout
									  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') " . $TGSTK . "
									  AND KODE_TRADER = '" . $kodeTrader . "'
									  AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'				  
									  AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP','MOVE-IN')";
					
					$sqlGetSaldoIn .= " GROUP BY KODE_BARANG, JNS_BARANG";

					$sqlGetSaldoOut = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_OUT', STR_TO_DATE(MAX(TANGGAL),'%Y-%m-%d') 'TGL_OUT'
								  FROM m_trader_barang_inout
								  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') " . $TGSTK . "
								  AND KODE_TRADER = '" . $kodeTrader . "'								  
								  AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'
								  AND TIPE IN ('GATE-OUT','PROCESS_IN','MOVE-OUT','MUSNAH','RUSAK')";
					
					$sqlGetSaldoOut .= " GROUP BY KODE_BARANG, JNS_BARANG";
					
					$RSGETSALDOAWALIN = $this->db->query($sqlGetSaldoIn)->row();
					$GETSALDOAWALIN = $RSGETSALDOAWALIN->AWAL_SALDO_IN;
					$RSGETSALDOAWALOUT = $this->db->query($sqlGetSaldoOut)->row();
					$GETSALDOAWALOUT = $RSGETSALDOAWALOUT->AWAL_SALDO_OUT;

					if ($GETSALDOAWALSTOCK == "") { //if($listData['KODE_BARANG']=="105098400") echo $RSGETSALDOAWALIN->AWAL_SALDO_IN;;
						$SALDOAWLGET = $GETSALDOAWALSTOCK + $GETSALDOAWALIN - $GETSALDOAWALOUT;
					} else {
						if ($RSSTOCKOPNAME->TANGGAL_STOCK == $tglAkhirInOut) {
							//if($listData['KODE_BARANG']=="105098400") echo "-";
							$SALDOAWLGET = $GETSALDOAWALSTOCK;
						} else {
							if ($RSSTOCKOPNAME->TANGGAL_STOCK == $RSGETSALDOAWALIN->TGL_IN || $RSSTOCKOPNAME->TANGGAL_STOCK == $RSGETSALDOAWALOUT->TGL_OUT) { //if($listData['KODE_BARANG']=="105098400") echo "- -";
								$SALDOAWLGET = $GETSALDOAWALSTOCK;
							} else {
								// if($listData['KODE_BARANG']=="105098400") echo "- - -";
								$SALDOAWLGET = $GETSALDOAWALSTOCK + $GETSALDOAWALIN - $GETSALDOAWALOUT;
							}
						}
					}
					$SALDOAWL = $SALDOAWLGET;
				} else {
					$SALDOAWL = $listData['JUMLAH_SALDO_AWAL'];
				}						   			 	   

	   		}else{
				$tglAkhirInOut=date('Y-m-d',strtotime($tglAwal."-1 day"));
				$sqlGetSaldoStock ="SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
									FROM m_trader_stockopname
									WHERE TANGGAL_STOCK <= '".$tglAwal."'
									AND KODE_BARANG ='".$listData['KODE_BARANG']."' 
									AND JNS_BARANG ='".$listData['JNS_BARANG']."'
									AND KODE_TRADER ='".$kodeTrader."'";
				
				$sqlGetSaldoStock .= " ORDER BY TANGGAL_STOCK DESC LIMIT 1";
										
				$RSSTOCKOPNAME=$this->db->query($sqlGetSaldoStock)->row(); 
				$GETSALDOAWALSTOCK=$RSSTOCKOPNAME->JUMLAH_STOCK;
				if($GETSALDOAWALSTOCK!=""){
					if($RSSTOCKOPNAME->TANGGAL_STOCK==$tglAkhirInOut){
						$SALDOAWL = $GETSALDOAWALSTOCK;
					}else{
						if($listData['JUMLAH_SALDO_AKHIR']==''){
							$SALDOAWL = $query->row(); 
							$SALDOAWL = $SALDOAWL->JUMLAH_SALDO_AKHIR? $SALDOAWL->JUMLAH_SALDO_AKHIR:$SALDOAWLGET;
						}else{							
							$SALDOAWL=$listData['JUMLAH_SALDO_AWAL'];
						}
					}
				}else{
					if($listData['JUMLAH_SALDO_AKHIR']==''){
						$SALDOAWL = $query->row(); 
						$SALDOAWL = $SALDOAWL->JUMLAH_SALDO_AKHIR? $SALDOAWL->JUMLAH_SALDO_AKHIR:$SALDOAWLGET;
					}else{
						$SALDOAWL=$listData['JUMLAH_SALDO_AWAL'];
					}
				}			
			}
				   		
			#KOLOM PENGELUARAN		  
			if($listData['PENGELUARAN']==''){
				$sqlGetKeluar = "SELECT SUM(JUMLAH) AS PENGELUARAN
						FROM m_trader_barang_inout
						WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '".$tglAwal."' AND '". $tglAkhir."'
						AND KODE_TRADER = '".$kodeTrader."'
						AND KODE_BARANG = '".$listData['KODE_BARANG']."' AND JNS_BARANG = '".$listData['JNS_BARANG']."'
						AND TIPE IN ('GATE-OUT','MUSNAH','RUSAK','PROCESS_IN')";
				
				$sqlGetKeluar .= " GROUP BY KODE_BARANG, JNS_BARANG";
							
				$KELUAR=$this->db->query($sqlGetKeluar)->row(); 
				$KELUAR = $KELUAR->PENGELUARAN;
			}else{ 
				$KELUAR=$listData['PENGELUARAN'];
			}				   			
			#KOLOM PENYESUAIAN
			if($listData['PENYESUAIAN']==''){
				$PENYESUAIAN = 0;
			}else{ 
				$PENYESUAIAN = $listData['PENYESUAIAN'];
			}
			#KOLOM SALDO AKHIR
			if($listData['JUMLAH_SALDO_AKHIR']==''){
				$SALDOAKHR = $SALDOAWL + $MASUK - $KELUAR + $PENYESUAIAN;
			}else{
				$SALDOAKHR=$listData['JUMLAH_SALDO_AKHIR']; 
			}
			#KOLOM STOCKOPNAME
			if($listData['STOCK_OPNAME']==''){
				$sqlGetStock ="SELECT JUMLAH
								FROM m_trader_stockopname
								WHERE TANGGAL_STOCK='".$TGLSTOCK."' 
								AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
								AND JNS_BARANG = '".$listData['JNS_BARANG']."'
								AND KODE_TRADER = '".$kodeTrader."'";
								
				$STOCK=$this->db->query($sqlGetStock);
				if($STOCK->num_rows() > 0){
					$STOCK = $STOCK->row(); 
					$STOCK = $STOCK->JUMLAH;
				}else{
					$STOCK="";
				}
			}else{ 
				$STOCK=$listData['STOCK_OPNAME'];
			}	
			
			#UNTUK MENAMPILKAN SEMUA BARANG BAIK YG DIMUTASI ATAU TIDAK
			if($ALL){
				if(!in_array($listData['KODE_BARANG'],$INARRAY)){
					$tglAkhirInOut = date('Y-m-d', strtotime($tglAwal . "-1 day"));
					$sqlGetSaldoStock = "SELECT JUMLAH AS 'JUMLAH_STOCK', TANGGAL_STOCK
								FROM m_trader_stockopname
								WHERE TANGGAL_STOCK <= '" . $tglAwal . "'
								AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' 
								AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'
								AND KODE_TRADER ='" . $kodeTrader . "'";
					if ($KODE_LOKASI) {
						$sqlGetSaldoStock .= " AND KODE_LOKASI = '" . $KODE_LOKASI . "'";
					}
					$sqlGetSaldoStock .= " ORDER BY TANGGAL_STOCK DESC LIMIT 1";

					$RSSTOCKOPNAME = $this->db->query($sqlGetSaldoStock)->row();
					$GETSALDOAWALSTOCK = $RSSTOCKOPNAME->JUMLAH_STOCK;

					$TGSTK = "";
					if ($RSSTOCKOPNAME->TANGGAL_STOCK != "") {
						$TGSTK = " BETWEEN '" . date('Y-m-d', strtotime($RSSTOCKOPNAME->TANGGAL_STOCK . "+1 day")) . "' AND '" . $tglAkhirInOut . "'";
					} else {
						$TGSTK = " <= '" . $tglAkhirInOut . "'";
					}

					$sqlGetSaldoIn = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_IN', STR_TO_DATE(MAX(TANGGAL),'%Y-%m-%d') 'TGL_IN'
								  FROM m_trader_barang_inout
								  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') " . $TGSTK . "
								  AND KODE_TRADER = '" . $kodeTrader . "'
								  AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'
								  AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP','MOVE-IN')";
					if ($KODE_LOKASI) {
						$sqlGetSaldoIn .= " AND KODE_LOKASI = '" . $KODE_LOKASI . "'";
					}
					$sqlGetSaldoIn .= " GROUP BY KODE_BARANG, JNS_BARANG";

					$sqlGetSaldoOut = "SELECT SUM(JUMLAH) AS 'AWAL_SALDO_OUT', STR_TO_DATE(MAX(TANGGAL),'%Y-%m-%d') 'TGL_OUT'
								  FROM m_trader_barang_inout
								  WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') " . $TGSTK . "
								  AND KODE_TRADER = '" . $kodeTrader . "'
								  AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'
								  AND TIPE IN ('GATE-OUT','PROCESS_IN','MOVE-OUT','MUSNAH','RUSAK')";
					if ($KODE_LOKASI) {
						$sqlGetSaldoOut .= " AND KODE_LOKASI = '" . $KODE_LOKASI . "'";
					}
					$sqlGetSaldoOut .= " GROUP BY KODE_BARANG, JNS_BARANG";

					$RSGETSALDOAWALIN = $this->db->query($sqlGetSaldoIn)->row();
					$GETSALDOAWALIN = $RSGETSALDOAWALIN->AWAL_SALDO_IN;
					$RSGETSALDOAWALOUT = $this->db->query($sqlGetSaldoOut)->row();
					$GETSALDOAWALOUT = $RSGETSALDOAWALOUT->AWAL_SALDO_OUT;

					if ($GETSALDOAWALSTOCK == "") {
						$SALDOAWLGET = $GETSALDOAWALSTOCK + $GETSALDOAWALIN - $GETSALDOAWALOUT;
					} else {
						if ($RSSTOCKOPNAME->TANGGAL_STOCK == $tglAkhirInOut) {
							$SALDOAWLGET = $GETSALDOAWALSTOCK;
						} else {
							if ($RSSTOCKOPNAME->TANGGAL_STOCK == $RSGETSALDOAWALIN->TGL_IN || $RSSTOCKOPNAME->TANGGAL_STOCK == $RSGETSALDOAWALOUT->TGL_OUT) {
								$SALDOAWLGET = $GETSALDOAWALSTOCK;
							} else {
								$SALDOAWLGET = $GETSALDOAWALSTOCK + $GETSALDOAWALIN - $GETSALDOAWALOUT;
							}
						}
					}
				
					 $sqlGetMasuk = "SELECT REPLACE(FORMAT(SUM(JUMLAH),2),',','') AS PEMASUKAN
						        FROM m_trader_barang_inout
						        WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '" . $tglAwal . "' AND '" . $tglAkhir . "'
						        AND KODE_TRADER = '" . $kodeTrader . "'
						        AND KODE_BARANG ='" . $listData['KODE_BARANG'] . "' 
						        AND JNS_BARANG ='" . $listData['JNS_BARANG'] . "'
						        AND TIPE IN ('GATE-IN','PROCESS_OUT','SCRAP','MOVE-IN')";

					$sqlGetMasuk .= " GROUP BY KODE_BARANG, JNS_BARANG";
						
					$MASUK=$this->db->query($sqlGetMasuk)->row();
				    $MASUK = $MASUK->PEMASUKAN;
				
					#KOLOM PENGELUARAN
					if($listData['PENGELUARAN']==''){						
						 $sqlGetKeluar = "SELECT REPLACE(FORMAT(SUM(JUMLAH),2),',','') AS PENGELUARAN
							   FROM m_trader_barang_inout
							   WHERE STR_TO_DATE(TANGGAL,'%Y-%m-%d') BETWEEN '" . $tglAwal . "' AND '" . $tglAkhir . "'
							   AND KODE_TRADER = '" . $kodeTrader . "'
							   AND KODE_BARANG = '" . $listData['KODE_BARANG'] . "' AND JNS_BARANG = '" . $listData['JNS_BARANG'] . "'
							   AND TIPE IN ('GATE-OUT','MUSNAH','RUSAK','PROCESS_IN','MOVE-OUT','MUSNAH','RUSAK')";

						$sqlGetKeluar .= " GROUP BY KODE_BARANG, JNS_BARANG";
						$KELUAR=$this->db->query($sqlGetKeluar)->row(); 
						$KELUAR = $KELUAR->PENGELUARAN;
					}else{ 
						$KELUAR=$listData['PENGELUARAN'];
					}
					
					$SALDOAWL = $SALDOAWLGET ;
					$SALDOAKHR = $SALDOAWL + $MASUK - $KELUAR + 0;

					#KOLOM STOCKOPNAME
					if($listData['STOCK_OPNAME']==''){
						$sqlGetStock ="SELECT JUMLAH
								FROM m_trader_stockopname
								WHERE TANGGAL_STOCK='".$TGLSTOCK."'
								AND KODE_BARANG = '".$listData['KODE_BARANG']."' 
								AND JNS_BARANG = '".$listData['JNS_BARANG']."'
								AND KODE_TRADER = '".$kodeTrader."'";
						$STOCK=$this->db->query($sqlGetStock);
						if($STOCK->num_rows() > 0){
							$STOCK = $STOCK->row(); 
							$STOCK = $STOCK->JUMLAH;
						}else{
							$STOCK="";
						}
					}else{ 
						$STOCK=$listData['STOCK_OPNAME'];
					}
				}
			}
			#==========================================================================
			#KOLOM SELISIH
                $SELISIH = "";
                if ($listData['SELISIH'] == '') {
                    if ((is_array($STOCK) && empty($STOCK)) || strlen($STOCK) === 0) {
                        $SELISIH = "";
                    } else {
                        $SELISIH = ($STOCK - $SALDOAKHR) + $PENYESUAIAN;
                    }
                } else {
                    $SELISIH = $listData['SELISIH'];
                }

                if ($SELISIH) {
                    if (number_format($SELISIH, 2) == '-0.00') {
                        $SELISIH = 0;
                    }
                }

                if ($SALDOAKHR) {
                    if (number_format($SALDOAKHR, 2) == '-0.00') {
                        $SALDOAKHR = 0;
                    }
                }

                if ($SALDOAWL) {
                    if (number_format($SALDOAWL, 2) == '-0.00') {
                        $SALDOAWL = 0;
                    }
                }	
	?>
    <tr>
    	<td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['KODE_BARANG'];?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($SALDOAWL,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($MASUK,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($KELUAR,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($PENYESUAIAN,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($SALDOAKHR,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($STOCK,2,',','.');?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= number_format($SELISIH,2,',','.');?></td>
        <td nowrap="nowrap"><?=$listData['KETERANGAN']?$listData['KETERANGAN']:getKeterangan($SELISIH)?></td>
    </tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Nihil</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="pengrusakan"||$tipe=="pemusnahan"){
function content(){
	$content  ="<tr align=\"left\">";
	$content .="<th>No</th>";
	$content .="<th>Tanggal ".ucwords(strtolower($tipe))."</th>";
	$content .="<th>Kode Barang</th>";
	$content .="<th>Uraian Barang</th>";
	$content .="<th>Uraian Jenis</th>";
	$content .="<th>Jumlah</th>";
	$content .="<th>Uraian Satuan</th>";
	$content .="<th>Keterangan</th>";
	$content .="</tr>";
return $content;	
}	
?>
<table class="tabelPopUp" width="100%">
    <?
	echo content();
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		$no=1;
		foreach($resultData as $listData){
		if($no%21==0){
			echo content();	
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['TGL'];?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['KDBARANG'];?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['URAIBARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIJENIS'];?></td>
        <td nowrap="nowrap" class="fnumber"><?= $listData['JUMLAH'];?></td>
        <td nowrap="nowrap"><?= $listData['SATUAN'];?></td>
        <td nowrap="nowrap"><?= $listData['KETERANGAN'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="8">Nihil</td>
    </tr>
    <? }?>
</table>

<? }elseif($tipe=="barang"){
function content(){
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th>No</th>";
	$content .="<th>KODE PERUSAHAAN</th>";
	$content .="<th>KODE BARANG</th>";
	$content .="<th>NAMA BARANG</th>";
	$content .="<th>JENIS BARANG</th>";
	$content .="<th>KODE HS</th>";
	$content .="<th>MERK</th>";
	$content .="<th>TIPE</th>";
	$content .="<th>UKURAN</th>";
	$content .="<th>SATUAN TERBESAR</th>";
	$content .="<th>SATUAN TERKECIL</th>";
	$content .="<th>KONVERSI SATUAN</th>";
	$content .="<th>STOCK GUDANG</th>";
	$content .="</tr>";
	$content .="</thead>";
	return $content;	
}	
?>
<table class="tabelPopUp" width="100%" border="1">
    <?
	echo content();
    $banyakData=count($resultData);
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$jm=21;
		foreach($resultData as $listData){
		if(($no/$jm)==1){
			 $jm=$jm+20;
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap"><?= $listData['KODE_PARTNER'];?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['KODE_BARANG'];?></td>
        <td class="ftext" width="30%"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['JNS_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_HS'];?></td>
        <td nowrap="nowrap"><?= $listData['MERK'];?></td>
        <td nowrap="nowrap"><?= $listData['TIPE'];?></td>
        <td nowrap="nowrap"><?= $listData['UKURAN'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['KODE_SATUAN_TERKECIL'];?></td>
        <td nowrap="nowrap" align="center" class="fnumber"><?= $listData['JML_SATUAN_TERKECIL'];?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= $listData['STOCK_AKHIR'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Nihil</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="stock_opname"){ ?>
<table class="tabelPopUp" width="100%" border="0">
		<thead>
		<tr>    
	        <th>No</th>
	        <th>Kode Perusahaan</th>
	        <th>Kode Barang</th>
	        <th>Jenis Barang</th>
	        <th>Nama Barang</th>
	        <th>Kode Satuan</th>
	        <th>Jumlah Stock Opname</th>
	        <th>Kode Dok. Masuk</th>
	        <th>Nomor Dok. Masuk</th>
	        <th>Tanggal Dok. Masuk</th>
	        <th>Jumlah</th>
	        <th>Keterangan</th>
	    </tr>   
	    </thead>
		<tbody>
	    <? 	$banyakData=count($list['header']);
			if($banyakData>0){
				$no=1;
				$s=0;
				foreach($list['header'] as $data){
					$SQL = "SELECT IDHDR, JENIS_DOK_MASUK, NO_DOK_MASUK, TGL_DOK_MASUK, JUMLAH 
						    FROM M_TRADER_STOCKOPNAME_DTL WHERE IDHDR = '".$data['ID']."'
						    ORDER BY TGL_DOK_MASUK";
					$rs = $this->db->query($SQL);
					$s = $rs->num_rows();
					$rowspan="";
					if($s!=0) $rowspan =  'rowspan="'.$s.'"';
				?>
					<tr>    
		                <td align="center" <?=$rowspan;?>><?= $no;?></td>
		                <td <?=$rowspan;?>><?=$data["KODE_PARTNER"]?></td>
		                <td <?=$rowspan;?> class="ftext"><?=$data["KODE_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["JENIS_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["URAIAN_BARANG"]?></td>
		                <td <?=$rowspan;?>><?=$data["KODE_SATUAN"]?></td>
		                <td <?=$rowspan;?> class="fnumber"><?=$data["JUMLAH"]?></td>
				<?
					$no++;											
					if($rs->num_rows()>0){
						$x=1;
						foreach($rs->result_array() as $dataDetil){
							if($x>1){
								echo '<tr>';
								echo '<td>'.$dataDetil["JENIS_DOK_MASUK"].'</td>
									  <td class="ftext">'.$dataDetil["NO_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["TGL_DOK_MASUK"].'</td>
									  <td class="fnumber">'.$dataDetil["JUMLAH"].'</td>';
							}else{
								echo '<td>'.$dataDetil["JENIS_DOK_MASUK"].'</td>
									  <td class="ftext">'.$dataDetil["NO_DOK_MASUK"].'</td>
									  <td>'.$dataDetil["TGL_DOK_MASUK"].'</td>
									  <td class="fnumber">'.$dataDetil["JUMLAH"].'</td>
									  <td '.$rowspan.'>'.$data["KETERANGAN"].'</td>';
							}	  
							$x++;	  
						}	
					}else{
						echo '<td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <td>'.$data["KETERANGAN"].'</td>';
					}					
				}            
			}else{?>
	    <tr>
	    	<td colspan="12">Nihil</td>
	    </tr>
	    <? }?>
		
		</tbody>
	</table>
<? }elseif($tipe=="floating_uplift"){
		function content(){
			$content  ="<tr align=\"left\">
							<th>NAMA AIRLINES</th>
							<th>NO. PENERBANGAN</th>
							<th>TANGGAL BERANGKAT</th>
							<th>TANGGAL KEMBALI</th>
							<th>CATATAN</th>
						</tr>";
			return $content;	
		}	
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th></th>
							<th>No</th>
							<th>KODE AIRLINES</th>
							<th>KODE BARANG</th>
							<th>NAMA BARANG</th>
							<th>JENIS BARANG</th>
							<th>UNIT</th>
							<th>STANDAR UPLIFT QTY</th>
							<th>RETURN GOOD</th>
							<th>RETURN NG</th>
							<th>CONSUMPTION</th>
							<th>REMARKS</th>
						</tr>";
			return $content;	
		}	
?>
<table class="tabelPopUp" width="100%" ="1">
    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
			echo content();
	?>
  	<tr>
        <td nowrap="nowrap" width="10%"><?= $listData['NAMA_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['NO_FLIGHT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_BERANGKAT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_KEMBALI'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['CATATAN'];?></td>
   	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['UPLIFT_ID']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
        <td nowrap="nowrap"></td>
        <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['STANDAR_UPLIFT_QTY'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_BAIK'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_NG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KONSUMSI'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['REMARKS'];?></td>
   	</tr>
	<? $no++;}
	echo "<tr><td colspan='12'>&nbsp;</td></tr>";
	}}} else{?>
    <tr>
    	<td colspan="12">Nihil</td>
    </tr>
    <? }?>
</table>
<? }elseif($tipe=="floating_pdf"){
		
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th>No</th>
							<th width=\"30%\">ITEMS</th>
							<th width=\"10%\">UNIT</th>
							<th width=\"15%\">STANDAR UPLIFT</th>
							<th width=\"15%\">RETURN GOOD</th>
							<th width=\"15%\">RETURN NG</th>
							<th>CONSUMPTION</th>
						</tr>";
			return $content;	
		}
		
		function contentBawah(){
			$content  ="<tr align=\"left\">
							<th colspan=\"2\">Delivered By :<br/><br/><br/><br/>(Operation Handling) </th>
							<th colspan=\"3\">Received By :<br/><br/><br/><br/>(Inflight Crew) </th>
							<th colspan=\"2\">Acknowledged By :<br/><br/><br/><br/>(Costums) </th>
						</tr>";
			return $content;	
		}
?>

    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
	?>
	<table class="tabelPopUp" width="100%" border="0">
	<tr>
		<th align="left">TO </th><th align="left">: IGC</th><th rowspan="2" colspan="3" style="font-size:20px;border:0;border-width:0" nowrap="nowrap"><?= $listData['NAMA_PARTNER'];?></th>
	</tr>
	<tr>
		<th align="left">FROM </th><th align="left">: OPERATION</th>
	</tr>
  	<tr>
		<th align="left">DATE</th> <th align="left">: <?= $listData['TGL_BERANGKAT'];?></th><th colspan="3"></th><th>JAL-<?= $listData['NO_FLIGHT'];?></th>
	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['UPLIFT_ID']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
       <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
       <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
       <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['STANDAR_UPLIFT_QTY'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_BAIK'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['SISA_NG'];?></td>
       <td nowrap="nowrap" align="center"><?= $listDataDetil['KONSUMSI'];?></td>
   	</tr>
	<? $no++;
	}
	echo "<tr><td colspan='7'>&nbsp;</td></tr>";
	echo contentBawah();
	?>
	<tr>
		<th align="left" colspan="2">Nama &nbsp;:&nbsp;<?= $bawah['DELIVERED_NAME']; ?></th>
		<th align="left" colspan="3">Nama &nbsp;:</th>
		<th align="left" colspan="2">Nama &nbsp;:</th>
	</tr>
	<tr>
		<th align="left" colspan="2">Nopeg :&nbsp;<?= $bawah['DELIVERED_NO']; ?></th>
		<th align="left" colspan="3">Nopeg :</th>
		<th align="left" colspan="2">Nopeg :</th>
	</tr>
	
	<?
	echo "<tr><td colspan='7'>&nbsp;</td></tr>";
	echo "</table><br/>";
	echo "<pagebreak>";
	}}
	//echo "</table><br/>";
	
	} else{?>
    <tr>
    	<td colspan="7">Nihil</td>
    </tr>
	</table>
    <? }?>
<? }elseif($tipe=="floating_store"){
		function content(){
			$content  ="<tr align=\"left\">
							<th>NOMOR PERMOHONAN</th>
							<th>TANGGAL PERMOHONAN</th>
							<th>NAMA AIRLINES</th>
							<th>JUMLAH ITEM BARANG</th>
							<th>STATUS</th>
						</tr>";
			return $content;	
		}	
		function contentDetil(){
			$content  ="<tr align=\"left\">
							<th></th>
							<th>No</th>
							<th>KODE BARANG</th>
							<th>NAMA BARANG</th>
							<th>JENIS BARANG</th>
							<th>UNIT</th>
							<th>SATUAN</th>
							<th>JUMLAH</th>
						</tr>";
			return $content;	
		}	
?>
<table class="tabelPopUp" width="100%" border="1">
    <?
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
			echo content();
	?>
  	<tr>
        <td nowrap="nowrap" width="10%"><?= $listData['NO_SRR'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['TGL_SRR'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['NAMA_PARTNER'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['JML_BARANG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listData['STATUS'];?></td>
   	</tr>
    <? 
	echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		$jm=21;
		foreach($resultDataDetil[$listData['NO_SRR']] as $listDataDetil){
			
			if(($no/$jm)==1){
				echo contentDetil();
				 $jm=$jm+20;
			}
	?>
	<tr>
        <td nowrap="nowrap"></td>
        <td nowrap="nowrap" width="3%" align="center"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SATUAN'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['JUMLAH'];?></td>
   	</tr>
	<? $no++;}
	echo "<tr><td colspan='12'>&nbsp;</td></tr>";
	}}} else{?>
    <tr>
    	<td colspan="12">Nihil</td>
    </tr>
    <? }?>
</table>
<? }elseif($tipe=="store_pdf"){
    $banyakData=count($resultData);//echo $banyakData;
	if($banyakData>0){
		foreach($resultData as $listData){
	?>
	<div id="myDIV"><strong>DATE : <?= $resultData[0]['TGL_SRR'];?></strong></div>
	<table class="tabelPopUp" width="100%" border="0">
	<tr>
		<th>No</th>
		<th>KODE BARANG</th>
		<th>NAMA BARANG</th>
		<th>SIZE</th>
		<th>UNIT</th>
	</tr>
	<?
	//echo contentDetil();
    $banyakDataDetil=count($resultDataDetil);
	if($banyakDataDetil>0){
		$no=1;
		foreach($resultDataDetil[$listData['NO_SRR']] as $listDataDetil){
	?>
	<tr>
        <td nowrap="nowrap"  align="center" width="3%"><?= $no;?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listDataDetil['NAMA_BARANG'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['UNIT'];?></td>
        <td nowrap="nowrap" align="center"><?= $listDataDetil['SATUAN'];?></td>
   	</tr>
	<? $no++;
	}
	echo "";
	?>
</table>
<tr><td>&nbsp;</td></tr>
<table>
	<tr>
		<th nowrap="nowrap" width="8%">Requested By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Acknowledged By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Delivered By :<br/><br/><br/><br/><br/><br/></th>
		<th nowrap="nowrap" width="8%">Received By :<br/><br/><br/><br/><br/><br/></th>
	</tr>
	<tr>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?= $bawah['DELIVERED_NAME']; ?> </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
		<th nowrap="nowrap" align="left" width="8%" class="border-l">Name &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; </th>
	</tr>
	<tr>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :&nbsp;<?= $bawah['DELIVERED_NO']; ?></th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
		<th nowrap="nowrap" align="left" width="8%">Reg. No. :</th>
	</tr>
	<?
	echo "</table><br/>";
	echo "<pagebreak>";
	}}} else{?>
    <tr>
    	<td colspan="5">Nihil</td>
    </tr>
	</table>
    <? }?>
<? }elseif($tipe=="produksi"){	
function content(){
	$content  ="<thead>";
	$content .="<tr align=\"left\">";
	$content .="<th width=\"10px\">No</th>";
	$content .="<th>KODE BARANG</th>";
	$content .="<th>NAMA BARANG</th>";
	$content .="<th>JENIS BARANG</th>";
	$content .="<th>JUMLAH</th>";
	$content .="<th>SATUAN</th>";
	$content .="<th>TANGGAL</th>";
	$content .="</tr>";
	$content .="</thead>";
	return $content;	
}	
?>
<table class="tabelPopUp" width="100%" border="0">
    <?
	echo content();
    $banyakData=count($resultData);
	echo "<tbody>";
	if($banyakData>0){
		$no=1;
		$jm=21;
		foreach($resultData as $listData){
		if(($no/$jm)==1){
			 $jm=$jm+20;
		}
	?>
  	<tr>
        <td nowrap="nowrap" align="center"><?= $no;?></td>
        <td nowrap="nowrap" class="ftext"><?= $listData['KODE_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['URAIAN_BARANG'];?></td>
        <td nowrap="nowrap"><?= $listData['JNS_BARANG'];?></td>
        <td nowrap="nowrap" align="right" class="fnumber"><?= $listData['JUMLAH'];?></td>
        <td nowrap="nowrap"><?= $listData['KODE_SATUAN'];?></td>
        <td nowrap="nowrap"><?= $listData['TANGGAL'];?></td>
   	</tr>
    <? $no++;}}else{?>
    <tr>
    	<td colspan="12" align="center">Nihil</td>
    </tr>
    <? }?>
    </tbody>
</table>
<? }elseif($tipe=="inout"){
	$no=1;		
?>  
 
<table class="tabelPopUp" width="100%" border="0">
	<thead>
	<tr>
    	<th rowspan="2" align="center">No</th>
        <th colspan="10">Dokumen <?=($jenis=="MASUK")?"Pemasukan":"Pengeluaran"?></th>
    </tr>    
    <tr>    
        <th>Jenis</th>
        <th>No. Dokumen</th>
        <th>Tgl. Dokumen</th>
        <th>Tgl. <?=ucwords(strtolower($jenis))?></th>
        <th>Kode Barang</th>
        <th>Seri Barang</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Jumlah</th>
        <th>Nilai Pabean</th>
    </tr>   
    </thead>
    <tbody>
    <? $banyakData=count($resultData);	
	   if($banyakData>0){
		$no=1; 	
		foreach($resultData as $rowmasuk){ 	
	?>
			<tr>    
				<td align="center"><?= $no;?></td>
				<td><?=$rowmasuk["JENIS_DOK"]?></td>
				<td class="ftext"><?=$rowmasuk["NO_DOK"]?></td>
				<td><?=$this->fungsi->dateFormat($rowmasuk["TGL_DOK"])?></td>
				<td><?=$this->fungsi->dateFormat($rowmasuk["TGL_MASUK"])?></td>
				<td class="ftext"><?=$rowmasuk["KODE_BARANG"]?></td>
				<td><?=$rowmasuk["SERI_BARANG"]?></td>
				<td><?=$rowmasuk["NAMA_BARANG"]?></td>
				<td><?=$rowmasuk["SATUAN"]?></td>
				<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($rowmasuk["JUMLAH"],2)?></td>
				<td align="right" class="fnumber"><?=$this->fungsi->FormatRupiah($rowmasuk["NILAI_PABEAN"],2)?></td>
			</tr>    
    <? $no++; } ?>                  	 
    </tbody>
	<? }else{?>
    <tr>
    	<td colspan="23" align="center">Nihil</td>
    </tr>
    <? }?>
</table>
<? } ?>



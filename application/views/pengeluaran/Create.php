<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR)
?>

<div class="header">
	<h3>Pilihan Pengeluaran</h3>
</div>
<div class="content">
	<table width="100%">
        <tbody>
    	<tr>
        	<td>
                <div class="stat-data">
                    <a href="javascript:void(0)" onClick="shortcut('bc282')" title="BC 2.8.2">
                        <div class="stat-blue  col-md-2">
                            <i class="fa fa-file-text-o fa-2x"></i>
                            <h2>BC 2.8.2</h2>
                        </div>
                    </a>
                    <a href="javascript:void(0)" onClick="shortcut('bc27')" title="BC 2.7">
                        <div class="stat-green  col-md-2" style="margin-left:1%">
                            <i class="fa fa-file-text-o fa-2x"></i>
                            <h2>BC 2.7</h2>
                        </div>
                    </a>
                    <a href="javascript:void(0)" onClick="shortcut('bc30')" title="BC 3.0">
                        <div class="stat-orange  col-md-2" style="margin-left:1%">
                            <i class="fa fa-file-text-o fa-2x"></i>
                            <h2>BC 3.0</h2>
                        </div>
                    </a>
                    <a href="javascript:void(0)" onClick="shortcut('bc41')" title="BC 4.1">
                        <div class="stat-red  col-md-2" style="margin-left:1%">
                            <i class="fa fa-file-text-o fa-2x"></i>
                            <h2>BC 4.1</h2>
                        </div>
                    </a>
                    <a href="javascript:void(0)" onClick="short_ppb('ppb')" title="PPB">
                        <div class="stat-info  col-md-2" style="margin-left:1%">
                            <i class="fa fa-file-text-o fa-2x"></i>
                            <h2>PPB</h2>
                        </div>
                    </a>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<script>
function shortcut(tipe){
	location.href = site_url+"/pengeluaran/create/"+tipe;
}
function short_ppb(tipe){
	location.href = site_url+"/pengeluaran/set_ppb/";
}
</script> 

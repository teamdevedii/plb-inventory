<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<span id="fdokumen_form">
<?php if(!$list){?>
<span id="DivHeaderForm">
<form id="fdokumen_" action="<?= site_url()."/pengeluaran/dokumen/bc41"; ?>" method="post" autocomplete="off" list="<?= site_url()."/pengeluaran/detil/dokumen/bc41" ?>">
<input type="hidden" name="act" value="<?= $act; ?>" />
<input type="hidden" name="kode_dok" id="kode_dok" value="<?= $kode_dok; ?>" />
<input type="hidden" name="NOMOR" id="NOMOR" value="<?= $sess['NOMOR']; ?>" />
<input type="hidden" name="NOMOR_AJU" id="NOMOR_AJU" value="<?= $aju; ?>" />
 <h5 class="header smaller lighter green"><b>DETIL DOKUMEN</b></h5>
<table>
<tr>
    <td width="145">Jenis Dokumen</td>
    <td>
	<? if($act=="update") echo form_dropdown('KODE_DOKUMEN', $kode_dokumen, $sess['KODE_DOKUMEN'], 'value="<?= $kode_dokumen;?>" id="kode_dokumen" class="text" wajib="yes"');
	 else echo form_dropdown('KODE_DOKUMEN', $kode_dokumen, $sess['KODE_DOKUMEN'], 'value="<?= $kode_dokumen;?>" id="kode_dokumen"  class="text" wajib="yes"');?>
    </td>
</tr>
<tr>
    <td>Nomor Dokumen</td>
    <td><input type="text" name="DOKUMEN[NOMOR]" id="NOMOR" class="text" value="<?= $sess['NOMOR']; ?>" maxlength="30" wajib="yes"/></td>
</tr>
<tr>
    <td>Tanggal*</td>
    <td><input type="text" name="DOKUMEN[TANGGAL]" id="TANGGAL" onfocus="ShowDP('TANGGAL')" value="<?= $sess['TANGGAL']; ?>" class="stext date" wajib="yes"/>&nbsp; YYYY-MM-DD</td>
</tr>
<tr id="tralasan_dok" style="display:none;">
    <td>Alasan Revisi</td>
    <td>
        <textarea id="ALASAN" class="mtext" wajib="yes" name="ALASAN"></textarea>
    </td>
</tr>
<tr style="display:none;">
    <td colspan="2" id="tdrevisi_dok"></td>
</tr> 
<tr>
 	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="3">
  <a href="javascript:void(0);" class="btn btn-success" id="ok_" onclick="save_detil('#fdokumen_','msgdokumen_');"><span><i class="fa fa-save"></i>&nbsp;<?= $act; ?>&nbsp;</span></a>&nbsp;<a href="javascript:;" class="btn btn-warning" id="cancel_" onclick="cancel('fdokumen_');"><span><i class="icon-undo"></i>&nbsp;Reset&nbsp;</span></a><span class="msgdokumen_" style="margin-left:20px">&nbsp;</span>
	</td>
</tr>	        
</table>
</form>
</span>
<?php } ?>
</span>
<?php if(!$edit){ ?>
<?php if($flagrevisi) { ?>
<input type="hidden" id="revisi_dok" name="revisi" value="1" />
<?php } ?>
<div id="fdokumen_list" style="margin-top:10px"><?= $list ?></div>
<?php } ?>
<script>
$(function(){FormReady();})
</script>
<script type="text/javascript">
    $(document).ready(function(){
        if ($('#revisi_dok').length > 0) {
            $('#tralasan_dok').show();
            $('#tdrevisi_dok').html('<input type="text" name="flagrevisi" value="1" />');
        }
    });
</script>

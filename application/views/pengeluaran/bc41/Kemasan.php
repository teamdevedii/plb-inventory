<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<span id="fkemasan_form">
<?php if(!$list){
if($act=='update'){ 
	$readonly="";
	//$readonly="readonly=readonly";
	$autocomplete="onfocus=Autocomp(this.id)";
}else{
	$readonly="";
	$autocomplete="onfocus=Autocomp(this.id)";
}
?>
<span id="DivHeaderForm">
<form id="fkemasan_" action="<?= site_url()."/pengeluaran/kemasan/bc41"; ?>" method="post" autocomplete="off" list="<?= site_url()."/pengeluaran/detil/kemasan/bc41" ?>">
<input type="hidden" name="act" value="<?= $act; ?>" />
<input type="hidden" name="kode_kemas" id="kode_kemas" value="<?= $kode_kemas; ?>" />
<input type="hidden" name="NOMOR_AJU" id="NOMOR_AJU" value="<?= $aju; ?>" />
 <h5 class="header smaller lighter green"><b>DETIL KEMASAN</b></h5>
<table width="100%">
    <tr>
        <td width="145">Merk </td>
        <td><input type="text" name="KEMASAN[MERK_KEMASAN]" id="MERK_KEMASAN" class="text" value="<?= $sess['MERK_KEMASAN']; ?>" wajib="yes" maxlength="30"/></td>
    </tr>
    <tr>
        <td>Jumlah </td>
        <td><input type="text" name="JUM_KEMAS" id="JUM_KEMAS" value="<?=$sess['JUMLAH']; ?>" onkeyup="this.value = ThausandSeperator('JUMLAH',this.value,4);" class="text" format="angka" wajib="yes"><input type="hidden" name="KEMASAN[JUMLAH]" id="JUMLAH" class="text" value="<?= $sess['JUMLAH']; ?>" maxlength="18"/>
        </td>
    </tr>
    <tr>
        <td>Jenis Kemasan </td>
            <td><input type="text" name="KEMASAN[KODE_KEMASAN]" id="KODE_KEMASAN" wajib="yes" <?= $readonly;?> url="<?= site_url(); ?>/autocomplete/kemasan" class="stext date" value="<?= $sess['KODE_KEMASAN']; ?>" urai="urjenis_kemasan;" <?= $autocomplete;?> />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kemasan','KODE_KEMASAN;urjenis_kemasan','Kode Kemasan',this.form.id,650,400)" value="...">&nbsp;
            <span id="urjenis_kemasan" class="uraian"><?= $sess['URAIAN_KEMASAN']==''?$URAIAN_KEMASAN:$sess['URAIAN_KEMASAN']; ?></span>
            </td>
    </tr>
    <tr id="tralasan_kms" style="display:none;">
        <td>Alasan Revisi</td>
        <td>
            <textarea id="ALASAN" class="mtext" wajib="yes" name="ALASAN"></textarea>
        </td>
    </tr>
    <tr style="display:none;">
        <td colspan="2" id="tdrevisi_kms"></td>
    </tr>
<tr>
 	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="3">
  <a href="javascript:void(0);" class="btn btn-success" id="ok_" onclick="save_detil('#fkemasan_','msgkemasan_');"><span><i class="fa fa-save"></i>&nbsp;<?= $act; ?>&nbsp;</span></a>&nbsp;<a href="javascript:;" class="btn btn-warning" id="cancel_" onclick="cancel('fkemasan_');"><span><i class="icon-undo"></i>&nbsp;Reset&nbsp;</span></a>&nbsp;<span class="msgkemasan_" style="margin-left:20px">&nbsp;</span>
	</td>
</tr>	        
</table>
</form>
</span>
<?php } ?>
</span>
<?php if(!$edit){ ?>
<?php if($flagrevisi) { ?>
<input type="hidden" id="revisi_kms" name="revisi" value="1" />
<?php } ?>
<div id="fkemasan_list" style="margin-top:10px"><?= $list ?></div>
<?php } ?>
<script>
$(function(){FormReady();})
</script>
<script type="text/javascript">
    $(document).ready(function(){
        if ($('#revisi_kms').length > 0) {
            $('#tralasan_kms').show();
            $('#tdrevisi_kms').html('<input type="text" name="flagrevisi" value="1" />');
        }
    });
</script>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>	
<table width="100%" border="0">
<tr><td width="45%" valign="top">						
<table width="100%" border="0">
	<tr>
 	<td colspan="2"><a href="javascript:void(0);" class="button next" onclick="print_('<?= base_url();?>index.php/pengeluaran/print_dok/bc41','<?= $aju;?>');"><span><span class="icon"></span>&nbsp;<?= "CETAK DOKUMEN"; ?>&nbsp;</span></a></td>
</tr>
<tr>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>-</td>
    <td>Nomor Aju </td>
    <td><b><?= $this->fungsi->FormatAju($aju); ?></b></td>
</tr>
    <tr>
        <td width="24">A.&nbsp;</td>
        <td width="139">Kantor Pabean</td>
        <td width="305"><?= $DATA['KODE_KPBC'];?></td>
    </tr>
    <tr>
        <td>B.&nbsp;</td>
        <td>Jenis TPB</td>
        <td><?= $DATA['JENIS_TPB'];?></td>
    </tr>
    <tr>
        <td>C.&nbsp;</td>
        <td>Tujuan Pengiriman</td>
        <td><?= $DATA['TUJUAN_KIRIM'];?></td>
    </tr>
</table>
</td><td width="55%">
	<table width="100%">
    <tr><td height="22px"></td></tr>
    <tr>
    <td>
	<fieldset>
    <legend><i>Informasi Bea dan Cukai</i></legend>
    <table width="80%">
        <tr>
            <td width="45%">Nomor Pendaftaran *</td>
            <td>:</td>
            <td><?= $DATA['NOMOR_PENDAFTARAN']; ?></td>
        </tr>
        <tr>
            <td>Tanggal Pendaftaran *</td>
            <td>:</td>
            <td><?= $DATA['TANGGAL_PENDAFTARAN']; ?></td>
        </tr>
        <tr>
            <td>Nomor Dokumen Pabean</td>
            <td>:</td>
            <td><?= $DATA['NOMOR_DOK_PABEAN']; ?></td>
        </tr>
        <tr>
            <td>Tanggal Pendaftaran</td>
            <td>:</td>
            <td><?= $DATA['TANGGAL_DOK_PABEAN']; ?></td>
        </tr>
    </table>
</fieldset>
	</td>
    </tr>
</table>

</td></tr></table>
<div class="judul">D. Data Pemberitahuan</div>
<table width="100%" border="0">
<tr>
	<td width="45%" valign="top">		
        <table width="100%" border="0">
        <tr>
        	<td colspan="3" class="rowheight"><b>Pengusaha TPB</b></td>
        </tr>
		<tr>
            <td>1.&nbsp;</td>
            <td>Identitas</td>
            <td><?= $DATA['KODE_ID_TRADER'].' - '.$DATA['URID_TRADER'].' = '.$this->fungsi->FormatNPWP($DATA['ID_TRADER']);?></td>
        </tr> 
		<tr>
        	<td>2.&nbsp;</td>
			<td class="top">Nama </td>
			<td><?= $DATA['NAMA_TRADER'];?></td>
		</tr>
		<tr>
        	<td>3.&nbsp;</td>
			<td>Alamat </td>
			<td><?= $DATA['ALAMAT_TRADER'];?></td>
		</tr>
        <tr>
        	<td>4.&nbsp;</td>
			<td class="top">No Ijin TPB </td>
			<td><?= $DATA['NOMOR_IZIN_TPB'];?></td>
		</tr>
        <!--<tr>
			<td colspan="3" class="rowheight"><h5>Riwayat Barang</h5></td>
		</tr>
        <tr>
        	<td>12.&nbsp;</td>
			<td>Nomor BC 4.0 Asal</td>
			<td><input type="text" name="HEADERX[NOMOR_BC]" id="nomor_bc" value="<?$sessX['NOMOR_BC']; ?>"  maxlength="20"class="mtext" /></td>
		</tr>
        <tr>
        	<td>&nbsp;</td>
			<td>Tanggal BC 4.0 Asal</td>
			<td><input type="text" name="HEADERX[TANGGAL_BC]" id="tanggal_bc" value="<?$sessX['TANGGAL_BC']; ?>"  maxlength="20"class="sstext" onfocus="ShowDP('tanggal_bc');" /></td>
		</tr>-->
        <tr>
			<td colspan="3" class="rowheight"><h5>Data Pengangkutan</h5></td>
		</tr>
        <tr>
        	<td>13.&nbsp;</td>
			<td>Nama Angkut </td>
			<td><?= $DATA['JENIS_SARANA_ANGKUT'];?></td>
		</tr>
		<tr>
        	<td>14.&nbsp;</td>
			<td>Nomor Polisi </td>
			<td><?= $DATA['NOMOR_POLISI'];?></td>
		</tr>
        <tr>
			<td colspan="3" class="rowheight"><h5>Data Perdagangan</h5></td>
		</tr>
		<tr>
        	<td width="24">15.&nbsp;</td>
			<td width="139">Harga Penyerahan(Rp) </td>
			<td><?= $DATA['HARGA_PENYERAHAN'];?></td>
		</tr>
        <tr>
			<td colspan="3" class="rowheight"><h5>Data Barang</h5></td>
		</tr>
		<tr>
        	<td width="24">19.&nbsp;</td>
			<td width="139">Volume(m3) </td>
			<td><?= $DATA['VOLUME'];?></td>
		</tr>
        <tr>
        	<td width="24">20.&nbsp;</td>
			<td width="139">Berat Kotor(Kg) </td>
			<td><?= $this->fungsi->FormatRupiah($DATA['BRUTO'],4);?></td>
		</tr>
        <tr>
        	<td width="24">21.&nbsp;</td>
			<td width="139">Berat Bersih(Kg) </td>
			<td><?= $this->fungsi->FormatRupiah($DATA['NETTO'],4);?></td>
		</tr>
		</table>
</td>
<td width="55%" valign="top">
		<table width="100%" border="0"> 
        <tr>
			<td colspan="3" class="rowheight"><b>Penerima Barang</b></td>
		</tr>
		<tr>
            <td width="4%">5.&nbsp;</td>
            <td width="24%">Identitas</td>
            <td width="72%"><?= $DATA['KODE_ID_PENERIMA'].' - '.$DATA['URID_PENERIMA'].' = '.$this->fungsi->FormatNPWP($DATA['ID_PENERIMA']);?></td>
        </tr>
		<tr>
        	<td>6.&nbsp;</td>
			<td>Nama</td>
			<td><?= $DATA['NAMA_PENERIMA'];?></td>
		</tr>
        <tr>
        	<td>7.&nbsp;</td>
			<td class="top">Alamat</td>
			<td><?= $DATA['ALAMAT_PENERIMA'];?></td>
		</tr>
        <tr>
			<td colspan="3" class="rowheight"><b>Tanda Tangan Pengusaha TPB</b></td>
		</tr>
		<tr>
			<td colspan="2">Nama</td>
			<td><?= $DATA['NAMA_TTD'];?></td>
		</tr>
		<tr>
			<td colspan="2">Tempat</td>
			<td><?= $DATA['KOTA_TTD'];?></td>
		</tr>
		<tr>
			<td colspan="2">Tanggal</td>
			<td><?= $DATA['TANGGAL_TTD'];?></td>
		</tr>
</table>

</td>
</tr>
<tr>
 	<td colspan="2">&nbsp;</td>
</tr>
</table>

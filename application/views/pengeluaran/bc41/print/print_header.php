<style>
.tablePrint{
	font-size:12px;
	width:190;
	height:280;
	border:solid 0.22 mm;
}
.left{
	border-left:solid 0.22 mm;	
}
.top{
	border-top::solid 0.22 mm;	
}
.bottom{
	border-bottom:solid 0.22 mm;	
}
.right{
	border-right:solid 0.22 mm;	
}
.font1{
	text-align:center;
	font-size:14px;
	width:750px;
}
.font2{
	font-size:14px;
}
.font3{
	font-size:12px;
}
.font4{
	font-size:10px;
}
.alignLeft{
	text-align:left;
}
.alignRight{
	text-align:right;
}
.alignCenter{
	text-align:center;
}
.marginLeft{
	margin-left:5px;	
}
</style>
<table class="tablePrint">
	<tr>
    	<td class="bottom right" style="width:50px;"><h2>BC 4.0</h2></td>
        <td class="font1 bottom" colspan="3">PEMBERITAHUAN PEMASUKAN BARANG ASAL TEMPAT LAIN DALAM DAERAH PABEAN KE<br>TEMPAT PENIMBUNAN BERIKAT</td>
    </tr>
    <tr>
    	<td colspan="4" class="font2 bottom">HEADER</td>    	
    </tr>
    <tr>
    	<td colspan="4" class="font4 alignRight">Halaman 1 dari 1</td>    	
    </tr>
    <tr>
    	<td colspan="2">
        	<table>
            	<tr>
                	<td>NOMOR PENGAJUAN</td>
                    <td>:</td>
                    <td><?= $sess['NOMOR_AJU']?></td>
                </tr>
                <tr>
                	<td>A.&nbsp;&nbsp;KANTOR PABEAN</td>
                    <td>:</td>
                    <td><?= $sess['URAIAN_KPBC']?></td>
                </tr>
                <tr>
                	<td>B.&nbsp;&nbsp;JENIS TPB</td>
                    <td>:</td>
                    <td><?= $sess['JENIS']?></td>
                </tr>
                <tr>
                	<td>C.&nbsp;&nbsp;TUJUAN PENGIRIMAN</td>
                    <td>:</td>
                    <td><?= $sess['TUJUAN']?></td>
                </tr>
            </table>
        </td>
        <td colspan="2" style="border-top:solid 0.22 mm;border-left:solid 0.22 mm">
        	<table>
                <tr>
                	<td colspan="3" class="top">F.&nbsp;&nbsp;KOLOM KHUSUS BEA DAN CUKAI</td>
                </tr>
                <tr>
                	<td>&nbsp;&nbsp;&nbsp;&nbsp;Nomor Pendaftaran</td>
                    <td>:</td>
                    <td><?= ($sess['NOMOR_BC'])?$sess['NOMOR_BC']:'-'?></td>
                </tr>
                <tr>
                	<td>&nbsp;&nbsp;&nbsp;&nbsp;Tanggal</td>
                    <td>:</td>
                    <td><?= ($sess['TANGGAL_BC']!='0000-00-00')?$this->fungsi->FormatDateLengkap($sess['TANGGAL_BC']):'-'; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="4" class="bottom" style="border-top:solid 0.22 mm;">D.DATA PEMBERITAHUAN</td>
    </tr>
    <tr>
    	<td colspan="2">
        	<table>
            	<tr class="bottom">
                	<td colspan="3">PENGUSAHA TPB</td>
                </tr>
                <tr>
                	<td>1.&nbsp;&nbsp;NPWP</td>
                    <td>:</td>
                    <td><?= $this->fungsi->FormatNPWP($sess['ID_PARTNER'])?></td>
                </tr>
                <tr>
                	<td>2.&nbsp;&nbsp;Nama</td>
                    <td>:</td>
                    <td><?= $sess['NAMA_PARTNER']?></td>
                </tr>
                <tr>
                	<td>3.&nbsp;&nbsp;Alamat</td>
                    <td>:</td>
                    <td><?= $sess['ALAMAT_PARTNER']?></td>
                </tr>
                <tr>
                	<td>4.&nbsp;&nbsp;No Izin TPB</td>
                    <td>:</td>
                    <td><?= $sess['REGISTRASI']?></td>
                </tr>
            </table>
        </td>
        <td colspan="2"  valign="top" class="left">
        	<table>
            	<tr>
                	<td class="bottom">PENGIRIM BARANG</td>
                    <td colspan="2" class="bottom" width="250px">&nbsp;</td>
                </tr>
                
                <tr>
                	<td>5.&nbsp;&nbsp;NPWP/KTP/Passport/Lainnya</td>
                    <td>:</td>
                    <td><?= $this->fungsi->FormatNPWP($sess['ID_TRADER'])?></td>
                </tr>
                <tr>
                	<td>6.&nbsp;&nbsp;Nama</td>
                    <td>:</td>
                    <td><?= $sess['NAMA_TRADER']?></td>
                </tr>
                <tr>
                	<td>7.&nbsp;&nbsp;Alamat</td>
                    <td>:</td>
                    <td><?= $sess['ALAMAT_TRADER']?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="bottom" style="border-top:solid 0.22 mm;">DOKUMEN PELENGKAP PABEAN</td>
    </tr>
    <tr>
    	<td colspan="2" valign="top">
        
        	<table>
                <tr>
                	<td>8.&nbsp;&nbsp;Packing List</td>
                    <td>:</td>
                    <td>&nbsp;...</td>
                </tr>
                <tr>
                	<td>9.&nbsp;&nbsp;Kontrak</td>
                    <td>:</td>
                    <td>&nbsp;...</td>
                </tr>
            </table>
        </td>
        <td colspan="2">
        	<table>
                <tr>
                	<td colspan="3">10.&nbsp;&nbsp;Surat Keputusan Persetujuan</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...</td>
                </tr>
                <tr>
                	<td colspan="3">11.&nbsp;&nbsp;Jenis/nomor/tanggal dokumen lainnya</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="bottom" style="border-top:solid 0.22 mm;">DATA PENGANGKUTAN</td>
    </tr>
    <tr>
    	<td colspan="2">
        	<table>
                <tr>
                	<td>12.&nbsp;&nbsp;Jenis Sarana Pengakut Darat</td>
                    <td>:</td>
                    <td><?= $sess['NAMA_ANGKUT']?></td>
                </tr>
            </table>
        </td>
        <td colspan="2">
        	<table>
                <tr>
                	<td>13.&nbsp;&nbsp;Nomor Polisi</td>
                    <td>:</td>
                    <td><?= $sess['NOMOR_ANGKUT']?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="bottom" style="border-top:solid 0.22 mm;">DATA PERDAGANGAN</td>
    </tr>
    <tr>
        <td colspan="2">
        	<table>
                <tr>
                	<td>14.&nbsp;&nbsp;Harga Penyerahan(Rp)</td>
                    <td>:</td>
                    <td><?= $this->fungsi->FormatRupiah($sess['HARGA'])?></td>
                </tr>
            </table>
        </td>
        <td colspan="2">
        	<table>
            	<tr>
                	<td colspan="3">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="bottom" style="border-top:solid 0.22 mm;">DATA PENGEMAS</td>
    </tr>
    <tr>
        <td colspan="2">
        	<table>
            	
                <tr>
                	<td>15.&nbsp;&nbsp;Jenis Kemasan</td>
                    <td>:</td>
                    <td>&nbsp;...</td>
                </tr>
                <tr>
                	<td>16.&nbsp;&nbsp;Merek Kemasan</td>
                    <td>:</td>
                    <td>&nbsp;...</td>
                </tr>
            </table>
        </td>
        <td colspan="2">
        	<table>
                <tr>
                	<td>17.&nbsp;&nbsp;Jumlah Kemasan</td>
                    <td>:</td>
                    <td>&nbsp;...</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="4" class="bottom">DATA BARANG</td>
    </tr>
    <tr>
    	<td colspan="4">
        	<table>
            	<tr>
                	<td>18.&nbsp;&nbsp;Volume(m3) : <?= $sess['VOLUME']?> </td>
                    <td>19.&nbsp;&nbsp;Berat Kotor(Kg) : <?= $sess['BRUTO']?> </td>
                    <td>20.&nbsp;&nbsp;Berat Bersih(Kg) : <?= $sess['NETTO']?> </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td height="30px" valign="top" style="border-top:solid 0.22 mm;border-bottom:solid 0.22 mm;">
        	<table>
            	<tr>
                	<td>21</td>
                </tr>
                <tr>
                	<td>No</td>
                </tr>
            </table>
        </td>
        <td height="30px" valign="top" class="left" style="border-top:solid 0.22 mm;border-bottom:solid 0.22 mm;">
        	<table>
            	<tr>
                	<td>22</td>
                </tr>
                <tr>
                	<td>Uraian jumlah dan jenis barang secara lengkap, Kode Barang merk. tipe, ukuran, dan spesifikasi lain</td>
                </tr>
            </table>
        </td>
        <td height="30px" valign="top" class="left" style="border-top:solid 0.22 mm;border-bottom:solid 0.22 mm;">
        	<table>
            	<tr>
                	<td>23</td>
                </tr>
                <tr>
                	<td>
                    	<table>
                        	<tr>
                            	<td>-Jumlah & Jenis Satuan</td>
                            </tr>
                            <tr>
                            	<td>-Berat Bersih (Kg)</td>
                            </tr>
                            <tr>
                            	<td>-Volume (m3)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td height="30px" valign="top" class="left" style="border-top:solid 0.22 mm;border-bottom:solid 0.22 mm;">
        	<table>
            	<tr>
                	<td>24</td>
                </tr>
                <tr>
                	<td>Harga Penyerahan (Rp)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td valign="top" height="150px;"></td>
        <td class="left" valign="top">
            <!--2,502 PU
            + 28 ( TWO THOUSAND FIVE HUNDRED TWO TRAYS
            AND TWENTY EIGHT ) OF
            1 FCL, 2,502 TRAYS OF CANNED PINEAPPLE
            CHOICE SLICES IN HEAVY SYRUP EO
            12/20 OZ "COOP" BRAND
            ADDITIONAL CHARGES OF SLIPSHEET LOADING = 28
            PIECES-->
	</td>
        <td class="left" valign="top">
        	<!--jumlah : 10000 Ton<br>
            berat bersih : 4500 Kg<br>
            volume : 100 m3 -->           
        </td>
        <td class="left" valign="top">
        	<!--1.000.000-->
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-top:solid 0.22 mm;" class="top">G.&nbsp;UNTUK PEJABAT BEA DAN CUKAI</td>
        <td colspan="2" class="left" style="border-top:solid 0.22 mm;">E.&nbsp;TANDA TANGAN PENGUSAHA TPB</td>
    </tr>
	<tr>
    	<td colspan="2" style="border-top:solid 0.22 mm;" class="alignRight">
        	<table>
                <tr>
                	<td style="text-align:center">
                    	<table>
                        	<tr>
                            	<td>Nama</td>
                                <td>:</td>
                                <td><?= $sess['NAMA_CP']?></td>
                            </tr>
                            <tr>
                            	<td>NIP</td>
                                <td>:</td>
                                <td><?= $sess['NIP_CP']?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td colspan="2" class="left" style="border-top:solid 0.22 mm;">
        	<table>

                <tr>
                	<td>Dengan ini saya menyampaikan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini.</td>
                </tr>
                <tr>
                	<td class="alignCenter"><?= $sess['KOTA_TTD'].',Tgl '.$this->fungsi->FormatDateLengkap($sess['TANGGAL_TTD'])?></td>
                </tr>
                <tr>
                	<td class="alignCenter">(<?= $sess['NAMA_TTD']?>)</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

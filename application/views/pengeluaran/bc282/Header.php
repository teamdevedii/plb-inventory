<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
?>
<span id="DivHeaderForm">
	<form id="fbc282" name="fbc282" action="<?= site_url()."/pengeluaran/bc282"; ?>" method="post" class="form-horizontal" autocomplete="off">
		<input type="hidden" name="act" id="act" value="<?= $act;?>" />
		<input type="hidden" name="HEADER[NOMOR_AJU]" id="noaju" value="<?= $aju;?>" />
		<input type="hidden" name="STATUS_DOK" id="STATUS_DOK" value="<?= $sess["STATUS_DOK"];?>" />
		<input type="hidden" name="HEADERDOK[NOMOR_AJU]" id="noajudok" value="<?= $aju;?>" />
		<span id="divtmp"></span>
		<table width="100%" border="0">
			<tr>
            	<td width="45%" valign="top">
                	<table width="100%" border="0">
                    	<tr>
                            <td>Kantor Pabean </td>
                            <td>
                            	<input type="text"  name="HEADER[KODE_KPBC]" id="kpbc_daftar" value="<?= $sess['KODE_KPBC']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urkt;" wajib="yes" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kpbc','kpbc_daftar;urkt','Kode Kpbc',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;
                            	<span id="urkt"><?= $sess['URAIAN_KPBC']==''?$URKANTOR_TUJUAN:$sess['URAIAN_KPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%">Jenis TPB</td>
                            <td width="69%">
								<?= form_dropdown('HEADER[JENIS_TPB]', $tujuan, $sess['JENIS_TPB'], 'id="jenis_tpb" class="mtext"  wajib="yes" '.$disabled); ?>
							</td>
                        </tr>
                        <tr>
                            <td>Jenis BC 2.5</td>
                            <td>
								<input type="radio" name="HEADER[JENIS_BC25]" value="1" <?php if($sess['JENIS_BC25']==1) echo"checked=\"checked\""; ?>  <?=$disabled?>/>&nbsp;
								1.Biasa &nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="HEADER[JENIS_BC25]" value="2"  <?php if($sess['JENIS_BC25']==2) echo"checked=\"checked\""; ?> <?=$disabled?>/>&nbsp;
								2.Berkala
							</td>
                        </tr>
                        <tr>
                            <td>Kondisi Barang</td>
                            <td>
								<input type="radio" name="HEADER[KONDISI_BARANG]" value="1" <?php if($sess['KONDISI_BARANG']==1) echo"checked=\"checked\""; ?> <?=$disabled?>/>&nbsp;
								1.Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="HEADER[KONDISI_BARANG]" value="2" <?php if($sess['KONDISI_BARANG']==2) echo"checked=\"checked\""; ?> <?=$disabled?>/>&nbsp;
								2.Rusak
							</td>
                        </tr>
                        <tr>
                        	<td>Kategori</td>
                            <td>
								<?= form_dropdown('HEADER[KATEGORI]', $jenis_barang, $sess['KATEGORI'], 'id="KATEGORI" class="mtext" onchange="cekkategori(this.value)" '.$disabled); ?>
							</td>
                     	</tr>
					</table>
				<td width="55%" valign="top">
					<h5 class="header smaller lighter red"><i>Informasi Bea dan Cukai</i></h5>
                    <table width="80%">
                        <tr>
                            <td>Nomor Pendaftaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                                <input type="hidden" name="HEADERDOK[NOMOR_PENDAFTARAN]" id="NOMOR_PENDAFTARAN" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6"  <?=$readonly?>/>
                                <input type="text" name="HEADERDOK[NOMOR_PENDAFTARAN_EDIT]" id="NOMOR_PENDAFTARAN_EDIT" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6"  <?=$readonly?>/>
							<?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" maxlength="6" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>"/>
							<?php }?>
                         	</td>
                        </tr>
                        <tr>
                            <td>Tanggal Pendaftaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                                <input type="hidden" name="HEADERDOK[TANGGAL_PENDAFTARAN]" id="TANGGAL_PENDAFTARAN" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"  <?=$readonly?>/>
                            	<input type="text" name="HEADERDOK[TANGGAL_PENDAFTARAN_EDIT]" id="TANGGAL_PENDAFTARAN_EDIT" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"  <?=$readonly?>/>
                            <?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"/><?php }?>&nbsp;YYYY-MM-DD
                        	</td>
                        </tr>
                        <tr>
                            <td>No. Persetujuan Pengeluaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                            	<input type="text" name="HEADERDOK[NOMOR_DOK_PABEAN]" id="NOMOR_DOK_PABEAN" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>" maxlength="30"  <?=$readonly?>/>
							<?php }else{?>
                            	<input type="text" disabled="disabled" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>" />
							<?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Persetujuan Pengeluaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                            	<input type="text" name="HEADERDOK[TANGGAL_DOK_PABEAN]" id="TANGGAL_DOK_PABEAN" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN']=='0000-00-00')?'':$sess['TANGGAL_DOK_PABEAN']; ?>" onfocus="ShowDP('TANGGAL_DOK_PABEAN');"  <?=$readonly?>/>
                            <?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN']=='0000-00-00')?'':$sess['TANGGAL_DOK_PABEAN']; ?>"/>
							<?php }?>&nbsp;YYYY-MM-DD
                            </td>
                        </tr>
                    </table>
				</td>
        	</tr>
    	</table>
        <h5 class="header smaller lighter green"><b>DATA PEMBERITAHUAN</b></h5>
        <table width="100%" border="0">
			<tr>
            	<td width="45%" valign="top">
					<table width="100%" border="0">
						<tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>PENGUSAHA TPB</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Identitas</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_ID_TRADER]', $kode_id_trader, $sess['KODE_ID_TRADER'], 'id="kode_id_trader" class="sstext" '.$disabled); ?> 
								<input type="hidden" name="HEADER[KODE_TRADER]" id="kode_trader" value="<?= $sess['KODE_TRADER']; ?>" class="stext date" size="20" maxlength="15"<?=$disabled?>/>
								<input type="text" name="HEADER[ID_TRADER]" id="id_trader" value="<?= $this->fungsi->FORMATNPWP($sess['ID_TRADER']); ?>" class="ltext" size="20" maxlength="15" <?=$readonly?>/>
                          	</td>
                        </tr> 
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_TRADER]" id="nama_trader" value="<?= $sess['NAMA_TRADER']; ?>" class="mtext" maxlength="50" wajib="yes" <?=$readonly?>/>
                           	</td>
                        </tr>
                        <tr>
                        	<td>Izin TPB</td>
                        	<td>
                        		<input type="text" name="HEADER[NOMOR_IZIN_TPB]" id="NOMOR_IZIN_TPB" value="<?= $sess['NOMOR_IZIN_TPB']?$sess['NOMOR_IZIN_TPB']:$sess['NOMOR_SKEP']; ?>" class="stext" maxlength="15" wajib="yes" <?=$readonly?>/>
                        		&nbsp;Jenis TPB
                        		<?= form_dropdown('HEADER[KODE_TPB]', $jenis_tpb, $sess['KODE_TPB'], 'wajib="yes" id="jenis_tpb" class="sstext"  '.$disabled); ?>
                        	</td>
                        </tr>
                        <tr>
                            <td class="top">Alamat</td>
                            <td>
                            	<textarea name="HEADER[ALAMAT_TRADER]" id="alamat_trader" class="mtext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatTrader')" <?=$disabled?>><?= $sess['ALAMAT_TRADER']; ?></textarea>
                            	<div id="limitAlamatTrader"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>APIU/P</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_API]', $kode_api, $sess['KODE_API'], 'id="KODE_API" class="sstext" '.$disabled); ?>
								<input type="text" name="HEADER[NOMOR_API]" id="NOMOR_API" value="<?= $sess['NOMOR_API']; ?>" class="ltext" <?=$disabled?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"  style="line-height:30px">
                            	<h5 class="smaller lighter blue"><b>PENERIMA BARANG</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Identitas</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_ID_PENERIMA]', $kode_id, $sess['KODE_ID_PENERIMA'], 'id="KODE_ID_PENERIMA" class="sstext"  '.$disabled); ?> 
								<input type="text" name="HEADER[ID_PENERIMA]" id="ID_PENERIMA" value="<?php if($sess['KODE_ID_PENERIMA']==5){echo $this->fungsi->FORMATNPWP($sess['ID_PENERIMA']);}else{ echo $sess['ID_PENERIMA'];}?>" class="ltext" size="20" wajib="yes" maxlength="20"<?=$disabled?>/>
								<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('pemasok','KODE_ID_PENERIMA;ID_PENERIMA;NAMA_PENERIMA;ALAMAT_PENERIMA','penerima','fbc282',600,500)" value="..." <?=$display?>>
                          	</td>
                        </tr> 
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_PENERIMA]" id="NAMA_PENERIMA" value="<?= $sess['NAMA_PENERIMA']; ?>" url="<?= site_url(); ?>/autocomplete/penerima_bc25" wajib="yes" onfocus="Autocomp(this.id)" urai="alamat_penerima;niper_penerima;id_penerima;" class="mtext" <?=$disabled?>/>
                           	</td>
                        </tr>
                        
                        <tr>
                            <td class="top">Alamat</td>
                            <td>
                            	<textarea name="HEADER[ALAMAT_PENERIMA]" id="ALAMAT_PENERIMA" class="mtext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatPnerima')" <?=$disabled?>><?= $sess['ALAMAT_PENERIMA']; ?></textarea>
                            	<div id="limitAlamatPnerima"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Niper (Jika KITE)</td>
                            <td>
                            	<input type="text" name="HEADER[NIPER_PENERIMA]" id="niper_penerima" class="mtext" value="<?= $sess['NIPER_PENERIMA']; ?>"<?=$disabled?>/>
                        	</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>PEMBERITAHUAN KESIAPAN BARANG</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Alamat Pemeriksaan </td>
                            <td>
                            	<input type="text" name="HEADER[ALAMAT_PERIKSA_FISIK]" id="alamat_periksa_fisik" value="<?= $sess['ALAMAT_PERIKSA_FISIK']?>" class="mtext" wajib="yes" <?=$disabled?>>
                          	</td>
                        </tr>
                        <tr>
                            <td>Tanggal </td>
                            <td>
                            	<input type="text" name="HEADER[TANGGAL_PERIKSA_FISIK]" id="tanggal_periksa_fisik" value="<?= $sess['TANGGAL_PERIKSA_FISIK']?>" onfocus="ShowDP('tanggal_periksa_fisik')" class="stext date" wajib="yes" <?=$disabled?>>
                            	&nbsp; Jam
                            	<input type="text" name="HEADER[WAKTU_PERIKSA_FISIK]" id="WAKTU_PERIKSA_FISIK" value="<?= $sess['WAKTU_PERIKSA_FISIK']?>" class="ssstext" onfocus="ShowTime('WAKTU_PERIKSA_FISIK');" wajib="yes" <?=$disabled?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td>Kontak Person </td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_CP]" id="nama_cp" value="<?= $sess['NAMA_CP']?>" class="mtext" <?=$disabled?>>
                          	</td>
                        </tr>
                        <tr>
                            <td>No Telepon/Fax </td>
                            <td>
                            	<input type="text" name="HEADER[TELP_CP]" id="telp_cp" value="<?= $sess['TELP_CP']?>" class="mtext" <?=$disabled?>>
                          	</td>
                        </tr>
                   	</table>
                    <h5 class="smaller lighter blue"><b>DATA PUNGUTAN :</b></h5>
                    <table border="0" width="80%" cellpadding="5" cellspacing="0" class="table-striped table-bordered no-margin-bottom">
                        <tr>
                            <td width="50%"  align="center" class="border-brlt">Jenis Pungutan</td>
                            <td width="20%" align="center" class="border-brt">Dibayar (Rp) </td>
                            <td width="30%" align="center" class="border-brt">Ditangguhkan (Rp)</td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">BM</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$A =($sess['PGT_BM']!="")? $this->fungsi->FormatRupiah($sess['PGT_BM'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$AA =($sess['PGT_BM_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_BM_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">Cukai</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$B =($sess['PGT_CUKAI']!="")? $this->fungsi->FormatRupiah($sess['PGT_CUKAI'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$BB =($sess['PGT_CUKAI_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_CUKAI_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPN</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$C =($sess['PGT_PPN']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPN'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$CC =($sess['PGT_PPN_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPN_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPnBM</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$D =($sess['PGT_PPNBM']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPNBM'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$DD =($sess['PGT_PPNBM_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPNBM_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPh</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;" ><?=$E =($sess['PGT_PPH']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPH'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$EE =($sess['PGT_PPH_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPH_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">PNBP</td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$F =($sess['PGT_PNBP']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$FF =($sess['PGT_PNBP_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">
                            	Denda/bunga BM dan cukai(D/B)
                            </td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$G =($sess['PGT_PNBP']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$GG =($sess['PGT_DENDA_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_DENDA_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl">
                            	Bunga PPN dan PPnBM
                            </td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$H =($sess['PGT_PNBP']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP'],0):0;?></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><?=$HH =($sess['PGT_BUNGA_STATUS']!="")? $this->fungsi->FormatRupiah($sess['PGT_BUNGA_STATUS'],0):0;?></td>
                        </tr>
                        <tr>
                            <td width="20%" style="padding-left:20px;" class="border-brl"><strong>TOTAL</strong></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><b><?=$this->fungsi->FormatRupiah(str_replace(',','',$A)+str_replace(',','',$B)+str_replace(',','',$C)+str_replace(',','',$D)+str_replace(',','',$E)+str_replace(',','',$F)+str_replace(',','',$G)+str_replace(',','',$H),0)?></b></td>
                            <td width="20%" align="right" class="border-br" style="padding-right:10px;"><b><?=$this->fungsi->FormatRupiah(str_replace(',','',$AA)+str_replace(',','',$BB)+str_replace(',','',$CC)+str_replace(',','',$DD)+str_replace(',','',$EE)+str_replace(',','',$FF)+str_replace(',','',$GG)+str_replace(',','',$HH),0)?></b></td>
                        </tr>
                	</table>
             	</td>
                <td width="55%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="3" class="rowheight">
                             	<h5 class="smaller lighter blue"><b>DATA PERDAGANGAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Valuta Asing </td>
                            <td>
                            	<input type="text" name="HEADER[KODE_VALUTA]" id="kode_valuta" value="<?= $sess['KODE_VALUTA']?>" class="stext date" url="<?= site_url(); ?>/autocomplete/valuta" urai="valuta;kdvaluta;" wajib="yes" onfocus="Autocomp(this.id)"<?=$disabled?>/> &nbsp;
                            	<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('valuta','kode_valuta;valuta','Kode Valuta',this.form.id,650,400)" value="..." <?=$display?>>&nbsp;
                            	<span id="valuta"><?= $sess['URAIAN_VALUTA'] ?></span>
                          	</td>
                        </tr>
                        <tr>
                            <td>NDPBM (Kurs) </td>
                            <td>
                               <input type="text" name="NILAI_NDPBM" id="ndpbm_nilai" class="mtext" value="<?= $this->fungsi->FormatRupiah($sess['NDPBM'],4); ?>" maxlength="30" wajib="yes" onkeyup="this.value = ThausandSeperator('ndpbm',this.value,4);ProsesHeader()"<?=$disabled?>/>
                               <input type="hidden" name="HEADER[NDPBM]" id="ndpbm" value="<?= $sess['NDPBM']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Nilai CIF <span id="kdvaluta" style="padding-left:55px;"></span></td>
                            <td>
                               <input type="text" name="CIF_TARIF" id="cifTarif" class="mtext" value="<?= $this->fungsi->FormatRupiah($sess['CIF'],2); ?>" maxlength="30" wajib="yes" onkeyup="this.value = ThausandSeperator('cif',this.value,2);ProsesHeader();" <?=$disabled?>/>
                               <input type="hidden" name="HEADER[CIF]" id="cif" value="<?= $sess['CIF']?>" <?=$disabled?>/>
                            </td>
                        </tr>
                    	<tr>
                            <td><span style="float:right">Rp</span></td>
                            <td>
                                <input type="text"  id="CIFRP" class="mtext" readonly="readonly" value="<?= $this->fungsi->FormatRupiah($sess['CIFRP'],4) ?>" >
                            </td>
  		             	</tr>
                        <tr>
                            <td>Harga Penyerahan</td>
                            <td>
                            	<input type="text" name="HARGA" id="harga" class="mtext" value="<?= $this->fungsi->FormatRupiah($sess['HARGA_PENYERAHAN']); ?>" maxlength="30"  onkeyup="this.value = ThausandSeperator('harga_penyerahan',this.value,2)"<?=$disabled?>/>
                            	<input type="hidden" name="HEADER[HARGA_PENYERAHAN]" id="harga_penyerahan" value="<?= $sess['HARGA_PENYERAHAN']?>" <?=$disabled?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td>Volume(M3) </td>
                            <td>
                            	<input type="text" name="volumeur" id="volumeur" value="<?= $this->fungsi->FormatRupiah($sess['VOLUME'],2)?>" class="mtext" wajib="yes" format="angka"  onkeyup="this.value = ThausandSeperator('volume',this.value,2)" <?=$disabled?> />
                              	<input type="hidden" name="HEADER[VOLUME]" id="volume" value="<?= $sess['VOLUME']?>" <?=$disabled?>/>
                         	</td>
                        </tr>
                        <tr>
                            <td>Bruto </td>
                            <td>
                            	<input type="text" name="brutour" id="brutour" value="<?= $this->fungsi->FormatRupiah($sess['BRUTO'],2); ?>" class="stext date" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('bruto',this.value,2)"<?=$disabled?>/>&nbsp; Kilogram (KGM)
                            	<input type="hidden" name="HEADER[BRUTO]" id="bruto" value="<?= $sess['BRUTO']?>" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td class="rowheight">Netto</td>
                            <td><?= $sess['JUM_NETTO']; ?>&nbsp; Kilogram (KGM)
                            	<input type="hidden" name="HEADER[NETTO]" value="<?= $sess['JUM_NETTO']; ?>" class="text" wajib="yes"<?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Otomatis terisi dari jumlah total Netto Detil Barang</td>
                        </tr>
                        <tr>
                            <td>PNBP</td>
                            <td>
                            	<input type="text" name="NILAI_BEBANUR" id="NILAI_BEBANUR" value="<?php if($sess['KODE_FASILITAS']=="0"){ echo $this->fungsi->FormatRupiah($sess['PGT_PNBP'],2);}elseif($sess['KODE_FASILITAS']==4){echo $this->fungsi->FormatRupiah($sess['PGT_PNBP_STATUS'],2);} ?>" class="sstext"  format="angka" onkeyup="this.value = ThausandSeperator('NILAI_BEBAN',this.value,2);"<?=$disabled?>/>
                            	<input type="hidden" name="BEBAN[NILAI_BEBAN]" id="NILAI_BEBAN" value="<?php if($sess['KODE_FASILITAS']=="0"){ echo $this->fungsi->FormatRupiah($sess['PGT_PNBP'],2);}elseif($sess['KODE_FASILITAS']==4){echo $this->fungsi->FormatRupiah($sess['PGT_PNBP_STATUS'],2);} ?>" class="sstext"  wajib="yes" format="angka"<?=$disabled?>/>
                            	<input type="hidden" name="BEBAN[KODE_BEBAN]" id="KODE_BEBAN" value="8" class="sstext" <?=$disabled?>/>&nbsp; <?= form_dropdown('BEBAN[KODE_FASILITAS]', $kode_pnbp, $sess['KODE_FASILITAS'], 'id="KODE_FASILITAS" class="stext"   '.$disabled); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>TANDA TANGAN PENGUSAHA TPB</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_TTD]" id="nama_ttd" value="<?= $sess['NAMA_TTD']; ?>" class="mtext" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td>
                            	<input type="text" name="HEADER[KOTA_TTD]" id="kota_ttd" value="<?= $sess['KOTA_TTD']; ?>" class="mtext" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>
                            	<input type="text" name="HEADER[TANGGAL_TTD]" id="tanggal_ttd" onfocus="ShowDP('tanggal_ttd');" value="<?php if($act=="save") echo date("Y-m-d"); else echo $sess['TANGGAL_TTD']; ?>" class="stext date" <?=$disabled?>/>&nbsp; YYYY-MM-DD
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>PENERIMA BARANG</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_TTD_PENERIMA]" id="nama_ttd_penerima" value="<?= $sess['NAMA_TTD_PENERIMA']; ?>" class="mtext" wajib="yes" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td>
                            	<input type="text" name="HEADER[KOTA_TTD_PENERIMA]" id="kota_ttd_penerima" value="<?= $sess['KOTA_TTD_PENERIMA']; ?>" class="mtext" wajib="yes" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>
                            	<input type="text" name="HEADER[TANGGAL_TTD_PENERIMA]" id="tanggal_ttd_penerima" onfocus="ShowDP('tanggal_ttd_penerima');" value="<?= $sess['TANGGAL_TTD_PENERIMA']; ?>" class="stext date" wajib="yes"  <?=$disabled?>/>&nbsp; YYYY-MM-DD
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA CONTACT PERSON</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_CP]" id="nama_cp" value="<?= $sess['NAMA_CP']; ?>" class="mtext" readonly="readonly"<?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor Telepon</td>
                            <td>
                            	<input type="text" name="HEADER[TELP_CP]" id="telp_cp" value="<?= $sess['TELP_CP']; ?>" class="mtext" readonly="readonly"<?=$disabled?>/>
                            </td>
                        </tr>
                    </table>
             	</td>
          	</tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <?php if(!$priview){?>
            <tr>
                <td colspan="2">
                	<a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="save_header('#fbc282');"><i class="icon-save"></i>&nbsp;<?=ucwords($act)?></a>&nbsp;
                    <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('fbc282');"><i class="icon-undo"></i>Reset</a>&nbsp;
                    <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                </td>
            </tr>
            <?php } ?>
    	</table>
	</form>
</span>
<?php  
if($priview){
	echo $DETILPRIVIEW;
} 
?>
<script>
$(function(){FormReady();})
$(document).ready(function(){
	// jkwaktu();
});
// function jkwaktu(){
// 	if($('#jenis_impor').val()=='2'){
// 		$('#jangkawaktu').show();
// 	}else{
// 		$('#jangkawaktu').hide();
// 	}
// }
<? if($priview){ ?>
$('#fbc282 input:visible, #fbc282 select').attr('disabled',true);
<? } ?>
</script>
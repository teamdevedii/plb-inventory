<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);

$func = get_instance();
$dokumen = $dokumen;
$jns_barang = $jns_barang;
$aju = $aju;
$tipeproses = $tipeproses;
?>

<div class="header">
  <h3><?=$judul?></h3>
    <?php if($aju){ ?>
    <a href="javascript:void(0)" onclick="Cekstatus('<?=site_url()."/pemasukan/cekstatus/".$aju."/bc282"?>');" style="float:right;margin:4px 4px 0px 0px" class="btn btn-small btn-yellow"><i class="icon-info-sign"></i> Cek Status</a>    
    <a href="javascript: window.history.go(-1)" style="float:right;margin:4px 4px 0px 0px" class="btn btn-small btn-success"><i class="icon-arrow-left"></i>Back</a>
    <?php }else{ ?>
    <span id="SpanStatus"></span>
    <?php } ?>
</div>
<div class="content">
  <div class="tabbable">
    <ul class="nav nav-tabs" id="myTab">
      <li class="active"><a data-toggle="tab" href="#tabHeader">Data Header</a></li>
      <li><a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
      <li><a data-toggle="tab" href="#tabKemasan">Data Kemasan</a></li>
      <li><a data-toggle="tab" href="#tabDokumen">Data Dokumen</a></li>
      <li><a data-toggle="tab" href="#tabPpn">PPN Penyerahan</a></li>
    </ul>
    <div class="tab-content">
      <div id="tabHeader" class="tab-pane active">
        <?php 
          $func->load->model("bc282/header_act");
          $data = $func->header_act->get_header($aju);
          $this->load->view("pengeluaran/bc282/Header",$data); 
        ?>
      </div>
      <div id="tabBarang" class="tab-pane">
        Barang
        <?php
          // if ($aju) {
          //   $func->load->model("bc282/detil_act");
          //   $arrdata = $func->detil_act->detil('barang', $dokumen, $aju, 'edit');
          //   $list = $this->load->view('view', $arrdata, true);
          // }
          // $func->load->model("bc282/barang_act");
          // $data = $func->barang_act->get_barang($aju);
          // if ($aju) $data = array_merge($data, array('list' => $list));
          //     $this->load->view("pengeluaran/bc282/Barang",$data);
        ?>
      </div>
      <div id="tabKemasan" class="tab-pane">
        Kemasan
      </div>
      <div id="tabDokumen" class="tab-pane">
        Dokumen
      </div>
      <div id="tabPpn" class="tab-pane">
        PPN Penyerahan
      </div>
    </div>
  </div>
</div>
<script>
<?php if(!$aju){?>
$(".tabbable ul.nav li a").addClass('disabled');
$(".nav-tabs a[data-toggle=tab]").on("click", function(e) { 
  if($(this).hasClass("disabled")) {
    e.preventDefault();
    return false;
  }
});
<?php } ?>
</script>
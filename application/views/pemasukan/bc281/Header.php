<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
?>
<span id="DivHeaderForm">
	<form id="fbc281" name="fbc281" action="<?= site_url()."/pemasukan/bc281"; ?>" method="post" class="form-horizontal" autocomplete="off">
		<input type="hidden" name="act" id="act" value="<?= $act;?>" />
		<input type="hidden" name="HEADER[NOMOR_AJU]" id="noaju" value="<?= $aju;?>" />
		<input type="hidden" name="STATUS_DOK" id="STATUS_DOK" value="<?= $sess["STATUS_DOK"];?>" />
		<input type="hidden" name="HEADERDOK[NOMOR_AJU]" id="noajudok" value="<?= $aju;?>" />
		<span id="divtmp"></span>
		<table width="100%" border="0">
			<tr>
            	<td width="45%" valign="top">
                	<table width="100%" border="0">
                    	<tr>
                            <td>KPBC Pendaftaran </td>
                            <td>
                            	<input type="text"  name="HEADER[KODE_KPBC]" id="kpbc_daftar" value="<?= $sess['KODE_KPBC']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urkt;" wajib="yes" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kpbc','kpbc_daftar;urkt','Kode Kpbc',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urkt"><?= $sess['URAIAN_KPBC']==''?$URKANTOR_TUJUAN:$sess['URAIAN_KPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>KPBC Bongkar </td>
                            <td>
                            	<input type="text" name="HEADER[KODE_KPBC_BONGKAR]" id="kpbc_bongkar" value="<?= $sess['KODE_KPBC_BONGKAR']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urktbongkar;" wajib="yes" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kpbc','kpbc_bongkar;urktbongkar','Kode Kpbc',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urktbongkar"><?= $sess['URAIAN_KPBC_BONGKAR']==''?$URKANTOR_TUJUAN:$sess['URAIAN_KPBC_BONGKAR']; ?></span>
                          	</td>
                        </tr>
                        <tr>
                            <td>KPBC Pengawas </td>
                            <td>
                            	<input type="text" name="HEADER[KODE_KPBC_AWAS]" id="kpbc_pengawas" value="<?= $sess['KODE_KPBC_AWAS']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urktawas;" wajib="yes" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka" <?=$readonly?> /> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kpbc','kpbc_pengawas;urktawas','Kode Kpbc',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urktawas"><?= $sess['URAIAN_KPBC_AWAS']==''?$URKANTOR_TUJUAN:$sess['URAIAN_KPBC_AWAS']; ?></span>
							</td>
                        </tr>
                        <tr>
                            <td width="30%">Tujuan</td>
                            <td width="69%">
								<?= form_dropdown('HEADER[TUJUAN]', $tujuan, $sess['TUJUAN'], 'id="tujuan" class="mtext"  wajib="yes" '.$disabled.' '.$width); ?>
							</td>
                        </tr>
                        <tr>
                            <td>Jenis Barang</td>
                            <td>
								<?= form_dropdown('HEADER[JENIS_BARANG]', $jenis_barang, $sess['JENIS_BARANG'], 'id="jenis_barang" class="mtext" '.$disabled); ?>
							</td>
                        </tr>
                        <tr>
                        	<td>Tujuan Kirim</td>
                            <td>
								<?= form_dropdown('HEADER[TUJUAN_KIRIM]', $tujuan_kirim, $sess['TUJUAN_KIRIM'], 'id="tujuan_kirim" class="mtext" wajib="yes" '.$disabled); ?>
							</td>
                     	</tr>
					</table>
				<td width="55%" valign="top">
					<h5 class="header smaller lighter red"><i>Informasi Bea dan Cukai</i></h5>
                    <table width="80%">
                        <tr>
                            <td>Nomor Pendaftaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                                <input type="hidden" name="HEADERDOK[NOMOR_PENDAFTARAN]" id="NOMOR_PENDAFTARAN" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6"  <?=$readonly?>/>
                                <input type="text" name="HEADERDOK[NOMOR_PENDAFTARAN_EDIT]" id="NOMOR_PENDAFTARAN_EDIT" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6"  <?=$readonly?>/>
							<?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" maxlength="6" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>"/>
							<?php }?>
                         	</td>
                        </tr>
                        <tr>
                            <td>Tanggal Pendaftaran</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                                <input type="hidden" name="HEADERDOK[TANGGAL_PENDAFTARAN]" id="TANGGAL_PENDAFTARAN" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"  <?=$readonly?>/>
                            	<input type="text" name="HEADERDOK[TANGGAL_PENDAFTARAN_EDIT]" id="TANGGAL_PENDAFTARAN_EDIT" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"  <?=$readonly?>/>
                            <?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"/><?php }?>&nbsp;YYYY-MM-DD
                        	</td>
                        </tr>
                        <tr>
                            <td>No. Persetujuan Pemasukan</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                            	<input type="text" name="HEADERDOK[NOMOR_DOK_PABEAN]" id="NOMOR_DOK_PABEAN" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>" maxlength="30"  <?=$readonly?>/>
							<?php }else{?>
                            	<input type="text" disabled="disabled" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>" />
							<?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Persetujuan Pemasukan</td>
                            <td>
                            <?php if($sess['STATUS_DOK']=="LENGKAP"){?>
                            	<input type="text" name="HEADERDOK[TANGGAL_DOK_PABEAN]" id="TANGGAL_DOK_PABEAN" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN']=='0000-00-00')?'':$sess['TANGGAL_DOK_PABEAN']; ?>" onfocus="ShowDP('TANGGAL_DOK_PABEAN');"  <?=$readonly?>/>
                            <?php }else{?>
                            	<input type="text" disabled="disabled" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN']=='0000-00-00')?'':$sess['TANGGAL_DOK_PABEAN']; ?>"/>
							<?php }?>&nbsp;YYYY-MM-DD
                            </td>
                        </tr>
                    </table>
				</td>
        	</tr>
    	</table>
        <h5 class="header smaller lighter green"><b>DATA PEMBERITAHUAN</b></h5>
        <table width="100%" border="0">
			<tr>
            	<td width="45%" valign="top">
					<table width="100%" border="0">
						<tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue"><b>DATA PEMASOK</b></h5></td>
                        </tr>
                        <tr>
                            <td width="29%">Nama </td>
                            <td width="69%">
                                <input type="hidden" id="IDHIDE1" readonly />
                                <input type="hidden" id="IDHIDE2" readonly />
                                <input type="text" name="HEADER[NAMA_PEMASOK]" id="nama_partner" value="<?= $sess['NAMA_PEMASOK']; ?>" url="<?= site_url(); ?>/autocomplete/pemasok" wajib="yes" onfocus="Autocomp(this.id)" urai="alamat_partner;negara_asal_partner;urnegara_asal_part;" class="mtext"  <?=$readonly?>/>&nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('pemasok','IDHIDE1;IDHIDE2;nama_partner;alamat_partner;negara_asal_partner;urnegara_asal_partner','penerima','fbc281',630,480)" value="..." <?=$display?> />
                           	</td>
                        </tr>
                        <tr>
                            <td class="top">Alamat </td>
                            <td>
                            	<textarea name="HEADER[ALAMAT_PEMASOK]" id="alamat_partner" wajib="yes" class="mtext" onkeyup="limitChars(this.id, 70, 'limitAlamatPemasok')" <?=$disabled?>><?= $sess['ALAMAT_PEMASOK']; ?></textarea>
                            	<div id="limitAlamatPemasok"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Negara Asal </td>
                            <td>
                            	<input type="text" name="HEADER[NEGARA_PEMASOK]" id="negara_asal_partner" value="<?= $sess['NEGARA_PEMASOK']; ?>" url="<?= site_url(); ?>/autocomplete/negara" onfocus="Autocomp(this.id)" class="ssstext" urai="urnegara_asal_partner;" wajib="yes" <?=$readonly?>/> &nbsp; 
                                <input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('negara','negara_asal_partner;urnegara_asal_partner','Kode Negara',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urnegara_asal_partner"><?= $sess['URAIAN_NEGARA_PEMASOK']==''?$URAIAN_NEGARA:$sess['URAIAN_NEGARA_PEMASOK']; ?></span>&nbsp;<span id="urnegara_asal_part"><?= $URAIAN_NEGARA; ?></span>
                          	</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"  style="line-height:30px"><h5 class="smaller lighter blue"><b>DATA IMPORTIR</b></h5></td>
                        </tr>
                        <tr>
                            <td>Identitas</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_ID_TRADER]', $kode_id_trader, $sess['KODE_ID_TRADER'], 'wajib="yes" id="kode_id_trader" class="sstext" '.$disabled); ?> <input type="text" name="HEADER[ID_TRADER]" id="id_trader" value="<?= $this->fungsi->FORMATNPWP($sess['ID_TRADER']); ?>" class="ltext" size="20" maxlength="15" wajib="yes" <?=$readonly?>/>
                          	</td>
                        </tr> 
                        <tr>
                            <td>Nama</td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_TRADER]" id="nama_trader" value="<?= $sess['NAMA_TRADER']; ?>" class="mtext" maxlength="50" wajib="yes" <?=$readonly?>/>
                           	</td>
                        </tr>
                        
                        <tr>
                            <td class="top">Alamat</td>
                            <td>
                            	<textarea name="HEADER[ALAMAT_TRADER]" id="alamat_trader" class="mtext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatTrader')" <?=$disabled?>><?= $sess['ALAMAT_TRADER']; ?></textarea>
                            	<div id="limitAlamatTrader"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Skep Izin</td>
                            <td>
                            	<input type="text" name="HEADER[REGISTRASI]" id="registrasi" class="mtext" value="<?= $sess['REGISTRASI']; ?>" maxlength="30" <?=$readonly?>/>
                        	</td>
                        </tr>
                        <tr>
                            <td>Jenis TPB</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_TPB]', $jenis_tpb, $sess['KODE_TPB'], 'wajib="yes" id="jenis_tpb" class="mtext" '.$disabled); ?>
                          	</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                           	<td>
						   		<?= form_dropdown('HEADER[STATUS_TRADER]', $status, $sess['STATUS_TRADER'], 'id="status" class="mtext" '.$disabled); ?>
                        	</td>
                        </tr>
                        <tr>
                            <td>APIU / APIP</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_API]', $kode_api, $sess['KODE_API'], 'id="kode_api" class="sstext" '.$disabled); ?> <input type="text" name="HEADER[NOMOR_API]" id="no_api" value="<?= $sess['NOMOR_API']?>" url="<?= site_url(); ?>/autocomplete/importir" class="ltext" maxlength="15" <?=$readonly?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue"><b>DATA SARANA ANGKUT</b></h5></td>
                        </tr>
                        <tr>
                            <td>Moda Transportasi</td>
                            <td>
								<?= form_dropdown('HEADER[MODA]', $cara_angkut, $sess['MODA'], 'wajib="yes" id="cara_angkut" class="mtext" '.$disabled); ?>
                           	</td>
                        </tr>
                        <tr>
                            <td>Nama Sarana Angkut </td>
                            <td>
                            	<input type="text" name="HEADER[NAMA_ANGKUT]" id="nama_angkut" value="<?= $sess['NAMA_ANGKUT']; ?>" wajib="yes" class="mtext"  <?=$readonly?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td>No. Voy/Flight </td>
                            <td>
                            	<input type="text" name="HEADER[NOMOR_ANGKUT]" id="nomor_voy" value="<?= $sess['NOMOR_ANGKUT']; ?>" class="mtext" maxlength="7" wajib="yes" <?=$readonly?>/>
                          	</td>
                        </tr>
                        <tr>
                            <td>Bendera</td>
                            <td>
                            	<input type="text" name="HEADER[BENDERA]" id="bendera" value="<?= $sess['BENDERA']; ?>" class="ssstext" url="<?= site_url(); ?>/autocomplete/negara" onfocus="Autocomp(this.id)" urai="urbendera;"  <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('negara','bendera;urbendera','Kode Negara',this.form.id,650,400)" value="..." <?=$display?>>&nbsp;
                            	<span id="urbendera"><?= $sess['URAIAN_BENDERA']==''?$URAIAN_NEGARA:$sess['URAIAN_BENDERA']; ?></span>
                          	</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue"><b>DATA PELABUHAN</b></h5></td>
                        </tr>
                        <tr>
                            <td width="29%">Muat </td>
                            <td width="69%">
                            	<input type="text" name="HEADER[PELABUHAN_MUAT]" id="pelabuhan_muat" value="<?= $sess['PELABUHAN_MUAT']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan" onfocus="Autocomp(this.id)" urai="urpelabuhan_muat;" class="sstext date" wajib="yes" maxlength="5" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('pelabuhan','pelabuhan_muat;urpelabuhan_muat','Kode Pelabuhan',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urpelabuhan_muat"><?= $sess['URAIAN_MUAT']==''?$urpelabuhan_muat:$sess['URAIAN_MUAT']; ?></span>
                     		</td>
                        </tr>
                        <tr>
                            <td>Transit</td>
                            <td>
                            	<input type="text" name="HEADER[PELABUHAN_TRANSIT]" id="pelabuhan_transit" url="<?= site_url(); ?>/autocomplete/pelabuhan" onfocus="Autocomp(this.id)" urai="urpelabuhan_transit;" class="stext date" value="<?= $sess['PELABUHAN_TRANSIT']; ?>" maxlength="5" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('pelabuhan','pelabuhan_transit;urpelabuhan_transit','Kode Pelabuhan',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urpelabuhan_transit"><?= $sess['URAIAN_TRANSIT']==''?$urpelabuhan_transit:$sess['URAIAN_TRANSIT']; ?></span>
                       		</td>
                        </tr>
                        <tr>
                            <td>Bongkar </td>
                            <td>
                            	<input type="text" name="HEADER[PELABUHAN_BONGKAR]" id="pelabuhan_bongkar" value="<?= $sess['PELABUHAN_BONGKAR']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan/dalam" onfocus="Autocomp(this.id)" urai="urpelabuhan_bongkar;" class="stext date" wajib="yes" maxlength="5" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('pelabuhan','pelabuhan_bongkar;urpelabuhan_bongkar','Kode Pelabuhan',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urpelabuhan_bongkar"><?= $sess['URAIAN_BONGKAR']==''?$urpelabuhan_bongkar:$sess['URAIAN_BONGKAR']; ?></span>
                       		</td>
                        </tr>
                   	</table>
                    <h5 class="smaller lighter blue"><b>DATA PUNGUTAN :</b></h5>
                    <table border="0" width="82%" cellpadding="5" cellspacing="0" class="table-striped table-bordered no-margin-bottom">
                        <tr>
                            <td width="20%"  align="center" class="border-brlt">Jenis Pungutan</td>
                            <td width="20%" align="center" class="border-brt">Dibayar (Rp) </td>
                            <td width="20%" align="center" class="border-brt">Ditangguhkan (Rp)</td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">BM</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$A =($sess['PGT_BM']!="")? $this->fungsi->FormatRupiah($sess['PGT_BM'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$AA =($sess['PGT_BM_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_BM_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">Cukai</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$B =($sess['PGT_CUKAI']!="")? $this->fungsi->FormatRupiah($sess['PGT_CUKAI'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$BB =($sess['PGT_CUKAI_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_CUKAI_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPN</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$C =($sess['PGT_PPN']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPN'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$CC =($sess['PGT_PPN_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPN_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPnBM</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$D =($sess['PGT_PPNBM']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPNBM'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$DD =($sess['PGT_PPNBM_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPNBM_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">PPh</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;" ><?=$E =($sess['PGT_PPH']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPH'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$EE =($sess['PGT_PPH_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_PPH_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl">PNBP</td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$F =($sess['PGT_PNBP']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP'],0):0;?></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><?=$FF =($sess['PGT_PNBP_DIT']!="")? $this->fungsi->FormatRupiah($sess['PGT_PNBP_DIT'],0):0;?></td>
                        </tr>
                        <tr>
                            
                            <td width="20%" style="padding-left:20px;" class="border-brl"><strong>TOTAL</strong></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><b><?= $this->fungsi->FormatRupiah(str_replace(',','',$A) + str_replace(',','',$B) + str_replace(',','',$C) + str_replace(',','',$D) + str_replace(',','',$E) + str_replace(',','',$F),0)?></b></td>
                            <td width="30%" align="right" class="border-br" style="padding-right:10px;"><b><?=$this->fungsi->FormatRupiah(str_replace(',','',$AA) + str_replace(',','',$BB) + str_replace(',','',$CC) + str_replace(',','',$DD) + str_replace(',','',$EE) + str_replace(',','',$FF),0)?></b></td>
                        </tr>
                	</table>
             	</td>
                <td width="55%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                             <td colspan="3" class="rowheight"><h5 class="smaller lighter blue"><b>INFORMASI LAIN</b></h5></td>
                        </tr>
                        <tr>
                            <td width="117">Penutup</td>
                            <td width="435">
								<?= form_dropdown('HEADER[KODE_PENUTUP]', $kode_penutup, $sess['KODE_PENUTUP'], 'id="kode_penutup" class="mtext date" '.$disabled); ?>
                          	</td>
                        </tr>
                        <tr>
                            <td>Nomor&nbsp;</td>
                            <td>
                            	<input type="text" name="HEADER[NOMOR_PENUTUP]" id="nomor_penutup" value="<?= $sess['NOMOR_PENUTUP']?>" class="stext date" maxlength="6" format="angka" <?=$disabled?>/>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal&nbsp;</td>
                            <td>
                            	<input type="text" name="HEADER[TANGGAL_PENUTUP]" id="tanggal_penutup" value="<?= $sess['TANGGAL_PENUTUP']?>" onfocus="ShowDP('tanggal_penutup')" class="stext date" <?=$disabled?>>&nbsp;YYYY-MM-DD
                      		</td>
                        </tr>
                        <tr>
                            <td>Nomor Pos&nbsp;</td>
                            <td>
                            	<input type="text" name="HEADER[NOMOR_POS]" id="nomor_pos" value="<?= $sess['NOMOR_POS']?>" class="ssstext" maxlength="4" format="angka" <?=$disabled?>>&nbsp;Subpos/Sub-subpos<input type="text" name="HEADER[SUB_POS]" id="sub_pos" value="<?= $sess['SUB_POS']?>" class="ssstext" maxlength="4" format="angka" <?=$disabled?>>&nbsp;/&nbsp;<input type="text" name="HEADER[SUB_SUB_POS]" id="sub_sub_pos" value="<?= $sess['SUB_SUB_POS']?>" class="ssstext" maxlength="4" format="angka" <?=$disabled?>>
                            </td>
                        </tr>
                        <tr>
                            <td>Tempat Timbun </td>
                            <td>
                            	<input type="text" name="HEADER[KODE_TIMBUN]" id="tempat_timbun" value="<?= $sess['KODE_TIMBUN']; ?>" url="<?= site_url(); ?>/autocomplete/timbun" onfocus="Autocomp(this.id)" urai="urtempat_timbun;" class="stext date" wajib="yes" maxlength="4" <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('timbun','tempat_timbun;urtempat_timbun','Kode Timbun',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;<span id="urtempat_timbun"><?= $sess['URAIAN_TIMBUN']==''?$urtempat_timbun:$sess['URAIAN_TIMBUN']; ?></span>
                         	</td>
                        </tr>
                        <tr>
                            <td>Valuta </td>
                            <td>
                            	<input type="text" name="HEADER[KODE_VALUTA]" id="valuta" value="<?= $sess['KODE_VALUTA']; ?>" url="<?= site_url(); ?>/autocomplete/valuta" onfocus="Autocomp(this.id)" urai="urvaluta;" class="stext date" wajib="yes"  <?=$readonly?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('valuta','valuta;urvaluta','Kode Valuta',this.form.id,650,470)" value="..." <?=$display?>>&nbsp;
                            	<span id="urvaluta"><?= $sess['URAIAN_VALUTA']==''?$urvaluta:$sess['URAIAN_VALUTA']; ?></span>
                          	</td>
                        </tr>
                        <tr>
                            <td>NDPBM (Kurs) </td>
                            <td>
                               <input type="text" name="nilai_ndpbm" id="nilai_ndpbm" class="mtext" value="<?= $this->fungsi->FormatRupiah($sess['NDPBM'],4); ?>" maxlength="30" wajib="yes" onkeyup="this.value = ThausandSeperator('ndpbm',this.value,4);prosesHargaHeader('fbc281')" <?=$readonly?>/>
                               <input type="hidden" name="HEADER[NDPBM]" id="ndpbm" value="<?= $sess['NDPBM']?>" />
                            </td>
                        </tr>
                    	<tr>
                            <td><span id="22">FOB</span></td>
                            <td>
                                <input type="text" name="fobur" id="fobur" value="<?= $this->fungsi->FormatRupiah($sess['FOB'],2); ?>" class="mtext" readonly onkeypress="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice','fbc281')">
                                <input type="hidden" name="HEADER[FOB]" id="fob" value="<?= $sess['FOB']?>" />
                                <input type="button" class="btn btn-primary btn-xs" value="..." onclick="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice','fbc281')"  <?=$display?>/>
                            </td>
  		             	</tr>
                        <tr>
                            <td>Freight</td>
                            <td>
                            	<input type="text" disabled="disabled" name="freightur" id="freightur" value="<?= $this->fungsi->FormatRupiah($sess['FREIGHT'],2); ?>" class="mtext" readonly onkeypress="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice',this.form.id)">
                            	<input type="hidden" name="HEADER[FREIGHT]" id="freight" value="<?= $sess['FREIGHT']?>" />
                          	</td>
                        </tr>
                        <tr>
                            <td>Asuransi</td>
                            <td>
                            	<input type="text" name="asuransiur" id="asuransiur" value="<?= $this->fungsi->FormatRupiah($sess['ASURANSI'],2);?>" class="mtext" onkeypress="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice',this.form.id)" readonly><input type="hidden" name="HEADER[ASURANSI]" id="asuransi" value="<?= $sess['ASURANSI']?>" />
                    		</td>
                        </tr>
                        <tr>
                            <td>Nilai CIF </td>
                            <td>
                            	<input type="text" name="cifur" id="cifur" value="<?= $this->fungsi->FormatRupiah($sess['CIF'],2);?>" class="mtext" wajib="yes" onkeypress="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice',this.form.id)" readonly>
                              	<input type="hidden" name="HEADER[CIF]" id="cif" value="<?= $sess['CIF']?>" />
                         	</td>
                        </tr>
                        <tr>
                            <td>CIF (Rp)</td>
                            <td>
                            	<input type="text" name="cifrpur" id="cifrpur" value="<?= $this->fungsi->FormatRupiah($sess['CIFRP'],0);?>" class="mtext" onkeypress="EditHarga('ndpbm;valuta;cif;fob;freight;asuransi;kodeharga;tambahan;diskon;kode_asuransi;cifrp;nilaiinvoice',this.form.id)" readonly >
                                <input type="hidden" name="HEADER[CIFRP]" id="cifrp"  value="<?= $sess['CIFRP'];?>" />
                          	</td>
                        </tr>
                        <tr>
                            <td>Bruto </td>
                            <td><input type="text" name="BRUTOUR" id="BRUTOUR" value="<?= $this->fungsi->FormatRupiah($sess['BRUTO'],2); ?>" class="mtext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('BRUTO',this.value,2);"  <?=$readonly?>/>&nbsp; Kilogram (KGM)
                            <input type="hidden" name="HEADER[BRUTO]" id="BRUTO" value="<?= $sess['BRUTO']?>" /></td>
                        </tr>
                        <tr>
                            <td class="rowheight">Netto</td>
                            <td><?= $this->fungsi->FormatRupiah($sess['JUM_NETTO'],4); ?>&nbsp; Kilogram (KGM)<input type="hidden" name="HEADER[NETTO]" value="<?= $sess['JUM_NETTO']; ?>" class="text"/></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Otomatis terisi dari jumlah total Netto Detil Barang</td>
                        </tr>
                        <tr>
                            <td>PNBP</td>
                            <td><input type="text" name="NILAI_BEBANUR" id="NILAI_BEBANUR" value="<?php if($sess['KODE_FASILITAS']=="0"){ echo $this->fungsi->FormatRupiah($sess['PGT_PNBP'],2);}elseif($sess['KODE_FASILITAS']==2){echo $this->fungsi->FormatRupiah($sess['PGT_PNBP_DIT'],2);} ?>" class="sstext" format="angka" onkeyup="this.value = ThausandSeperator('NILAI_BEBAN',this.value,2);" <?=$readonly?>/>
                            
                            <input type="hidden" name="BEBAN[NILAI_BEBAN]" id="NILAI_BEBAN" value="<?php if($sess['KODE_FASILITAS']=="0"){ echo $this->fungsi->FormatRupiah($sess['PGT_PNBP'],2);}elseif($sess['KODE_FASILITAS']==2){echo $this->fungsi->FormatRupiah($sess['PGT_PNBP_DIT'],2);} ?>" class="sstext"  wajib="yes" format="angka"/>
                            
                            <input type="hidden" name="BEBAN[KODE_BEBAN]" id="KODE_BEBAN" value="8" class="sstext" />&nbsp; <?= form_dropdown('BEBAN[KODE_FASILITAS]', $kode_pnbp, $sess['KODE_FASILITAS'], 'id="KODE_FASILITAS" class="stext" '.$disabled); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue"><b>DATA PENANDATANGAN DOKUMEN</b></h5></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="HEADER[NAMA_TTD]" id="nama_ttd" value="<?= $sess['NAMA_TTD']; ?>" class="mtext"  <?=$readonly?>/></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="HEADER[KOTA_TTD]" id="tempat_ttd" value="<?= $sess['KOTA_TTD']; ?>" class="mtext"  <?=$readonly?>/></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><input type="text" name="HEADER[TANGGAL_TTD]" id="tanggal_ttd" onfocus="ShowDP('tanggal_ttd');" onclick="ShowDP('tanggal_ttd');" value="<?php if($act=="save") echo date("Y-m-d"); else echo $sess['TANGGAL_TTD']; ?>" class="stext date"  <?=$disabled?>/>&nbsp;YYYY-MM-DD</td>
                        </tr>
                    </table>
             	</td>
          	</tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <?php if(!$priview){?>
            <tr>
                <td colspan="2">
                	<a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="save_header('#fbc281');"><i class="icon-save"></i>&nbsp;<?=ucwords($act)?></a>&nbsp;
                    <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('fbc281');"><i class="icon-undo"></i>Reset</a>&nbsp;
                    <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                </td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="2"><h4 class="header smaller lighter green"><b>Detil Dokumen</b></h4>
                <?=$DETILPRIVIEW;?></td>
            </tr>
            <?php } ?>
            <input type="hidden" readonly name="HEADER[KODE_ASURANSI]" id="kode_asuransi" value="<?= $sess['KODE_ASURANSI']; ?>" class="sstext" />
            <input type="hidden" readonly name="HEADER[TAMBAHAN]" id="tambahan" value="<?= $sess['TAMBAHAN']; ?>" class="sstext" />
            <input type="hidden" readonly name="HEADER[DISKON]" id="diskon" value="<?= $sess['DISKON']; ?>" class="sstext" />
            <input type="hidden" readonly name="HEADER[KODE_HARGA]" id="kodeharga" value="<?= $sess['KODE_HARGA']; ?>" class="sstext" />
            <input type="hidden" readonly name="HEADER[NILAI_INVOICE]" id="nilaiinvoice" value="<?= $sess['NILAI_INVOICE']; ?>" class="sstext" />
    	</table>
	</form>
</span> 
<script>
	$(function(){FormReady();})
	$(document).ready(function(){
		jkwaktu();
	});
	function jkwaktu(){
		if($('#jenis_impor').val()=='2'){
			$('#jangkawaktu').show();
		}else{
			$('#jangkawaktu').hide();
		}
	}
	<? if($priview){ ?>
		$('#fbc281 input:visible, #281 select').attr('disabled',true);
	<? } ?>
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);

$func = get_instance();
$id = $IDSSTB;
?>
<div class="header">
	<h3><strong>Form Surat Serah Terima Barang (SSTB)</strong></h3>
</div>
<div class="content">
  <div class="tabbable">
    <ul class="nav nav-tabs" id="myTab">
      <li class="active"><a data-toggle="tab" href="#tabHeader">Data Header</a></li>
      <li><a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
    </ul>
    <div class="tab-content">
      <div id="tabHeader" class="tab-pane active">
        <?php 
			$func->load->model("sstb/header_act");
			$data = $func->header_act->get_sstb($act,$id);
			$this->load->view("pemasukan/sstb/Header", $data);	
		?>
      </div>
      <div id="tabBarang" class="tab-pane">
        <?php
			if ($id) {
				$func->load->model("sstb/detil_act");
				$arrdata = $func->detil_act->detil('barang', 'sstb', $id);
				$list = $this->load->view('view', $arrdata, true);	
			}
			$func->load->model("sstb/barang_act");
			$data = $func->barang_act->get_barang($id);
			if ($id) $data = array_merge($data, array('list' => $list));
        	$this->load->view("pemasukan/sstb/Barang",$data);
		?>
      </div>
    </div>
  </div>
</div>

<script>
$(function(){
})
<?php if($act=='save'){?>
$(".tabbable ul.nav li a").addClass('disabled');
$(".nav-tabs a[data-toggle=tab]").on("click", function(e) { 
  if($(this).hasClass("disabled")) {
    e.preventDefault();
    return false;
  }
});
<?php } ?>			
</script>
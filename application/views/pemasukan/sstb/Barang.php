<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<span id="fbarang_form">
	<?php if(!$list){?>
	<form id="fbarang_" name="fbarangsstb" action="<?= site_url()."/pemasukan/barang/sstb"; ?>" method="post" autocomplete="off" enctype="multipart/form-data" list="<?=  site_url()."/pemasukan/detil/barang/sstb" ?>" class="form-horizontal">
        <input type="hidden" name="act" value="<?= $act; ?>"  />
        <input type="hidden" name="SERIAL" id="SERIAL" value="<?=$DATA["SERIAL"];?>" />
        <input type="hidden" name="SERIAL_HIDE" id="SERIAL_HIDE" value="<?=$DATA["SERIAL"];?>">
        <input type="hidden" name="KODE_SATUAN_TERKECIL" id="KODE_SATUAN_TERKECIL" value="">
        <input type="hidden" name="KODE_SATUANNYA" id="KODE_SATUANNYA" value="">
        <input type="hidden" name="URAIAN_KD_SATUAN" id="URAIAN_KD_SATUAN" value="">
        <input type="hidden" name="SATUAN" id="SATUAN" value="">
        <input type="hidden" name="SERI" id="SERI" value="<?= $seri; ?>" />
        <input type="hidden" name="NOMOR_AJU" id="NOMOR_AJU" value="<?= $IDSSTB; ?>" />
		<h5 class="header smaller lighter green"><b>DETIL BARANG</b></h5>
		<table width="100%">
            <tr>
                <td>Kode Barang</td>
                <td>
                    <input type="text" name="DATA[KODE_BARANG]" id="KODE_BARANG" value="<?= $DATA['KODE_BARANG']; ?>" class="text" url="<?= site_url(); ?>/autocomplete/bc_barang"  wajib="no" urai="URAIAN_BARANG;JNS_BARANG;SATUAN;SATUAN_TERKECIL;KODE_SATUAN;urjenis_satuan;urjenis_satuan" onfocus="Autocomp(this.id, this.form.id);"/>
                    &nbsp;
                    <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('barangsstb','KODE_BARANG;URAIAN_BARANG;JNS_BARANG;KODE_SATUAN;urjenis_satuan;ur_jns_barang','Kode Barang',this.form.id,650,400)" value="...">
                    <input type="hidden" id="UKURAN" name="UKURAN" />
              	</td>
            </tr>
            <tr>
                <td>Uraian Barang</td>
                <td><input type="text" name="DATA[URAIAN_BARANG]" id="URAIAN_BARANG" class="text" wajib="no" value="<?= $DATA['URAIAN_BARANG']; ?>" /></td>
            </tr>
            <tr>
                <td>Jenis Barang</td>
                <td>
                	<input type="text" id="ur_jns_barang" class="text" value="<?=$DATA["UR_JENIS_BARANG"]?>"/>
                	<input type="hidden" name="DATA[JNS_BARANG]" id="JNS_BARANG" value="<?= $DATA['JNS_BARANG']; ?>"  /></td>
            </tr>
            <tr>
                <td>Kode Satuan</td>
                <td><input type="text" name="DATA[KODE_SATUAN]" id="KODE_SATUAN" url="<?= site_url(); ?>/autocomplete/satuan" class="sstext" wajib="yes" value="<?= $DATA['KODE_SATUAN']; ?>" urai="urjenis_satuan;" maxlength="3" readonly/>
                &nbsp;
                <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('satuan','KODE_SATUAN;urjenis_satuan','Kode Satuan',this.form.id,650,400,'SATUAN;SATUAN_TERKECIL;')" value="...">
                <span id="urjenis_satuan">
                <?= $DATA['KODE_SATUANUR']; ?>
                </span></td>
            </tr>
            <tr>
                <td>Jumlah</td>
                <td>
                	<input type="text" id="UR_JUMLAH_SATUAN" class="text" wajib="no"  value="<?= $this->fungsi->FormatRupiah($DATA['JUMLAH_SATUAN'],2); ?>" onkeyup="this.value = ThausandSeperator('JUMLAH_SATUAN',this.value,2);"/>
                    <input type="hidden" id="JUMLAH_SATUAN" name="DATA[JUMLAH_SATUAN]" value="<?= $DATA['JUMLAH_SATUAN']; ?>" />
              	</td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
            	<td>&nbsp;</td>
                <td>
                	<a href="javascript:void(0);" class="btn btn-sm btn-success" id="ok_" onclick="save_detil('#fbarang_','msgbarang_');"> 
                    	<i class="icon-save"></i><?=$act; ?>
					</a>&nbsp; 
                    <a href="javascript:;" class="btn btn-sm btn-warning" id="cancel_" onclick="cancel('fbarang_');"> 
                    	<i class="icon-undo"></i>&nbsp;Reset 
                   	</a>
                  	<span class="msgbarang_" style="margin-left:20px">&nbsp;</span>
                </td>
            </tr>
        </table>
	</form>
    <?php } ?>
</span>
<?php 
if($edit){
	echo '<h5 class="header smaller lighter green"><b>&nbsp;</b></h5>';
}
?>
<?php if (!$edit) { ?>
    <div id="fbarang_list"><?= $list ?></div>
<?php } ?>
<script>
$(function(){FormReady();})
</script>
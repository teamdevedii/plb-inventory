<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
$uri = $this->uri->segment(1);
if($uri=="pemasukan") $DOK = "MASUK";
else $DOK = "KELUAR";
?>
<span id="DivHeaderForm">
	<form id="fsstb" name="fsstb" action="<?= site_url()."/pemasukan/set_sstb"; ?>" method="post" class="form-horizontal"  autocomplete="off">
        <input type="hidden" name="act" id="act" value="<?= $act;?>" />
        <input type="hidden" name="STATUS_DOK" id="STATUS_DOK" value="<?= $DATA["STATUS_DOK"];?>" />
        <input type="hidden" name="IDSSTB" id="IDSSTB" value="<?=$DATA["IDSSTB"];?>" />
        <input type="hidden" name="IDSSTB_HIDE" id="IDSSTB_HIDE" value="<?=$DATA["IDSSTB"];?>">
        <input type="hidden" name="IDSSTB" id="IDSSTB" value="<?=$id;?>" />
        <input type="hidden" name="DATA[TIPE_DOK]" id="TIPE_DOK" value="<?=$DOK;?>" />
        <table width="100%" border="0">
            <tr>
                <td width="45%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td width="29%">Nomor Dokumen</td>
                            <td width="71%"><input type="text" name="DATA[NOMOR_SURAT]" id="NOMOR_SURAT" class="text" value="<?=$DATA["NOMOR_SURAT"]?>" wajib="yes" />
                        </tr>
                        <tr>
                            <td>Tanggal Dokumen</td>
                            <td><input type="text" name="DATA[TANGGAL_SURAT]" id="TANGGAL_SURAT" value="<?= $DATA['TANGGAL_SURAT']?>" onfocus="ShowDP('TANGGAL_SURAT')" class="stext date" wajib="yes">
                        &nbsp;YYYY-MM-DD</td>
                        </tr>
                  	</table>
            	</td>
             	<td width="55%">
                	<table width="80%">
                    	<tr>
                            <td width="36%">Nomor Pendaftaran</td>
                            <td width="64%"><input type="text" name="DATADOK[NOMOR_PENDAFTARAN]" id="NOMOR_PENDAFTARAN" class="text" maxlength="8" value="<?= $DATA['NOMOR_PENDAFTARAN']; ?>" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal Pendaftaran</td>
                            <td><input type="text" name="DATADOK[TANGGAL_PENDAFTARAN]" id="TANGGAL_PENDAFTARAN" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $DATA['TANGGAL_PENDAFTARAN']; ?>" />
                            &nbsp;YYYY-MM-DD</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <h4 class="header smaller lighter green"></h4>
        <table width="100%" border="0">
        	<tr>
        		<td width="45%" valign="top">
        			<table width="100%" border="0">
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Data Pengirim:</h5></td>
                        </tr>
                        <tr>
                            <td width="25%">NPWP</td>
                            <td width="75%">
                            	<?= form_dropdown('DATA[KODE_ID_PENGIRIM]', $kode_id, $DATA['KODE_ID_PENGIRIM'], 'id="kode_id_pengirim" class="sstext" '); ?> 
                                <input type="text" name="DATA[NPWP_PERUSAHAAN_PENGIRIM]" id="id_trader" value="<?=$DATA['NPWP_PERUSAHAAN_PENGIRIM']?$this->fungsi->FORMATNPWP($DATA['NPWP_PERUSAHAAN_PENGIRIM']):''; ?>" class="ltext" wajib="yes"/>
                       		</td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="DATA[NAMA_PERUSAHAAN_PENGIRIM]" id="NAMA_PERUSAHAAN_PENGIRIM" value="<?= $DATA['NAMA_PERUSAHAAN_PENGIRIM']; ?>" class="mtext" maxlength="50" wajib="yes" /></td>
                        </tr>
                        <tr>
                            <td class="top">Alamat </td>
                            <td><textarea name="DATA[ALAMAT_PERUSAHAAN_PENGIRIM]" id="ALAMAT_PERUSAHAAN_PENGIRIM" wajib="yes" class="mtext"><?= $DATA['ALAMAT_PERUSAHAAN_PENGIRIM']; ?>
                            </textarea></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Pengiriman Barang/Stuffing dilakukan:</h5></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="DATA[LOKASI_PENGIRIMAN]" id="LOKASI_PENGIRIMAN" value="<?= $DATA['LOKASI_PENGIRIMAN']; ?>" class="mtext" maxlength="50" wajib="yes" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><input type="text" name="DATA[TANGGAL_PENGIRIMAN]" id="TANGGAL_PENGIRIMAN" value="<?= $DATA['TANGGAL_PENGIRIMAN']?>" onfocus="ShowDP('TANGGAL_PENGIRIMAN')" class="stext date" wajib="yes">
                        &nbsp;YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td>Jumlah Kemasan</td>
                            <td>
                                <input type="text" name="JUMLAH_KEMASANUR" id="JUMLAH_KEMASANUR" class="ltext" size="20" value="<?= $this->fungsi->FormatRupiah($DATA['JUMLAH_KEMASAN'],0); ?>" onkeyup="this.value = ThausandSeperator('JUMLAH_KEMASAN',this.value,2);" wajib="yes"/>
                                <input type="hidden" name="DATA[JUMLAH_KEMASAN]" id="JUMLAH_KEMASAN" value="<?= $DATA['JUMLAH_KEMASAN']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Kemasan</td>
                            <td>
                                <input type="text" name="DATA[KODE_KEMASAN]" wajib="yes" id="KODE_KEMASAN" value="<?= $DATA['KODE_KEMASAN']; ?>" url="<?= site_url(); ?>/autocomplete/kemasan" class="sstext" urai="urjenis_kemasan;" onfocus="Autocomp(this.id);"/>
                                &nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary mousedown" onclick="tb_search('kemasan','KODE_KEMASAN;urjenis_kemasan','Kode Kemasan',this.form.id,650,400)" value="...">
                                <span id="urjenis_kemasan"><?= $DATA['KODE_KEMASAN']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama Petugas</td>
                            <td><input type="text" name="DATA[NAMA_PETUGAS]" id="NAMA_PETUGAS" value="<?= $DATA['NAMA_PETUGAS']; ?>" class="mtext" maxlength="50" /></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Penandatangan Pengirim:</h5></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="DATA[NAMA_TTD_PENGIRIM]" id="NAMA_TTD_PENGIRIM" value="<?= $DATA['NAMA_TTD_PENGIRIM']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td><input type="text" name="DATA[JABATAN_TTD_PENGIRIM]" id="JABATAN_TTD_PENGIRIM" value="<?= $DATA['JABATAN_TTD_PENGIRIM']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="DATA[KOTA_TTD_PENGIRIM]" id="KOTA_TTD_PENGIRIM" value="<?= $DATA['KOTA_TTD_PENGIRIM']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><input type="text" name="DATA[TANGGAL_TTD_PENGIRIM]" id="TANGGAL_TTD_PENGIRIM" onfocus="ShowDP('TANGGAL_TTD_PENGIRIM');" onclick="ShowDP('TANGGAL_TTD_PENGIRIM');" value="<?php if($act=="save") echo date("Y-m-d"); else echo $DATA['TANGGAL_TTD_PENGIRIM']; ?>" class="stext date" />
                        &nbsp;YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Catatan Pengiriman Barang:</h5></td>
                        </tr>
                        <tr>
                            <td width="29%">KPBC </td>
                            <td width="69%">
                                <input type="text"  name="DATA[KPBC_PENGIRIM]" id="kpbc_daftar" value="<?= $DATA['KPBC_PENGIRIM']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urkt;" wajib="no" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka"/>
                                &nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('kpbc','kpbc_daftar;urkt','Kode Kpbc',this.form.id,650,400)" value="...">
                                &nbsp;<span id="urkt"><?= $DATA['URAIAN_KPBC']==''?$URKANTOR_TUJUAN:$DATA['URAIAN_KPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Hasil Pemeriksaan</td>
                            <td valign="top">
                                <input type="radio" name="DATA[HASIL_PERIKSA]" id="HASIL_PERIKSA" value="1" <?php echo ($DATA['HASIL_PERIKSA'] == '1') ?  "checked" : "" ;  ?>>
                                Sesuai&nbsp;&nbsp;
                                <input type="radio" name="DATA[HASIL_PERIKSA]" id="HASIL_PERIKSA" value="2" <?php echo ($DATA['HASIL_PERIKSA'] == '2') ?  "checked" : "" ;  ?>>
                                Tidak Sesuai
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor Segel</td>
                            <td><input type="text" name="DATA[NOMOR_SEGEL]" id="NOMOR_SEGEL" value="<?= $DATA['NOMOR_SEGEL']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal Segel</td>
                            <td>
                                <input type="text" name="DATA[TANGGAL_SEGEL]" id="TANGGAL_SEGEL" value="<?= $DATA['TANGGAL_SEGEL']?>" onfocus="ShowDP('TANGGAL_SEGEL')" class="stext date">
                                &nbsp;YYYY-MM-DD
                            </td>
                        </tr>
                        <tr>
                            <td>Nama Pemeriksa</td>
                            <td><input type="text" name="DATA[NAMA_PEMERIKSA]" id="NAMA_PEMERIKSA" value="<?= $DATA['NAMA_PEMERIKSA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>NIP Pemeriksa</td>
                            <td><input type="text" name="DATA[NIP_PEMERIKSA]" id="NIP_PEMERIKSA" value="<?= $DATA['NIP_PEMERIKSA']; ?>" class="mtext" /></td>
                        </tr>
         			</table>
        		</td>
          		<td width="55%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Data Penerima:</h5></td>
                        </tr>
                        <tr>
                            <td width="25%">NPWP</td>
                            <td width="75%">
                            	<?= form_dropdown('DATA[KODE_ID_PENERIMA]', $kode_id, $DATA['KODE_ID_PENERIMA'], 'id="kode_id_penerima" class="sstext" '); ?> 
                                <input type="text" name="DATA[NPWP_PERUSAHAAN_PENERIMA]" id="npwp_penerima" value="<?=$DATA['NPWP_PERUSAHAAN_PENERIMA']?$this->fungsi->FORMATNPWP($DATA['NPWP_PERUSAHAAN_PENERIMA']):''; ?>" class="ltext" wajib="yes" />
                            </td>
                        </tr>
                        <tr>
                            <td width="29%">Nama </td>
                            <td width="69%">
                                <input type="text" name="DATA[NAMA_PERUSAHAAN_PENERIMA]" id="nama_penerima" value="<?= $DATA['NAMA_PERUSAHAAN_PENERIMA']; ?>" url="<?= site_url(); ?>/autocomplete/pemasok" wajib="yes" onfocus="Autocomp(this.id)" urai="nama_partner;alamat_partner" class="mtext" />
                                &nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pemasok','kode_id_penerima;npwp_penerima;nama_penerima;alamat_penerima','Pemasok','fsstb',700,500)" value="...">
                            </td>
                        </tr>
                        <tr>
                            <td class="top">Alamat </td>
                            <td>
                                <textarea name="DATA[ALAMAT_PERUSAHAAN_PENERIMA]" id="alamat_penerima" wajib="yes" class="mtext"><?= $DATA['ALAMAT_PERUSAHAAN_PENERIMA']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Barang telah diterima:</h5></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="DATA[LOKASI_PENERIMA]" id="LOKASI_PENERIMA" value="<?= $DATA['LOKASI_PENERIMA']; ?>" class="mtext" maxlength="50" wajib="no" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><input type="text" name="DATA[TANGGAL_PENERIMA]" id="TANGGAL_PENERIMA" value="<?= $DATA['TANGGAL_PENERIMA']?>" onfocus="ShowDP('TANGGAL_PENERIMA')" class="stext date">
                            &nbsp;YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td>Kondisi</td>
                            <td valign="top">
                                <input type="radio" name="DATA[KONDISI_TERIMA]" id="HASIL_PERIKSA" value="1" <?php echo ($DATA['KONDISI_TERIMA'] == '1') ?  "checked" : "" ;  ?>>
                                Sesuai&nbsp;&nbsp;
                                <input type="radio" name="DATA[KONDISI_TERIMA]" id="KONDISI_TERIMA" value="2" <?php echo ($DATA['KONDISI_TERIMA'] == '2') ?  "checked" : "" ;  ?>>
                                Tidak Sesuai
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Penandatangan Penerima:</h5></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="DATA[NAMA_TTD_PENERIMA]" id="NAMA_TTD_PENERIMA" value="<?= $DATA['NAMA_TTD_PENERIMA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td><input type="text" name="DATA[JABATAN_TTD_PENERIMA]" id="JABATAN_TTD_PENERIMA" value="<?= $DATA['JABATAN_TTD_PENERIMA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="DATA[KOTA_TTD_PENERIMA]" id="KOTA_TTD_PENERIMA" value="<?= $DATA['KOTA_TTD_PENERIMA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>
                                <input type="text" name="DATA[TANGGAL_TTD_PENERIMA]" id="TANGGAL_TTD_PENERIMA" onfocus="ShowDP('TANGGAL_TTD_PENERIMA');" onclick="ShowDP('TANGGAL_TTD_PENERIMA');" value="<?php if($act=="save") echo date("Y-m-d"); else echo $DATA['TANGGAL_TTD_PENERIMA']; ?>" class="stext date" />
                                &nbsp;YYYY-MM-DD
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Catatan Penerimaan Barang:</h5></td>
                        <tr>
                            <td width="29%">KPBC </td>
                            <td width="69%">
                                <input type="text"  name="DATA[KPBC_PENERIMA]" id="kpbc_daftar_penerima" value="<?= $DATA['KPBC_PENERIMA']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" urai="urkt;" wajib="no" onfocus="Autocomp(this.id)" class="stext date" maxlength="6" format="angka"/>
                                &nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('kpbc','kpbc_daftar_penerima;urktpenerima','Kode Kpbc',this.form.id,650,400)" value="...">
                                &nbsp;<span id="urktpenerima"><?= $DATA['URAIAN_KPBC']==''?$URKANTOR_TUJUAN:$DATA['URAIAN_KPBC']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Segel</td>
                            <td valign="top">
                                <input type="radio" name="DATA[SEGEL_PENERIMA]" id="HASIL_PERIKSA" value="1" <?php echo ($DATA['SEGEL_PENERIMA'] == '1') ?  "checked" : "" ;  ?>>
                                Utuh&nbsp;&nbsp;
                                <input type="radio" name="DATA[SEGEL_PENERIMA]" id="SEGEL_PENERIMA" value="2" <?php echo ($DATA['SEGEL_PENERIMA'] == '2') ?  "checked" : "" ;  ?>>
                                Rusak
                                &nbsp;&nbsp;
                                <input type="radio" name="DATA[SEGEL_PENERIMA]" id="SEGEL_PENERIMA" value="3" <?php echo ($DATA['SEGEL_PENERIMA'] == '3') ?  "checked" : "" ;  ?>>
                                Tidak Sesuai 
                            </td>
                        </tr>
                        <tr>
                            <td>Nama Pemeriksa</td>
                            <td><input type="text" name="DATA[NAMA_PEMERIKSA_PENERIMA]" id="NAMA_PEMERIKSA_PENERIMA" value="<?= $DATA['NAMA_PEMERIKSA_PENERIMA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>NIP Pemeriksa</td>
                            <td><input type="text" name="DATA[NIP_PEMERIKSA_PENERIMA]" id="NIP_PEMERIKSA_PENERIMA" value="<?= $DATA['NIP_PEMERIKSA_PENERIMA']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight"><h5 class="smaller lighter blue">Pejabat Pemeriksa Dokumen Ekspor:</h5></td>
                        </tr>
                        <tr>
                            <td>Nama Pemeriksa</td>
                            <td><input type="text" name="DATA[NAMA_PEMERIKSA_EKS]" id="NAMA_PEMERIKSA_EKS" value="<?= $DATA['NAMA_PEMERIKSA_EKS']; ?>" class="mtext" /></td>
                        </tr>
                        <tr>
                            <td>NIP Pemeriksa</td>
                            <td><input type="text" name="DATA[NIP_PEMERIKSA_EKS]" id="NIP_PEMERIKSA_EKS" value="<?= $DATA['NIP_PEMERIKSA_EKS']; ?>" class="mtext" /></td>
                        </tr>
                    </table>
          		</td>
			</tr>
        	<tr>
        		<td colspan="2">&nbsp;</td>
      		</tr>
        	<? if(!$priview){?>
        	<tr>
                <td>
                    <a href="javascript:void(0);" class="btn btn-sm btn-success" id="ok_" onclick="save_header('#fsstb');">
                        <i class="icon-save"></i>&nbsp;<?= ucwords($act); ?>
                    </a>&nbsp;
                    <a href="javascript:;" class="btn btn-sm btn-warning" id="cancel_" onclick="cancel('fsstb');">
                    	<i class="icon-undo"></i>Reset
                   	</a>
                    <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
              	</td>
                <td>&nbsp;</td>
        	</tr>
			<? }else{ ?>
            <tr>
                <td colspan="2"><h4 class="header smaller lighter green">Detil Barang</h4>
                <?=$DETILPRIVIEW;?></td>
            </tr>
            <? } ?>
            </table>
		</form>
</span> 
<script>
	$(document).ready(function(){
		jkwaktu();
	});
	$(function(){FormReady();})
	function jkwaktu(){
		if($('#jenis_impor').val()=='2'){
			$('#jangkawaktu').show();
		}else{
			$('#jangkawaktu').hide();
		}
	}
</script>
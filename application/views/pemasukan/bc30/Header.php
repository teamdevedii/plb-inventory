<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
if ($this->uri->segment(1) == 'pemasukan') $tipe_dok = "MASUK";
elseif ($this->uri->segment(1) == 'pengeluaran') $tipe_dok = "KELUAR";
?>
<span id="DivHeaderForm">
	<form name="fbc30_" id="fbc30_" action="<?= site_url() . "/pemasukan/bc30"; ?>" method="post" autocomplete="off">
        <input type="hidden" name="act" id="act" value="<?= $act; ?>" />
        <input type="hidden" name="STATUS_DOK" id="STATUS_DOK" value="<?= $sess["STATUS_DOK"]; ?>" />
        <input type="hidden" name="HEADERDOK[NOMOR_AJU]" id="noajudok" value="<?= $aju; ?>" />
        <input type="hidden" name="HEADER[NOMOR_AJU]" id="noaju" value="<?= $aju; ?>" />
        <input type="hidden"  name="DOKUMEN" value="<?= $dokumen ?>"/> 
        <input type="hidden"  name="TEMPAT_ASAL" value="<?= $tmp_asal ?>"/> 
        <input type="hidden"  name="JENIS_BARANG" value="<?= $jns_barang ?>"/> 
        <input type="hidden"  name="HEADER[TIPE_DOK]" value="<?= $tipe_dok ?>"/> 
        <span id="divtmp"></span>
		<table width="100%" border="0">
            <tr>
            	<td width="45%" valign="top">
                	<?php if($act=="save"){ ?>	
                    <table width="100%" border="0">
                    	<tr>
                        	<td  width="28%">Nomor Aju</td>
                            <td  width="71%"><input type="text" name="NOMOR_AJU" id="NOMOR_AJU" class="mtext" value="<?= $sess['NOMOR_AJU']; ?>" maxlength="26" /></td>
                        </tr>
                    </table>
                    <?php } ?>
                    <table width="100%" border="0">
                        <tr>
                            <td width="28%">Kantor Pabean Muat</td>
                            <td width="71%"><input type="text"  name="HEADER[KODE_KPBC]" id="kode_kpbc" class="stext date" value="<?= $sess['KODE_KPBC']; ?>" url="<?= site_url() ?>/autocomplete/kpbc" urai="urkt;" wajib="yes" onfocus="Autocomp(this.id)" <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('kpbc', 'kode_kpbc;urkt', 'Kode Kpbc', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urkt"><?= $sess['URAIAN_KPBC'] == '' ? $URKANTOR_TUJUAN : $sess['URAIAN_KPBC']; ?></span> </td>
                        </tr>
                        <tr>
                            <td>Jenis Ekspor</td>
                            <td><?= form_dropdown('HEADER[JENIS_EKSPOR]', $jenis_ekspor, $sess['JENIS_EKSPOR'], 'id="JENIS_EKSPOR" class="mtext" wajib="yes" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td>Kategori Ekspor</td>
                            <td><?= form_dropdown('HEADER[KATEGORI_EKSPOR]', $kategori_ekspor, $sess['KATEGORI_EKSPOR'], 'id="KATEGORI_EKSPOR" class="mtext" onchange="getPkb(this.value);" wajib="yes" ' . $disabled); ?>&nbsp;<input type="button" name="PKB" id="PKB" class="btn btn-xs btn-primary" onclick="PKBInput('bc30', 'PKB', '<?= $aju; ?>');" value="PKB" <?= $display ?>></td>
                        </tr>
                        <tr>
                            <td>Cara Perdagangan</td>
                            <td><?= form_dropdown('HEADER[CARA_DAGANG]', $cara_dagang, $sess['CARA_DAGANG'], 'id="CARA_DAGANG" class="mtext" wajib="yes" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td>Cara Pembayaran</td>
                            <td><?= form_dropdown('HEADER[CARA_BAYAR]', $cara_bayar, $sess['CARA_BAYAR'], 'id="CARA_BAYAR" class="mtext" wajib="yes" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td>Kode Hanggar</td>
                            <td><input type="text"  name="HEADER[KODE_HANGGAR]" id="KODE_HANGGAR" class="stext date" value="<?= $sess['KODE_HANGGAR']; ?>" url="<?= site_url() ?>/autocomplete/hanggar" urai="URAIAN_HANGGAR;" wajib="yes" onfocus="Autocomp(this.id)" maxlength="9" <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('hanggar', 'KODE_HANGGAR;URAIAN_HANGGAR', 'Kode Hanggar', this.form.id, 650, 400)" value="..." <?= $display ?>></td>
                        </tr>
                        <tr>
                            <td>Uraian Hanggar</td>
                            <td><input type="text"  name="URAIAN_HANGGAR" id="URAIAN_HANGGAR" class="mtext" value="<?= $sess['URAIAN_HANGGAR']; ?>" wajib="yes" maxlength="50" <?= $disabled ?>/></td>
                        </tr>
                    </table>
				</td>
                <td width="55%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                        	<td colspan="4" valign="top">
                        		<h5 class="header smaller lighter red"><i>Informasi Bea dan Cukai</i></h5>
                                <table width="80%">
                                    <tr>
                                        <td>Nomor Pendaftaran</td>
                                        <td>
                                        <?php if ($sess['STATUS_DOK'] == "LENGKAP") { ?>
                                        <input type="hidden" name="HEADERDOK[NOMOR_PENDAFTARAN]" id="NOMOR_PENDAFTARAN" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6" <?= $disabled ?>/>
                                        <input type="text" name="HEADERDOK[NOMOR_PENDAFTARAN_EDIT]" id="NOMOR_PENDAFTARAN_EDIT" class="stext date" value="<?= $sess['NOMOR_PENDAFTARAN']; ?>" maxlength="6" <?= $disabled ?>/>
                                        <?php } else { ?>
                                        <input type="text" disabled="disabled" class="stext date" maxlength="6"  value="<?= $sess['NOMOR_PENDAFTARAN']; ?>"/>
                                        <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Pendaftaran</td>
                                        <td>
                                        <?php if ($sess['STATUS_DOK'] == "LENGKAP") { ?>
                                        <input type="hidden" name="HEADERDOK[TANGGAL_PENDAFTARAN]" id="TANGGAL_PENDAFTARAN" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>" <?= $disabled ?>/>
                                        <input type="text" name="HEADERDOK[TANGGAL_PENDAFTARAN_EDIT]" id="TANGGAL_PENDAFTARAN_EDIT" onfocus="ShowDP('TANGGAL_PENDAFTARAN');" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>" <?= $disabled ?>/>
                                        <?php } else { ?>
                                        <input type="text" disabled="disabled" class="stext date" value="<?= $sess['TANGGAL_PENDAFTARAN']; ?>"/>
                                        <?php } ?>&nbsp; YYYY-MM-DD
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Persetujuan Pemasukan</td>
                                        <td>
                                        <?php if ($sess['STATUS_DOK'] == "LENGKAP") { ?>
                                        <input type="text" name="HEADERDOK[NOMOR_DOK_PABEAN]" id="NOMOR_DOK_PABEAN" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>" maxlength="30"  <?= $disabled ?>/><?php } else { ?><input type="text" disabled="disabled" class="text" value="<?= $sess['NOMOR_DOK_PABEAN']; ?>"/><?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Persetujuan Pemasukan</td>
                                        <td>
                                        <?php if ($sess['STATUS_DOK'] == "LENGKAP") { ?>
                                        <input type="text" name="HEADERDOK[TANGGAL_DOK_PABEAN]" id="TANGGAL_DOK_PABEAN" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN'] == '0000-00-00') ? '' : $sess['TANGGAL_DOK_PABEAN']; ?>" onfocus="ShowDP('TANGGAL_DOK_PABEAN');"/>
                                        <?php } else { ?><input type="text" disabled="disabled" class="stext date" value="<?= ($sess['TANGGAL_DOK_PABEAN'] == '0000-00-00') ? '' : $sess['TANGGAL_DOK_PABEAN']; ?>"/><?php } ?>&nbsp; YYYY-MM-DD
                                        </td>
                                    </tr>
                                </table>
                        	</td>
                        </tr>
                    </table>
				</td>
			</tr>
		</table>
		<h5 class="header smaller lighter green"><b>DATA PEMBERITAHUAN</b></h5>
        <table width="100%" border="0">
        	<tr>
        		<td width="45%" valign="top">
        			<table width="100%" border="0">
                        <tr>
                            <td colspan="3" class="rowheight">
                            <h5 class="smaller lighter blue"><b>EKSPORTIR TPB</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td width="28%">Identitas </td>
                            <td  width="71%">
                            <?= form_dropdown('HEADER[KODE_ID_TRADER]', $kode_id, $sess['KODE_ID_TRADER'], 'id="kode_id_trader" class="sstext" ' . $disabled); ?>
                            <input type="hidden" name="HEADER[KODE_TRADER]" id="KODE_TRADER" value="<?= $sess['KODE_TRADER']; ?>" class="stext" wajib="yes" maxlength="15"  <?= $disabled ?>/>
                            <input type="text" name="HEADER[ID_TRADER]" id="ID_TRADER" value="<?= $this->fungsi->FORMATNPWP($sess['ID_TRADER']); ?>" class="ltext" wajib="yes" maxlength="15"  <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td class="top">Nama </td>
                            <td><input type="text" name="HEADER[NAMA_TRADER]" id="NAMA_TRADER" value="<?= $sess['NAMA_TRADER']; ?>" url="<?= site_url(); ?>/autocomplete/kpbc" class="mtext" wajib="yes" <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td>Alamat </td>
                            <td>
                                <textarea name="HEADER[ALAMAT_TRADER]" id="ALAMAT_TRADER" class="mtext" wajib="yes"  onkeyup="limitChars(this.id, 70, 'limitAlamatTrader')"  <?= $disabled ?>><?= $sess['ALAMAT_TRADER']; ?></textarea>
                                <div id="limitAlamatTrader"></div>
                        	</td>
                        </tr>
                        <tr>
                            <td class="top">NIPER </td>
                            <td><input type="text" name="HEADER[NIPER]" id="NIPER" value="<?= $sess['NIPER']; ?>" maxlength="4" class="mtext"  <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td class="top">Status </td>
                            <td><?= form_dropdown('HEADER[STATUS_TRADER]', $status, $sess['STATUS_TRADER'], 'id="status" class="mtext" wajib="yes" ' . $disabled, FALSE); ?></td>
                        </tr>
                        <tr>
                            <td class="top">Nomor TDP</td>
                            <td><input type="text" name="HEADER[NOMOR_TDP]" id="NOMOR_TDP" value="<?= $sess['NOMOR_TDP']; ?>" maxlength="30" class="mtext" wajib="yes" <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td class="top">Tanggal TDP</td>
                            <td><input type="text" name="HEADER[TANGGAL_TDP]" id="TANGGAL_TDP" value="<?= $sess['TANGGAL_TDP']; ?>" maxlength="20" class="stext date" wajib="yes" onfocus="ShowDP('TANGGAL_TDP');"  <?= $disabled ?>/>&nbsp; YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td class="top">&nbsp;</td>
                            <td>
                                <span class="checkboxWrapper">
                                    <span id="QQcheckBox"></span>
                                    <div class="checkboxOverlay" onclick="QQInput('bc30', 'QQ');">QQ (Data QQ Identitor)</div>
                                </span>
                                <!--<input type="checkbox" disabled="disabled" name="QQCheck" id="QQCheck" value="QQ" onclick="QQInput('bc30','QQ');" />-->
                                <input type="hidden" name="HEADER[KODE_ID_QQ]" id="KODE_ID_QQ" value="<?= $sess['KODE_ID_QQ'] ? $sess['KODE_ID_QQ'] : $sess['KODE_ID_TRADER']; ?>" />
                                <input type="hidden" name="HEADER[ID_QQ]" id="ID_QQ" value="<?= $sess['ID_QQ'] ? $sess['ID_QQ'] : $sess['ID_TRADER']; ?>" />
                                <input type="hidden" name="HEADER[NAMA_QQ]" id="NAMA_QQ" value="<?= $sess['NAMA_QQ'] ? $sess['NAMA_QQ'] : $sess['NAMA_TRADER']; ?>" />
                                <input type="hidden" name="HEADER[ALAMAT_QQ]" id="ALAMAT_QQ" value="<?= $sess['ALAMAT_QQ'] ? $sess['ALAMAT_QQ'] : $sess['ALAMAT_TRADER']; ?>" />
                                <input type="hidden" name="HEADER[NIPER_QQ]" id="NIPER_QQ" value="<?= $sess['NIPER_QQ'] ? $sess['NIPER_QQ'] : ''; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA PENGANGKUTAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td class="top">Cara Pengangkutan</td>
                            <td><?= form_dropdown('HEADER[MODA]', $cara_angkut, $sess['MODA'], 'id="status" class="mtext" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td>Nama Angkut </td>
                            <td><input type="text" name="HEADER[NAMA_ANGKUT]" id="nama_angkut" value="<?= $sess['NAMA_ANGKUT']; ?>"  maxlength="20" class="mtext" wajib="yes" <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td>Nomor Pengangkut </td>
                            <td><input type="text" name="HEADER[NOMOR_ANGKUT]" id="nomor_angkut" value="<?= $sess['NOMOR_ANGKUT']; ?>" class="stext date" maxlength="7" wajib="yes" <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td>Bendera Kapal </td>
                            <td>
                            	<input type="text" name="HEADER[BENDERA]" id="bendera" value="<?= $sess['BENDERA']; ?>" class="stext date" url="<?= site_url(); ?>/autocomplete/negara" onfocus="Autocomp(this.id)" urai="urbendera;"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('negara', 'bendera;urbendera', 'Kode Negara', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;
                            	<span id="urbendera"><?= $sess['BENDERAUR']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="top">Tanggal Perkiraan Ekspor</td>
                            <td><input type="text" name="HEADER[TANGGAL_EKSPOR]" id="TANGGAL_EKSPOR" value="<?= $sess['TANGGAL_EKSPOR']; ?>" maxlength="20" class="stext date" wajib="yes" onfocus="ShowDP('TANGGAL_EKSPOR');" <?= $disabled ?>/>&nbsp; YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA PERDAGANGAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Daerah Asal Barang</td>
                            <td>
                            	<input type="text" name="HEADER[PROPINSI_BARANG]" id="PROPINSI_BARANG" value="<?= $sess['PROPINSI_BARANG']; ?>" class="stext date" url="<?= site_url(); ?>/autocomplete/daerah" onfocus="Autocomp(this.id)" wajib="yes" urai="urdaerah;"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('daerah', 'PROPINSI_BARANG;urdaerah', 'Kode Daerah', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;
                            	<span id="urdaerah"><?= $sess['URAIAN_DAERAH_ASL'] == '' ? $URAIAN_DAERAH : $sess['URAIAN_DAERAH_ASL']; ?></span>
                        	</td>
                        </tr>
                        <tr>
                            <td width="139">Negara Tujuan Ekspor</td>
                            <td>
                                <input type="text" name="HEADER[NEGARA_TUJUAN]" id="NEGARA_TUJUAN" value="<?= $sess['NEGARA_TUJUAN']; ?>" class="stext date" url="<?= site_url(); ?>/autocomplete/negara" onfocus="Autocomp(this.id)" wajib="yes" urai="urnegaratuj;"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('negara', 'NEGARA_TUJUAN;urnegaratuj', 'Kode Negara', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;
                                <span id="urnegaratuj"><?= $sess['URAIAN_NEG_TUJUAN'] == '' ? $URAIAN_NEGARA : $sess['URAIAN_NEG_TUJUAN']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Cara Penyerahan Barang</td>
                            <td><?= form_dropdown('HEADER[CARA_PENYERAHAN_BARANG]', $cara_penyerahan, $sess['CARA_PENYERAHAN_BARANG'], 'id="CARA_PENYERAHAN_BARANG" class="mtext" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA TRANSAKSI EKSPOR</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Jenis Valuta Asing</td>
                            <td>
                                <input type="text" name="HEADER[KODE_VALUTA]" id="valuta" value="<?= $sess['KODE_VALUTA']; ?>" url="<?= site_url(); ?>/autocomplete/valuta" onfocus="Autocomp(this.id)" urai="urvaluta;" class="stext date" wajib="yes"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('valuta', 'valuta;urvaluta', 'Kode Valuta', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;
                                <span id="urvaluta"><?= $sess['URAIAN_VALUTA'] == '' ? $urvaluta : $sess['URAIAN_VALUTA']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Freight</td>
                            <td>
                                <input type="text" name="FREIGHT_CONV" id="FREIGHT_CONV" value="<?= $this->fungsi->FormatRupiah($sess['FREIGHT'], 4); ?>" class="mtext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('FREIGHT', this.value, 4);" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                                <input type="hidden" name="HEADER[FREIGHT]" id="FREIGHT" value="<?= $sess['FREIGHT']; ?>" class="mtext" wajib="yes" format="angka" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Asuransi(LN/DN)</td>
                            <td>
								<?= form_dropdown('HEADER[KODE_ASURANSI]', $jenis_asuransi, $sess['KODE_ASURANSI'], 'id="KODE_ASURANSI" class="sstext" wajib="yes" ' . $disabled); ?>&nbsp;<input type="text" name="ASURANSI_CONV" id="ASURANSI_CONV" value="<?= $this->fungsi->FormatRupiah($sess['ASURANSI'], 4); ?>" class="ltext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('ASURANSI', this.value, 4);" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                                <input type="hidden" name="HEADER[ASURANSI]" id="ASURANSI" value="<?= $sess['ASURANSI']; ?>" class="ltext" wajib="yes" format="angka" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">FOB</td>
                            <td>
                                <input type="text" name="FOB_CONV" id="FOB_CONV" value="<?= $this->fungsi->FormatRupiah($sess['FOB'], 4); ?>" class="stext" wajib="yes" format="angka" onkeyup="this.value = ThausandSeperator('FOB', this.value, 4);" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                                <input type="hidden" name="HEADER[FOB]" id="FOB" value="<?= $sess['FOB']; ?>" class="text" wajib="yes" format="angka" onclick="hitungan('bc30', 'hitungan');" <?= $disabled ?>/>
                                <input type="hidden" name="HEADER[KODE_HARGA]" id="KODE_HARGA" value="<?= $sess['KODE_HARGA']; ?>" class="text" />
                                <input type="hidden" name="N_CIF" id="N_CIF" value="" class="text" />
                                <input type="hidden" name="HEADER[NILAI_INVOICE]" id="N_INVOICE" value="" class="text" />
                                &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="hitungan('bc30', 'hitungan');" value="..." <?= $display ?>>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA BARANG</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Komoditi</td>
                            <td><?= form_dropdown('HEADER[KOMODITAS]', $jenis_komoditi, $sess['KOMODITAS'], 'id="KOMODITAS" class="mtext" wajib="yes" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td width="139">Volume (M3) </td>
                            <td>
                            	<input type="text" name="VOLM" id="VOLM" value="<?= $this->fungsi->FormatRupiah($sess['VOLUME'], 4); ?>" onkeyup="this.value = ThausandSeperator('VOLUME', this.value, 4);" class="mtext" format="angka" wajib="yes"  <?= $disabled ?>><input type="hidden" name="HEADER[VOLUME]" id="VOLUME" class="text" value="<?= $sess['VOLUME']; ?>" maxlength="18" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Berat Kotor (Kg) </td>
                            <td>
                                <input type="text" name="BRUT" id="BRUT" value="<?= $this->fungsi->FormatRupiah($sess['BRUTO'], 4); ?>" onkeyup="this.value = ThausandSeperator('BRUTO', this.value, 4);" class="mtext" format="angka" wajib="yes"  <?= $disabled ?>><input type="hidden" name="HEADER[BRUTO]" id="BRUTO" class="text" value="<?= $sess['BRUTO']; ?>" maxlength="18" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td width="139">Berat Bersih (Kg) </td>
                            <td>
                            	<input type="text" name="NETT" id="NETT" value="<?= $this->fungsi->FormatRupiah($sess['NETTO'], 4); ?>" onkeyup="this.value = ThausandSeperator('NETTO', this.value, 4);" class="mtext" format="angka" wajib="yes" readonly><input type="hidden" name="HEADER[NETTO]" id="NETTO" class="text" value="<?= $sess['NETTO']; ?>" maxlength="18" <?= $disabled ?>/>
                            </td>
                        </tr>
					</table>
				</td>
				<td width="55%" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA PENERIMA</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td width="26%">Nama</td>
                            <td width="74%"><input type="text" name="HEADER[NAMA_PEMBELI]" id="NAMA_PEMBELI" value="<?= $sess['NAMA_PEMBELI']; ?>" class="mtext" maxlength="15" wajib="yes" <?= $disabled ?>/>&nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pembeli', 'NAMA_PEMBELI;ALAMAT_PEMBELI;NEGARA_PEMBELI', 'penerima', 'fbc30_', 600, 400)" value="..." <?= $display ?>></td>
                        </tr>
                        <tr>
                            <td valign="top">Alamat</td>
                            <td>
                                <textarea name="HEADER[ALAMAT_PEMBELI]" id="ALAMAT_PEMBELI" class="mtext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatBeli')"  <?= $disabled ?>><?= $sess['ALAMAT_PEMBELI']; ?></textarea>
                                <div id="limitAlamatBeli"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>Negara</td>
                            <td>
                                <input type="text" name="HEADER[NEGARA_PEMBELI]" id="NEGARA_PEMBELI" value="<?= $sess['NEGARA_PEMBELI']; ?>" class="stext date" url="<?= site_url(); ?>/autocomplete/negara" onfocus="Autocomp(this.id)" wajib="yes" urai="negPemb;"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('negara', 'NEGARA_PEMBELI;negPemb', 'Kode Negara', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;
                                <span id="negPemb"><?= $sess['URAIAN_BENDERA'] == '' ? $URAIAN_BENDERA : $sess['URAIAN_BENDERA']; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA PELABUHAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Pelabuhan Muat Asal</td>
                            <td><input type="text" name="HEADER[PELABUHAN_MUAT]" id="PELABUHAN_MUAT" value="<?= $sess['PELABUHAN_MUAT']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan/dalam" onfocus="Autocomp(this.id)" urai="urpelabuhan_muat;" class="stext date" wajib="yes"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pelabuhan', 'PELABUHAN_MUAT;urpelabuhan_muat', 'Kode Pelabuhan', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urpelabuhan_muat"><?= $sess['URAIAN_MUAT'] == '' ? $urpelabuhan_muat : $sess['URAIAN_MUAT']; ?></span></td>
                        </tr>
                        <tr>
                            <td>Pelabuhan Muat Ekspor</td>
                            <td><input type="text" name="HEADER[PELABUHAN_MUAT_EKSPOR]" id="PELABUHAN_MUAT_EKSPOR" value="<?= $sess['PELABUHAN_MUAT_EKSPOR']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan/dalam" onfocus="Autocomp(this.id)" urai="urpelabuhan_muat_eks;" class="stext date" wajib="yes"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pelabuhan', 'PELABUHAN_MUAT_EKSPOR;urpelabuhan_muat_eks', 'Kode Pelabuhan', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urpelabuhan_muat_eks"><?= $sess['URAIAN_MUAT_EKS'] == '' ? $urpelabuhan_muat_eks : $sess['URAIAN_MUAT_EKS']; ?></span></td>
                        </tr>
                        <tr>
                            <td>Pelabuhan Transit LN</td>
                            <td><input type="text" name="HEADER[PELABUHAN_TRANSIT]" id="PELABUHAN_TRANSIT" value="<?= $sess['PELABUHAN_TRANSIT']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan" onfocus="Autocomp(this.id)" urai="urpelabuhan_transit;" class="stext date"   <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pelabuhan', 'PELABUHAN_TRANSIT;urpelabuhan_transit', 'Kode Pelabuhan', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urpelabuhan_transit"><?= $sess['URAIAN_TRANSIT'] == '' ? $urpelabuhan_transit : $sess['URAIAN_TRANSIT']; ?></span></td>
                        </tr>
                        <tr>
                            <td>Pelabuhan Bongkar</td>
                            <td><input type="text" name="HEADER[PELABUHAN_BONGKAR]" id="PELABUHAN_BONGKAR" value="<?= $sess['PELABUHAN_BONGKAR']; ?>" url="<?= site_url(); ?>/autocomplete/pelabuhan" onfocus="Autocomp(this.id)" urai="urpelabuhan_bongkar;" class="stext date" wajib="yes"  <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('pelabuhan', 'PELABUHAN_BONGKAR;urpelabuhan_bongkar', 'Kode Pelabuhan', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urpelabuhan_bongkar"><?= $sess['URAIAN_BONGKAR'] == '' ? $urpelabuhan_bongkar : $sess['URAIAN_BONGKAR']; ?></span></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA TEMPAT PEMERIKSAAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Lokasi Pemeriksaan</td>
                            <td><?= form_dropdown('HEADER[LOKASI_PERIKSA]', $lokasi_pemeriksaan, $sess['LOKASI_PERIKSA'], 'id="LOKASI_PERIKSA" class="mtext" wajib="yes" ' . $disabled); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Pemeriksaan</td>
                            <td><input type="text" name="HEADER[TANGGAL_PERIKSA]" id="TANGGAL_PERIKSA" value="<?= $sess['TANGGAL_PERIKSA']; ?>" class="stext date" maxlength="15"  onfocus="ShowDP('TANGGAL_PERIKSA');" <?= $disabled ?>/>&nbsp; YYYY-MM-DD</td>
                        </tr>
                        <tr>
                            <td>Kantor Pabean Pemeriksaan</td>
                            <td><input type="text"  name="HEADER[KPBC_PERIKSA]" id="KPBC_PERIKSA" class="stext date" value="<?= $sess['KPBC_PERIKSA']; ?>" url="<?= site_url() ?>/autocomplete/kpbc" urai="urktawas;" wajib="yes" onfocus="Autocomp(this.id)" <?= $disabled ?>/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-xs btn-primary" onclick="tb_search('kpbc', 'KPBC_PERIKSA;urktawas', 'Kode Kpbc', this.form.id, 650, 400)" value="..." <?= $display ?>>&nbsp;<span id="urktawas"><?= $sess['URAIAN_KPBC_AWAS'] == '' ? $URKANTOR_TUJUAN : $sess['URAIAN_KPBC_AWAS']; ?></span> </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA PENERIMAAN NEGARA</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Nilai Tukar Mata Uang</td>
                            <td>
                            	<input type="text" name="TUKAR" id="TUKAR" readonly value="<?= $this->fungsi->FormatRupiah($sess['NDPBM'], 4); ?>" onkeyup="this.value = ThausandSeperator('NDPBM', this.value, 4);" class="ltext" format="angka" wajib="yes"><input type="hidden" name="HEADER[NDPBM]" id="NDPBM" class="text" value="<?= $sess['NDPBM']; ?>" maxlength="18" <?= $disabled ?>/>
                        	</td>
                        </tr>
                        <tr>
                            <td>Nilai BK (Rp)</td>
                            <td>
                            	<input type="text" name="BK_NILAI" id="BK_NILAI" readonly value="<?= $this->fungsi->FormatRupiah($sess['JUM_BK'], 4); ?>" class="ltext" format="angka" >
                            </td>
                        </tr>
                        <tr>
                            <td>PNPB</td>
                            <td>
                            	<input type="text" name="PNBP_NILAI" id="PNBP_NILAI" value="<?= $this->fungsi->FormatRupiah($sess['PNBP'], 4); ?>" onkeyup="this.value = ThausandSeperator('PNBP', this.value, 4);" class="ltext" format="angka"  <?= $disabled ?>><input type="hidden" name="HEADER[PNBP]" id="PNBP" class="ltext" value="<?= $sess['PNBP']; ?>" maxlength="18" <?= $disabled ?>/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            	<h5 class="smaller lighter blue"><b>DATA KEMASAN</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Merk Kemasan </td>
                            <td>
                            	<input type="text" name="HEADER[MERK_KEMASAN]" id="MERK_KEMASAN" value="<?= $sess['MERK_KEMASAN']; ?>" class="mtext"  <?= $disabled ?>>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="rowheight">
                            <h5 class="smaller lighter blue"><b>TANDA TANGAN PENGUSAHA EKSPORTIR</b></h5>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="HEADER[NAMA_TTD]" id="NAMA_TTD" value="<?= $sess['NAMA_TTD']; ?>" class="mtext"  <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td>Tempat</td>
                            <td><input type="text" name="HEADER[KOTA_TTD]" id="KOTA_TTD" value="<?= $sess['KOTA_TTD']; ?>" class="mtext"  <?= $disabled ?>/></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>
                            	<input type="text" name="HEADER[TANGGAL_TTD]" id="TANGGAL_TTD" value="<?= $sess['TANGGAL_TTD'] ? $sess['TANGGAL_TTD'] : date('Y-m-d'); ?>"  onFocus="ShowDP('TANGGAL_TTD')" class="stext date" <?= $disabled ?>/>
                        		&nbsp; YYYY-MM-DD
                        	</td>
                        </tr>
					</table>
				</td>
            <tr>
            	<td colspan="2">&nbsp;</td>
            </tr>
            <?php if (!$priview) { ?>
            <tr>
            	<td width="45%">
                	<table width="100%">
                    	<tr>
                        	<td  width="28%">&nbsp;</td>
                            <td  width="72%">
                            	<a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="save_header('#fbc30_');">
                                    <i class="icon-save"></i>&nbsp;<?= ucwords($act); ?>
                                </a>
                                <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('fbc30_');">
                                    <i class="icon-undo"></i>&nbsp;Reset
                                </a>
                                <span class="msgheader_" style="margin-left:20px">&nbsp;</span>
                            </td>
                        </tr>
                    </table>
            	</td>
                <td>&nbsp;</td>
            </tr>
            <?php } ?> 
     	</table>
	</form>
</span>
<?php
if ($priview) {
echo '<h4 class="header smaller lighter green"><b>Detil Dokumen</b></h4>'.$DETILPRIVIEW;
}
?>


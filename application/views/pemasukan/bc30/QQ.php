<h5 class="header smaller lighter green"><b>Data QQ / Indentor</b></h5>
<form method="post" action="#" name="Frm_QQ" id="Frm_QQ">
    <table border="0" width="100%">
        <tr>
            <td width="21%">Identitas</td>
            <td width="79%">
				<?= form_dropdown('KODE_ID_QQ', $kode_id, $kode_id_qq, 'id="KODE_ID_QQ" class="sstext"');?>
                <input type="text" name="ID_QQ" id="ID_QQ" value="<?= $id_qq; ?>" class="ltext" wajib="yes" maxlength="15" />
           	</td>
        </tr>
        <tr>
            <td class="top">Nama</td>
            <td><input type="text" name="NAMA_QQ" id="NAMA_QQ" value="<?= $nama_qq; ?>" maxlength="50" class="mtext" wajib="yes"/></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><textarea name="ALAMAT_QQ" id="ALAMAT_QQ" class="mtext" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatQQ')"><?= $alamat_qq; ?></textarea><div id="limitAlamatQQ"></div></td>
        </tr>
        <tr>
            <td class="top">NIPER </td>
            <td><input type="text" name="NIPER_QQ" id="NIPER_QQ" value="<?= $niper_qq; ?>" maxlength="2" class="text" wajib="yes"/></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>
            	<a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="getPopup('Frm_QQ');">
                	<i class="icon-save"></i>&nbsp;Save
				</a>
                <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('harga');">
                	<i class="icon-undo"></i>&nbsp;Reset
               	</a>
            </td>
        </tr>
    </table>
</form>
<script>
$(function(){FormReady();})
</script>

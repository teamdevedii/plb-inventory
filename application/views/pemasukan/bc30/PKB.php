<form name="Frm_PKB" id="Frm_PKB" action="<?= site_url()."/pengeluaran/popup/bc30/PKB"; ?>" method="post" autocomplete="off">
	<input type="hidden" name="act" id="act" value="<?= $act;?>" />
	<input type="hidden" name="PKB[NOMOR_AJU]" id="NOMOR_AJU" value="<?= $aju;?>" />
	<h5 class="header smaller lighter green"><b>PKB - PEMBERITAHUAN KESIAPAN BARANG</b></h5>
    <table border="0" width="100%" >
    	<tr>
        	<td>
                <h5 class="smaller lighter blue"><b>I. DATA PEMOHON</b></h5>
                <table width="100%" >
                    <tr>
                        <td width="55%">
                            <table width="100%" >
                                <tr>
                                    <td width="100">Nama</td>
                                    <td>
                                    <input type="text" disabled="disabled" name="NAMA" id="NAMA" 
                                    value="<?= $sess['NAMA_TRADER']?$sess['NAMA_TRADER']:$resultHeader->NAMA_TRADER; ?>" class="text"  maxlength="15" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">NPWP </td>
                                    <td>
                                    <input type="text" disabled="disabled" name="NPWP" id="NPWP" value="<?= $sess['ID_TRADER']?$this->fungsi->FormatNPWP($sess['ID_TRADER']):$this->fungsi->FormatNPWP($resultHeader->ID_TRADER); ?>" class="stext"  maxlength="15" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="45%">
                            <table width="100%" >
                                <tr>
                                    <td width="50" valign="top">Alamat </td>
                                    <td><textarea name="ALAMAT" disabled="disabled" id="ALAMAT" class="text"  ><?= $sess['ALAMAT_TRADER']?$sess['ALAMAT_TRADER']:$resultHeader->ALAMAT_TRADER; ?></textarea>
                                    
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                </table>
			</td>
    	</tr>
    	<tr>
        	<td>
                <h5 class="smaller lighter blue"><b>II. Khusus KITE : Fasilitas yang di minta</b></h5>
                <table width="100%" >
                    <tr>
                        <td width="55%">
                            <table width="100%" >
                                <tr>
                                    <td width="100">
                                    <input type="text" name="PKB[FASILITAS]" id="FASILITAS" readonly class="numtext" value="<?= $kode_pkb?>" style="text-align:center" wajib="yes" />&nbsp;<?= $uraian_pkb;?></td>
                                </tr>
                            </table>
                        </td>
                        <td width="45%">
                            <table width="100%" >
                                <tr>
                                    <td width="22%">Zoning KITE</td>
                                    <td width="78%"><input wajib="yes" type="text" name="PKB[ZONING_KITE]" id="ZONING_KITE" class="numtext" onclick="tb_search('zoning_kite','ZONING_KITE;urZoning','ZONING KITE',this.form.id,650,445)" value="<?=$sess['ZONING_KITE'];?>" />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('zoning_kite','ZONING_KITE;urZoning','ZONING KITE',this.form.id,650,445)" value="...">&nbsp;<span id="urZoning"><?=$sess['URAI_ZONING'];?></span>
                                    </td>
                                </tr>
                             </table>
                        </td>
                     </tr>
                </table>
        	</td>
    	</tr>
    	<tr>
        	<td>
        		<h5 class="smaller lighter blue"><b>III. KESIAPAN BARANG</b></h5>
        		<table width="100%" >
                    <tr>
                        <td width="55%">
                            <table width="100%" >
                                <tr>
                                    <td width="219">1.Jns Barang</td>
                                    <td width="353">
                                    	<input type="text" name="PKB[JNS_BARANG]" id="JNS_BARANG" readonly class="numtext" onclick="tb_search('jns_barang','JNS_BARANG;urJnsBrg','JENIS BARANG',this.form.id,650,445)" value="<?=$sess['JNS_BARANG'];?>" wajib="yes" />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('jns_barang','JNS_BARANG;urJnsBrg','JENIS BARANG',this.form.id,650,445)" value="...">&nbsp;<span id="urJnsBrg"><?=$sess['URAI_JNS_BRG'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="219">2.Tempat Simpan Brg</td>
                                    <td>
                                    	<input type="text" name="PKB[GUDANG]" id="GUDANG" readonly class="numtext" onclick="tb_search('gudang_pkb','GUDANG;urGudang','GUDANG PKB',this.form.id,650,445)" value="<?=$sess['GUDANG'];?>" wajib="yes" />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('gudang_pkb','GUDANG;urGudang','GUDANG PKB',this.form.id,650,445)" value="...">&nbsp;<span id="urGudang"><?=$sess['URAI_GUDANG'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">3.Permintaan Pemeriksaan :</td>
                                </tr>
                                <tr>
                                    <td width="219"><font style="margin-left:20px">A. Tanggal</font></td>
                                    <td>
                                        <input type="text" name="PKB[TANGGAL_PERIKSA]" id="TANGGAL_PERIKSAA" class="stext date" onfocus="ShowDP('TANGGAL_PERIKSAA');"  value="<?=$sess['TANGGAL_PERIKSA'];?>" wajib="yes" />&nbsp; Jam &nbsp;
                                        <input type="text" name="PKB[WAKTU_PERIKSA]" id="WAKTU_PERIKSAA" class="numtext" value="<?=$sess['WAKTU_PERIKSA'];?>" onfocus="ShowTime('WAKTU_PERIKSAA');" wajib="yes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="219" valign="top"><font style="margin-left:20px">B. Alamat</font></td>
                                    <td>
                                        <textarea name="PKB[ALAMAT_PERIKSA]"  id="ALAMAT_PERIKSA" class="text" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatPrksa')" ><?=$sess['ALAMAT_PERIKSA']; ?></textarea>
                                        <div id="limitAlamatPrksa"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="219"><font style="margin-left:32px">Telepon</font></td>
                                    <td>
                                    	<input type="text" name="PKB[NO_TELP]" id="NO_TELP" class="text"  value="<?=$sess['NO_TELP'];?>" wajib="yes"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="219"><font style="margin-left:32px">Fax</font></td>
                                    <td>
                                    	<input type="text" name="PKB[NO_FAX]" id="NO_FAX" class="stext"  value="<?=$sess['NO_FAX'];?>" wajib="yes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="219"><font style="margin-left:20px">C. Ct.Person</font></td>
                                    <td>
                                    	<input type="text" name="PKB[PETUGAS]" id="PETUGAS" class="text" value="<?=$sess['PETUGAS'];?>" wajib="yes" />
                                   	</td>
                                </tr>
                            </table>
                        </td>
                     	<td width="45%">
                            <table width="100%" >
                                <tr>
                                    <td width="100" colspan="2">4.Pelaksanaan Stuffing :</td>
                                </tr>
                                <tr>
                                    <td width="100"><font style="margin-left:20px">Tanggal</font></td>
                                    <td>
                                    <input type="text" name="PKB[TANGGAL_STUFFING]" id="TANGGAL_STUFFING" class="stext date" onfocus="ShowDP('TANGGAL_STUFFING');"  value="<?=$sess['TANGGAL_STUFFING'];?>" wajib="yes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100" valign="top"><font style="margin-left:20px">Tempat</font></td>
                                    <td>
                                    <textarea name="PKB[ALAMAT_STUFFING]"  id="ALAMAT_STUFFING" class="text" wajib="yes" onkeyup="limitChars(this.id, 70, 'limitAlamatStuff')"><?= $sess['ALAMAT_STUFFING']; ?></textarea>
                                    <div id="limitAlamatStuff"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100" colspan="2">5.Jumlah Peti Kemas :</td>
                                </tr>
                                <tr>
                                    <td width="100"><font style="margin-left:20px">Peti Kemas</font></td>
                                    <td>
                                    <input type="text" name="PKB[JUMLAH_CONT20]" id="JUMLAH_CONT20" class="numtext"  value="<?=$sess['JUMLAH_CONT20'];?>" />&nbsp; X 20 FEET &nbsp;
                                    <input type="text" name="PKB[JUMLAH_CONT40]" id="JUMLAH_CONT40" class="numtext" value="<?=$sess['JUMLAH_CONT40'];?>" />&nbsp; X 40 FEET &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">6.Cara Stuffing</td>
                                    <td>
                                    <input type="text" name="PKB[CARA_STUFFING]" id="CARA_STUFFING" readonly class="numtext" onclick="tb_search('stuffing','CARA_STUFFING;urStuff','GUDANG PKB',this.form.id,650,445)" value="<?=$sess['CARA_STUFFING'];?>" />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('stuffing','CARA_STUFFING;urStuff','GUDANG PKB',this.form.id,650,445)" value="...">&nbsp;<span id="urStuff"><?=$sess['URAI_STUFF'];?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">7.Dlm Hal Part OF</td>
                                    <td>
                                    <input type="text" name="PKB[JNPARTOF]" id="JNPARTOF" readonly class="numtext" onclick="tb_search('partof','JNPARTOF;urPartOf','GUDANG PKB',this.form.id,650,445)" value="<?=$sess['JNPARTOF'];?>" />&nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('partof','JNPARTOF;urPartOf','GUDANG PKB',this.form.id,650,445)" value="...">&nbsp;<span id="urPartOf"><?=$sess['URAI_JNPARTOF'];?></span>
                                    </td>
                                </tr>
                             </table>
                         </td>
     				</tr>
                </table>
			</td>
		</tr>
	</table>
	<p>
    	<a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="save_POP('#Frm_PKB','msgPKB_','','','<?= $act;?>','divPKB');">
			<i class="icon-save"></i>&nbsp;<?=ucwords($act)?>
       	</a>
        <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('Frm_PKB');">
        	<i class="icon-undo"></i>&nbsp;Reset
        </a>
        <span class="msgPKB_" style="margin-left:20px">&nbsp;</span>
  	</p>
</form>
<script>
$("input, textarea, select").focus(function(){
	if($(this).attr('wajib')=="yes"){
		$(".msg_").fadeOut('slow');
		$(this).removeClass('wajib');
	}
});
</script>

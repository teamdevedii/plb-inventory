<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
if($kode_harga==1){
	$readonly="readonly=readonly";
}elseif($kode_harga==3){
	$readonly="";	
}
?>
<span id="fbarang_form">
	<?php if(!$list){?>
	<h5 class="header smaller lighter green"><b>DETIL BARANG</b></h5>
	<form id="fbarang_" action="<?= site_url()."/pemasukan/barang/bc30"; ?>" method="post" autocomplete="off" enctype="multipart/form-data" list="<?=  site_url()."/pemasukan/detil/barang/bc30" ?>">
        <input type="hidden" name="act" value="<?= $act; ?>" />
        <input type="hidden" name="seri" id="seri" value="<?= $seri; ?>" />
        <input type="hidden" name="NOMOR_AJU" id="NOMOR_AJU" value="<?= $aju; ?>" />
        <table width="100%">
            <tr>
            	<td width="48%" valign="top">  
            		<table width="100%">
                        <tr>
                            <td width="108">Kode HS</td>
                            <td width="415"><input type="text" name="BARANG[KODE_HS]" id="KODE_HS" url="<?= site_url(); ?>/autocomplete/hs" class="sstext" wajib="yes" value="<?= $this->fungsi->FormatHS($sess['KODE_HS']); ?>" onfocus="Autocomp(this.id);" urai="SERI_HS;"  maxlength="13" onblur="this.value = FormatHS(this.value)"/> <input type="hidden" name="SERI_HS" id="SERI_HS"/></td>
                        </tr>
                        <tr>
                            <td>Kode Barang</td>                
                            <td>
                                <input type="text" name="KODE_BARANG" id="KODE_BARANG1" value="<?= $sess['KODE_BARANG']; ?>" class="text" url="<?= site_url(); ?>/autocomplete/bc_barang"  wajib="yes" readonly urai="URAIAN_BARANG1;MERK;TIPE;UKURAN;SPF;JNS_BARANG;KODE_HS;SERI_HS;KD_SAT_BESAR;KD_SAT_KECIL;KODE_SATUAN;urjenis_satuan;"/>&nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('barang','KODE_BARANG1;URAIAN_BARANG1;MERK;TIPE;UKURAN;SPF;JNS_BARANG;KODE_HS;SERI_HS;KD_SAT_BESAR;KD_SAT_KECIL;KODE_SATUAN;urjenis_satuan','Kode Barang',this.form.id,650,400)" value="...">
                                <input type="hidden" name="JNS_BARANG" id="JNS_BARANG" class="text" value="<?= $sess['JNS_BARANG']; ?>"  />                		
                                <input type="hidden" id="KD_SAT_BESAR" name="KD_SAT_BESAR" value="<?= $sess['KD_SAT_BESAR']; ?>"/> 
                                <input type="hidden" id="KD_SAT_KECIL" name="KD_SAT_KECIL" value="<?= $sess['KD_SAT_KECIL']; ?>"/>
                                <input type="hidden" id="KODE_BARANG_HIDE" name="KODE_BARANG_HIDE" value="<?= $sess['KODE_BARANG']; ?>" />
                            </td>
                        </tr>		         
                        <tr>
                            <td>Uraian Barang <span style="float:right">1.</span></td>
                            <td><input type="text" name="BARANG[URAIAN_BARANG1]" id="URAIAN_BARANG1" class="text" value="<?= $sess['URAIAN_BARANG1']; ?>"  maxlength="40" /></td>
                        <tr>
                            <td align="right">2.</td>
                            <td><input type="text" name="BARANG[URAIAN_BARANG2]" id="URAIAN_BARANG2" class="text" value="<?= $sess['URAIAN_BARANG2']; ?>"  maxlength="40"/></td>
                        <tr>
                            <td align="right">3.</td>
                            <td><input type="text" name="BARANG[URAIAN_BARANG3]" id="URAIAN_BARANG3" class="text" value="<?= $sess['URAIAN_BARANG3']; ?>"  maxlength="40"/></td>
                        </tr> 
                        <tr>
                            <td align="right">4.</td>
                            <td><input type="text" name="BARANG[URAIAN_BARANG4]" id="URAIAN_BARANG4" class="text" value="<?= $sess['URAIAN_BARANG4']; ?>"  maxlength="40"/></td>
                        </tr>             
                        <tr>
                            <td>Merk</td>
                            <td><input type="text" name="BARANG[MERK]" id="MERK" class="text" value="<?= $sess['MERK']; ?>"  /></td>
                        </tr>
                        <tr>
                            <td>Tipe</td>
                            <td><input type="text" name="BARANG[TIPE]" id="TIPE" class="text" value="<?= $sess['TIPE']; ?>"  /></td>
                        </tr>
                        <tr>
                            <td>Ukuran</td>
                            <td><input type="text" name="BARANG[UKURAN]" id="UKURAN" class="text" value="<?= $sess['UKURAN']; ?>"  /></td>
                        </tr>
                        <tr>
                            <td>Spesifikasi Lain</td>
                            <td><input type="text" name="BARANG[SPF]" id="SPF" class="text" value="<?= $sess['SPF']; ?>" /></td>
                        </tr>
            		</table>          	
    			</td>
                <td width="52%" valign="top">  
                    <table width="100%">		
                        <tr>
                            <td colspan="2" class="rowheight"><h5 class="smaller lighter blue"><b>DATA HARGA</b></h5></td>
                        </tr>
                        <tr style="display:none" id="taghargacif">
                            <td width="22%">Harga CIF</td>
                            <td width="78%">
                                <input type="text" name="INVO" id="INVO" wajib="yes" class="text" value="<?=$this->fungsi->FormatRupiah($sess['INVOICE'],4); ?>" onkeyup="this.value = ThausandSeperator('INVOICE',this.value,4);satuan()"  />
                                <input type="hidden" name="BARANG[INVOICE]" id="INVOICE" value="<?= $sess['INVOICE']?>" />   
                            </td>
                        </tr>
                        <tr>
                            <td>Harga FOB</td>
                            <td>
                                <input type="text" name="FOB_DTL" id="FOB_DTL" wajib="yes" class="text" <?php // $readonly;?> value="<?=$this->fungsi->FormatRupiah($sess['FOB_PER_BARANG'],4); ?>" onkeyup="this.value = ThausandSeperator('FOB_PER_BARANG',this.value,4);satuan()" />
                                <input type="hidden" name="BARANG[FOB_PER_BARANG]" id="FOB_PER_BARANG" value="<?= $sess['FOB_PER_BARANG']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Jumlah Satuan</td>
                            <td>
                                <input type="text" name="JUML_SAT" id="JUML_SAT" wajib="yes" class="text" value="<?=$this->fungsi->FormatRupiah($sess['JUMLAH_SATUAN'],4); ?>" onkeyup="this.value = ThausandSeperator('JUMLAH_SATUAN',this.value,4);satuan()"  />
                                <input type="hidden" name="JUMLAH_SATUAN" id="JUMLAH_SATUAN" value="<?= $sess['JUMLAH_SATUAN']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Satuan</td>
                            <td>
                                <input type="text" name="BARANG[KODE_SATUAN]" id="KODE_SATUAN" url="<?= site_url(); ?>/autocomplete/satuan" class="sstext" wajib="yes" value="<?= $sess['KODE_SATUAN']; ?>" urai="urjenis_satuan;" readonly /> &nbsp;
                                <input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('satuan','KODE_SATUAN;urjenis_satuan','Kode Satuan',this.form.id,650,400,'KD_SAT_BESAR;KD_SAT_KECIL;')" value="...">
                                <span id="urjenis_satuan"><?= $sess['URAIAN_SATUAN']?$sess['URAIAN_SATUAN']:$urkd_stn; ?></span>
                            </td>            
                        </tr>        
                        <tr>
                            <td>Harga Satuan</td>
                            <td>
                                <input type="text" name="BARANG[FOB_PER_SATUAN]" id="FOB_PER_SATUAN" readonly class="text" value="<?=$this->fungsi->FormatRupiah($sess['FOB_PER_SATUAN'],4); ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="rowheight"><h5 class="smaller lighter blue"><b>DATA KEMASAN</b></h5></td>
                        </tr>
                        <tr>
                            <td>Jumlah</td>
                            <td>
                                <input type="text" name="JUML_KMS" id="JUML_KMS" wajib="yes" class="text" value="<?=$this->fungsi->FormatRupiah($sess['JUMLAH_KEMASAN'],4); ?>" onkeyup="this.value = ThausandSeperator('JUMLAH_KEMASAN',this.value,4);satuan()"  />
                                <input type="hidden" name="BARANG[JUMLAH_KEMASAN]" id="JUMLAH_KEMASAN" value="<?= $sess['JUMLAH_KEMASAN']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Jenis Kemasan</td>
                            <td>
                                <input type="text" name="BARANG[KODE_KEMASAN]" id="KODE_KEMASAN" value="<?= $sess['KODE_KEMASAN']; ?>" url="<?= site_url(); ?>/autocomplete/kemasan" class="stext date" urai="urjenis_kemasan;" onfocus="Autocomp(this.id);"/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('kemasan','KODE_KEMASAN;urjenis_kemasan','Kode Kemasan',this.form.id,650,400)" value="...">
                                <span id="urjenis_kemasan"><?= $urjenis_kemasan; ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Volume</td>
                            <td>
                                <input type="text" name="VOLUME_BRG" id="VOLUME_BRG" wajib="yes" class="text" value="<?=$this->fungsi->FormatRupiah($sess['VOLUME'],4); ?>" onkeyup="this.value = ThausandSeperator('VOLUMES',this.value,4);"  />
                                <input type="hidden" name="BARANG[VOLUME]" id="VOLUMES" value="<?= $sess['VOLUME']?>" />&nbsp; (M3)
                            </td>
                        </tr>	
                        <tr>
                            <td>Netto</td>
                            <td>
                                <input type="text" name="NETTO_BRG" id="NETTO_BRG" wajib="yes" class="text" value="<?=$this->fungsi->FormatRupiah($sess['NETTO'],4); ?>" onkeyup="this.value = ThausandSeperator('NETTOS',this.value,4);"  />
                                <input type="hidden" name="BARANG[NETTO]" id="NETTOS" value="<?= $sess['NETTO']?>" />
                            </td>
                        </tr>		
                        <tr>
                            <td>Negara Asal</td>
                            <td>
                                <input type="text" name="BARANG[NEGARA_ASAL]" id="NEGARA_ASAL" url="<?= site_url(); ?>/autocomplete/negara" class="stext date" value="<?= $sess['NEGARA_ASAL']; ?>"  urai="urngr_asl;" onfocus="Autocomp(this.id);"/> &nbsp;<input type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search('negara','NEGARA_ASAL;urngr_asl','Kode Negara',this.form.id,650,400)" value="...">&nbsp;
                                <span id="urngr_asl"><?= $sess['URAIAN_NEGARA']==''?$urngr_asl:$sess['URAIAN_NEGARA']; ?></span>
                            </td>
                        </tr> 
                    </table>            
                </td>
			</tr>
		</table> 
		<h5 class="header smaller lighter green"><b>DATA PEMBERITAHUAN</b></h5>
		<table width="100%">
            <tr>
            	<td width="48%" valign="top">  
                    <table width="100%">			
                        <tr>
                            <td colspan="2" class="rowheight"><h5 class="smaller lighter blue"><b>Izin Keluar :</b></h5></td>
                        </tr>
                        <tr>
                            <td width="108">Jenis Izin</td>
                            <td width="415"><?= form_dropdown('BARANG[KODE_IZIN]', $izin_ekspor, $sess['KODE_IZIN'], 'id="KODE_IZIN" class="text" '); ?></td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td><input type="text" name="BARANG[NOMOR_IZIN]" id="NOMOR_IZIN"  class="text" value="<?= $sess['NOMOR_IZIN']; ?>" maxlength="30"/> </td>
                        </tr>		         
                        <tr>
                            <td>Tanggal</td>
                            <td><input type="text" name="BARANG[TANGGAL_IZIN]" id="TANGGAL_IZIN"  class="stext date" value="<?= $sess['TANGGAL_IZIN']; ?>" onfocus="ShowDP('TANGGAL_IZIN')"/>&nbsp; YYYY-MM-DD </td>
                        </tr>
                    </table>          	
            	</td>
            	<td valign="top" width="52%">       
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="rowheight"><h5 class="smaller lighter blue"><b>Bea Keluar :</b></h5></td>
                        </tr>
                        <tr>
                            <td width="22%">Jenis Tarif</td>
                            <td width="78%"><?= form_dropdown('BARANG[KODE_PE]', $jenis_tarif, $sess['KODE_PE'], 'id="KODE_PE" value="<?= $jenis_tarif;?>" class="text"  onchange="cekTarif();"'); ?></td>
                        </tr>
                        <tr id="tarif_1" class="detTarif1" style="display:none;">
                            <td colspan="2">
                                <div class="msg_load" style="position:absolute; margin-left:320px;margin-top:-21px;"></div>
                                <div id="beaAdvolarum"></div>
                            </td>
                        </tr>
                        <tr id="tarif_2" class="detTarif2" style="display:none">
                            <td colspan="2">
                                <div class="msg_load" style="position:absolute;margin-left:320px; margin-top:-21px;"></div>
                                <div id="beaSpesifik"></div>
                            </td>
                        </tr>
                    </table>            
				</td>
                <td width="37%">
                	<?php //if($act=="update" && $sess['KATEGORI_EKSPOR']=="33"){ ?>
                    <table width="100%" id="PJT-TABLE" style="display:none">			
                        <tr>
                            <td colspan="2" class="rowheight"><h5 class="smaller lighter blue"><b>Pengirim/Penerima (PJT) :</b></h5></td>
                        </tr>
                        <tr>
                            <td width="69"><b>Pengirim</b></td>
                            <td width="304"><?= form_dropdown('PJT[ID_EKSPROTIR]', $kode_id_trader, $pjt['ID_EKSPROTIR'], 'wajib="yes" id="ID_EKSPROTIR" class="sstext"'); ?> <input type="text" name="PJT[NPWP_EKSPORTIR]" id="NPWP_EKSPORTIR" value="<? if($pjt['NPWP_EKSPORTIR']) echo $this->fungsi->FORMATNPWP($pjt['NPWP_EKSPORTIR']) ?>" class="stext" size="20" maxlength="15" wajib="yes"/></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="PJT[NAMA_EKSPORTIR]" id="NAMA_EKSPORTIR" wajib="yes" class="text" value="<?= $pjt['NAMA_EKSPORTIR']; ?>"/> </td>
                        </tr>	
                        <tr>
                            <td>Alamat</td>
                            <td><input type="text" name="PJT[ALAMAT_EKSPORTIR]" id="ALAMAT_EKSPORTIR" wajib="yes" class="text" value="<?= $pjt['ALAMAT_EKSPORTIR']; ?>"/> </td>
                        </tr>    
                        <tr>
                            <td><b>Penerima</b></td>
                            <td><input type="text" name="PJT[NAMA_PEMBELI]" id="NAMA_PEMBELI" wajib="yes" class="text" value="<?= $pjt['NAMA_PEMBELI']; ?>"/> </td>
                        </tr>    
                        <tr>
                            <td>Alamat</td>
                            <td><input type="text" name="PJT[ALAMAT_PEMBELI]" id="ALAMAT_PEMBELI" wajib="yes" class="text" value="<?= $pjt['ALAMAT_PEMBELI']; ?>"/> </td>    
                        </tr>
                        <tr>
                            <td>Negara</td>
                            <td><input type="text" name="PJT[NEGARA_PEMBELI]" id="NEGARA_PEMBELI" url="<?= site_url(); ?>/autocomplete/negara" class="stext date" value="<?= $pjt['NEGARA_PEMBELI']; ?>"  urai="urngr_beli;" onfocus="Autocomp(this.id);"/> &nbsp;<input type="button" name="cari" id="cari" class="button" onclick="tb_search('negara','NEGARA_PEMBELI;urngr_beli','Kode Negara',this.form.id,650,400)" value="...">&nbsp;
                            <span id="urngr_beli"><?= $pjt['URNEGARA_PEMBELI']; ?></span>
                            </td>
                        </tr>
                    </table>
                <? //} ?>    
                </td>
        	</tr>
		</table>
        <div class="ibutton" >
            <a href="javascript:void(0);" class="btn btn-success btn-sm" id="ok_" onclick="save_detil('#fbarang_','msgbarang_');">
            	<i class="icon-save"></i>&nbsp;<?=ucwords($act); ?>
           	</a>
            <a href="javascript:;" class="btn btn-warning btn-sm" id="cancel_" onclick="cancel('fbarang_');">
            	<span><span class="icon"></span>&nbsp;Reset&nbsp;</span>
          	</a>&nbsp;
            <span class="msgbarang_" style="margin-left:20px">&nbsp;</span>
        </div>		
	</form> 
<?php } ?>
</span>
<?php 
if($edit){
	echo '<h5 class="header smaller lighter green"><b>&nbsp;</b></h5>';
}
?>
<?php if(!$edit){ ?>
<div id="fbarang_list"><?= $list ?></div>
<?php } ?>

<script>
satuan();
$(function(){FormReady();})

$("#fbc30_").find("#KATEGORI_EKSPOR").bind('change keyup',function(){
    if($(this).val()=="33"){
            $("#PJT-TABLE").show();	
    }else{
            $("#PJT-TABLE").hide();	
    }
});

if($("#fbc30_").find("#KATEGORI_EKSPOR").val()=="33"){
    $("#PJT-TABLE").show();	
}else{
    $("#PJT-TABLE").hide();	
}


if($("#fbc30_").find("#KODE_HARGA").val()=="1"){
    $("#taghargacif").show();	
}else{	
    $("#taghargacif").hide();	
}
</script>

  

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	if(in_array($jenis,array('pemasukan','breakdown_pemasukan','in'))){
		if($jenis == "in") $url = site_url()."/realisasi/daftar_dok/realisasiin_act/".$jenis;
		else $url = site_url()."/".$jenis."/daftar_dok/".$tipe;
?>
<div class="btn-group" style="float:right">
	<button class="btn btn-small btn-warning">Pilih Dokumen</button>
  	<button data-toggle="dropdown" class="btn btn-small btn-warning dropdown-toggle"> <i class="icon-angle-down"></i> </button>
  	<ul class="dropdown-menu dropdown-warning pull-right">
            <li><a href="javascript:void(0)" onclick="list_tbl('bc281','dataDivin','<?=$url?>')">BC 2.8.1</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc27','dataDivin','<?=$url?>')">BC 2.7</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc40','dataDivin','<?=$url?>')">BC 4.0</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc24','dataDivin','<?=$url?>')">BC 2.4</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc30','dataDivin','<?=$url?>')">BC 3.0</a></li>
        <li class="divider"></li>
        <li><a href="javascript:void(0)" onclick="list_tbl('all','dataDivin','<?=$url?>')">Semua Dokumen</a></li>
  	</ul>
</div>
<?php } ?>
<div class="header">
	<h3><?php
		echo $title;
		if($subjudul){ ?>
        <small> <i class="icon-double-angle-right"></i>
        <?=$subjudul?>
        </small>
        <?php } ?>
     </h3>
</div>
<div class="content" id="dataDivin">
	<center>
		<?=$tabel?>
	</center>
</div>





<!--<div class="page-header position-relative">
  <h1>
    <?php
		echo $title;
		if($subjudul){ ?>
        <small> <i class="icon-double-angle-right"></i>
        <?=$subjudul?>
        </small>
    <?php     
		} 
		if(in_array($jenis,array('pemasukan','breakdown_pemasukan','in'))){
			if($jenis == "in") $url = site_url()."/realisasi/daftar_dok/realisasiin_act/".$jenis;
			else $url = site_url()."/".$jenis."/daftar_dok/".$tipe;
	?>
        <div class="btn-group" style="float:right">
          <button class="btn btn-small btn-yellow">Pilih Dokumen</button>
          <button data-toggle="dropdown" class="btn btn-small btn-yellow dropdown-toggle"> <i class="icon-angle-down"></i> </button>
          <ul class="dropdown-menu dropdown-yellow pull-right">
            <li><a href="javascript:void(0)" onclick="list_tbl('bc20','dataDivin','<?=$url?>')">BC 2.0</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc21','dataDivin','<?=$url?>')">BC 2.1</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc25','dataDivin','<?=$url?>')">BC 2.5</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc41','dataDivin','<?=$url?>')">BC 4.1</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('subkon','dataDivin','<?=$url?>')">Subkontrak</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('lokal','dataDivin','<?=$url?>')">Lokal</a></li>
            <li class="divider"></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('all','dataDivin','<?=$url?>')">Semua Dokumen</a></li>
          </ul>
        </div>
    <? }elseif(in_array($jenis,array('pengeluaran','breakdown_pengeluaran','out'))){
			if($jenis == "out") $url = site_url()."/realisasi/daftar_dok/realisasiout_act/".$jenis;
			else $url = site_url()."/".$jenis."/daftar_dok/".$tipe; 
	?>
        <div class="btn-group" style="float:right">
          <button class="btn btn-small btn-yellow">Pilih Dokumen</button>
          <button data-toggle="dropdown" class="btn btn-small btn-yellow dropdown-toggle"> <i class="icon-angle-down"></i> </button>
          <ul class="dropdown-menu dropdown-yellow pull-right">
            <li><a href="javascript:void(0)" onclick="list_tbl('bc24','dataDivin','<?=$url?>')">BC 2.4</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc30','dataDivin','<?=$url?>')">BC 3.0</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('bc40','dataDivin','<?=$url?>')">BC 4.0</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('subkon','dataDivin','<?=$url?>')">Subkontrak</a></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('lokal','dataDivin','<?=$url?>')">Lokal</a></li>
            <li class="divider"></li>
            <li><a href="javascript:void(0)" onclick="list_tbl('all','dataDivin','<?=$url?>')">Semua Dokumen</a></li>
          </ul>
        </div>
    <? } ?>
  </h1>
</div>
<div class="row-fluid">
  <div class="span12">
    <div id="dataDivin">
      <?=$tabel?>
    </div>
  </div>
</div>-->

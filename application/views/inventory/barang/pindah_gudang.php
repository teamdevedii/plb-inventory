<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>	
<div class="content">
<a href="javascript: window.history.go(-1)" style="float:right;margin:-5px 0px 0px 0px" class="btn btn-sm btn-success"><i class="icon-arrow-left"></i>&nbsp;Back</a>
<form name="fpindahgudang_" id="fpindahgudang_" action="<?= site_url()."/inventory/pindah_gudang/".$KODE_BARANG."/".$JNS_BARANG; ?>" method="post" autocomplete="off">
<div class="header"><h3>Pindah Gudang</h3></div>
<br><br>
<input type="hidden" name="KODE_BARANG" value="<?=$KODE_BARANG?>" readonly />
<input type="hidden" name="JNS_BARANG" value="<?=$JNS_BARANG?>" readonly />
<input type="hidden" name="JUMGUDANG" value="<?=count($KODE_GUDANG)-1?>" readonly />
<table width="100%" border="0">
    <tr>
        <td>
            <table>
                <tr>
                    <td>Nomor Transaksi</td>
                    <td width="30%">
                        <input type="text" wajib="yes" name="NOMOR_PROSES" class="mtext" maxlength="14" />
                    </td>
                    <td>&nbsp;&nbsp;Tanggal / Jam</td>
                    <td>
                        <input id="TANGGAL_PROSES" class="stext date" type="text" wajib="yes" onfocus="ShowDP('TANGGAL_PROSES');" onmouseover="ShowDP('TANGGAL_PROSES');" name="TANGGAL_PROSES">
                    </td>
                    <td>
                        <input id="WAKTU" class="stext" style="width:50px" type="text" onmouseover="ShowTime(this.id)" onfocus="ShowTime(this.id)" onclick="ShowTime(this.id)" name="WAKTU">
                    </td>
                    <td>YYYY-MM-DD HH:MI</td>
                </tr>
                <tr>
                	<td valign="top">Keterangan</td>
                    <td>
                    	<textarea name="KETERANGAN" class="mtext"></textarea>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td><hr style="border: 0.5px solid #ccc;" /></td>
    </tr>
    <tr>
        <td>
            <div style="float:left;">
            	<b>Kode Barang : [ <span style="color:#D35400"><?=strtoupper($KODE_BARANG)?></span> ]</b> dengan 
            	<b>Jenis Barang : [ <span style="color:#D35400"><?=$UR_JENIS_BARANG?> </span>]</b> dan 
                <b>Jumah Stock Tersedia : [ <span style="color:#D35400"><?= $STOCK_AKHIR." ".$KODE_SATUAN ?></span> ]</b>
            </div>
            <div style="float:right;">
                <a class="btn btn-sm btn-primary" onclick="gudang_all();" href="javascript:void(0);">
                    <i class="fa fa-plus"></i>&nbsp; Tambah Gudang
                </a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table id="tbl-gudang" class="tabelajax" width="100%">
                <tr>
                    <th width="2%">No</th>
                    <th>Gudang Asal</th>
                    <th>Kondisi Barang</th>
                    <th>Jumlah Tersedia</th>
                    <th>Gudang Tujuan</th>
                    <th>Kondisi Barang</th>
                    <th colspan="2">Jumlah</th>
                </tr>
                <tr id="tr-1" class="data">
                    <td class="alt"><span class="nop">1</span></td>
                    <td class="alt" style="border-right:none;">
                        <?php  echo form_dropdown('GUDANG_ASAL[1][]', $KODE_GUDANG, '', 'wajib="yes" id="GUDANG_ASAL1" class="text" onChange="getjumlah(this.value,\'1\')"') ?>
                    </td>
                    <td class="alt" style="border-left:none; border-right:none;">
                    	<?php  echo form_dropdown('KONDISI_ASAL[1][]', array(""=>"","BAIK"=>"BAIK","RUSAK"=>"RUSAK"), '', 'wajib="yes" id="KONDISI_ASAL1" class="stext" onChange="getjumlah2(this.value,\'1\')"') ?>
                    </td>
                    <td class="alt" style="border-left:none;">
                        <input type="text" readonly name="JUMLAH_ASAL_HIDE" id="JUMLAH_ASAL_HIDE1" class="stext" style="text-align:right" />
                        <input type="hidden" name="JUMLAH_ASAL[1][]" id="JUMLAH_ASAL1" />
                    </td>
                    <td class="alt" style="border-right:none;">
                        <div>
                            <?php  echo form_dropdown('GUDANG_TUJUAN[1][]', $KODE_GUDANG, '', 'wajib="yes" id="GUDANG_TUJUAN1" class="text"') ?>
                        </div>
                    </td>
                    <td class="alt" style="border-left:none;border-right:none;">
                    	<?php  echo form_dropdown('KONDISI_TUJUAN[1][]', array(""=>"","BAIK"=>"BAIK","RUSAK"=>"RUSAK"), '', 'wajib="yes" id="KONDISI_TUJUAN1" class="stext"') ?>
                    </td>
                    <td class="alt" style="border-left:none;border-right:none;">
                        <input type="text" name="JUMLAH_TUJUAN[1][]" id="JUMLAH_TUJUAN1" class="stext" style="text-align:right;" wajib="yes" onKeyPress="return intInput(event, /[.0-9]/)" />
                        <input type="hidden" name="LOOPS[]" id="LOOPS1" value="1" />
                    </td>
                    <td class="alt" style="border-left:none;">
                        <a onclick="gudang_list(1);" href="javascript:void(0);" style="color:#60C060;font-size:22px">
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
</table>
<a id="ok_" class="btn btn-success" onclick="save_post('#fpindahgudang_');" href="javascript:void(0);">
    <i class="fa fa-save"></i>&nbsp;Proses
</a>
<span class="msg_" style="margin-left:20px">&nbsp;</span>
</form>
</div>

<script>
$(function(){FormReady();})
</script>
<script type="text/javascript">
	if($("#JUMGUDANG").val() < 2){	
		closedialog('dialog-tbl');
		jAlert("Data tidak dapat dipindahkan.<br>Kode Barang hanya memiliki 1 (satu) gudang saja.","PLB Inventory");
	}
    function getjumlah(val,urut){
		var kondisiasal = $("#KONDISI_ASAL"+urut).val();
		$.ajax({
			type: 'POST',
			url: site_url+"/inventory/get_jumlah",
			data: "kdbrg=<?=$KODE_BARANG?>&jnsbrg=<?=$JNS_BARANG?>&kdgdng="+val+"&kondisi="+kondisiasal,
			dataType:'json',
			success: function(data){
				$("#JUMLAH_ASAL_HIDE"+urut).val(data.JUM);
				$("#JUMLAH_ASAL"+urut).val(data.JUMLAH);
			}
		});		
	}
	
	function getjumlah2(val,urut){
		var gudangasal = $("#GUDANG_ASAL"+urut).val();
		$.ajax({
			type: 'POST',
			url: site_url+"/inventory/get_jumlah",
			data: "kdbrg=<?=$KODE_BARANG?>&jnsbrg=<?=$JNS_BARANG?>&kdgdng="+gudangasal+"&kondisi="+val,
			dataType:'json',
			success: function(data){
				$("#JUMLAH_ASAL_HIDE"+urut).val(data.JUM);
				$("#JUMLAH_ASAL"+urut).val(data.JUMLAH);
			}
		});		
	}
	
	function gudang_all(){	
		var no = $("tr.data").length+1; 
		var rand = GetRandomMath();				
		var jumall = <?= count($KODE_GUDANG) - 1?>; 
		if((no-1)!=jumall){
			var html = '<tr id="tr-'+rand+'" class="data">\
								<td class="alt"><span class="nop">'+no+'</span></td>\
								<td class="alt" id="kdgasal'+rand+'" style="border-right:none;"></td>\
								<td class="alt" id="kondisiasal'+rand+'" style="border-left:none; border-right:none;"></td>\
								<td class="alt" style="border-left:none;">\
								<input type="text" name="JUMLAH_ASAL_HIDE" id="JUMLAH_ASAL_HIDE'+rand+'" class="stext" style="text-align:right;" wajib="yes" readonly="readonly"/><input type="hidden" name="JUMLAH_ASAL['+rand+'][]" id="JUMLAH_ASAL'+rand+'" /></td>\
								<td class="alt" id="kdgtuju'+rand+'" style="border-right:none;"></td>\
								<td class="alt" id="kondisituju'+rand+'" style="border-left:none;border-right:none;"></td>\
								<td class="alt" style="border-left:none;border-right:none;"><input type="text" name="JUMLAH_TUJUAN['+rand+'][]" id="JUMLAH_TUJUAN'+rand+'" class="stext" wajib="yes"  style="text-align:right;" onKeyPress="return intInput(event, /[.0-9]/)"/>\
								<input type="hidden" name="LOOPS[]" id="LOOPS1" value="'+rand+'" />\
								<td class="alt" style="border-left:none"><a onclick="gudang_list('+rand+')" style="color:#60C060;font-size:22px;cursor:pointer"> <i class="fa fa-plus-circle"></i></a>&nbsp;<a style="color:#DF4B33;font-size:22px;cursor:pointer" onclick="hapus_gudangrow('+rand+')"><i class="fa fa-minus-circle"></i></a></td></tr>';
			$("#tbl-gudang").append(html);
			$("#GUDANG_ASAL1").clone(true).removeAttr('id').attr('id','GUDANG_ASAL'+rand)
			.removeAttr('onChange').attr('onChange','getjumlah(this.value,'+rand+')')
			.removeAttr('name').attr('name','GUDANG_ASAL['+rand+'][]')
			.appendTo('#kdgasal'+rand);
			
			$("#KONDISI_ASAL1").clone(true).removeAttr('id').attr('id','KONDISI_ASAL'+rand)
			.removeAttr('onChange').attr('onChange','getjumlah2(this.value,'+rand+')')
			.removeAttr('name').attr('name','KONDISI_ASAL['+rand+'][]')
			.appendTo('#kondisiasal'+rand);
			
			$("#GUDANG_TUJUAN1").clone(true).removeAttr('id').attr('id','GUDANG_TUJUAN'+rand)		
			.removeAttr('name').attr('name','GUDANG_TUJUAN['+rand+'][]')
			.appendTo('#kdgtuju'+rand);
			
			$("#KONDISI_TUJUAN1").clone(true).removeAttr('id').attr('id','KONDISI_TUJUAN'+rand)
			.removeAttr('name').attr('name','KONDISI_TUJUAN['+rand+'][]')
			.appendTo('#kondisituju'+rand);
		}else{
			jAlert("Maksimal "+jumall+" pilihan.<br>Gudang tujuan hanya punya "+jumall+" Gudang","PLB Inventory");return false;  
		}
	}
	
	function gudang_list(urut){
		var no = $("#tr-"+urut+'.child').length+1;
		var rand = GetRandomMath()+1;			
		var jumall = <?= count($KODE_GUDANG) - 1 ?>;
		var urutid = (urut+'-'+no);
		if((no)!=jumall){
			var html = '<tr id="tr-'+rand+'" class="child">\
							<td class="alt"><span class="nopx"></span></td>\
							<td class="alt" style="border-right:none;"></td>\
							<td class="alt" style="border-left:none; border-right:none;"></td>\
							<td class="alt" style="border-left:none;"></td>\
							<td class="alt" id="kdgtuju'+rand+'" style="border-right:none;"></td>\
							<td class="alt" id="kondisituju'+rand+'" style="border-left:none;border-right:none;"></td>\
							<td class="alt" style="border-left:none;border-right:none;"><input type="text" name="JUMLAH_TUJUAN['+urut+'][]" id="JUMLAH_TUJUAN'+rand+'"  class="stext" style="text-align:right;" wajib="yes" onKeyPress="return intInput(event, /[.0-9]/)"/>\
							<td class="alt" style="border-left:none"><a style="color:#DF4B33;font-size:22px;cursor:pointer" onclick="hapus_gudangrow('+rand+')"><i class="fa fa-minus-circle"></i></a></td></tr>';
			$("#tr-"+urut).after(html);
			
			$("#GUDANG_TUJUAN"+urut).clone(true).removeAttr('id').attr('id','GUDANG_TUJUAN'+rand)	
			.removeAttr('name').attr('name','GUDANG_TUJUAN['+urut+'][]')
			.appendTo('#kdgtuju'+rand);
			
			$("#KONDISI_TUJUAN"+urut).clone(true).removeAttr('id').attr('id','KONDISI_TUJUAN'+rand)	
			.removeAttr('name').attr('name','KONDISI_TUJUAN['+urut+'][]')
			.appendTo('#kondisituju'+rand);
		}else{
			jAlert("Maksimal "+jumall+" pilihan.<br>Gudang tujuan hanya punya "+jumall,"GB Inventory");return false;  
		}			
	}
	
	function hapus_gudangrow(no){
	  $("#tr-"+no).remove();
		$("#tbl-gudang tbody tr .nop").each(function(index, element) {
			$(this).html(parseFloat(index)+1);
		});	
	}
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
?>

<div class="content_luar">
  <div class="content_dalam"> 
    <h4><span class="info_">&nbsp;</span>Penelusuran Keluar Masuk Barang</h4>
    <form name="frminout" id="frminout" method="post" action="<?php echo site_url()?>/inventory/inout">
    <input type="hidden" name="JNS_BARANG" id="JNS_BARANG" />
    <input type="hidden" name="MERK" id="MERK" />
    <input type="hidden" name="TIPE" id="TIPE" />
    <input type="hidden" name="SPF" id="SPF" />
    <input type="hidden" name="UKURAN" id="UKURAN" />
      <table class="normal" cellpadding="2" width="100%">
      	 <tr>
          <td width="8%"> Periode</td>          
          <td width="1%">:</td>
          <td>
            <input type="text" name="TANGGAL_AWAL" id="TANGGAL_AWAL" onFocus="ShowDP('TANGGAL_AWAL');" wajib="yes" class="stext date" value="<?=$TANGGAL_AWAL?>">&nbsp;s/d&nbsp;
            <input type="text" name="TANGGAL_AKHIR" id="TANGGAL_AKHIR" onFocus="ShowDP('TANGGAL_AKHIR');" wajib="yes" class="stext date" value="<?=$TANGGAL_AKHIR?>"></td>
        </tr>
        <tr>
          <td width="8%">Kode Barang</td>
          <td width="1%">:</td>
          <td><input type="text" name="KODE_BARANG" id="KODE_BARANG" wajib="yes" class="text">&nbsp; <input type="button" name="cari" id="cari" class="button" onclick="tb_search('barang','KODE_BARANG;URAIAN_BARANG;MERK;TIPE;UKURAN;SPF;JENIS_BARANG','Kode Barang',this.form.id,650,400)" value="..."><input type="hidden" name="URAIAN_BARANG" id="URAIAN_BARANG" /></td>
        </tr>
        <tr>
          <td width="8%">Jenis Barang</td>
          <td width="1%">:</td>
          <td><?= form_dropdown('JENIS_BARANG', array(""=>"","1"=>"BARANG IMPOR","2"=>"BARANG HASIL PENGERJAAN SEDERHANA","3"=>"SISA POTONGAN","4"=>"SCRAP","5"=>"BARANG BUSUK/BANTUK TERTENTU"), '', 'id="JENIS_BARANG" class="text" wajib ="yes"');?>&nbsp;  </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><a href="javascript:void(0);" class="button next" onclick="ajaxproses('#frminout','spanview')"><span><span class="icon"></span>&nbsp;Proses&nbsp;</span></a></td>
        </tr>
        <tr>
          <td colspan="3"><div class="spanview"></div></td>
        </tr>
      </table>
    </form>
  </div>
</div>
<script>
function updateinout(){
	jConfirm('Perhatian:<br>Proses berikut akan merubah jumlah stock akhir barang sejumlah Total saldo dibawah.<br>Anda yakin Akan memproses data ini?', " GB Inventory ", 
	function(r){if(r==true){			
		var kode_barang = $("#KODE_BARANG").val();
		var jns_barang = $("#JENIS_BARANG").val();
		var saldo =  parseFloat($("#JUMSALDO").val());
		jloadings();
		$.ajax({
			type: 'POST',
			url: site_url+'/inventory/updatestock',
			data: 'kode_barang='+kode_barang+'&jns_barang='+jns_barang+'&saldo='+saldo,
			success: function(data){			
				jAlert(data);
			}
		});
		return false;
	}else{return false;}});
}
</script>
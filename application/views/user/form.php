<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
if($act=="save"){
	$addUrl="add/user";
}else{
	$disable ="readonly=readonly";
	$readonly="readonly=readonly";
	$addUrl="editUser/user";
}
?>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/treeview.css" />
<script type="text/javascript" src="<?= base_url(); ?>assets/js/treeview.js"></script>
<div class="header">
	<a href="javascript:void(0)" 
onclick="window.location.href='<?= site_url()."/user/userlist/draftUser"?>'" style="float:right;margin:-5px 0px 0px 0px" class="btn btn-success prev" id="ok_"><span><i class="icon-backward"></i>&nbsp;Selesai&nbsp;</span></a>
	<h4>&nbsp;<?= $judul;?></h4>    
</div>
<div class="content"> 
    <form name="fuser" id="fuser" action="<?= site_url()."/user/".$addUrl; ?>" method="post" autocomplete="off">
      <input type="hidden" name="act" id="act" value="<?= $act;?>" />
      <input type="hidden" name="idUser" id="idUser" value="<?=$sess['USER_ID'];?>" />      
      <span id="divtmp"></span>
      <table width="100%" border="0">
        <tr>
          <td width="41%"><table width="100%" border="0">
              <tr>
                <td width="30%">Nama </td>
                <td width="69%"><input type="text" name="USER[NAMA]" id="NAMA" value="<?= $sess['NAMA']?>"  class="mtext" wajib="yes"></td>
                <td width="1%">&nbsp;</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><textarea name="USER[ALAMAT]" id="ALAMAT" class="mtext"><?= $sess['ALAMAT']; ?>
</textarea></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Nomor Telepon</td>
                <td><input type="text" name="USER[TELEPON]" id="TELEPON" value="<?= $sess['TELEPON']; ?>" class="mtext"   /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Email</td>
                <td><input type="text" name="USER[EMAIL]" id="EMAIL" value="<?= $sess['EMAIL']; ?>" class="mtext"   wajib="yes" /></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td><input type="text" name="USER[JABATAN]" id="JABATAN" value="<?= $sess['JABATAN']?>"  class="mtext"></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          <td width="59%" valign="top"><table width="100%" border="0">
              <tr>
                <td width="22%" >Username</td>
                <td width="78%"><input type="text" name="USER[USERNAME]" id="USERNAME" value="<?= $sess['USERNAME']; ?>" class="mtext" wajib="yes"   <?= $readonly ?>/></td>
              </tr>
              <tr>
                <td >Password</td>
                <td><input type="password" name="USER[PASSWORD]" id="PASSWORD" value="<?= $sess['PASSWORD']; ?>" class="mtext" wajib="yes"  <?= $readonly ?>/></td>
              </tr>
              <tr>
                <td>Konfirmasi Password</td>
                <td><input type="password" name="KONFPASSWORD" id="KONFPASSWORD" value="<?= $sess['PASSWORD']; ?>" class="mtext" wajib="yes"  <?= $readonly ?>/></td>
              </tr>
              <tr>
                <td>Group User</td>
                <td><?= form_dropdown('USER_ROLE[KODE_ROLE]', $kode_role, $sess['KODE_ROLE'], ' class="mtext"  wajib="yes" onchange="changeHakAkses(this.value,document.fuser.idUser.value)"'); ?></td>
              </tr>
              <tr>
                <td>Status</td>
                <td><?= form_dropdown('USER[STATUS]', $status_user, $sess['STATUS'], ' class="mtext"  wajib="yes"'); ?></td>
              </tr>
            </table></td>
        <tr>
          <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="6"><h4 class="header smaller green"><i class="icon-th"></i>&nbsp;&nbsp;Hak Akses Menu</h4>
            <span class="klik">* Klik expand pada untuk melihat detil menu</span></td>
        </tr>
        <tr>
          <td colspan="6"><span id="spanHakAkses"><?php echo $menu;?></span></td>
        </tr>
        <tr>
          <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="6" ><a href="javascript:void(0);" class="btn btn-success save" id="ok_" onclick="save_user('#fuser');"><span><i class="fa fa-save"></i>&nbsp;
            <?= $act; ?>
            &nbsp;</span></a>&nbsp;<a href="javascript:;" class="btn btn-warning cancel" id="cancel_" onclick="cancel('fuser');"><span><i class="fa fa-save"></i>&nbsp;reset&nbsp;</span></a><span class="msg_" style="margin-left:20px">&nbsp;</span></td>
        </tr>
      </table>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  	$("#treeView").treeTable();
});
</script>
<?php if($act=="save"){?>
<script type="text/javascript">
$(document).ready(function()  {
	$(".akses_w").attr('checked',true);
  	$("#checkAllmenu").attr('checked',true);
  	menucheckAll('fuser');
});
</script>
<?php	
}
?>

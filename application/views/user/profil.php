<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
if ($act=="Ubah"){
	$disable = "disabled=disabled";	
}else{
	$disable = "";
	$url = "../edit";
}
?>
<div class="header">
	<h4>&nbsp;Profil User</h4>
</div>
  <div class="content">    
    <form id="fprofil_" action="<?= $url; ?>" method="post" autocomplete="off">
      <table width="100%" border="0">
        <tr>
          <td width="35%" valign="top"><table width="100%" border="0">
              <tr>
                <td width="35%">Nama</td>
                <td width="65%"><input type="text" name="PROFIL[NAMA]" id="NAMA" class="text" value="<?=$sess['NAMA']?>" <?=$disable;?>/></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><input type="text" name="PROFIL[ALAMAT]" id="ALAMAT" class="text" value="<?=$sess['ALAMAT']?>" <?=$disable;?>/></td>
              </tr>
              <tr>
                <td>Nomor Telepon</td>
                <td><input type="text" name="PROFIL[TELEPON]" id="TELEPON" class="text" value="<?=$sess['TELEPON']?>" <?=$disable;?>/></td>
              <tr>
                <td>Email</td>
                <td><input type="text" name="PROFIL[EMAIL]" id="EMAIL" class="text" value="<?=$sess['EMAIL']?>" <?=$disable;?>/></td>
              </tr>
            </table>
          <td width="65%" valign="top">
          <table width="100%" border="0">
              <tr>
                <td width="17%">Jabatan</td>
                <td width="83%">
                  <input type="text" name="PROFIL[JABATAN]" id="JABATAN" class="text" value="<?=$sess['JABATAN'];?>" disabled="disabled"/></td>
              </tr>
              <tr>
                <td>User ID</td>
                <td><input type="text" name="PROFIL[USERNAME]" id="USERNAME" class="text" value="<?=$sess['USERNAME'];?>" disabled="disabled"/></td>
              </tr>
              <tr>
                <td>User Role</td>
                <td><?= form_dropdown('PROFIL1[KODE_ROLE]', $kode_role, $sess['KODE_ROLE'], ' class="text" disabled '); ?></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0">
        <tr>
          <td  colspan="8">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="8"><a href="javascript:void(0);" class="btn btn-success save" id="ok_" onclick="rubah('#fprofil_');"><span><i class="fa fa-save"></i>&nbsp;
            <?=$act;?>
            &nbsp;</span></a>&nbsp; &nbsp;
            <?php if($act!="Ubah"){ ?>
            <a href="javascript:void(0);" class="btn btn-danger cancel" id="ok_" onclick="window.location.href='<?php echo site_url('user/profil/lihat'); ?>'"><span><i class="fa fa-times"></i>&nbsp;Batal&nbsp;</span></a>
            <?php }?>
            &nbsp;&nbsp;&nbsp;<span class="msg_" align="left">&nbsp;</span></td>
        </tr>
      </table>
    </form>
</div>
<script>
function rubah(formid){
	<?php if($act=="Ubah"){ ?>
	window.location = site_url+"/user/profil/ubah";
	<?php }else{ ?>
		save_post(formid);
	<?php }?>
}	
</script>

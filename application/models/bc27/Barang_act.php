<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Barang_act extends CI_Model {
	function get_barang($aju = "", $seri = "", $kdbarang = "") {
        $data = array();
        $conn = get_instance();
        $conn->load->model("main");
        $kode_trader = $this->newsession->userdata('KODE_TRADER');
        if ($aju && $seri) {
            $query = "SELECT A.NOMOR_AJU, A.SERI, A.KODE_HS, A.SERI_HS, A.URAIAN_BARANG, A.KODE_BARANG, A.JNS_BARANG, A.MERK, A.UKURAN,
						A.TIPE, A.SPF, A.PENGGUNAAN, A.KODE_KEMASAN, A.JUMLAH_KEMASAN, A.KODE_SATUAN, A.JUMLAH_SATUAN, A.BRUTO, A.NETTO,
						A.VOLUME, A.CIF, A.HARGA_PENYERAHAN, A.JUMLAH_BB, A.BREAKDOWN_FLAG, A.PERUBAHAN_FLAG, A.REALISASI_FLAG, 
						A.PARSIAL_FLAG, f_kemas(A.KODE_KEMASAN) AS URAIAN_KEMASAN, f_satuan(A.KODE_SATUAN) AS URAIAN_SATUAN,
                		(CIF/JUMLAH_SATUAN) AS 'HARGA_SATUAN', B.KODE_SATUAN AS KD_SAT_BESAR, B.KODE_SATUAN_TERKECIL AS KD_SAT_KECIL 
                	FROM t_bc27_dtl A 
                		LEFT JOIN M_TRADER_BARANG B ON B.KODE_BARANG=A.KODE_BARANG AND B.JNS_BARANG=A.JNS_BARANG AND B.KODE_TRADER=A.KODE_TRADER
                	WHERE A.NOMOR_AJU = '" . $aju . "' AND A.SERI = '" . $seri . "' AND A.KODE_TRADER = '" . $kode_trader . "'";
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    echo $row[""];
                    $data = array('act' => 'update', 'sess' => $row, 'aju' => $aju, 'seri' => $seri);
                }
            }
        } else {
            $data = array('act' => 'save', 'aju' => $aju);
        }
		$data = array_merge($data, array('penggunaan' => $conn->main->get_mtabel('PENGGUNAAN_BARANG')));
        return $data;
    }
	
	function set_barang($type = "", $isajax = "") {
		$func = & get_instance();
        $func->load->model("main", "main", true);
        $aju = $this->input->post('NOMOR_AJU');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
        $SQL = "SELECT TIPE_DOK FROM T_BC27_HDR WHERE NOMOR_AJU='".$aju."' AND KODE_TRADER = '".$kode_trader."'";
        $rs = $this->db->query($SQL)->row();
        $TIPE_DOK = $rs->TIPE_DOK;
		if ($type == "save" || $type == "update") {
           foreach ($this->input->post('BARANG') as $a => $b) {
               $BARANG[$a] = $b;
           }
           $BARANG["KODE_HS"] 		= str_replace(".", "", $BARANG["KODE_HS"]);
           $BARANG["NOMOR_AJU"] 	= $aju;
           $BARANG["STATUS"] 		= "04";
           $BARANG["KODE_TRADER"] 	= $kode_trader;
		   if ($type == "save") {
               $seri = (int) $func->main->get_uraian("SELECT MAX(SERI) AS MAX FROM t_bc27_dtl WHERE NOMOR_AJU = '" . $aju . "' AND KODE_TRADER = '".$kode_trader."'", "MAX") + 1;
               $BARANG["SERI"] = $seri;
               $exec = $this->db->insert('t_bc27_dtl', $BARANG);
               if ($exec) {
                   $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc27('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                   $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju));
                   $this->db->update('t_bc27_hdr', array("NETTO" => $netto));
                   $func->main->cekkodehs($BARANG["KODE_HS"], $BARANG["SERI_HS"], array());
                   $func->main->activity_log('ADD DETIL BC27', 'CAR=' . $aju . ', SERI=' . $seri);
                   if ($TIPE_DOK == "MASUK") {
                       echo "MSG#OK#Simpan data Barang Berhasil#edit#" . site_url() . "/pemasukan/LoadHeader/bc27/" . $aju . "#";
                   } else {
                       echo "MSG#OK#Simpan data Barang Berhasil#edit#" . site_url() . "/pengeluaran/LoadHeader/bc27/" . $aju . "#";
                   }
               } else {
                   echo "MSG#ERR#Simpan data Barang Gagal#";
               }
           } else {
               $seri = $this->input->post('seri');
               if ($this->input->post('PERBAIKAN')) {
                   $ARRADD = array("AJU" => $aju, "SERI" => $seri);
                   $this->set_perbaikan("update", array_merge($this->input->post('PERBAIKAN'), $ARRADD), $BARANG);
               } else {
                   $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                   $exec = $this->db->update('t_bc27_dtl', $BARANG);
                   if ($exec) {
                       $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc27('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                       $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju));
                       $this->db->update('t_bc27_hdr', array("NETTO" => $netto));
                       $func->main->cekkodehs($BARANG["KODE_HS"], $BARANG["SERI_HS"], array());
                       $func->main->activity_log('EDIT DETIL BC27', 'CAR=' . $aju . ', SERI=' . $seri);
                       if ($TIPE_DOK == "MASUK") {
                           echo "MSG#OK#Update data Barang Berhasil#edit#" . site_url() . "/pemasukan/LoadHeader/bc27/" . $aju . "#";
                       } else {
                           echo "MSG#OK#Update data Barang Berhasil#edit#" . site_url() . "/pengeluaran/LoadHeader/bc27/" . $aju . "#";
                       }
                   } else {
                       echo "MSG#ERR#Update data Barang Gagal#edit#";
                   }
               }
			}
		 }else if ($type == "delete") {	
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                $exec = $this->db->delete('t_bc27_dtl');
                $func->main->activity_log('DELETE DETIL BC27', 'CAR=' . $aju . ', SERI=' . $seri);
            }
            if ($exec) {
                $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc27('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju));
                $this->db->update('t_bc27_hdr', array("NETTO" => $netto));
                if ($TIPE_DOK == "MASUK") {
                    echo "MSG#OK#Hapus data Barang Berhasil#" . site_url() . "/pemasukan/detil/barang/bc27/" . $aju . "#" . site_url() . "/pemasukan/LoadHeader/bc27/" . $aju . "#";
                    die();
                } else {
                    echo "MSG#OK#Hapus data Barang Berhasil#" . site_url() . "/pemasukan/detil/barang/bc27/" . $aju . "#" . site_url() . "/pengeluaran/LoadHeader/bc27/" . $aju . "#";
                    die();
                }
            } else {
                echo "MSG#ERR#Hapus data Barang Gagal#";
                die();
            }
        }
	}
}
?>
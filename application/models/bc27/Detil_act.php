<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Detil_act extends CI_Model {
    function list_detil($tipe = "", $aju = "") {
        $func = get_instance();
        $func->load->model("main");
        $this->load->library('newtable');
        $kode_trader = $this->newsession->userdata('KODE_TRADER');
        if($tipe == "barang"){
            $SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
                    	f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
                    	CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
                    	CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
                    	CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
                    	f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU 
					FROM t_bc27_dtl 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("SERI");
			$this->newtable->sortby("ASC");
        	$this->newtable->hiddens('NOMOR_AJU');
            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'),
										  array('URAIAN_BARANG', 'URAIAN BARANG'),
										  array('JENIS_BARANG', 'JENIS BARANG', 'tag-select', $func->main->get_mtabel('ASAL_JENIS_BARANG'))));
        }elseif($tipe == "barang_jadi"){
            $SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG',
						CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'SATUAN', NOMOR_AJU, SERIBJ AS 'SERI',
						IFNULL(f_jumBB_bc27(SERIBJ,NOMOR_AJU,KODE_TRADER),0) AS 'JUMLAH BAHAN BAKU'
					FROM t_bc27_bj 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("SERIBJ");
			$this->newtable->sortby("ASC");	
			$this->newtable->hiddens('NOMOR_AJU', 'SERI');
            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'), array('URAIAN_BARANG', 'URAIAN BARANG')));
        }elseif($tipe == "kemasan"){
            $SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', 
						MERK_KEMASAN AS 'MEREK', NOMOR_AJU	
					FROM t_bc27_kms 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->sortby("DESC");	
			$this->newtable->hiddens('NOMOR_AJU');
            $this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));
        }elseif($tipe == "kontainer"){
            $SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', 
						f_ref('JENIS_STUFF',TIPE) AS 'TIPE', NOMOR_AJU	
					FROM t_bc27_cnt 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("NOMOR");
			$this->newtable->sortby("DESC");
			$this->newtable->hiddens('NOMOR_AJU');
            $this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TIPE', 'TIPE')));
        }elseif($tipe == "dokumen"){
            $SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', 
						NOMOR AS 'NOMOR DOKUMEN', DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', NOMOR_AJU 
					FROM t_bc27_dok  
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->sortby("DESC");
			$this->newtable->hiddens('NOMOR_AJU');
            $this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));
        }else{
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url() . "/pemasukan/listdetil/" . $tipe . "/" . $aju . "/bc27");
        $this->newtable->cidb($this->db);
        $this->newtable->count_keys(array("NOMOR_AJU"));
        $this->newtable->ciuri($ciuri);
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->rowcount(10);
        $this->newtable->show_chk(false);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $tabel .= $this->newtable->generate($SQL);
        return $tabel;
    }

    function list_tab($aju = "") {
		$content = '<div class="row-fluid">
				  		<div class="tabbable">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active"> <a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
					 			<li> <a data-toggle="tab" href="#tabBarangJadi">Data Barang Jadi</a></li>
	 							<li> <a data-toggle="tab" href="#tabKemasan">Data Kemasan</a></li>					  
	 							<li> <a data-toggle="tab" href="#tabKontainer">Data Kontainer</a></li>
	 							<li> <a data-toggle="tab" href="#tabDokumen">Data Dokumen</a></li>
							</ul>
							<div class="tab-content">
								<div id="tabBarang" class="tab-pane in active">'.$this->list_detil("barang",$aju).'</div>
								<div id="tabBarangJadi" class="tab-pane">'.$this->list_detil("barang_jadi",$aju).'</div>
							 	<div id="tabKemasan" class="tab-pane">'.$this->list_detil("kemasan",$aju).'</div>
							 	<div id="tabKontainer" class="tab-pane">'.$this->list_detil("kontainer",$aju).'</div>
							 	<div id="tabDokumen" class="tab-pane">'.$this->list_detil("dokumen",$aju).'</div>
							</div>
				  		</div>
					</div>';
        return $content;
    }

    function detil($type = "", $dokumen = "", $aju = "", $action = "", $PERBAIKAN = "") {
        $kode_trader 	= $this->newsession->userdata('KODE_TRADER');
        $SERIBJ 		= $this->uri->segment(6);
        $this->load->library('newtable');
        if($type == "barang"){
            $judul = "Daftar Detil Barang";
            $SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG', KODE_BARANG,
                    	f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
                    	f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
                    	CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
                    	CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
                    	CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
                    	f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU 
					FROM t_bc27_dtl 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("SERI");
			$this->newtable->sortby("ASC");		
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG"));
            $this->newtable->hiddens(array('NOMOR_AJU', 'KODE_BARANG'));
            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'),
                						  array('URAIAN_BARANG', 'URAIAN BARANG')));
        }elseif($type == "barang_jadi"){
            $judul = "Daftar Detil Barang Jadi";
            $SQL = "SELECT KODE_BARANG AS 'KODE BARANG',URAIAN_BARANG AS 'URAIAN BARANG',f_satuan(KODE_SATUAN) AS 'URAI SATUAN',
						NOMOR_AJU,SERIBJ AS 'SERI',IFNULL(f_jumBB_bc27(SERIBJ,NOMOR_AJU,KODE_TRADER),0) AS 'JUMLAH BAHAN BAKU',KODE_BARANG
					FROM t_bc27_bj 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("SERIBJ");
			$this->newtable->sortby("ASC");
            $this->newtable->keys(array("NOMOR_AJU", "SERI","KODE_BARANG"));
            $this->newtable->hiddens(array('NOMOR_AJU', 'SERI','KODE_BARANG'));
            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'), array('URAIAN_BARANG', 'URAIAN BARANG')));
        }elseif($type == "bahan_baku"){
            $judul = "Daftar Detil Bahan Baku";
            $SQL = "SELECT KODE_HS AS 'KODE HS', KODE_BARANG AS 'KODE BARANG', URAIAN_BARANG AS 'URAIAN BARANG', MERK, 
						KODE_SATUAN AS 'KODE SATUAN', JUMLAH_SATUAN AS 'JUMLAH SATUAN',NOMOR_AJU,SERIBJ,SERIBB, SERIBB AS KODE_BARANG
					FROM t_bc27_bb 
						WHERE NOMOR_AJU= '".$aju."' AND SERIBJ='$SERIBJ' AND KODE_TRADER='".$kode_trader."'";
            
			$this->newtable->orderby("KODE_HS");
			$this->newtable->sortby("ASC");
			$this->newtable->keys(array("NOMOR_AJU", "SERIBJ", "KODE_BARANG"));
            $this->newtable->hiddens(array("NOMOR_AJU", "SERIBJ", "SERIBB",'KODE_BARANG'));
            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'), array('URAIAN_BARANG', 'URAIAN BARANG')));
        }elseif($type == "kemasan"){
            $judul = "Daftar Detil Kemasan";
            $SQL = "SELECT JUMLAH AS 'JUMLAH', KODE_KEMASAN AS 'SERI', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', 
						MERK_KEMASAN AS 'MEREK', NOMOR_AJU , KODE_KEMASAN AS KODE_BARANG
					FROM t_bc27_kms 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
					
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->sortby("ASC");
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG"));
            $this->newtable->hiddens(array('SERI', 'NOMOR_AJU'));
            $this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));
        }elseif($type == "kontainer"){
            $judul = "Daftar Detil Kontainer";
            $SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', NOMOR AS 'SERI', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', 
						f_ref('JENIS_STUFF',TIPE) AS 'TIPE', NOMOR_AJU, NOMOR AS KODE_BARANG
					FROM t_bc27_cnt 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
            
			$this->newtable->orderby("NOMOR");
			$this->newtable->sortby("ASC");
			$this->newtable->keys(array("NOMOR_AJU", "SERI","KODE_BARANG"));
            $this->newtable->hiddens(array('SERI', 'NOMOR_AJU',"KODE_BARANG"));
            $this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TIPE', 'TIPE')));
		}elseif($type == "dokumen"){
            $judul = "Daftar Detil Dokumen";
            $SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', NOMOR AS 'NOMOR DOKUMEN',
						DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN',
						REPLACE(NOMOR,'/','~') AS 'SERI', KODE_DOKUMEN AS KODE_BARANG, NOMOR_AJU 
					FROM t_bc27_dok 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->sortby("ASC");	
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG"));
            $this->newtable->hiddens(array('SERI', 'KODE_DOKUMEN', 'NOMOR_AJU', 'KODE_BARANG'));
            $this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));
        } else {
            return "Failed";
            exit();
        }
        if ($type != 'bahan_baku')
            $this->newtable->action(site_url() . "/pemasukan/detil/" . $type . "/" . $dokumen . "/" . $aju);
        if ($type == "bahan_baku")
            $this->newtable->action(site_url() . "/pemasukan/list_BB/" . $type . "/" . $dokumen . "/" . $aju . "/" . $SERIBJ);

        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->cidb($this->db);
		$this->newtable->count_keys(array("NOMOR_AJU"));
        $this->newtable->tipe_proses('button');
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(20);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);

        if ($PERBAIKAN) {
            $FPERBAIKAN = '<form id="fperbaikan_">
			<input type="hidden" name="PERBAIKAN[KPBC_UBAH]" id="PERBAIKAN_KODE_KPBC" value="' . $PERBAIKAN["KPBC_UBAH"] . '"/>
			<input type="hidden" name="PERBAIKAN[NO_SURAT_UBAH]" id="PERBAIKAN_NOMOR_SURAT" value="' . $PERBAIKAN["NO_SURAT_UBAH"] . '"/>
			<input type="hidden" name="PERBAIKAN[TGL_SURAT_UBAH]" id="PERBAIKAN_TANGGAL_SURAT" value="' . $PERBAIKAN["TGL_SURAT_UBAH"] . '"/>
			<input type="hidden" name="PERBAIKAN[NM_PEJABAT_UBAH]" id="PERBAIKAN_NAMA_PEJABAT" value="' . $PERBAIKAN["NM_PEJABAT_UBAH"] . '"/>
			<input type="hidden" name="PERBAIKAN[JABATAN_UBAH]" id="PERBAIKAN_JABATAN" value="' . $PERBAIKAN["JABATAN_UBAH"] . '"/>
			<input type="hidden" name="PERBAIKAN[NIP_UBAH]" id="PERBAIKAN_NIP" value="' . $PERBAIKAN["NIP_UBAH"] . '"/>
			</form>';
            $FPERBAIKAN .= '<p id="notify" class="notify" style="display:none">Perubahan Data Barang Berhasil, Untuk melihat hasil perubahannya silahkan klik menu History perubahan</p>';
            echo $FPERBAIKAN;
        }

        if ($type == 'bahan_baku') {
            if ($type == 'bahan_baku')
                $tipe = "barang";
            	$process = array('Tambah' => array('GET2POP', site_url() . '/pemasukan/' . $type . '/bc27/add/' . $aju . '/' . $SERIBJ, '0', 'icon-plus'),
                'Ubah' => array('GET2POP', site_url() . '/pemasukan/' . $type . '/bc27/edit', '1', 'icon-pencil'),
                'Hapus' => array('DELETE', site_url() . '/pemasukan/' . $type . '/bc27/delete', 'bahan_baku_bc27', 'icon-remove red'));

            $this->newtable->set_divid("divbahan_bakubc27");
        }else {
            if ($type == 'barang_jadi')
                $tipe = "barang";
            else
                $tipe = $type;
            if ($PERBAIKAN) {
                $process = array('Ubah' => array('EDIT', site_url() . '/pemasukan/' . $type . '/bc27', 'N', 'icon-pencil'));
            } else {
                $process = array('Tambah' => array('ADD', site_url() . '/pemasukan/' . $type . '/bc27/' . $aju, '0', 'icon-plus'),
                    'Ubah' => array('EDIT', site_url() . '/pemasukan/' . $type . '/bc27', 'N', 'icon-pencil'),
                    'Hapus' => array('DEL', site_url() . '/pemasukan/' . $type . '/bc27', 'N', 'icon-remove red'));
            }
            if ($action == "perbaikan")
                $process = array('Ubah' => array('EDIT', site_url() . '/pemasukan/' . $type . '/bc27', 'N', 'fa fa-pencil'));
            $this->newtable->set_divid("div" . $type);
        }
        $this->newtable->menu($process);
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array("judul" => $judul,
            "tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

    function list_detil_perubahan($aju = "") {
        $KODETRADER = $this->newsession->userdata('KODE_TRADER');
        $func = get_instance();
        $func->load->model("main");
        $this->load->library('newtable');

        $SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
				f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
				CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
				CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
				CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
				f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU FROM t_bc27_perubahan WHERE NOMOR_AJU= '$aju' AND KODE_TRADER='" . $KODETRADER . "'";

        $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'),
            array('URAIAN_BARANG', 'URAIAN BARANG'),
            array('JENIS_BARANG', 'JENIS BARANG', 'tag-select', $func->main->get_mtabel('ASAL_JENIS_BARANG'))));

        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url() . "/pemasukan/list_detil_perubahan/" . $aju . "/bc27");
        $this->newtable->hiddens(array('NOMOR_AJU', 'SERI'));
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->set_formid("fpbarang");
        $this->newtable->set_divid("divfpbarang");
        $this->newtable->orderby(2);
        $this->newtable->sortby("DESC");
        $this->newtable->rowcount(10);
        $this->newtable->show_chk(false);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $tabel .= $this->newtable->generate($SQL);
        return $tabel;
    }

}

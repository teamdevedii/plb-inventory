<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Header_act extends CI_Model {
	function get_header($aju = "") {
        $data = array();
        $conn = get_instance();
        $conn->load->model("main");
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		$user_id = $this->newsession->userdata("USER_ID");
		
		if(!$aju){
			#JIKA ACTION NYA SAVE
			$this->db->where('KODE_TRADER', $kode_trader);
            $this->db->select_max('TANGGAL_SKEP');
            $resultSkep = $this->db->get('m_trader_skep')->row();
            $maxTglSkep = $resultSkep->TANGGAL_SKEP;

            if ($this->uri->segment(1) != 'pemasukan') {
				#JIKA PENGELUARAN
                $query = "SELECT A.KODE_TRADER AS 'KODE_TRADER', A.KODE_ID 'KODE_ID_TRADER_ASAL', A.ID AS 'ID_TRADER_ASAL', 
                        	A.NAMA 'NAMA_TRADER_ASAL', A.ALAMAT 'ALAMAT_TRADER_ASAL', A.TELEPON, 
                        	A.KODE_API, A.NOMOR_API, D.LAST_AJU, D.NAMA_TTD, D.KOTA_TTD, 
                        	B.NOMOR_SKEP 'REGISTRASI',  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
                        	B.NOMOR_SKEP
                        FROM M_TRADER A INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                        	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=C.KODE_TRADER 
                        	LEFT JOIN M_SETTING D ON A.KODE_TRADER=C.KODE_TRADER 
                        WHERE B.TANGGAL_SKEP='" . $maxTglSkep . "' 
                        	AND A.KODE_TRADER='" . $kode_trader. "'
                        	AND C.USER_ID='" . $user_id . "' LIMIT 0,1";
            } else {
				#JIKA PEMASUKAN
                $query = "SELECT A.KODE_TRADER AS 'KODE_TRADER', A.KODE_ID 'KODE_ID_TRADER_TUJUAN', A.ID AS 'ID_TRADER_TUJUAN', 
                        	A.NAMA 'NAMA_TRADER_TUJUAN', A.ALAMAT 'ALAMAT_TRADER_TUJUAN', A.TELEPON, 
                        	A.KODE_API, A.NOMOR_API, D.LAST_AJU, D.NAMA_TTD, D.KOTA_TTD, 
                        	B.NOMOR_SKEP 'REGISTRASI',  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
                        	B.NOMOR_SKEP
                        FROM M_TRADER A INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                        	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=C.KODE_TRADER 
                        	LEFT JOIN M_SETTING D ON A.KODE_TRADER=C.KODE_TRADER 
                        WHERE B.TANGGAL_SKEP='" . $maxTglSkep . "' 
                        	AND A.KODE_TRADER='" . $kode_trader . "'
                        	AND C.USER_ID='" . $user_id . "' LIMIT 0,1";
            }
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'save', 'aju' => $aju);
		}else{
			#JIKA ACTIONNYA UPDATE
			$query = "SELECT A.NOMOR_AJU, A.KODE_KPBC_ASAL, A.KODE_KPBC_TUJUAN, A.JENIS_TPB_ASAL, A.JENIS_TPB_TUJUAN, A.TUJUAN_KIRIM,
						A.KODE_TRADER, A.KODE_ID_TRADER_ASAL, A.ID_TRADER_ASAL, A.NAMA_TRADER_ASAL, A.ALAMAT_TRADER_ASAL, 
						A.NOMOR_IZIN_TPB_ASAL, A.KODE_ID_TRADER_TUJUAN, A.ID_TRADER_TUJUAN, A.NAMA_TRADER_TUJUAN, A.ALAMAT_TRADER_TUJUAN,
						A.NOMOR_IZIN_TPB_TUJUAN, A.NOMOR_BC27_ASAL, A.TANGGAL_BC27_ASAL, A.KODE_VALUTA, A.NDPBM, A.HARGA_PENYERAHAN, 
						A.CIF, A.JENIS_SARANA_ANGKUT, A.NOMOR_POLISI, A.NOMOR_SEGEL_BC, A.JENIS_SEGEL_BC, A.CATATAN_BC_TUJUAN, 
						A.BRUTO, A.NETTO, A.VOLUME, A.JUMLAH_DTL, A.TANGGAL_TTD, A.KOTA_TTD, A.NAMA_TTD, A.NAMA_PEJABAT_BC_ASAL,
						A.NIP_PEJABAT_BC_ASAL, A.NAMA_PEJABAT_BC_TUJUAN, A.NIP_PEJABAT_BC_TUJUAN, A.NOMOR_PENDAFTARAN, A.TANGGAL_PENDAFTARAN, 
						A.NOMOR_DOK_PABEAN, A.TANGGAL_DOK_PABEAN, A.NOMOR_DOK_INTERNAL, A.TANGGAL_DOK_INTERNAL, A.SNRF, A.TANGGAL_REALISASI,
						A.STATUS_REALISASI, A.BREAKDOWN_FLAG, A.PARSIAL_FLAG, A.TIPE_DOK, A.STATUS, A.KPBC_UBAH, A.NO_SURAT_UBAH, 
						A.TGL_SURAT_UBAH, A.NM_PEJABAT_UBAH, A.JABATAN_UBAH, A.NIP_UBAH,
						f_kpbc(A.KODE_KPBC_ASAL) URAIAN_KPBC, f_kpbc(A.KODE_KPBC_TUJUAN) URAIAN_KPBC_BONGKAR, 
						f_valuta(A.KODE_VALUTA) URAIAN_VALUTA,f_status_bc27(A.NOMOR_AJU,1,KODE_TRADER) STATUS_DOK,
						IFNULL(f_jumnetto_bc27(A.NOMOR_AJU),0) AS 'JUM_NETTO' 
					  FROM t_bc27_hdr A 
					  	WHERE A.NOMOR_AJU = '".$aju."' AND KODE_TRADER='".$kode_trader."'";
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
			#SELECT DOKUMEN ASAL
			$SQL = "SELECT ID, KODE_TRADER, NOMOR_AJU, NOMOR_DAFTAR, TANGGAL_DAFTAR
					FROM T_BC27_DOKASAL WHERE KODE_TRADER='".$kode_trader."' AND NOMOR_AJU='".$aju."'";		
			$hasil = $conn->main->get_result($SQL);
			$asal = "";
			if($hasil){
				$no=1;	
				foreach($SQL->result_array() as $data){			
					$asal .= '<tr id="trnomor'.$no.'">
								<td width="29%">Nomor BC27 Asal</td>
								<td width="71%"><input type="text" name="ASAL[NOMOR_DAFTAR][]" id="NOMOR_DAFTAR'.$no.'" maxlength="8" class="text" value="'.$data["NOMOR_DAFTAR"].'"/><input style="margin-left:5px" type="button" name="cari" id="cari" class="button" onclick="tb_search(\'BC27ASAL\',\'NOMOR_DAFTAR'.$no.';TANGGAL_DAFTAR'.$no.'\',\'Dokumen BC 2.7 Asal\',this.form.id,650,400,\'TIPE_DOK;\')" value="..."></td>
							  </tr>
							  <tr id="trtanggal'.$no.'">
							  <td>Tanggal BC27 Asal</td>
							  <td><input type="text" name="ASAL[TANGGAL_DAFTAR][]" id="TANGGAL_DAFTAR'.$no.'" class="sstext" maxlength="10" onFocus="ShowDP(this.id);" value="'.$data["TANGGAL_DAFTAR"].'"/>&nbsp;&nbsp;YYYY-MM-DD&nbsp;';
					if($no!=1){		  
						$asal .='<a href="javascript:void(0)" onClick="hapusasal('.$no.')"><img width="16px" height="16px" align="texttop" style="border:none" src="'.base_url().'/img/tbl_delete.png">&nbsp;Hapus</a>';		  
					}
					$asal .='</td></tr>';	
					$no++;
				}
			}else{
				$asal = '<tr id="trnomor">
							<td width="29%">Nomor BC27 Asal</td>
							<td width="71%" id="tdnomor"><input type="text" name="ASAL[NOMOR_DAFTAR][]" id="NOMOR_DAFTAR" maxlength="8" class="text" /><input style="margin-left:5px" type="button" name="cari" id="cari" class="button" onclick="tb_search(\'BC27ASAL\',\'NOMOR_DAFTAR;TANGGAL_DAFTAR\',\'Dokumen BC 2.7 Asal\',this.form.id,650,400,\'TIPE_DOK;\')" value="...">
						   </td>
						  </tr>
						  <tr id="trtanggal">
							<td width="29%">Tanggal BC27 Asal</td>
							<td width="71%" id="tdtanggal"><input type="text" name="ASAL[TANGGAL_DAFTAR][]" id="TANGGAL_DAFTAR" class="sstext" maxlength="10" onFocus="ShowDP(this.id);" />
							  &nbsp;&nbsp;YYYY-MM-DD</td>
						  </tr>';	
			}
			
			if ($dataarray["KODE_ID_TRADER_ASAL"] == "") {
                $this->db->where('KODE_TRADER', $kode_trader);
                $this->db->select_max('TANGGAL_SKEP');
                $resultSkep = $this->db->get('m_trader_skep')->row();
                $maxTglSkep = $resultSkep->TANGGAL_SKEP;

                if ($this->uri->segment(1) != 'pemasukan') {
                    $query = "SELECT A.KODE_TRADER AS 'KODE_TRADER', A.KODE_ID 'KODE_ID_TRADER_ASAL', A.ID AS 'ID_TRADER_ASAL', 
								A.NAMA 'NAMA_TRADER_ASAL', A.ALAMAT 'ALAMAT_TRADER_ASAL', A.TELEPON, 
								A.KODE_API, A.NOMOR_API, D.LAST_AJU, D.NAMA_TTD, D.KOTA_TTD, 
								B.NOMOR_SKEP 'REGISTRASI',  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
								B.NOMOR_SKEP
							FROM M_TRADER A INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
								LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=C.KODE_TRADER 
								LEFT JOIN M_SETTING D ON A.KODE_TRADER=C.KODE_TRADER 
							WHERE B.TANGGAL_SKEP='" . $maxTglSkep . "' 
								AND A.KODE_TRADER='" . $kode_trader . "'
								AND C.USER_ID='" . $user_id . "' LIMIT 0,1";
                } else {
                    $query = "SELECT A.KODE_TRADER AS 'KODE_TRADER', A.KODE_ID 'KODE_ID_TRADER_TUJUAN', A.ID AS 'ID_TRADER_TUJUAN', 
                            A.NAMA 'NAMA_TRADER_TUJUAN', A.ALAMAT 'ALAMAT_TRADER_TUJUAN', A.TELEPON, 
                            A.KODE_API, A.NOMOR_API, D.LAST_AJU, D.NAMA_TTD, D.KOTA_TTD, 
                            B.NOMOR_SKEP 'REGISTRASI',  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
                            B.NOMOR_SKEP
                            FROM M_TRADER A INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                            LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=C.KODE_TRADER 
                            LEFT JOIN M_SETTING D ON A.KODE_TRADER=C.KODE_TRADER 
                            WHERE B.TANGGAL_SKEP='" . $maxTglSkep . "' 
                            AND A.KODE_TRADER='" . $kode_trader . "'
                            AND C.USER_ID='" . $user_id . "'";
                }
                $hasil = $conn->main->get_result($query);
                if ($hasil) {
                    foreach ($query->result_array() as $row) {
                        $dataarray = $row;
                    }
                }
            }
			$data = array('act' => 'update');
		}
		$data = array_merge($data, array('aju' => $aju,
										'sess' => $dataarray,
										'asal' => $conn->main->get_mtabel('KBINOUT'),
										'jenis_barang' => $conn->main->get_mtabel('JENIS_BARANG'),
										'tujuan' => $conn->main->get_mtabel('TUJUAN'),
										'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_KIRIMBC27'),
										'kode_id' => $conn->main->get_mtabel('KODE_ID', 1, false),
										'kode_penutup' => $conn->main->get_mtabel('KODE_PENUTUP'),
										'cara_angkut' => $conn->main->get_mtabel('MODA'),
										'jenis_segel' => $conn->main->get_mtabel('JENIS_SEGEL'),
										'tpb_asal' => $conn->main->get_mtabel('JENIS_TPB_ASAL'),
										'dataasal'=>$asal));
        return $data;
	}
	
	function set_header($type = "", $isajax = "") {
        $func = & get_instance();
        $func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
        if (strtolower($type) == "save" || strtolower($type) == "update") {
            foreach ($this->input->post('HEADER') as $a => $b) {
                $HEADER[$a] = $b;
				$HEADER["KODE_TRADER"] = $kode_trader;
            }			
            $ID_TRADER_ASAL = str_replace('-', '', $HEADER['ID_TRADER_ASAL']);
            $HEADER['ID_TRADER_ASAL'] = str_replace('.', '', $ID_TRADER_ASAL);
            $ID_TRADER_TUJUAN = str_replace('-', '', $HEADER['ID_TRADER_TUJUAN']);
            $HEADER['ID_TRADER_TUJUAN'] = str_replace('.', '', $ID_TRADER_TUJUAN);
            if (strtolower($type) == "save") {
                if ($this->input->post('NOMOR_AJU') != '') {
                    $car = $this->input->post('NOMOR_AJU');
                } else {
                    $func->main->get_car($car, 'BC27');
                }
                $HEADER["CREATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["CREATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $HEADER["NOMOR_AJU"] = $car;
                
                $sq27 = $this->db->query("SELECT NOMOR_AJU FROM t_bc27_hdr WHERE NOMOR_AJU='" . $car . "' AND KODE_TRADER='".$kode_trader."'");
                if ($sq27->num_rows() > 0) {
                    echo "MSG#ERR#Nomor Aju yang anda masukan sudah ada di database.#";
                    die();
                }
				
				$this->db->where(array('NOMOR_AJU'=>$car,"KODE_TRADER"=>$kode_trader));
				$this->db->delete('T_BC27_DOKASAL');
				$arrasal= $this->input->post('ASAL');
				$arrkeys = array_keys($arrasal);
				for($i=0;$i<count($arrasal[$arrkeys[0]]);$i++){			
					for($j=0;$j<count($arrkeys);$j++){
						$data[$arrkeys[$j]] = $arrasal[$arrkeys[$j]][$i];
					}		
					$data["KODE_TRADER"] = $kode_trader;
					$data["NOMOR_AJU"] = $car;
					if($data["NOMOR_DAFTAR"]&&$data["TANGGAL_DAFTAR"]){
						$this->db->insert('T_BC27_DOKASAL',$data);
					}
				}
                $exec = $this->db->insert('t_bc27_hdr', $HEADER);
                if ($exec) {
                    if ($this->input->post('NOMOR_AJU') == '') $func->main->set_car('BC27');
                    $func->main->activity_log('ADD HEADER BC27', 'CAR=' . $car);
                    if ($HEADER['TIPE_DOK'] == 'KELUAR') {
                        echo "MSG#OK#Proses header Berhasil#" . $car . "#" . site_url() . "/pengeluaran/LoadHeader/bc27/" . $car . "#bc27#";
                    } else {
                        echo "MSG#OK#Proses header Berhasil#" . $car . "#" . site_url() . "/pemasukan/LoadHeader/bc27/" . $car . "#bc27#";
                    }
                } else {
                    echo "MSG#ERR#Simpan data header Gagal#";
                }
            } else if (strtolower($type) == "update") {
                foreach ($this->input->post('HEADERDOK') as $a => $b) {
                    $HEADERDOK[$a] = $b;
                }
                unset($HEADERDOK["NOMOR_PENDAFTARAN_EDIT"],$HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"]);
                $CAR = $HEADER["NOMOR_AJU"];
                $HEADER["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["UPDATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $this->db->where(array('NOMOR_AJU' => $HEADER["NOMOR_AJU"], 'KODE_TRADER'=>$kode_trader));
                $TGLREALISASI = $func->main->get_uraian("SELECT TANGGAL_REALISASI FROM t_bc27_hdr WHERE NOMOR_AJU = '" . $CAR . "' AND KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'", "TANGGAL_REALISASI");
                if ($this->input->post("STATUS_DOK") == "LENGKAP" && $NODAF && $TGDAF && $TGLREALISASI == "" || $TGLREALISASI == "null") {
                    $HEADERDOK["STATUS"] = '07';
                    $exec = $this->db->update('t_bc27_hdr', array_merge($HEADERDOK, $HEADER));
                } else {
                    $exec = $this->db->update('t_bc27_hdr', $HEADER);
                }
				$this->db->where(array('NOMOR_AJU'=>$CAR,"KODE_TRADER"=>$kode_trader));
				$this->db->delete('T_BC27_DOKASAL');
				$arrasal= $this->input->post('ASAL');
				$arrkeys = array_keys($arrasal);
				for($i=0;$i<count($arrasal[$arrkeys[0]]);$i++){			
					for($j=0;$j<count($arrkeys);$j++){
						$data[$arrkeys[$j]] = $arrasal[$arrkeys[$j]][$i];
					}		
					$data["KODE_TRADER"] = $kode_trader;
					$data["NOMOR_AJU"] = $CAR;
					if($data["NOMOR_DAFTAR"]&&$data["TANGGAL_DAFTAR"]){
						$this->db->insert('T_BC27_DOKASAL',$data);
					}
				}
                if ($exec) {
                    $func->main->activity_log('ADD HEADER BC27', 'CAR=' . $CAR);
                    if ($this->input->post('flagrevisi')) {
                        echo "MSG#OK#Proses header Berhasil#" . $CAR . "#";
                    } else {
                        if ($HEADER['TIPE_DOK'] == 'KELUAR') {
                            echo "MSG#OK#Proses header Berhasil#" . $CAR . "#" . site_url() . "/pengeluaran/LoadHeader/bc27/" . $CAR . "#bc27#";
                        } else {
                            echo "MSG#OK#Proses header Berhasil#" . $CAR . "#" . site_url() . "/pemasukan/LoadHeader/bc27/" . $CAR . "#bc27#";
                        }
                    }
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            }
        }
    }
}
?>
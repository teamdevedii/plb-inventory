<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Bahan_baku_act extends CI_Model{	
	function addBB(){
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$seri = $this->input->post('SERI');
		$KodeBrg = $this->input->post('KODE_BARANG');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		foreach($this->input->post('BARANGBB') as $a => $b){				
			$arrinsertBB[$a] = $b;
			$arrinsertBB["KODE_TRADER"] = $kode_trader;
		}
		$seriBB = (int)$func->main->get_uraian("SELECT MAX(SERIBB) AS MAXSERI FROM t_bc27_bb 
												WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' AND SERIBJ='".$seri."'", "MAXSERI") + 1;
		$jumDta = (int)$func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS JUMREC FROM t_bc27_bb 
												WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' AND SERIBJ='".$seri."' 
												AND KODE_BARANG='".$KodeBrg."'", "JUMREC"); 
												
		$SQL1="SELECT KODE_BARANG FROM T_BC27_BJ WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU='".$aju."' AND SERIBJ='".$seri."'";
		$getKodeBrg = $this->db->query($SQL1)->row(); 
		$getKodeBrg = $getKodeBrg->KODE_BARANG;
		
		$arrinsertBB["KODE_HS"] = str_replace(".","",$arrinsertBB["KODE_HS"]);
		$arrinsertBB["STATUS"] = '04';
		$arrinsertBB["KODE_BARANG"] = $KodeBrg;
		$arrinsertBB["SERIBJ"] = $seri;
		$arrinsertBB["SERIBB"] = $seriBB;
		$arrinsertBB["NOMOR_AJU"] = $aju;
		 
		if($KodeBrg==$getKodeBrg){
			echo "MSG#ERR#Simpan data Barang Gagal, Kode Barang Sudah Pernah Digunakan.#";die();
		}
		
		if($jumDta > 0){
			echo "MSG#ERR#Simpan data Barang Gagal, Kode Barang Sudah Pernah Digunakan.#";die();
		}else{
			$exec = $this->db->insert('t_bc27_bb', $arrinsertBB);
			if($exec){
				$func->main->activity_log('ADD BB BC27','CAR='.$aju,' SERIBJ='.$seri.', SERIBB='.$seriBB);
				echo "MSG#OK#Simpan data Barang Berhasil#";
			}else{					
				echo "MSG#ERR#Simpan data Barang Gagal#";die();
			}
		}
	}
	
	function editBB(){
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$seri = $this->input->post('SERI');
		$seri_bb = $this->input->post('SERI_BB');
		$KodeBrg = $this->input->post('KODE_BARANG');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		
		foreach($this->input->post('BARANGBB') as $a => $b){
			$arrinsertBB[$a] = $b;
			$arrinsertBB["KODE_TRADER"] = $kode_trader;
		}
		$arrinsertBB["KODE_BARANG"] = $KodeBrg;
		$arrinsertBB["KODE_HS"] = str_replace(".","",$arrinsertBB["KODE_HS"]);
		$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERIBJ' => $seri, 'SERIBB' => $seri_bb));
		$exec = $this->db->update('t_bc27_bb', $arrinsertBB);	
		if($exec){
			$func->main->activity_log('EDIT BB BC27','CAR='.$aju,' SERIBJ='.$seri.', SERIBB='.$seri_bb);
			echo "MSG#OK#Ubah data Barang Berhasil#";
		}else{					
			echo "MSG#ERR#Ubah data Barang Gagal#";die();
		}
	}
	
	function deleteBB(){
		$func = get_instance();
		$func->load->model("main","main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		foreach($this->input->post('tb_chkfbahan_baku') as $chkitem){
			$arrchk = explode("|", $chkitem);
			$aju  = $arrchk[0];
			$key1 = $arrchk[1];
			$key2 = $arrchk[2];				
			$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERIBJ' => $key1,'SERIBB' => $key2));
			$exec = $this->db->delete('t_bc27_bb');	
			$func->main->activity_log('EDIT BB BC27','CAR='.$aju,' SERIBJ='.$key1.', SERIBB='.$key2);
		}
			
		if($exec){
			echo "MSG#OK#Hapus data Barang Berhasil#".site_url()."/pemasukan/load_list_BB/bahan_baku/bc27/".$aju."/".$key1."#";
		}else{					
			echo "MSG#ERR#Hapus data Barang Gagal#";die();
		}

	}
	function listDataBB($aju,$seri,$key){
		$func = get_instance();
		$func->load->model("main","main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		$sql = "SELECT *, f_satuan(KODE_SATUAN) AS URAIAN_SATUAN FROM t_bc27_bb
				WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU='".$aju."' AND SERIBJ='".$seri."' AND SERIBB='".$key."'";
		$hasil = $func->main->get_result($sql);
		if($hasil){
			foreach($sql->result_array() as $row){
				$dataarray = $row;
			}
		}	
		return $dataarray;
	}	
}
?>
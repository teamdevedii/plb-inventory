<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kemasan_act extends CI_Model{
	function get_kemasan($aju="",$seri=""){		
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($aju && $seri){
			$query = "SELECT NOMOR_AJU, KODE_KEMASAN, JUMLAH, MERK_KEMASAN,f_kemas(KODE_KEMASAN) URAIAN_KEMASAN 
						FROM t_bc27_kms WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' AND KODE_KEMASAN = '".$seri."'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update',
								  'sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save');
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,
						  				 'seri' => $seri));
		return $data;
	}
	
	function set_kemasan($type="", $isajax=""){		
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($type=="save" || $type=="update"){	
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('KEMASAN') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"] = $kode_trader;
			}
			if($type=="save"){
				$arrinsert["NOMOR_AJU"] = $aju;
				$SQL = "SELECT KODE_KEMASAN FROM t_bc27_kms WHERE KODE_TRADER = '".$kode_trader."' AND KODE_KEMASAN='".$arrinsert["KODE_KEMASAN"]."' AND NOMOR_AJU='".$aju."'";
				$data = $this->db->query($SQL);
				if($data->num_rows() > 0){
					echo "MSG#ERR#Jenis Kemasan Sudah ada#";die();
				}
				$exec = $this->db->insert('t_bc27_kms', $arrinsert);
				if($exec){
					$func->main->activity_log('ADD KEMASAN BC27','CAR='.$aju.', KODE_KEMASAN='.$arrinsert["KODE_KEMASAN"]);
					echo "MSG#OK#Simpan data Kemasan Berhasil#";
				}else{					
					echo "MSG#ERR#Simpan data Kemasan Gagal#";
				}	
			}else{						
				$seri = $this->input->post('seri');					
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $seri));
				$exec = $this->db->update('t_bc27_kms', $arrinsert);	
				if($exec){
					$func->main->activity_log('EDIT KEMASAN BC27','CAR='.$aju.', KODE_KEMASAN='.$seri);
					echo "MSG#OK#Update data Barang Berhasil#edit#";
				}else{					
					echo "MSG#ERR#Update data Barang Gagal#edit#";
				}				
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkemasan') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$kd = $arrchk[1];				
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kd));
				$exec = $this->db->delete('t_bc27_kms');	
				$func->main->activity_log('DELETE KEMASAN BC27','CAR='.$aju.', KODE_KEMASAN='.$kd);
			}
			if($exec){
				echo "MSG#OK#Hapus data Kemasan Berhasil#".site_url()."/pemasukan/detil/kemasan/bc27/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Kemasan Gagal#del#";die();
			}
		}
	}
}
?>
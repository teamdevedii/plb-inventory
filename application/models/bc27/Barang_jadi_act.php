<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Barang_jadi_act extends CI_Model{
	function get_barang_jadi($aju="",$seri=""){		
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($aju && $seri){
			$query = "Select NOMOR_AJU, SERIBJ, KODE_HS, URAIAN_BARANG, MERK, TIPE, UKURAN, SPF, KODE_BARANG, KODE_SATUAN, 
						JUMLAH_SATUAN, JUMLAH_BB, f_satuan(KODE_SATUAN) AS URAIAN_SATUAN,(SELECT COUNT(NOMOR_AJU) 
						FROM t_bc27_bb WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' 
					  	AND SERIBJ= '".$seri."') AS 'JUM_BB' 
					FROM t_bc27_bj 
						WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' AND SERIBJ = '$seri'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update', 'sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save');
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju, 'seri' => $seri));
		return $data;
	}
	
	function set_barang_jadi($type="", $isajax=""){		
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($type=="save" || $type=="update"){	
			$aju = $this->input->post('NOMOR_AJUNYA');
			$KodeBrg = $this->input->post('KODE_BARANG');
			
			foreach($this->input->post('BARANGJD') as $a => $b){
				$arrinsert[$a] = $b;
			}
			
			$arrinsert["KODE_TRADER"] = $kode_trader;
			if($type=="save"){
				$seriBJ = (int)$func->main->get_uraian("SELECT MAX(SERIBJ) AS MAXSERI FROM t_bc27_bj WHERE NOMOR_AJU = '".$aju."' AND KODE_TRADER = '".$kode_trader."'", "MAXSERI") + 1;								  				
				$jumDt  = (int)$func->main->get_uraian("SELECT COUNT(*) AS JUM FROM t_bc27_bj  WHERE KODE_BARANG='".$KodeBrg."' AND NOMOR_AJU='".$aju."' AND KODE_TRADER = '".$kode_trader."'", "JUM");
				if($jumDt > 0){
					echo "MSG#ERR#Penyimpanan Data Gagal.<br><p style=\"margin-left:168px;\">Kode Barang sudah digunakan.</p>#";
				}else{
					$arrinsert["KODE_BARANG"] = $KodeBrg;
					$arrinsert["SERIBJ"] = $seriBJ;
					$arrinsert["NOMOR_AJU"] = $aju;
					$arrinsert["STATUS"] = '04';
					$arrinsert["KODE_HS"] = str_replace(".","",$arrinsert["KODE_HS"]);
					$exec = $this->db->insert('t_bc27_bj', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD BJ BC27','CAR='.$aju,' SERIBJ='.$seriBJ);
						echo "MSG#OK#Simpan data Barang Jadi Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Barang Jadi Gagal#";
					}
				}
			}else{
				$arrinsert["KODE_BARANG"] = $KodeBrg;		
				$arrinsert["STATUS"] = '04';		
				$arrinsert["KODE_HS"] = str_replace(".","",$arrinsert["KODE_HS"]);		
				$SERIBJ = $this->input->post('SERIBJ');					
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERIBJ' => $SERIBJ));
				$exec = $this->db->update('t_bc27_bj', $arrinsert);	
				if($exec){
					$func->main->activity_log('EDIT BJ BC27','CAR='.$aju,' SERIBJ='.$SERIBJ);
					echo "MSG#OK#Update data Barang Jadi Berhasil#edit#";
				}else{					
					echo "MSG#ERR#Update data Barang Jadi Gagal#edit#";
				}				
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfbarang_jadi') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$seri = $arrchk[1];				
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERIBJ' => $seri));
				$exec = $this->db->delete('t_bc27_bj');	
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERIBJ' => $seri));
				$exec = $this->db->delete('t_bc27_bb');	
				$func->main->activity_log('DELETE BJ BC27','CAR='.$aju,' SERIBJ='.$seri);
			}
			if($exec){
				echo "MSG#OK#Hapus data Barang Jadi Berhasil#".site_url()."/pemasukan/detil/barang_jadi/bc27/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Barang Jadi Gagal#del#";die();
			}
		}
	}
}
?>
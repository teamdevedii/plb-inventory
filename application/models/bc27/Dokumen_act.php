<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Dokumen_act extends CI_Model{
	function set_dokumen($type="", $isajax=""){	
		$kode_trader = $this->newsession->userdata('KODE_TRADER');	
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kd_dok = $this->input->post('kd_dok');
		if($type=="save" || $type=="update"){	
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('DOKUMEN') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"] = $kode_trader;
			}
			if($type=="save"){
				$arrinsert["NOMOR_AJU"] = $aju;
				$NOMOR = $arrinsert["NOMOR"];
				$KODEDOKUMEN = $arrinsert["KODE_DOKUMEN"]; 
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(*) AS JUM FROM T_bc27_DOK WHERE NOMOR_AJU='".$aju."' 
														   AND KODE_DOKUMEN = '".$KODEDOKUMEN."' AND NOMOR='".$arrinsert["NOMOR"]."'
														   AND KODE_TRADER='".$kode_trader."'", "JUM");
				if($countKode > 0){
					#JIKA NOMOR SUDAH ADA DI DATABASE
					echo "MSG#ERR#Kode dan Nomor Dokumen sudah Pernah digunakan#";
				}else{
					$exec = $this->db->insert('t_bc27_dok', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD DOKUMEN BC27','CAR='.$aju.', KODE_DOKUMEN='.$KODEDOKUMEN.', NOMOR='.$NOMOR);
						echo "MSG#OK#Simpan data Dokumen Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Dokumen Gagal#";
					}
				}
			}else{		
				$KODEDOKUMEN = $arrinsert["KODE_DOKUMEN"];		
				$seri = $this->input->post('seri');					
				$this->db->where(array('NOMOR_AJU'=>$aju,'KODE_DOKUMEN'=>$kd_dok, "NOMOR"=>$seri,"KODE_TRADER"=>$kode_trader));
				$exec = $this->db->update('t_bc27_dok', $arrinsert);	
				if($exec){
					$func->main->activity_log('EDIT DOKUMEN BC27','CAR='.$aju.', KODE_DOKUMEN='.$KODEDOKUMEN.', NOMOR='.$seri);
					echo "MSG#OK#Update data Barang Berhasil#edit#";
				}else{					
					echo "MSG#ERR#Update data Barang Gagal#edit#";
				}					
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfdokumen') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$NOMOR = str_replace("~","/",$arrchk[1]);
				$kode_dok = $arrchk[2];				
				$this->db->where(array('NOMOR_AJU'=>$aju,'KODE_DOKUMEN'=>$kode_dok, "NOMOR"=>$NOMOR,"KODE_TRADER"=>$kode_trader));
				$exec = $this->db->delete('t_bc27_dok');
				$func->main->activity_log('DELETE DOKUMEN BC27','CAR='.$aju.', KODE_DOKUMEN='.$kode_dok.', NOMOR='.$NOMOR);	
			}
			if($exec){
				echo "MSG#OK#Hapus data Dokumen Berhasil#".site_url()."/pemasukan/detil/dokumen/bc27/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Dokumen Gagal#del#";die();
			}
		}
	}
	
	function get_dokumen($aju="",$seri="",$kode_dok=""){	
		$kode_trader = $this->newsession->userdata('KODE_TRADER');		
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		if($aju && $seri){
			$query = "SELECT KODE_DOKUMEN, NOMOR, TANGGAL, NOMOR_AJU_BC23 
						FROM t_bc27_dok 
					  WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' AND 
					  	KODE_DOKUMEN = '".$kode_dok."' AND NOMOR='".$seri."'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save','KODE'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,'seri' => $seri,'KODE'=> $conn->main->get_mtabel('DOKUMEN_UTAMA')));
		return $data;
	}
}
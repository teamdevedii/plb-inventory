<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Header_act extends CI_Model{
	function get_sstb($act="",$id=""){
		$func =&get_instance();
		$func->load->model("main", "main", true);
		$KODE_TRADER = $this->newsession->userdata("KODE_TRADER");
		$generate = $this->input->post("generate");
		#$DATA = array();
		if($act=='save'){			
			$SQL = "SELECT KODE_ID AS KODE_ID_PENGIRIM,ID AS NPWP_PERUSAHAAN_PENGIRIM,  NAMA AS NAMA_PERUSAHAAN_PENGIRIM, 
					ALAMAT AS ALAMAT_PERUSAHAAN_PENGIRIM FROM M_TRADER 
					WHERE KODE_TRADER='".$KODE_TRADER."'";
			$hasil = $func->main->get_result($SQL);
		}
		elseif($act=="update"){
			$SQL = "SELECT * FROM T_SSTB_HDR WHERE IDSSTB='".$id."'
					AND KODE_TRADER='".$KODE_TRADER."'";
			$hasil = $func->main->get_result($SQL);		
		}
		if($hasil){
			foreach($SQL->result_array() as $row){
				$DATA = $row;
			}
		}					
		return array("act" => $act,"sess" => $DATA,"IDSSTB" => $id,'kode_id' => $func->main->get_mtabel('KODE_ID', 1, FALSE));	
	}
	
	function set_sstb(){
		$func =&get_instance();
		$func->load->model("main", "main", true);
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		
		$act = $this->input->post('act');							
		foreach($this->input->post('DATA') as $a => $b){
			$DATA[$a] = $b; 
		}
		$ret = "MSG#ERR#Proses Data Gagal.#";		
		if($act=="save"){
			$DATA["STATUS"] = '00';
			$DATA["CREATED_BY"]	= $this->newsession->userdata('USER_ID');
			$DATA["CREATED_TIME"]	= date('Y-m-d H:i:s',time()-60*60*1);	
			$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
			
			$SQL = "SELECT NOMOR_SURAT FROM T_SSTB_HDR WHERE NOMOR_SURAT='".$DATA["NOMOR_SURAT"]."' 
					AND KODE_TRADER ='".$KODE_TRADER."'";
			$rs = $this->db->query($SQL);				
			if($rs->num_rows()>0){
				$ret =  "MSG#ERR#Nomor Dokumen ".$DATA["NOMOR_SURAT"]." sudah ada di dalam aplikasi.";
			}else{
				$DATA["KODE_TRADER"] = $KODE_TRADER;
				$NPWP_PERUSAHAAN_PENGIRIM_TEMP = str_replace(".","",$DATA["NPWP_PERUSAHAAN_PENGIRIM"]);
				$NPWP_PERUSAHAAN_PENGIRIM = str_replace("-","",$NPWP_PERUSAHAAN_PENGIRIM_TEMP);
				$DATA["NPWP_PERUSAHAAN_PENGIRIM"] = $NPWP_PERUSAHAAN_PENGIRIM;
				
				$NPWP_PERUSAHAAN_PENERIMA_TEMP = str_replace(".","",$DATA["NPWP_PERUSAHAAN_PENERIMA"]);
				$NPWP_PERUSAHAAN_PENERIMA = str_replace("-","",$NPWP_PERUSAHAAN_PENERIMA_TEMP);
				$DATA["NPWP_PERUSAHAAN_PENERIMA"] = $NPWP_PERUSAHAAN_PENERIMA;
				$exec = $this->db->insert('T_SSTB_HDR', $DATA);
				$IDSSTB = $this->db->insert_id();
				$ret = "MSG#OK#Proses Data Berhasil#".$IDSSTB."#".site_url()."/pemasukan/LoadSSTB/update/".$IDSSTB;
			}
		}
		elseif($act=="update"){
			$NPWP_PERUSAHAAN_PENGIRIM_TEMP = str_replace(".","",$DATA["NPWP_PERUSAHAAN_PENGIRIM"]);
			$NPWP_PERUSAHAAN_PENGIRIM = str_replace("-","",$NPWP_PERUSAHAAN_PENGIRIM_TEMP);
			$DATA["NPWP_PERUSAHAAN_PENGIRIM"] = $NPWP_PERUSAHAAN_PENGIRIM;
			
			$NPWP_PERUSAHAAN_PENERIMA_TEMP = str_replace(".","",$DATA["NPWP_PERUSAHAAN_PENERIMA"]);
			$NPWP_PERUSAHAAN_PENERIMA = str_replace("-","",$NPWP_PERUSAHAAN_PENERIMA_TEMP);
			$DATA["NPWP_PERUSAHAAN_PENERIMA"] = $NPWP_PERUSAHAAN_PENERIMA;
			$DATA["CREATED_BY"]	= $this->newsession->userdata('USER_ID');
			$DATA["CREATED_TIME"]	= date('Y-m-d H:i:s',time()-60*60*1);
			$IDSSTB_HIDE = $this->input->post('IDSSTB_HIDE');
			$this->db->where(array('IDSSTB'=>$IDSSTB_HIDE,'KODE_TRADER'=>$KODE_TRADER));
			$exec = $this->db->update('T_SSTB_HDR', $DATA);
			$ret = "MSG#OK#Proses Data Berhasil#".site_url()."/pemasukan/sstb/ajax";
		}
		elseif($act=="delete"){
			$dataCheck = $this->input->post('tb_chkfsstb');
			foreach($dataCheck as $chkitem){
				$arrchk = explode("|", $chkitem);
				$this->db->where(array('IDSSTB'=>$chkitem));
				$this->db->delete('T_SSTB_HDR');
				$this->db->where(array('IDSSTB'=>$chkitem));
				$this->db->delete('T_SSTB_DTL');
				$ret = "MSG#OK#Proses Data Berhasil#".site_url()."/pemasukan/sstb/ajax";
			}		
		}			
		echo $ret;
	}
}
?>
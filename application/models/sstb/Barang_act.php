<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Barang_act extends CI_Model{
	function set_barang(){  
		$func =&get_instance();
		$func->load->model("main", "main", true);
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		$IDSSTB = $this->input->post('NOMOR_AJU');
		$act = $this->input->post('act');
		
		foreach($this->input->post('DATA') as $a => $b){
			$DATA[$a] = $b; 
		}
		$ret = "MSG#ERR#Proses Data Gagal.#";		
		if($act=="save"){
			$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		
			$SQL = "SELECT SERIAL FROM T_SSTB_DTL WHERE SERIAL='".$DATA["SERIAL"]."' 
					AND KODE_TRADER ='".$KODE_TRADER."'";
			$rs = $this->db->query($SQL);				
			if($rs->num_rows()>0){
				$ret =  "MSG#ERR#Kode sstb tersebut sudah ada";
			}else{
				$DATA["IDSSTB"] = $IDSSTB;
				$DATA["KODE_TRADER"] = $KODE_TRADER;
				$exec = $this->db->insert('T_SSTB_DTL', $DATA);
				$ret = "MSG#OK#Simpan data Barang Berhasil.#edit";
			}
		}elseif($act=="update"){
			$SERIAL_HIDE = $this->input->post('SERIAL_HIDE');
			$this->db->where(array('SERIAL'=>$SERIAL_HIDE,'KODE_TRADER'=>$KODE_TRADER));
			$exec = $this->db->update('T_SSTB_DTL', $DATA);
			$ret = "MSG#OK#Simpan data Barang Berhasil.#edit";
		}elseif($act=="delete"){
			foreach($this->input->post('tb_chkfbarang') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$id  = $arrchk[0];
				$seri = $arrchk[1];
				$this->db->where(array('IDSSTB' => $id, 'SERIAL' => $seri));
				$exec = $this->db->delete('T_SSTB_DTL');
			}
			if($exec){
				$ret = "MSG#OK#Hapus data Barang Berhasil#" . site_url() . "/pemasukan/detil/barang/sstb/" . $id;
			}else{
				$ret = "MSG#ERR#Hapus data Barang Gagal.";
			}
		}			
		echo $ret;
	}
	
	
	function get_barang($id="",$seri=""){
		$func = get_instance();
		$func->load->model("main");
		$KODE_TRADER = $this->newsession->userdata("KODE_TRADER");
		$DATA = array();			
		
		if($id && $seri){
			$SQL = "SELECT *, f_satuan(KODE_SATUAN) AS KODE_SATUANUR , f_ref('ASAL_JENIS_BARANG',JNS_BARANG) AS UR_JENIS_BARANG
					FROM T_SSTB_DTL WHERE IDSSTB = '".$id."' AND SERIAL = '".$seri."' AND
			  		KODE_TRADER='".$KODE_TRADER."'";
			$hasil = $func->main->get_result($SQL);
			if($hasil){
				foreach($SQL->result_array() as $row){
					$DATA = $row;
				}
			}	
			$ret  = array("act" => "update","DATA" => $DATA ,"IDSSTB" => $id);		
		}else{
			$ret  = array("act" => "save","DATA" => $DATA ,"IDSSTB" => $id);		
		}
		
		
		return $ret ;
	}
}
?>
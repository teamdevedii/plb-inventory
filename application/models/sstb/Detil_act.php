<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Detil_act extends CI_Model{
	
	function list_detil($tipe="",$IDSSTB=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		if($tipe=="barang"){
			$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
				SERIAL 'SERI', CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'SATUAN', JUMLAH_SATUAN AS 'JUMLAH',  
				IDSSTB FROM t_sstb_dtl ";	
						
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
							array('URAIAN_BARANG', 'URAIAN BARANG'),
							array('JNS_BARANG', 'JENIS BARANG','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG'))));	
			
		}else{
			return "Failed";exit();
		}	
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pengeluaran/listdetil/".$tipe."/".$IDSSTB."/sstb");
		$this->newtable->hiddens(array('IDSSTB','SERI'));			
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("f".$tipe);
		$this->newtable->set_divid("div".$tipe);
		$this->newtable->orderby(2);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
		
	function list_tab($id=""){ 
		$content='<div class="row-fluid">
				  	<div class="tabbable">
						<ul class="nav nav-tabs" id="myTab">
					  		<li class="active"> <a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
						</ul>
						<div class="tab-content">
							<div id="tabBarang" class="tab-pane in active">'.$this->list_detil("barang",$id).'</div>
						</div>
				  	</div>
				</div>';
		return $content;		
	}
	
	function detil($type="",$dokumen="",$id=""){  
		$this->load->library('newtable');
		$dialog = 'DIALOG-500;260|Form Barang';
		$judul = "Daftar Detil Barang";			
		$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
				SERIAL 'SERI',  
				CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'SATUAN', JUMLAH_SATUAN AS 'JUMLAH',  
			    IDSSTB FROM t_sstb_dtl  WHERE IDSSTB = '".$id."'";
				
		$this->newtable->keys(array("IDSSTB","SERI","JUMLAH","KODE BARANG"));
		$this->newtable->hiddens(array('IDSSTB','SERI'));
		$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
									  array('URAIAN_BARANG', 'URAIAN BARANG')));
		
		$ciuri = (!$this->input->post("ajax")||$act=="ajax")?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/detil/barang/sstb/".$id);
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->orderby(4);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(20);
		$this->newtable->clear();  
		$this->newtable->set_formid("fbarang");		
		$this->newtable->set_divid("divbarang");
		$this->newtable->tipe_proses('button');		
		
		$process = array('Tambah ' => array('ADD', site_url().'/pemasukan/barang/sstb/'.$id, '0', 'icon-plus'),
						 'Ubah ' => array('EDIT', site_url().'/pemasukan/barang/sstb', '1', 'icon-edit'),
						 'Hapus ' => array('DEL', site_url().'/pemasukan/barang/sstb/ajax', 'N','icon-remove red'));
		
		$this->newtable->menu($process);
		$tabel = $this->newtable->generate($SQL);			
		$arrdata = array("judul" => $judul,
						 "tabel" => $tabel);
		if($this->input->post("ajax")||$act=="ajax") return $tabel;				 
		else return $arrdata;	
	}
}
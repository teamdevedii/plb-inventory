<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search_act extends CI_Model{	

	function search($tipe="",$indexField="",$formName="",$getdata=""){
		$func = get_instance();
		$func->load->model("main","main", true);
		$this->load->library('newtable');
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		$NIPER = $this->newsession->userdata('NIPER');
		$rowcount = 8;
		switch($tipe){
			case "BC27ASAL":
				$judul = "Pencarian Data Dokumen BC 2.7";
				$SQL 	= "SELECT NOMOR_AJU, NOMOR_PENDAFTARAN 'NOMOR PENDAFTARAN', 
						   DATE_FORMAT(TANGGAL_PENDAFTARAN,'%d-%m-%Y') 'TANGGAL PENDAFTARAN',  TANGGAL_PENDAFTARAN
						   FROM t_bc27_HDR A
						   WHERE KODE_TRADER='".$KODE_TRADER."' AND TANGGAL_REALISASI IS NOT NULL";
				if($getdata!=""){
					if($getdata=="MASUK;") $TIPE_DOK = "KELUAR";
					else $TIPE_DOK = "MASUK";
					$SQL .= " AND TIPE_DOK = '".$TIPE_DOK."'";
				}
				$order  = "3";
				$hidden = array("TANGGAL_PENDAFTARAN");
				$sort   = "DESC";		
				$key    = array("NOMOR PENDAFTARAN","TANGGAL_PENDAFTARAN");
				$search = array(array('NOMOR_AJU', 'NOMOR AJU'),
						  		array('NOMOR_PENDAFTARAN', 'NOMOR PENDAFTARAN')
						  );			
				$field  = explode(";",$indexField);	
				$show_search=true;	
				
				$SQL_COUNT = "SELECT COUNT(A.NOMOR_AJU) AS JML FROM t_bc27_HDR A
						   WHERE KODE_TRADER='".$KODE_TRADER."' AND TANGGAL_REALISASI IS NOT NULL";
					
			break;
			case "barang_gudang_inout":
				$data = str_replace(";","",$getdata);
				$judul  = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_BARANG 'KODE BARANG', REPLACE(REPLACE(REPLACE(URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG', 
						   f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG', JNS_BARANG
						   FROM T_MST_BARANG 
						   WHERE KODE_TRADER='".$KODE_TRADER."' AND NIPER='".$NIPER."'";		   						   
				$order  = "KODE_BARANG";
				$hidden = array("JNS_BARANG");	
				$key    = array("KODE BARANG","URAIAN BARANG","JNS_BARANG");	
				$search = array(array('KODE_BARANG', 'Kode Barang'),
								array('URAIAN_BARANG', 'Uraian Barang'));		
				$sort   = "ASC";											
				$field  = explode(";",$indexField);	
				$show_search=true;			
				$rowcount = 10;		
			break;
			case "partner":
				$judul = "Pencarian Data Perusahaan";
				$SQL 	= "SELECT KODE_PARTNER 'Kode Perusahaan',NAMA_PARTNER 'Nama Partner' FROM m_trader_partner WHERE KODE_TRADER='".$KODE_TRADER."'";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Perusahaan","Nama Partner");
				$search = array(array('KODE_PARTNER', 'Kode Perusahaan'),array('NAMA_PARTNER', 'Nama Partner'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "perusahaan":
				$judul = "Pencarian Data Perusahaan";
				$SQL 	= "SELECT KODE_TRADER, NIPER, KODE_PARTNER, KODE_ID_PARTNER, ID_PARTNER, 
						   NAMA_PARTNER, ALAMAT_PARTNER, NEGARA_PARTNER, STATUS_PERUSAHAAN, JENIS_PERUSAHAAN
						   FROM m_trader_pemasok WHERE KODE_TRADER='".$KODE_TRADER."'";
				$order  = "2";	
				$sort   = "ASC";		
				$hidden = array();
				$key    = array("Kode Perusahan","Nama Perusahaan");
				$search = array(array('KODE_PARTNER', 'Kode Perusahan'),
							    array('NAMA_PARTNER', 'Nama Perusahaan'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "barangnew":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang";
				if($formName=='fbarangBahanBaku'){
					$id = explode("|",$data);
					$KONDISI = "AND B.ASAL_BARANG = '".$id[1]."' 
								AND B.KONDISI_BARANG = '".$id[2]."' AND B.KODE_GUDANG = '".$id[3]."' 
								AND B.JNS_BARANG = '".$id[4]."' AND B.KODE_BARANG != '".$id[5]."'";
				}
				$SQL 	= "SELECT DISTINCT B.KONDISI_BARANG AS KONDISI_BARANGX, A.KODE_TRADER 'Kode Trader',
						   A.KODE_BARANG 'Kode',f_ref('ASAL_JENIS_BARANG',A.JNS_BARANG) 
						   'Jenis&nbsp;Barang',A.JNS_BARANG 'jns_barang', REPLACE(A.URAIAN_BARANG,'\n','') 'Uraian&nbsp;Barang',
						   A.MERK 'Merk',A.TIPE 'Tipe', A.UKURAN 'Ukuran',A.SPFLAIN 'SPF&nbsp;Lain',
						   FORMAT(A.STOCK_AKHIR,0) 'Stock&nbsp;Akhir',A.HS 'Kode HS',0 'Seri HS',
						   A.SATUAN,A.SATUAN_TERKECIL,A.SATUAN AS KODE_SATUANNYA,f_satuan(A.SATUAN) AS URAIAN_KD_SATUAN, 
						   B.KONDISI_BARANG AS 'Kondisi Barang'
						   FROM T_MST_BARANG A, T_MST_GUDANG_BARANG B
						   WHERE A.KODE_TRADER = B.KODE_TRADER AND A.NIPER = B.NIPER
						   AND A.KODE_BARANG = B.KODE_BARANG AND A.JNS_BARANG = B.JNS_BARANG 
						   AND A.KODE_TRADER='".$KODE_TRADER."' AND A.NIPER = '".$NIPER."' $KONDISI";
				$order  = "A.JNS_BARANG ASC, A.KODE_BARANG";
				$hidden = array("Kode Trader","jns_barang","Kode HS","Seri HS","KODE_SATUAN","SATUAN_TERKECIL","KODE_SATUANNYA",
								"URAIAN_KD_SATUAN",'Merk','Tipe',
						   'Ukuran','SPF&nbsp;Lain','KONDISI_BARANGX');
				$sort   = "ASC";		
				$key    = array("Kode","Uraian&nbsp;Barang","Merk","Tipe","Ukuran","SPF&nbsp;Lain","jns_barang","Kode HS","Seri HS",
								"KODE_SATUAN","KODE_SATUAN_TERKECIL","KODE_SATUANNYA","URAIAN_KD_SATUAN","SATUAN","Kondisi Barang");
				$search = array(array('A.KODE_BARANG', 'Kode Barang'),array('A.URAIAN_BARANG', 'Uraian Barang'),
				          array('A.jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;	
				$rowcount = 10;	
			break;	
			case "barang_produksi":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_BARANG 'KODE BARANG',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
						   REPLACE(REPLACE(REPLACE(URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG',
						   IF(SATUAN_TERKECIL='',SATUAN,IF(SATUAN_TERKECIL IS NULL,SATUAN,SATUAN_TERKECIL)) 'SATUAN',
						   FORMAT(STOCK_AKHIR,2) 'STOCK' , STOCK_AKHIR, JNS_BARANG, HS
						   FROM T_MST_BARANG WHERE KODE_TRADER='".$KODE_TRADER."' AND NIPER='".$NIPER."' ";	
				if($data=="masuk") $SQL.=" AND JNS_BARANG='1'";		   
				if($data=="wip") $SQL.=" AND JNS_BARANG='2'";		   
				if($data=="keluar") $SQL.=" AND JNS_BARANG='3'";		  
				if($data=="sisa") $SQL.=" AND JNS_BARANG='4'";			  
				if($data=="keluarwip") $SQL.=" AND JNS_BARANG in ('3','2')";		    						   					   						   
				$order  = "JNS_BARANG ASC, KODE_BARANG";		
				$sort   = "ASC";											
				$hidden = array("STOCK_AKHIR","JNS_BARANG","HS");	
				$key    = array("KODE BARANG","URAIAN BARANG","JNS_BARANG","JENIS BARANG","SATUAN","STOCK_AKHIR","HS");	
				$search = array(array('KODE_BARANG', 'Kode Barang'),
								array('URAIAN_BARANG', 'Uraian Barang'));
				$field  = explode(";",$indexField);	
				$show_search=true;		
				$rowcount = 10;		
			break;				
			case "barang_gudang":
				$data 	= str_replace(";","",$getdata);
				$judul 	= "Pencarian Data Barang";
				$SQL 	= "SELECT A.KODE_BARANG 'KODE BARANG', REPLACE(REPLACE(REPLACE(A.URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG', 
						   A.JNS_BARANG 'JENIS BARANG', 
						   f_gudang(B.KODE_GUDANG,A.KODE_TRADER) 'GUDANG', FORMAT(B.JUMLAH,2) 'STOCK&nbsp;AKHIR', 
						   B.SATUAN, A.JNS_BARANG, B.KODE_GUDANG, f_satuan(B.SATUAN) 'URAIAN_SATUAN',
						   B.KONDISI_BARANG 'KONDISI'
						   FROM M_TRADER_BARANG A, M_TRADER_BARANG_GUDANG B 
						   WHERE A.KODE_TRADER=B.KODE_TRADER
						   AND A.KODE_BARANG=B.KODE_BARANG AND A.JNS_BARANG=B.JNS_BARANG
						   AND A.KODE_TRADER='".$KODE_TRADER."'";	
				//echo $SQL;
				//die();	   						   
				$order  = "A.KODE_BARANG";
				$hidden = array("JNS_BARANG","KODE_GUDANG","URAIAN_SATUAN");	
				$key    = array("KODE BARANG","URAIAN BARANG","JNS_BARANG","JENIS BARANG","SATUAN","URAIAN_SATUAN");	
				$search = array(array('KODE_BARANG', 'Kode Barang'),
								array('URAIAN_BARANG', 'Uraian Barang'),
								array('KODE_GUDANG','Gudang','tag-select',$func->main->get_combobox("SELECT KODE_GUDANG,NAMA_GUDANG 
								FROM M_TRADER_GUDANG WHERE KODE_TRADER='".$KODE_TRADER."' 
								ORDER BY KODE_GUDANG ","KODE_GUDANG", "NAMA_GUDANG", TRUE)));		
				$sort   = "ASC";											
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "barang_impor":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang Impor";
				$SQL 	= "SELECT A.KODE_BARANG 'KODE BARANG', REPLACE(REPLACE(REPLACE(A.URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG', 
						   f_ref('ASAL_JENIS_BARANG',A.JNS_BARANG) 'JENIS BARANG',B.SATUAN, A.JNS_BARANG, 'IMPOR' AS IMPOR			    
						   FROM T_MST_BARANG A, T_MST_GUDANG_BARANG B 
						   WHERE A.KODE_TRADER=B.KODE_TRADER 
						   AND A.KODE_BARANG=B.KODE_BARANG AND A.JNS_BARANG=B.JNS_BARANG
						   AND A.KODE_TRADER='".$KODE_TRADER."' 
						   AND B.ASAL_BARANG='IMPOR' AND A.JNS_BARANG = '1'";		   						   
				$order  = "A.KODE_BARANG";
				$hidden = array("JNS_BARANG","KODE_GUDANG","IMPOR");	
				$key    = array("KODE BARANG","JNS_BARANG","SATUAN","IMPOR");	
				$search = array(array('A.KODE_BARANG', 'Kode Barang'),
								array('A.URAIAN_BARANG', 'Uraian Barang'));		
				$sort   = "ASC";											
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "barang_lokal":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang Impor";
				$SQL 	= "SELECT A.KODE_BARANG 'KODE BARANG', REPLACE(REPLACE(REPLACE(A.URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG', 
						   f_ref('ASAL_JENIS_BARANG',A.JNS_BARANG) 'JENIS BARANG',B.SATUAN, A.JNS_BARANG, 'LOKAL' AS LOKAL				    
						   FROM T_MST_BARANG A, T_MST_GUDANG_BARANG B 
						   WHERE A.KODE_TRADER=B.KODE_TRADER AND A.NIPER=B.NIPER
						   AND A.KODE_BARANG=B.KODE_BARANG AND A.JNS_BARANG=B.JNS_BARANG
						   AND A.KODE_TRADER='".$KODE_TRADER."' AND A.NIPER='".$NIPER."'
						   AND B.ASAL_BARANG='LOKAL' AND A.JNS_BARANG = '1'";		   						   
				$order  = "A.KODE_BARANG";
				$hidden = array("JNS_BARANG","KODE_GUDANG","LOKAL");	
				$key    = array("KODE BARANG","JNS_BARANG","SATUAN","LOKAL");	
				$search = array(array('A.KODE_BARANG', 'Kode Barang'),
								array('A.URAIAN_BARANG', 'Uraian Barang'));		
				$sort   = "ASC";											
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;	
			case "barangsearch":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT A.KODE_BARANG 'KODE BARANG', REPLACE(REPLACE(REPLACE(A.URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'URAIAN BARANG', 
						   f_ref('ASAL_JENIS_BARANG',A.JNS_BARANG) 'JENIS BARANG',B.SATUAN, A.JNS_BARANG, 'LOKAL' AS LOKAL				    
						   FROM T_MST_BARANG A, T_MST_GUDANG_BARANG B 
						   WHERE A.KODE_TRADER=B.KODE_TRADER AND A.NIPER=B.NIPER
						   AND A.KODE_BARANG=B.KODE_BARANG AND A.JNS_BARANG=B.JNS_BARANG
						   AND A.KODE_TRADER='".$KODE_TRADER."' AND A.NIPER='".$NIPER."'
						   AND B.ASAL_BARANG='IMPOR'";		   						   
				$order  = "A.KODE_BARANG";
				$hidden = array("JNS_BARANG","KODE_GUDANG","LOKAL");	
				$key    = array("KODE BARANG");	
				$search = array(array('A.KODE_BARANG', 'Kode Barang'),
								array('A.URAIAN_BARANG', 'Uraian Barang'));		
				$sort   = "ASC";											
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;	
			case "dokumen_izin":
				$judul = "Pencarian Data Dokumen";
				$SQL 	= "SELECT KODE 'KODE',URAIAN 'URAIAN'  FROM M_TABEL where JENIS='DOKUMEN_IZIN' ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE','KODE'),array('URAIAN','URAIAN'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;			
			case "valuta":
				$judul = "Pencarian Data Valuta";
                $SQL = "SELECT KODE_VALUTA 'Kode Valuta',URAIAN_VALUTA 'Uraian Valuta' FROM m_valuta ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Valuta", "Uraian Valuta");
                $search = array(array('KODE_VALUTA', 'Kode Valuta'), array('URAIAN_VALUTA', 'Uraian Valuta'));
                $field = explode(";", $indexField);
                $show_search = true;				
			break;
			case "kpbc":
				$judul = "Pencarian Data KPBC";
				$SQL 	= "SELECT KODE_KPBC 'Kode Kpbc',URAIAN_KPBC 'Uraian Kpbc' FROM m_kpbc ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Kpbc","Uraian Kpbc");
				$search = array(array('KODE_KPBC', 'Kode Kpbc'),array('URAIAN_KPBC', 'Uraian Kpbc'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "pembeli":
				$judul = "Pencarian Data Pembeli";
				$SQL 	= "SELECT NAMA_PARTNER 'Nama Pemasok',ALAMAT_PARTNER 'Alamat Pemasok',
				           f_negara(NEGARA_PARTNER) 'Negara Pemasok', NEGARA_PARTNER 'Negara' 
						   FROM m_trader_partner WHERE KODE_TRADER='".$KODE_TRADER."'";
				$order  = "1";
				$hidden = array("Negara");	
				$sort   = "ASC";		
				$key    = array("Nama Pemasok","Alamat Pemasok","Negara");
				$search = array(array('NAMA_PARTNER', 'Nama Pemasok'),array('ALAMAT_PARTNER', 'Alamat Pemasok'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;			
			case "barang":
				$judul = "Pencarian Data Barang";
                $SQL = "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 
				           'Jenis&nbsp;Barang',JNS_BARANG 'jns_barang',
						    REPLACE(REPLACE(REPLACE(URAIAN_BARANG,'\n',''),'&QUOT;',''),';','') 'Uraian&nbsp;Barang',
                            MERK 'Merk',TIPE 'Tipe',
						   REPLACE(UKURAN,'\"','&quot;') 'Ukuran',SPFLAIN 'SPF&nbsp;Lain',FORMAT(STOCK_AKHIR,0) 'Stock&nbsp;Akhir',KODE_HS 'Kode HS','0' SeriHS,
						   KODE_SATUAN,KODE_SATUAN_TERKECIL,KODE_SATUAN AS KODE_SATUANNYA,f_satuan(KODE_SATUAN) AS URAIAN_KD_SATUAN
						   FROM m_trader_barang 
						   WHERE KODE_TRADER='" . $KODE_TRADER . "'";
                $order = "2";
                $hidden = array("Kode Trader", "jns_barang", "Kode HS", "Seri HS", "KODE_SATUAN", "KODE_SATUAN_TERKECIL", "KODE_SATUANNYA",
                    "URAIAN_KD_SATUAN","SeriHS");
                $sort = "ASC";
                $key = array("Kode", "Uraian&nbsp;Barang", "Merk", "Tipe", "Ukuran", "SPF&nbsp;Lain", "jns_barang", "Kode HS", "SeriHS",
                    "KODE_SATUAN", "KODE_SATUAN_TERKECIL", "KODE_SATUANNYA", "URAIAN_KD_SATUAN");
                $search = array(array('KODE_BARANG', 'Kode Barang'), 
								array('URAIAN_BARANG', 'Uraian Barang'),
								array('UKURAN', 'Ukuran'),
								array('TIPE', 'Tipe'),
								array('SPFLAIN', 'Spesifikasi Lain'),
                    	   		array('jns_barang', 'Jenis Barang', 'tag-select', $func->main->get_mtabel('ASAL_JENIS_BARANG')));
                $field = explode(";", $indexField);
                $show_search = true;
				$show_search=true;		
			break;				
			case "barangpib":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 
				           'Jenis&nbsp;Barang',JNS_BARANG 'jns_barang', REPLACE(URAIAN_BARANG,'\n','') 'Uraian&nbsp;Barang',MERK 'Merk',
						   TIPE 'Tipe',
						   UKURAN 'Ukuran',SPFLAIN 'SPF&nbsp;Lain',FORMAT(STOCK_AKHIR,0) 'Stock&nbsp;Akhir',HS 'Kode HS',0 'Seri HS',
						   SATUAN,SATUAN_TERKECIL,SATUAN AS KODE_SATUANNYA,f_satuan(SATUAN) AS URAIAN_KD_SATUAN
						   FROM T_MST_BARANG 
						   WHERE KODE_TRADER='".$KODE_TRADER."'"; //echo $SQL;die();
				$order  = "2";
				$hidden = array("Kode Trader","jns_barang","Kode HS","Seri HS","SATUAN","SATUAN_TERKECIL","KODE_SATUANNYA",
								"URAIAN_KD_SATUAN",'Merk','Tipe',
						   'Ukuran','SPF&nbsp;Lain');
				$sort   = "ASC";		
				$key    = array("Kode","Uraian&nbsp;Barang","Merk","Tipe","Ukuran","SPF&nbsp;Lain","jns_barang","Kode HS","Seri HS",
								"SATUAN","SATUAN_TERKECIL","KODE_SATUANNYA","URAIAN_KD_SATUAN","SATUAN");
				$search = array(array('KODE_BARANG', 'Kode Barang'),array('URAIAN_BARANG', 'Uraian Barang'),
				          array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
			break;
			case "barangtemp":
				$data = str_replace(";","",$getdata);
				$judul = "Pencarian Data Barang";
				if($formName=='fbarangBahanBaku'){
					$id = explode("|",$data);
					$KONDISI = "AND B.ASAL_BARANG = '".$id[1]."' 
								AND B.KONDISI_BARANG = '".$id[2]."' AND B.KODE_GUDANG = '".$id[3]."' 
								AND B.JNS_BARANG = '".$id[4]."' AND B.KODE_BARANG != '".$id[5]."'";
				}
				
				$SQL = "SELECT DISTINCT B.KONDISI_BARANG AS KONDISI_BARANGX, A.KODE_TRADER 'Kode Trader',
						A.KODE_BARANG 		'Kode',f_ref('ASAL_JENIS_BARANG',A.JNS_BARANG) 	'Jenis&nbsp;Barang',
						A.JNS_BARANG 'jns_barang', REPLACE(A.URAIAN_BARANG,'\n','') 'Uraian&nbsp;Barang',
						FORMAT(A.STOCK_AKHIR,0)'Stock&nbsp;Akhir',A.HS 'Kode HS',0 'Seri HS', A.SATUAN,
						A.SATUAN_TERKECIL,A.SATUAN AS KODE_SATUANNYA,f_satuan(A.SATUAN) AS URAIAN_KD_SATUAN,
						B.KONDISI_BARANG AS 'Kondisi Barang'
						FROM T_MST_BARANG A, T_MST_GUDANG_BARANG B
						WHERE A.KODE_TRADER = B.KODE_TRADER AND A.NIPER = B.NIPER
						AND A.KODE_BARANG = B.KODE_BARANG AND A.JNS_BARANG = B.JNS_BARANG 
						AND A.KODE_TRADER='".$KODE_TRADER."' AND A.NIPER = '".$NIPER."' $KONDISI";
				$order  = "2";
				$hidden = array("Kode Trader","jns_barang","Kode HS","Seri HS","KODE_SATUAN",
								"SATUAN_TERKECIL","KODE_SATUANNYA",
"URAIAN_KD_SATUAN",'KONDISI_BARANGX');
				$sort   = "ASC";		
				$key    = array("Kode","Uraian&nbsp;Barang","jns_barang","KODE_SATUAN","KODE_SATUAN_TERKECIL",
							"KODE_SATUANNYA","URAIAN_KD_SATUAN","SATUAN","Kondisi Barang");
				$search = array(array('A.KODE_BARANG', 'Kode Barang'),array('A.URAIAN_BARANG', 'Uraian Barang'), 
						array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
				break;										
			case "satuan":			
				if ($getdata != "") {
                    $data = str_replace(";", "','", $getdata);
                    $data = substr($data, 0, strlen($data) - 3);
                    $SQL = "SELECT KODE_SATUAN 'Kode Satuan',URAIAN_SATUAN 'Uraian Satuan' FROM m_satuan WHERE KODE_SATUAN IN('" . $data . "')";
                    $show_search = false;
                } else {
                    $SQL = "SELECT KODE_SATUAN 'Kode Satuan',URAIAN_SATUAN 'Uraian Satuan' FROM m_satuan ";
                    $show_search = true;
                }
                $judul = "Pencarian Data Satuan";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Satuan", "Uraian Satuan");
                $search = array(array('KODE_SATUAN', 'Kode Satuan'), array('URAIAN_SATUAN', 'Uraian Satuan'));
                $field = explode(";", $indexField);		
			break;				
			case "pemasok":
				$judul = "Pencarian Data Pemasok";
                $SQL = "SELECT CONCAT('&nbsp;',f_ref('KODE_ID',KODE_ID_PARTNER),'<br>',ID_PARTNER) 'Identitas',NAMA_PARTNER 'Nama Pemasok', 
						   REPLACE(ALAMAT_PARTNER,'\n','') 'Alamat Pemasok', NEGARA_PARTNER, F_NEGARA(NEGARA_PARTNER) URNEGARA_PARTNER,
						   KODE_ID_PARTNER,ID_PARTNER FROM m_trader_partner 
						   WHERE KODE_TRADER='" . $KODE_TRADER . "'";
                $order = "2";
                $sort = "ASC";
                $hidden = array("KODE_ID_PARTNER", "ID_PARTNER", "NEGARA_PARTNER", "URNEGARA_PARTNER");
                $key = array("KODE_ID_PARTNER", "ID_PARTNER", "Nama Pemasok", "Alamat Pemasok", "NEGARA_PARTNER", "URNEGARA_PARTNER");
                $search = array(array('ID_PARTNER', 'No. Identitas'),
                    array('NAMA_PARTNER', 'Nama Pemasok'),
                    array('ALAMAT_PARTNER', 'Alamat Pemasok'));
                $field = explode(";", $indexField);
                $show_search = true;		
			break;
			case "pemasoksubkon":
				$judul = "Pencarian Data Pemasok";
				$SQL 	= "SELECT CONCAT('&nbsp;',f_ref('KODE_ID',KODE_ID_PARTNER),'<br>',ID_PARTNER) 'Identitas',NAMA_PARTNER 'Nama Pemasok', 
						   ALAMAT_PARTNER 'Alamat Pemasok', KODE_ID_PARTNER,ID_PARTNER FROM T_MST_PEMASOK
						   WHERE NEGARA_PARTNER = 'ID' AND  KODE_TRADER='".$KODE_TRADER."'";
				$order  = "2";	
				$sort   = "ASC";		
				$hidden = array("KODE_ID_PARTNER","ID_PARTNER");
				$key    = array("KODE_ID_PARTNER","ID_PARTNER","Nama Pemasok","Alamat Pemasok");
				$search = array(array('ID_PARTNER', 'No. Identitas'),
							    array('NAMA_PARTNER', 'Nama Pemasok'),
								array('ALAMAT_PARTNER', 'Alamat Pemasok'));			
				$field  = explode(";",$indexField);		
				$show_search=true;			
			break;
			case "pemasoklokal":
				$judul = "Pencarian Data Pemasok";
				$SQL 	= "SELECT CONCAT('&nbsp;',f_ref('KODE_ID',KODE_ID_PARTNER),'<br>',ID_PARTNER) 'Identitas',NAMA_PARTNER 'Nama Pemasok', 
						   ALAMAT_PARTNER 'Alamat Pemasok', KODE_ID_PARTNER,ID_PARTNER FROM T_MST_PEMASOK
						   WHERE NEGARA_PARTNER = 'ID' AND  KODE_TRADER='".$KODE_TRADER."'";
				$order  = "2";	
				$sort   = "ASC";		
				$hidden = array("KODE_ID_PARTNER","ID_PARTNER");
				$key    = array("KODE_ID_PARTNER","ID_PARTNER","Nama Pemasok","Alamat Pemasok");
				$search = array(array('ID_PARTNER', 'No. Identitas'),
							    array('NAMA_PARTNER', 'Nama Pemasok'),
								array('ALAMAT_PARTNER', 'Alamat Pemasok'));			
				$field  = explode(";",$indexField);		
				$show_search=true;			
			break;
			case "kemasan":
				$judul = "Pencarian Data Kemasan";
                $SQL = "SELECT KODE_KEMASAN 'Kode Kemasan',URAIAN_KEMASAN 'Uraian Kemasan' FROM m_kemasan ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Kemasan", "Uraian Kemasan");
                $search = array(array('KODE_KEMASAN', 'Kode Kemasan'), array('URAIAN_KEMASAN', 'Uraian Kemasan'));
                $field = explode(";", $indexField);
                $show_search = true;
			break;	
			case "negara":
				$judul = "Pencarian Data Negara";
				$SQL 	= "SELECT KODE_NEGARA 'Kode Negara',URAIAN_NEGARA 'Uraian Negara' FROM m_negara ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Negara","Uraian Negara");
				$search = array(array('KODE_NEGARA', 'Kode Negara'),array('URAIAN_NEGARA', 'Uraian Negara'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "gudang":
				$judul = "Pencarian Data Gudang";
				$SQL 	= "SELECT KODE_GUDANG 'Kode Gudang',NAMA_GUDANG 'Nama Gudang' FROM m_trader_gudang WHERE KODE_TRADER='".$KODE_TRADER."' ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Gudang","Nama Gudang");
				$search = array(array('KODE_GUDANG', 'Kode Gudang'),array('NAMA_GUDANG', 'Nama Gudang'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "rak":
				$judul = "Pencarian Data Rak";
				$SQL 	= "SELECT  a.NAMA_GUDANG AS 'Nama Gudang', b.KODE_RAK AS 'Kode Rak', b.NAMA_RAK AS 'Nama Rak', a.KODE_GUDANG FROM m_trader_gudang a , m_trader_rak b WHERE a.KODE_TRADER='".$KODE_TRADER."' AND a.KODE_GUDANG=b.KODE_GUDANG";
				if($getdata!=""){
					$data 		 = str_replace(";","",$getdata);		
					$KODE_GUDANG = $data;
					$SQL		.= " AND a.KODE_GUDANG='".$KODE_GUDANG."'";
				}
				//echo $SQL;
				//die();				
				$order  = "1";
				$hidden = array("KODE_GUDANG");	
				$sort   = "ASC";		
				$key    = array("Kode Rak","KODE_GUDANG");
				$search = array(array('KODE_RAK', 'Kode Rak'),array('NAMA_RAK', 'Nama Rak'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "penyelenggara":
				$judul = "Pencarian Data Penyelenggara";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',ID 'Id', NAMA 'Nama Penyelenggara',ALAMAT 'Alamat' FROM m_trader WHERE JENIS_PLB='00'";
				$order  = "1";
				$hidden = array('Kode Trader');	
				$sort   = "ASC";		
				$key    = array("Kode Trader","Nama Penyelenggara");
				$search = array(array('KODE_TRADER', 'Kode Trader'),array('NAMA', 'Nama Penyelenggara'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "pelabuhan":
				$judul = "Pencarian Data Pelabuhan";
                $SQL = "SELECT KODE_PELABUHAN 'Kode Pelabuhan',URAIAN_PELABUHAN 'Uraian Pelabuhan' FROM m_pelabuhan ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Pelabuhan", "Uraian Pelabuhan");
                $search = array(array('KODE_PELABUHAN', 'Kode Pelabuhan'), array('URAIAN_PELABUHAN', 'Uraian Pelabuhan'));
                $field = explode(";", $indexField);
                $show_search = true;				
			break;	
			case "timbun":
				$judul = "Pencarian Data Penimbunan";
                $SQL = "SELECT KODE_TIMBUN 'Kode Timbun',URAIAN_TIMBUN 'Uraian Timbun' FROM m_timbun ";
                $order = "1";
                $hidden = array();
                $sort = "ASC";
                $key = array("Kode Timbun", "Uraian Timbun");
                $search = array(array('KODE_TIMBUN', 'Kode Timbun'), array('URAIAN_TIMBUN', 'Uraian Timbun'));
                $field = explode(";", $indexField);
                $show_search = true;
			break;
			case "hanggar":
				$judul = "Pencarian Data Hanggar";
				$SQL 	= "SELECT KODE_HANGGAR 'Kode Hanggar',URAIAN_HANGGAR 'Uraian Hanggar' FROM m_hanggar ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Hanggar","Uraian Hanggar");
				$search = array(array('KODE_HANGGAR', 'Kode Hanggar'),array('URAIAN_HANGGAR', 'Uraian Hanggar'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "daerah":
				$judul = "Pencarian Data Daerah";
				$SQL 	= "SELECT KODE_DAERAH 'Kode Daerah',URAIAN_DAERAH 'Uraian Daerah' FROM m_daerah ";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Uraian Daerah","Kode Daerah");
				$search = array(array('KODE_DAERAH', 'Kode Daerah'),array('URAIAN_DAERAH', 'Uraian Daerah'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;	
			case "realisasi_in":
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pemasukan";
				$WHERE = " WHERE STATUS = '07' AND (TANGGAL_REALISASI IS NULL OR STATUS_REALISASI='N') 
						   AND KODE_TRADER ='".$KODE_TRADER."' AND NIPER='".$NIPER."'";	
				$SQL = "SELECT 'BC25' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						NAMA_PENGIRIM AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc25(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc25_hdr";
				$SQL .=	$WHERE;		
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC41' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 						
						NAMA_TRADER AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc41(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc41_hdr";
				$SQL .=	$WHERE;				
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));								
				$SQL .=	" UNION
						SELECT 'BC21' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						NAMA_PENGIRIM AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc21(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc21_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION 
						SELECT 'BC20' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						PASOKNAMA AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc20(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc20_hdr";
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));				
				$SQL .=	" UNION
						SELECT 'SUBKON' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', '-' AS 'NOMOR DAFTAR', '-' AS TANGGAL_PENDAFTARAN, 						
						NAMA_PARTNER AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_subkon(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_subkon_hdr"; 
				$SQL .=	$WHERE." AND TIPE_SUBKON='MASUK' ";
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'LOKAL' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', '-' AS 'NOMOR DAFTAR', '-' AS TANGGAL_PENDAFTARAN, 					
						NAMA_PARTNER AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_lokal(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_dok_lokal_hdr"; 
				$SQL .=	$WHERE." AND TIPE_DOKUMEN='MASUK' ";								
				$order  = "1";
				$sort   = "DESC";		
				$hidden = array("TANGGAL_PENDAFTARAN","DETIL BARANG");	
				$key    = array("NOMOR DAFTAR","TANGGAL_PENDAFTARAN","DOKUMEN","PEMASOK/PENGIRIM","DETIL BARANG","NOMOR AJU");
				$search = array(array('NOMOR_AJU', 'NOMOR AJU&nbsp;'));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
				$rowcount = 10;
			break;	
			case "realisasi_in_parsial":
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pemasukan";
				$WHERE = " WHERE STATUS IN ('07','19') AND (STATUS_REALISASI='N' OR TANGGAL_REALISASI IS NULL) 
						   AND KODE_TRADER ='".$KODE_TRADER."' AND NIPER='".$NIPER."'";						   
				$SQL = "SELECT 'BC25' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						NAMA_PENGIRIM AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc25(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG', 	
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL' 
						FROM t_bc25_hdr";
				$SQL .=	$WHERE;		
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC41' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 						
						NAMA_TRADER AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc41(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'	
						FROM t_bc41_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));								
				$SQL .=	" UNION
						SELECT 'BC21' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						NAMA_PENGIRIM AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc21(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'
						FROM t_bc21_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION 
						SELECT 'BC20' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN, 
						PASOKNAMA AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_bc20(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'
						FROM t_bc20_hdr";
				$SQL .=	$WHERE;						
				$order  = "1";
				$sort   = "DESC";		
				$hidden = array("TANGGAL_PENDAFTARAN","DETIL BARANG");	
				$key    = array("NOMOR DAFTAR","TANGGAL_PENDAFTARAN","DOKUMEN","PEMASOK/PENGIRIM","DETIL BARANG","NOMOR AJU");
				$search = array(array('NOMOR_AJU', 'NOMOR AJU&nbsp;'));			
				$field  = explode(";",$indexField);	
				$show_search=true;
				$this->newtable->align(array('LEFT','LEFT','LEFT','LEFT','CENTER'));	
				$rowcount = 10;
			break;	
			case "realisasi_out":
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pemasukan";
				$WHERE = " WHERE STATUS = '07' AND (TANGGAL_REALISASI IS NULL OR STATUS_REALISASI='N') 
						   AND KODE_TRADER ='".$KODE_TRADER."' AND NIPER='".$NIPER."'";	
				$SQL = "SELECT 'BC24' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU',NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  
						NAMA_PENERIMA AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc24(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc24_hdr";
				$SQL .=	$WHERE;		
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC40' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  						
						NAMA_TRADER AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc40(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc40_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC30' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  
						NAMA_PEMBELI AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc30(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_bc30_hdr";
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'SUBKON' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', '-' AS  'NOMOR DAFTAR', '-' AS TANGGAL_PENDAFTARAN, 						
						NAMA_PARTNER AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_subkon(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_subkon_hdr"; 
				$SQL .=	$WHERE." AND TIPE_SUBKON='KELUAR' ";	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'LOKAL' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', '-' AS 'NOMOR DAFTAR', '-' AS TANGGAL_PENDAFTARAN, 					
						NAMA_PARTNER AS 'PEMASOK/PENGIRIM', IFNULL(f_jumbrg_lokal(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG'
						FROM t_dok_lokal_hdr"; 
				$SQL .=	$WHERE." AND TIPE_DOKUMEN='KELUAR' ";																
				$order  = "1";
				$sort   = "DESC";		
				$hidden = array("TANGGAL_PENDAFTARAN","DETIL BARANG");	
				$key    = array("NOMOR DAFTAR","TANGGAL_PENDAFTARAN","DOKUMEN","PEMBELI/PENERIMA","DETIL BARANG","NOMOR AJU");
				$search = array(array('NOMOR_AJU', 'NOMOR AJU&nbsp;'));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
				$rowcount = 10;
			break;
			case "realisasi_out_parsial":
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pemasukan";
				$WHERE = " WHERE STATUS IN ('07','19') AND (STATUS_REALISASI='N' OR TANGGAL_REALISASI IS NULL) 
						   AND KODE_TRADER ='".$KODE_TRADER."' AND NIPER='".$NIPER."'";						   
				$SQL = "SELECT 'BC24' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU',NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  
						NAMA_PENERIMA AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc24(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG', 	
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'
						FROM t_bc24_hdr";
				$SQL .=	$WHERE;		
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC40' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  						
						NAMA_TRADER AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc40(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'
						FROM t_bc40_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));
				$SQL .=	" UNION
						SELECT 'BC30' AS 'DOKUMEN', NOMOR_AJU AS 'NOMOR AJU', NOMOR_PENDAFTARAN 'NOMOR DAFTAR', TANGGAL_PENDAFTARAN,  
						NAMA_PEMBELI AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc30(NOMOR_AJU,KODE_TRADER,NIPER),0) AS 'DETIL BARANG',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL'
						FROM t_bc30_hdr";
				$SQL .=	$WHERE;																
				$order  = "1";
				$sort   = "DESC";		
				$hidden = array("TANGGAL_PENDAFTARAN","DETIL BARANG");	
				$key    = array("NOMOR DAFTAR","TANGGAL_PENDAFTARAN","DOKUMEN","PEMBELI/PENERIMA","DETIL BARANG","NOMOR AJU");
				$search = array(array('NOMOR_AJU', 'NOMOR AJU&nbsp;'));				
				$field  = explode(";",$indexField);	
				$show_search=true;
				$this->newtable->align(array('LEFT','LEFT','LEFT','LEFT','CENTER'));	
				$rowcount = 10;
			break;	
			#==============================================================================================================================#
			case "barang_popup":
				if($formName=="fkonversihp"){ 
					$ADDSQL=" AND JNS_BARANG='3'";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("2"=>"HASIL PRODUKSI")));	
				}				
				elseif($formName=="fkonversibb"){
					$ADDSQL=" AND JNS_BARANG IN ('1')";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("1"=>"BAHAN BAKU")));	
				}
				elseif($formName=="fkonvdet"||$formName=="fkonvsubdet"){
					$ADDSQL=" AND JNS_BARANG='2'";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("2"=>"HASIL PRODUKSI")));	
				}
				elseif($formName=='fHasilProduksi'){
					$ADDSQL=" AND JNS_BARANG IN ('6','1','2','8')";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("1"=>"BAHAN BAKU","2"=>"BAHAN PENOLONG","6"=>"HASIL PRODUKSI","8"=>"PENGEMAS")));	
				}
				elseif($formName=='fHasilSisa'){
					$ADDSQL=" AND JNS_BARANG IN ('1','2','6','7','8')";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("1"=>"BAHAN BAKU","2"=>"BAHAN PENOLONG","6"=>"HASIL PRODUKSI","7"=>"SISA PRODUKSI","8"=>"PENGEMAS")));	
				}
				elseif($formName=='fKonv'||$formName=="fKonvSub"){
					$ADDSQL=" AND JNS_BARANG NOT IN ('6','7')";
					$search = array(array('KODE_BARANG', 'Kode Barang'),
						 		  array('URAIAN_BARANG', 'Uraian Barang'),	
					   			  array('jns_barang', 'Jenis Barang','tag-select',
								  array("1"=>"BAHAN BAKU",
								  		"2"=>"BAHAN PENOLONG",
										"3"=>"BARANG MODAL",
										"4"=>"PERALATAN PERKANTORAN",
										"5"=>"BARANG CONTOH",
										"8"=>"PENGEMAS",
										"9"=>"BARANG KONSTRUKSI")));	
				}
				else{
					$search = array(array('KODE_BARANG', 'Kode Barang'),
								  array('URAIAN_BARANG', 'Uraian Barang'),
				          		  array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG','',FALSE)));		
				}
				
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode Barang',HS 'HS',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 
				           'Jenis Barang',JNS_BARANG 'jns_barang',URAIAN_BARANG 'Uraian Barang',MERK 'Merk',TIPE 'Tipe',
						   UKURAN 'Ukuran',SPFLAIN 'Spf Lain',IF(SATUAN_TERKECIL='',SATUAN,IF(SATUAN_TERKECIL IS NULL,SATUAN,SATUAN_TERKECIL)) 'Kode Satuan',
						   f_satuan(IF(SATUAN_TERKECIL='',SATUAN,IF(SATUAN_TERKECIL IS NULL,SATUAN,SATUAN_TERKECIL))) 'Uraian Satuan',
						   FORMAT(STOCK_AKHIR,2) 'Stock Akhir' , STOCK_AKHIR
						   FROM t_mst_barang WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."' ".$ADDSQL;
						   						   
				$order  = "3, 2";
				$hidden = array("Kode Trader","jns_barang","Kode Satuan","Merk","Tipe","Ukuran","Spf Lain","STOCK_AKHIR");		
				$sort   = "ASC";
				if(($formName=='fBahanBaku')||($formName=='fHasilProduksi')||($formName=='fHasilSisa')||($formName=='fpengrusakan')||($formName=='fpemusnahan')){		
					$key    = array("Kode Barang","Uraian Barang","jns_barang","Jenis Barang","Kode Satuan","Uraian Satuan","STOCK_AKHIR");
				}
				elseif($formName=="fkonversihp"||$formName=="fkonversibb"){
					$key    = array("Kode Barang","HS","Uraian Barang","Kode Satuan","Uraian Satuan","jns_barang","Jenis Barang");	
				}
				elseif($formName=="fkonvdet"||$formName=="fkonvsubdet"||$formName=="fKonv"||$formName=="fKonvSub"){
					$key    = array("Kode Barang","Uraian Barang","jns_barang","Jenis Barang","Merk","Tipe","Ukuran","Spf Lain","Kode Satuan","Uraian Satuan");	
				}else{
					$key    = array("Kode Barang","Uraian Barang","jns_barang","Jenis Barang","Merk","Tipe","Ukuran","Spf Lain","Kode Satuan");	
				}
						
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "barang_maping":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode Barang',URAIAN_BARANG 'Uraian Barang',
				           f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'Jenis Barang',JNS_BARANG 'jns_barang'
						   FROM m_trader_barang WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."'";
						   //.$ADDSQL;
				$order  = "2";
				$hidden = array("Kode Trader","jns_barang","Kode Satuan");	
				$sort   = "ASC";
				$key    = array("Kode Barang","Uraian Barang","jns_barang","Jenis Barang");	
				$search = array(array('KODE_BARANG', 'Kode Barang'),array('URAIAN_BARANG', 'Uraian Barang'),
				          array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "skep":
				$judul = "Pencarian Data Skep";
				$SQL 	= "SELECT KODE 'Kode', URAIAN 'Uraian Skep' FROM m_tabel WHERE JENIS='FAS_SKEMA'";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode","Uraian Skep");
				$search = array(array('KODE', 'Kode Skep'),array('URAIAN', 'Uraian Skep'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "BARANGPPB":
				if($getdata!=""){
					$data = str_replace(";","','",$getdata);							
					$data = substr($data,0,strlen($data)-3);	
					$WHERE = " AND KODE_LOKASI = '".$data."'";
				}
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_BARANG 'KODE BARANG',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG', 
				           KODE_HS 'KODE HS', KODE_SATUAN 'SATUAN', STOCK_AKHIR 'STOCK AKHIR', KODE_LOKASI 'KODE LOKASI', 
						   JNS_BARANG, KODE_SATUAN_TERKECIL  FROM m_trader_barang WHERE KODE_TRADER='".$this->newsession->userdata('KODE_TRADER')."' ";				
				$SQL .= $WHERE;	
				echo $SQL;
				die();
				$order  = "2";
				$sort   = "ASC";		
				$hidden = array("JNS_BARANG","KODE_SATUAN_TERKECIL");
				$key    = array("KODE BARANG","KODE HS","JNS_BARANG","JENIS BARANG","SATUAN","KODE_SATUAN_TERKECIL");
				$search = array(array('KODE_BARANG', 'Kode Barang'),
								array('KODE_HS', 'KODE HS'),
				          		array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
			break;		
			case "realisasi_out_withparsial":						
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pengeluaran";
								
				$FIELD = ",DATE_FORMAT(TANGGAL_REALISASI, '%d %M %Y %H:%m:%s') AS 'TANGGAL REALISASI/GATE'";
				$WHERE = " WHERE STATUS IN ('07','19') AND (STATUS_REALISASI='0' OR TANGGAL_REALISASI IS NULL) 
							AND KODE_TRADER ='".$this->newsession->userdata('KODE_TRADER')."' ";
				
				$SQL = "SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN,NOMOR_AJU AS 'NOMOR AJU', 'BC40' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER', DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.',
						NAMA_TRADER AS 'PEMBELI/PENERIMA', 
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL',
						IFNULL(f_jumbrg_bc40(NOMOR_AJU),0) AS 'DETIL BARANG',
						f_kodebrg_bc40(NOMOR_AJU) AS 'KODE_BARANG',f_urbrg_bc40(NOMOR_AJU) AS 'URAIAN_BARANG', 
						JENIS_TPB 'JNS_BARANG',f_kodedok_bc40(NOMOR_AJU) AS 'KODE_DOKUMEN',
						IF(STATUS='07','DISETUJUI',f_status_bc40(NOMOR_AJU,1,KODE_TRADER)) AS 'STATUS',f_jnsBrg_bc40(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc40(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc40(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc40_hdr";
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));	
				$SQL .=	" UNION ALL
						SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN,NOMOR_AJU AS 'NOMOR AJU', 'BC30' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER',DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.', NAMA_PEMBELI AS 'PEMBELI/PENERIMA',
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL', 
						IFNULL(f_jumbrg_bc30(NOMOR_AJU),0) AS 'DETIL BARANG',f_kodebrg_bc30(NOMOR_AJU) AS 'KODE_BARANG',
						f_urbrg_bc30(NOMOR_AJU) AS 'URAIAN_BARANG', KATEGORI_EKSPOR AS 'JNS_BARANG',f_kodedok_bc30(NOMOR_AJU) AS 'KODE_DOKUMEN',
						IF(STATUS='07','DISETUJUI',f_status_bc30(NOMOR_AJU,1)) AS 'STATUS',f_jnsBrg_bc30(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc30(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc30(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc30_hdr";
				$SQL .=	$WHERE;
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));									
				$SQL .=	" UNION ALL
						SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN, NOMOR_AJU AS 'NOMOR AJU', 'BC24' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER', DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.', 
						NAMA_PENERIMA AS 'PEMBELI/PENERIMA', 
						IF(PARSIAL_FLAG='Y','&radic;','') 'PARSIAL',
						IFNULL(f_jumbrg_bc24(NOMOR_AJU),0) AS 'DETIL BARANG',
						f_kodebrg_bc24(NOMOR_AJU) AS 'KODE_BARANG',f_urbrg_bc24(NOMOR_AJU) AS 'URAIAN_BARANG', 
						JNS_BARANG AS 'JNS_BARANG',f_kodedok_bc24(NOMOR_AJU) AS 'KODE_DOKUMEN',IF(STATUS='07','DISETUJUI',
						f_status_bc24(NOMOR_AJU,1)) AS 'STATUS',f_jnsBrg_bc24(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc24(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc24(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc24_hdr";
				$SQL .=	$WHERE;	
				$order  = "1";
				$hidden = array("NOMOR_PENDAFTARAN","TANGGAL_PENDAFTARAN","CREATED_TIME","KODE_DOKUMEN","KODE_TRADER",
								"DETIL BARANG","KODE_BARANG","KODE_JENIS","URAIAN_BARANG","JNS_BARANG","STATUS",
								"TANGGAL REALISASI/GATE","JUMLAH_SATUAN","SERI_BARANG");	
				$sort   = "DESC";		
				$key    = array("NOMOR_PENDAFTARAN","TANGGAL_PENDAFTARAN","DOKUMEN","PEMBELI/PENERIMA","KODE_TRADER",
								"DETIL BARANG","NOMOR AJU","KODE_BARANG","KODE_JENIS","JUMLAH_SATUAN","SERI_BARANG");
				$search = array(array('NOMOR_AJU', 'Nomor Aju&nbsp;'));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
				$this->newtable->align(array('LEFT','LEFT','LEFT','LEFT','CENTER'));		
			break;	
			
			/*case "realisasi_out":
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pengeluaran";
				$FIELD = ",DATE_FORMAT(TANGGAL_REALISASI, '%d %M %Y %H:%m:%s') AS 'TANGGAL REALISASI/GATE'";
				//$WHERE = " WHERE STATUS = '07' AND TANGGAL_REALISASI IS NULL AND KODE_TRADER =".$this->newsession->userdata('KODE_TRADER')." ";
				$WHERE = " WHERE STATUS = '07' AND (TANGGAL_REALISASI IS NULL OR STATUS_REALISASI='Y') 
						   AND KODE_TRADER ='".$this->newsession->userdata('KODE_TRADER')."' ";
				$SQL = "SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN, NOMOR_AJU AS 'NOMOR AJU', 'BC24' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER', DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.', 
						NAMA_PENERIMA AS 'PEMBELI/PENERIMA', IFNULL(f_jumbrg_bc24(NOMOR_AJU),0) AS 'DETIL BARANG',
						f_kodebrg_bc24(NOMOR_AJU) AS 'KODE_BARANG',f_urbrg_bc24(NOMOR_AJU) AS 'URAIAN_BARANG', 
						JNS_BARANG AS 'JNS_BARANG',f_kodedok_bc24(NOMOR_AJU) AS 'KODE_DOKUMEN',IF(STATUS='07','DISETUJUI',
						f_status_bc24(NOMOR_AJU)) AS 'STATUS',f_jnsBrg_bc24(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc24(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc24(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc24_hdr";
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));	
				$SQL .=	" UNION ALL
						SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN,NOMOR_AJU AS 'NOMOR AJU', 'BC40' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER', DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.', 
						NAMA_TRADER AS 'PEMBELI/PENERIMA',IFNULL(f_jumbrg_bc40(NOMOR_AJU),0) AS 'DETIL BARANG',
						f_kodebrg_bc40(NOMOR_AJU) AS 'KODE_BARANG',f_urbrg_bc40(NOMOR_AJU) AS 'URAIAN_BARANG', 
						JENIS_TPB AS 'JNS_BARANG',f_kodedok_bc40(NOMOR_AJU) AS 'KODE_DOKUMEN',
						IF(STATUS='07','DISETUJUI',f_status_bc40(NOMOR_AJU)) AS 'STATUS',f_jnsBrg_bc40(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc40(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc40(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc40_hdr";
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("NOMOR_AJU"));	
				$SQL .=	" UNION ALL
						SELECT CREATED_TIME, NOMOR_PENDAFTARAN, TANGGAL_PENDAFTARAN,NOMOR_AJU AS 'NOMOR AJU', 'BC30' AS 'DOKUMEN', 
						KODE_TRADER AS 'KODE_TRADER',DATE_FORMAT(CREATED_TIME, '%d %M %Y') AS 'TANGGAL DOK.', NAMA_PEMBELI AS 'PEMBELI/PENERIMA', 
						IFNULL(f_jumbrg_bc30(NOMOR_AJU),0) AS 'DETIL BARANG',f_kodebrg_bc30(NOMOR_AJU) AS 'KODE_BARANG',
						f_urbrg_bc30(NOMOR_AJU) AS 'URAIAN_BARANG', KATEGORI_EKSPOR AS 'JNS_BARANG',f_kodedok_bc30(NOMOR_AJU) AS 'KODE_DOKUMEN',
						IF(STATUS='07','DISETUJUI',f_status_bc30(NOMOR_AJU)) AS 'STATUS',f_jnsBrg_bc30(NOMOR_AJU) AS 'KODE_JENIS',
						f_jumsatuan_bc30(NOMOR_AJU) AS 'JUMLAH_SATUAN', f_seri_bc30(NOMOR_AJU) AS 'SERI_BARANG' ".$FIELD." FROM t_bc30_hdr";
				$SQL .=	$WHERE;	
				$order  = "1";
				$hidden = array("NOMOR_PENDAFTARAN","TANGGAL_PENDAFTARAN","CREATED_TIME","KODE_DOKUMEN","KODE_TRADER","DETIL BARANG","KODE_BARANG","KODE_JENIS","URAIAN_BARANG","JNS_BARANG","STATUS","TANGGAL REALISASI/GATE","JUMLAH_SATUAN","SERI_BARANG");	
				$sort   = "DESC";		
				$key    = array("NOMOR_PENDAFTARAN","TANGGAL_PENDAFTARAN","DOKUMEN","PEMBELI/PENERIMA","KODE_TRADER","DETIL BARANG","NOMOR AJU","KODE_BARANG","KODE_JENIS","JUMLAH_SATUAN","SERI_BARANG");
				$search = array(array('NOMOR_AJU', 'Nomor Aju&nbsp;'));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
			break;*/
			case "nomor_proses":
				$judul = "Pencarian Data Nomor Proses";
				$SQL 	= "SELECT NOMOR_PROSES 'Nomor Proses',DATE_FORMAT(TANGGAL,'%d %M %Y') 'Tanggal',WAKTU 'Waktu' FROM m_trader_proses WHERE KODE_TRADER ='".$this->newsession->userdata('KODE_TRADER')."'";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Nomor Proses");
				$search = array(array('NOMOR_PROSES', 'Nomor Proses'),
				                array('TANGGAL', 'Tanggal', 'tag-tanggal'));			
				$field  = explode(";",$indexField);
				$show_search=true;		
			break;
			case "trader":
				$judul = "Pencarian Data Perusahaan";
				$SQL 	= "SELECT CONCAT(f_ref('KODE_ID',KODE_NPWP),' - ',NPWP) 'ID Perusahaan',NAMA 'Nama Perusahaan', ALAMAT 'Alamat',
						   KODE_TRADER,NPWP FROM T_MST_PERUSAHAAN";
				$order  = "2";
				$hidden = array('KODE_TRADER','NPWP');	
				$sort   = "ASC";		
				$key    = array("KODE_TRADER","Nama Perusahaan");
				$search = array(array('NPWP', 'ID Perusahaan'),array('NAMA', 'Nama Perusahaan'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "kodebarang":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_BARANG 'Kode Barang',URAIAN_BARANG 'Uraian Barang', f_ref('ASAL_JENIS_BARANG',JNS_BARANG)  'JENIS BARANG'
						   FROM t_mst_barang WHERE KODE_TRADER='".$KODE_TRADER."'";
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("Kode Barang","Uraian Barang");
				$search = array(array('KODE_BARANG', 'Kode Barang'),array('URAIAN_BARANG', 'Uraian Barang'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			
			case "zoning_kite":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE ,URAIAN FROM M_TABEL WHERE JENIS = 'ZONING_KITE'";						   
				$order  = "2";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE', 'KODE'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			
			
			
			
			case "jns_barang":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE ,URAIAN FROM M_TABEL WHERE JENIS = 'JNS_BRG_PKB'";						   
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE', 'KODE'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);
				$show_search=true;					
			break;
			case "gudang_pkb":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE ,URAIAN FROM M_TABEL WHERE JENIS = 'TEMPAT_PKB'";						   
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE', 'KODE'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			
			case "stuffing":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE ,URAIAN FROM M_TABEL WHERE JENIS = 'JENIS_STUFF'";						   
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE', 'KODE'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "partof":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE ,URAIAN FROM M_TABEL WHERE JENIS = 'PART_OF'";						   
				$order  = "1";
				$hidden = array();	
				$sort   = "ASC";		
				$key    = array("KODE","URAIAN");
				$search = array(array('KODE', 'KODE'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);
				$show_search=true;					
			break;
			case "barang_break":
				$search = array(array('KODE_BARANG', 'Kode Barang'),
						  array('URAIAN_BARANG', 'Uraian Barang'),	
						   array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode Barang',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 
				           'Jenis Barang',JNS_BARANG 'jns_barang',
						   REPLACE(REPLACE(REPLACE(URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'Uraian Barang',MERK 'Merk',TIPE 'Tipe',
						   UKURAN 'Ukuran',SPFLAIN 'Spf Lain',KODE_SATUAN 'Kode Satuan',f_satuan(KODE_SATUAN) 'Uraian Satuan',
						   FORMAT(STOCK_AKHIR,2) 'Stock Akhir' , STOCK_AKHIR
						   FROM t_mst_barang WHERE KODE_TRADER=".$this->newsession->userdata('KODE_TRADER');
						   						   
				$order  = "3, 2";
				$hidden = array("Kode Trader","jns_barang","Kode Satuan","Merk","Tipe","Ukuran","Spf Lain","STOCK_AKHIR");		
				$sort   = "ASC";
				$key    = array("Kode Barang","Uraian Barang","jns_barang","Jenis Barang","Kode Satuan","Uraian Satuan","STOCK_AKHIR");						
				$field  = explode(";",$indexField);	
				$show_search=true;				
			break;
			case "konversi":
				$judul = "Pencarian Data Konversi";
				$SQL = "SELECT IDKONVERSI, NOMOR_KONVERSI 'NOMOR KONVERSI', KODE_BARANG_HP 'KODE BARANG JADI', 
						REPLACE(REPLACE(REPLACE(URAIAN_BARANG,'\n',''),'&QUOT;',''),';','')  'NAMA BARANG JADI',
						MASA_PRODUKSI 'MASA PRODUKSI',
						CONCAT(DATE_FORMAT(TANGGAL_AWAL_IMP,'%d-%M-%Y'),' ','s/d',' ',DATE_FORMAT(TANGGAL_AKHIR_IMP,'%d-%M-%Y')) AS 'PERIODE IMPOR'
						FROM T_KONVERSI_KITE WHERE  KODE_TRADER='".$KODE_TRADER."' AND NIPER='".$NIPER."' AND AKTIF='Y'";					
				$order  = "1";
				$hidden = array("IDKONVERSI");	
				$sort   = "ASC";		
				$key    = array("NOMOR KONVERSI","IDKONVERSI");
				$search = array(array('NOMOR_KONVERSI', 'NOMOR KONVERSI'),array('URAIAN', 'URAIAN'));			
				$field  = explode(";",$indexField);
				$show_search=true;					
			break;
			
		case "barangsstb":
				$judul = "Pencarian Data Barang";
				$SQL 	= "SELECT KODE_TRADER 'Kode Trader',KODE_BARANG 'Kode',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 
				           'Jenis&nbsp;Barang',JNS_BARANG 'jns_barang', REPLACE(URAIAN_BARANG,'\n','') 'Uraian&nbsp;Barang',FORMAT(STOCK_AKHIR,0) 'Stock&nbsp;Akhir',0 'Seri HS',
						   KODE_SATUAN,KODE_SATUAN_TERKECIL,KODE_SATUAN AS KODE_SATUANNYA,f_satuan(KODE_SATUAN) AS URAIAN_KD_SATUAN
						   FROM M_TRADER_BARANG 
						   WHERE KODE_TRADER = '".$KODE_TRADER."'";
				$order  = "2";
				$hidden = array("Kode Trader","jns_barang","Seri HS","KODE_SATUAN","KODE_SATUAN_TERKECIL","KODE_SATUANNYA",
								"URAIAN_KD_SATUAN");
				$sort   = "ASC";		
				$key    = array("Kode","Uraian&nbsp;Barang","jns_barang","KODE_SATUAN_TERKECIL","URAIAN_KD_SATUAN","Jenis&nbsp;Barang","");
				$search = array(array('KODE_BARANG', 'Kode Barang'),array('URAIAN_BARANG', 'Uraian Barang'),
				          array('jns_barang', 'Jenis Barang','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG')));			
				$field  = explode(";",$indexField);	
				$show_search=true;		
			break;	
		case "DOKUMEN_PEMASUKAN":
				if($getdata!=""){
					$data = str_replace(";","','",$getdata);							
					$data = substr($data,0,strlen($data)-3);	
					$WHERE = " AND B.KODE_BARANG = '".$data."'";
				}
				$POSTURI = $this->input->post("uri");
				$judul = "Pecarian Data Pemasukan";
				$SQL = "SELECT DISTINCT  A.NOMOR_AJU AS 'NOMOR AJU', 'BC23' AS 'DOKUMEN', A.NOMOR_PENDAFTARAN 'NOMOR PENDAFTARAN', 
						A.TANGGAL_PENDAFTARAN 'TANGGAL PENDAFTARAN' FROM t_bc23_hdr A, t_bc23_dtl B
						WHERE A.NOMOR_AJU=B.NOMOR_AJU AND A.STATUS = '07' AND A.TANGGAL_REALISASI IS NOT NULL 
						AND A.KODE_TRADER=B.KODE_TRADER
					    AND A.KODE_TRADER = '".$KODE_TRADER."'"; 
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("A.NOMOR_AJU"));	
				$SQL .=	" UNION
						SELECT DISTINCT A.NOMOR_AJU AS 'NOMOR AJU', 'BC27' AS 'DOKUMEN', A.NOMOR_PENDAFTARAN 'NOMOR PENDAFTARAN', 
						A.TANGGAL_PENDAFTARAN 'TANGGAL PENDAFTARAN' FROM t_bc27_hdr A, t_bc27_dtl B
						WHERE A.NOMOR_AJU=B.NOMOR_AJU AND A.STATUS = '07' AND A.TANGGAL_REALISASI IS NOT NULL 
						AND A.KODE_TRADER=B.KODE_TRADER
					    AND A.KODE_TRADER = '".$KODE_TRADER."' AND A.TIPE_DOK='MASUK'"; 
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("A.NOMOR_AJU"));	
				$SQL .=	" UNION
						SELECT DISTINCT A.NOMOR_AJU AS 'NOMOR AJU', 'BC40' AS 'DOKUMEN', A.NOMOR_PENDAFTARAN 'NOMOR PENDAFTARAN', 
						A.TANGGAL_PENDAFTARAN 'TANGGAL PENDAFTARAN' FROM t_bc40_hdr A, t_bc40_dtl B
						WHERE A.NOMOR_AJU=B.NOMOR_AJU AND A.STATUS = '07' AND A.TANGGAL_REALISASI IS NOT NULL 
						AND A.KODE_TRADER=B.KODE_TRADER
					    AND A.KODE_TRADER = '".$KODE_TRADER."'"; 
				$SQL .=	$WHERE;	
				$SQL .= $func->main->getWhere($SQL,$POSTURI,array("A.NOMOR_AJU"));	
				$SQL .=	" UNION
						SELECT DISTINCT A.NOMOR_AJU AS 'NOMOR AJU', 'BC262' AS 'DOKUMEN', A.NOMOR_PENDAFTARAN 'NOMOR PENDAFTARAN', 
						A.TANGGAL_PENDAFTARAN 'TANGGAL PENDAFTARAN' FROM t_bc262_hdr A, t_bc262_dtl B
						WHERE A.NOMOR_AJU=B.NOMOR_AJU AND A.STATUS = '07' AND A.TANGGAL_REALISASI IS NOT NULL 
						AND A.KODE_TRADER=B.KODE_TRADER
					    AND A.KODE_TRADER = '".$KODE_TRADER."'"; 
				$SQL .=	$WHERE; 
				echo $SQL;
				die();
				$order  = "1";
				$sort   = "DESC";		
				$hidden = array();	
				$key    = array("NOMOR PENDAFTARAN","TANGGAL PENDAFTARAN","DOKUMEN");
				$search = array(array('A.NOMOR_AJU', 'NOMOR AJU'),array('A.NOMOR_PENDAFTARAN', 'NOMOR PENDAFTARAN'));			
				$field  = explode(";",$indexField);	
				$show_search=true;
	
			break;			
			default:
				$judul = "Failed";
			break;
		} 
		$ciuri = $this->input->post("uri");		
		$this->newtable->action(site_url()."/search/getsearch/".$tipe."/".$indexField."/".$formName."/".$getdata);
		$this->newtable->hiddens($hidden);			
		$this->newtable->keys($key);			
		$this->newtable->indexField($field);
		$this->newtable->formField($formName);		
		$this->newtable->orderby($order);
		$this->newtable->sortby($sort);
		$this->newtable->detail_tipe('pilih');
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->show_search($show_search);
		$this->newtable->search($search);
		$this->newtable->show_chk(false);
		$this->newtable->field_order(false);		
		$this->newtable->set_formid("fdetildok");
		$this->newtable->set_divid("divfdetildok");
		$this->newtable->header_bg('#62A8D1');
		$this->newtable->rowcount($rowcount);
		$this->newtable->clear();  
		$tabel = $this->newtable->generate($SQL);
		$arrdata = array("judul" => $judul,"tabel" => $tabel);
		if($this->input->post("ajax")) return $tabel;				 
		else return $arrdata;		
	}
	
}
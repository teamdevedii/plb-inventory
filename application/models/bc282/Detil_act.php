<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Detil_act extends CI_Model{
	
	function list_detil($tipe="",$aju=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($tipe=="barang"){
			$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JENIS_BARANG) 'JENIS BARANG',
					f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
					CONCAT(NEGARA_ASAL,' - ',f_negara(NEGARA_ASAL)) AS 'ASAL BARANG', CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
					CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
					CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
					f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU FROM t_bc23_dtl 
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";		
			
			$this->newtable->orderby("SERI");
			$this->newtable->hiddens(array('NOMOR_AJU'));					
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
										  array('URAIAN_BARANG', 'URAIAN BARANG'),
										  array('JENIS_BARANG', 'JENIS BARANG','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG'))));	
			
		}else if($tipe=="kemasan"){
			$SQL = "SELECT JUMLAH AS 'JUMLAH',CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN',
					MERK_KEMASAN AS 'MEREK', NOMOR_AJU FROM t_bc23_kms 
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
			
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->hiddens(array('NOMOR_AJU'));			
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));	
					
		}else if($tipe=="container"){
			$SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', 
					f_ref('JENIS_STUFF',TIPE) AS 'TIPE', NOMOR_AJU	FROM t_bc23_cnt 
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";	
			
			$this->newtable->orderby("NOMOR");
			$this->newtable->hiddens(array('NOMOR_AJU'));
			$this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TIPE', 'TIPE')));	
					
		}else if($tipe=="dokumen"){
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', 
					NOMOR AS 'NOMOR DOKUMEN', DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', NOMOR_AJU FROM t_bc23_dok  
					WHERE NOMOR_AJU= '".$aju."'  AND KODE_TRADER = '".$kode_trader."'";	
			
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->hiddens(array('NOMOR_AJU'));
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));	
					
		}else{
			return "Failed";exit();
		}	
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/listdetil/".$tipe."/".$aju."/bc281");
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->count_keys(array("NOMOR_AJU"));
		$this->newtable->set_formid("f".$tipe);
		$this->newtable->set_divid("div".$tipe);
		$this->newtable->sortby("ASC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
		
	function list_tab($aju=""){
		$content='<div id="tab">
					<ul class="tab">
					  <li><a href="#tab-1">Data Barang</a></li>
					  <li><a href="#tab-2">Data Kemasan</a></li>
					  <li><a href="#tab-3">Data Kontainer</a></li>
					  <li><a href="#tab-4">Data Dokumen</a></li>
					</ul>
					<div id="tab-1">'.$this->list_detil("barang",$aju).'</div>
					<div id="tab-2">'.$this->list_detil("kemasan",$aju).'</div>
					<div id="tab-3">'.$this->list_detil("container",$aju).'</div>
					<div id="tab-4">'.$this->list_detil("dokumen",$aju).'</div>
				</div>';	
		return $content;			
	}
	
	function detil($type="",$dokumen="",$aju="",$action="",$PERBAIKAN=""){
		$this->load->library('newtable');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($type=="barang"){
			$judul = "Daftar Detil Barang";			
			$SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG', REPLACE(KODE_BARANG, '.', '^') 'KODE_BARANG',
					f_ref('ASAL_JENIS_BARANG',JENIS_BARANG) 'JENIS BARANG',
					f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
					CONCAT(NEGARA_ASAL,' - ',f_negara(NEGARA_ASAL)) AS 'ASAL BARANG', CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
					CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
					CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
					f_ref('STATUS',STATUS) AS 'STATUS', NOMOR_AJU, SERI_HS FROM t_bc23_dtl 
					WHERE NOMOR_AJU= '".$aju."'  AND KODE_TRADER = '".$kode_trader."'";
			
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERI_HS","KODE_BARANG"));
			$this->newtable->hiddens(array('NOMOR_AJU','SERI_HS', 'KODE_BARANG'));
			$this->newtable->orderby("SERI");
			$this->newtable->sortby("ASC");
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
							array('URAIAN_BARANG', 'URAIAN BARANG')));				
					
		}else if($type=="kemasan"){
			$judul = "Daftar Detil Kemasan";	
			$SQL = "SELECT JUMLAH AS 'JUMLAH', KODE_KEMASAN AS 'SERI',  CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', 
					MERK_KEMASAN AS 'MEREK', NOMOR_AJU FROM t_bc23_kms 
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
			
			$this->newtable->keys(array("NOMOR_AJU","SERI")); 	
			$this->newtable->hiddens(array("NOMOR_AJU","SERI"));	
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->sortby("DESC");
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));	
						
		}else if($type=="kontainer"){
			$judul = "Daftar Detil Kontainer";	
			$SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', NOMOR AS 'SERI', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', 
					f_ref('JENIS_STUFF',TIPE) AS 'TIPE', NOMOR_AJU	FROM t_bc23_cnt 
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
					
			$this->newtable->keys(array("NOMOR_AJU","SERI")); 
			$this->newtable->hiddens(array("NOMOR_AJU","SERI"));	
			$this->newtable->orderby("NOMOR");
			$this->newtable->sortby("DESC");
			$this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TIPE', 'TIPE')));			
							
		}else if($type=="dokumen"){
			$judul = "Daftar Detil Dokumen";	
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', 
					NOMOR AS 'NOMOR DOKUMEN',DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN',  
					NOMOR_AJU, 
					REPLACE(NOMOR,'/','~') AS 'SERI', 
					KODE_DOKUMEN AS SERI_HS 
					FROM t_bc23_dok  
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
					
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERI_HS")); 	
			$this->newtable->hiddens(array("NOMOR_AJU","SERI","SERI_HS"));	
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->sortby("DESC");
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));			
									
		}else{
			return "Failed";exit();
		}		
		
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/detil/".$type."/bc281/".$aju);
		$this->newtable->cidb($this->db);
		$this->newtable->tipe_proses('button');
		$this->newtable->count_keys(array("NOMOR_AJU"));
		$this->newtable->ciuri($ciuri);
		$this->newtable->rowcount(20);
		$this->newtable->clear();  
		$this->newtable->set_formid("f".$type);		
		$this->newtable->set_divid("div".$type);	
		
		
		if($PERBAIKAN){			
			$FPERBAIKAN ='<form id="fperbaikan_">
			<input type="hidden" name="PERBAIKAN[KPBC_UBAH]" id="PERBAIKAN_KODE_KPBC" value="'.$PERBAIKAN["KPBC_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NO_SURAT_UBAH]" id="PERBAIKAN_NOMOR_SURAT" value="'.$PERBAIKAN["NO_SURAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[TGL_SURAT_UBAH]" id="PERBAIKAN_TANGGAL_SURAT" value="'.$PERBAIKAN["TGL_SURAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NM_PEJABAT_UBAH]" id="PERBAIKAN_NAMA_PEJABAT" value="'.$PERBAIKAN["NM_PEJABAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[JABATAN_UBAH]" id="PERBAIKAN_JABATAN" value="'.$PERBAIKAN["JABATAN_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NIP_UBAH]" id="PERBAIKAN_NIP" value="'.$PERBAIKAN["NIP_UBAH"].'"/>
			</form>';
			$FPERBAIKAN .= '<p id="notify" class="notify" style="display:none">Perubahan Data Barang Berhasil, Untuk melihat hasil perubahannya silahkan klik menu History perubahan</p>';
			echo $FPERBAIKAN;
			$process = array('Ubah' => array('EDIT', site_url().'/pemasukan/'.$type.'/bc281', 'N', 'tbl_edit.png'));	
		}else{
			$process = array('Tambah' => array('ADD', site_url().'/pemasukan/'.$type.'/bc281/'.$aju, '0', 'icon-plus'),
					     	 'Ubah' => array('EDIT', site_url().'/pemasukan/'.$type.'/bc281', '1', 'icon-edit'),
						 	 'Hapus' => array('DEL', site_url().'/pemasukan/'.$type.'/bc281', 'N', 'icon-remove red'));		
		}		
		if($action=="perbaikan")$process = array('Ubah' => array('EDIT', site_url().'/pemasukan/'.$type.'/bc281', 'N', 'tbl_edit.png'));				
		
		$this->newtable->menu($process);
		$tabel = $this->newtable->generate($SQL);			
		$arrdata = array("judul" => $judul,
						 "tabel" => $tabel);
		if($this->input->post("ajax")) return $tabel;				 
		else return $arrdata;
	}  
	
	function list_detil_perubahan($aju=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');	
		
		$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JENIS_BARANG) 'JENIS BARANG',
				f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
				CONCAT(NEGARA_ASAL,' - ',f_negara(NEGARA_ASAL)) AS 'ASAL BARANG', CIF AS 'CIF', JUMLAH_SATUAN AS 'JUM. SATUAN', 
				CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
				CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
				f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU FROM t_bc23_perubahan WHERE NOMOR_AJU= '$aju'";		
				
		$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
								array('URAIAN_BARANG', 'URAIAN BARANG'),
								array('JENIS_BARANG', 'JENIS BARANG','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG'))));	
											
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/list_detil_perubahan/".$aju."/bc281");
		$this->newtable->hiddens(array('NOMOR_AJU','SERI'));					
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("fpbarang");
		$this->newtable->set_divid("divfpbarang");
		$this->newtable->orderby(2);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
}
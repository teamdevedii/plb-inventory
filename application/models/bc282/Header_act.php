<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Header_act extends CI_Model {
	function get_header($aju = "") {
		$data = array();
		$func = get_instance();
		$func->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$user_id =  $this->newsession->userdata('USER_ID');
		if(!$aju){
			$this->db->where('KODE_TRADER', $kode_trader);
            $this->db->select_max('TANGGAL_SKEP');
            $resultSkep = $this->db->get('m_trader_skep')->row();
            $maxTglSkep = $resultSkep->TANGGAL_SKEP;

            $query = "SELECT A.KODE_TRADER AS 'KODE_TRADER', A.KODE_ID 'KODE_ID_TRADER', A.ID AS 'ID_TRADER', 
					A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON, 
					A.KODE_API, A.NOMOR_API, D.LAST_AJU, D.NAMA_TTD, D.KOTA_TTD, 
					B.NOMOR_SKEP 'REGISTRASI',  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
					B.NOMOR_SKEP
					FROM M_TRADER A INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
					LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=C.KODE_TRADER 
					LEFT JOIN M_SETTING D ON A.KODE_TRADER=C.KODE_TRADER 
					WHERE B.TANGGAL_SKEP='" . $maxTglSkep . "' 
					AND A.KODE_TRADER='" . $kode_trader . "'
					AND C.USER_ID='" . $user_id . "'";

            $hasil = $func->main->get_result($query);
            if ($hasil) {
            	$objQuery = $this->db->query($query);
                foreach ($objQuery->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'save', 'aju' => $aju);
		}else{
			$query = "Select A.*,A.NOMOR_IZIN_TPB, f_kpbc(A.KODE_KPBC) URAIAN_KPBC,f_status_bc282(A.NOMOR_AJU,1) STATUS_DOK,
			    IFNULL(f_jumnetto_bc282(A.NOMOR_AJU),0) AS 'JUM_NETTO',IFNULL(A.CIF*A.NDPBM,0) AS CIFRP,f_valuta(a.KODE_VALUTA) 'URAIAN_VALUTA',
				(SELECT KODE_FASILITAS FROM T_BC282_PGT WHERE KODE_BEBAN='8' AND nomor_aju='$aju') AS KODE_FASILITAS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='1' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_BM,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='1' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_BM_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='2' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_PPN,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='2' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_PPN_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='3' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_PPNBM,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='3' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_PPNBM_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='4' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_PPH,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='4' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_PPH_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='5' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_CUKAI,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='5' AND KODE_FASILITAS='1' AND nomor_aju='$aju') AS PGT_CUKAI_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='8' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_PNBP,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='8' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_PNBP_STATUS,								
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='7' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_DENDA,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='7' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_DENDA_STATUS,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='9' AND KODE_FASILITAS='0' AND nomor_aju='$aju') AS PGT_BUNGA,
				(SELECT NILAI_BEBAN FROM T_BC282_PGT WHERE KODE_BEBAN='9' AND KODE_FASILITAS='4' AND nomor_aju='$aju') AS PGT_BUNGA_STATUS
			 FROM t_bc282_hdr A WHERE A.nomor_aju = '$aju'";
			$hasil = $func->main->get_result($query);
            if ($hasil) {
            	$objQuery = $this->db->query($query);
                foreach ($objQuery->result_array() as $row) {
                    $dataarray = $row;
                }
            }
			
			if (!$dataarray["KODE_ID_TRADER"]) {
                $query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID,  A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                        	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
							D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                        	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
                          FROM M_TRADER A 
                        	INNER JOIN T_USER C ON A.KODE_TRADER = C.KODE_TRADER 
                        	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER = B.KODE_TRADER 
                        	LEFT JOIN M_SETTING D ON A.KODE_TRADER = D.KODE_TRADER
                          WHERE A.KODE_TRADER = '".$kode_trader."'
                          AND C.USER_ID = ".$user_id." LIMIT 0,1";
                $hasil = $func->main->get_result($query);
                if ($hasil) {
                	$objQuery = $this->db->query($query);
                    foreach ($objQuery->result_array() as $row) {
                        $dt = $row;
                    }
                }
                $dataarray = array_merge($dataarray, $dt);
                $this->db->where('KODE_TRADER', $kode_trader);
                $this->db->select_max('TANGGAL_SKEP');
                $resultSkep = $this->db->get('m_trader_skep')->row();
                $maxTglSkep = $resultSkep->TANGGAL_SKEP;
                $SQL = "SELECT KODE_SKEP, NOMOR_SKEP, TANGGAL_SKEP, KODE_TRADER, SERI 
                        FROM m_trader_skep WHERE KODE_TRADER = '".$kode_trader."'
                        AND TANGGAL_SKEP='".$maxTglSkep."'";
                $hasil = $func->main->get_result($SQL);
                if ($hasil) {
                	$objQuery = $this->db->query($SQL);
                    foreach ($objQuery->result_array() as $row) {
                        $SKEP = $row;
                    }
                }
            }
            $data = array('act' => 'update');
		}
		$data = array_merge($data, array('aju' => $aju,
            'sess' => $dataarray,
            'jenis_barang' => $func->main->get_mtabel('KATEGORI'),
            'tujuan' => $func->main->get_mtabel('TUJUAN'),
            'tujuan_kirim' => $func->main->get_mtabel('TUJUAN_KIRIM'),
            'kode_id_trader' => $func->main->get_mtabel('KODE_ID'),
            'kode_id' => $func->main->get_mtabel('KODE_ID', 1, false),
            'jenis_tpb' => $func->main->get_mtabel('JENIS_TPB'),
            'status' => $func->main->get_mtabel('JENIS_IMPORTIR'),
            'kode_api' => $func->main->get_mtabel('API'),
            'cara_angkut' => $func->main->get_mtabel('MODA'),
            'kode_penutup' => $func->main->get_mtabel('KODE_PENUTUP'),
            'kode_pnbp' => $func->main->get_combobox("SELECT KODE , CONCAT(KODE,' - ',URAIAN) AS URAIAN 
									            	FROM M_TABEL 
													WHERE JENIS='STATUS_BAYAR' AND KODE IN('0','4') 
													ORDER BY KODE", "KODE", "URAIAN", TRUE))
        );
        return $data;
	}
	
	function set_header($type = "", $aju = "", $asal = "", $jns = "") {
        $func = & get_instance();
        $func->load->model("main", "main", true);
        if (strtolower($type) == "save" || strtolower($type) == "update") {
            foreach ($this->input->post('HEADER') as $a => $b) {
                $HEADER[$a] = $b;
            }
            foreach ($this->input->post('BEBAN') as $a1 => $b1) {
                $BEBAN[$a1] = $b1;
            }
            $ID_TRADER = str_replace('-', '', $HEADER['ID_TRADER']);
            $HEADER['ID_TRADER'] = str_replace('.', '', $ID_TRADER);
            $ID_PENERIMA = str_replace('-', '', $HEADER['ID_PENERIMA']);
            $HEADER['ID_PENERIMA'] = str_replace('.', '', $ID_PENERIMA);

            if ($this->input->post('LBH_4_TAHUN'))
                $HEADER["LBH_4_TAHUN"] = 1;
            else
                $HEADER["LBH_4_TAHUN"] = 0;

            if (strtolower($type) == "save") {
                $func->main->get_car($car, 'BC282');
                $BEBAN["NOMOR_AJU"] = $car;
                $HEADER["NOMOR_AJU"] = $car;
                $HEADER["STATUS"] = "04";
                $HEADER["CREATED_BY"] = $this->newsession->userdata('KODE_TRADER');
                $HEADER["CREATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                print_r($BEBAN); die();
                $exec = $this->db->insert('t_bc282_hdr', $HEADER);
                $exec = $this->db->insert('t_bc282_pgt', $BEBAN);
                $sql = "call hitungPungutanBC282('" . $car . "')";
                $this->db->query($sql);
                if ($exec) {
                    $func->main->set_car('BC282');
                    $func->main->activity_log('ADD HEADER BC82', 'CAR=' . $car);
                    echo "MSG#OK#Proses header Berhasil#" . $car . "#" . site_url() . "/pengeluaran/LoadHeader/bc82/" . $car . "#bc282#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            } else if (strtolower($type) == "update") {
                foreach ($this->input->post('HEADERDOK') as $a => $b) {
                    $HEADERDOK[$a] = $b;
                }
                $NODAF = $HEADERDOK["NOMOR_PENDAFTARAN"];
                $TGDAF = $HEADERDOK["TANGGAL_PENDAFTARAN"];
                $nodaftarEdit = $HEADERDOK["NOMOR_PENDAFTARAN_EDIT"];
                $tgldaftarEdit = $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"];
                $noDaftar = $HEADERDOK["NOMOR_PENDAFTARAN"];
                $tglDaftar = $HEADERDOK["TANGGAL_PENDAFTARAN"];
                $editHeader = false;
                if ($nodaftarEdit != $noDaftar || $tgldaftarEdit != $tglDaftar) {
                    $editHeader = true;
                    $kodeTrader = $this->newsession->userdata('KODE_TRADER');
                    $nodokOut = "";$nodokHdr="";
                    $tgldokOut = "";$tgldokHdr="";
                    if ($nodaftarEdit != $noDaftar) {
                        $nodokHdr = " NOMOR_PENDAFTARAN = '" . $nodaftarEdit . "' ";
                        $nodokOut = " NO_DOK = '" . $nodaftarEdit . "' ";
                        $HEADERDOK["NOMOR_PENDAFTARAN"] = $HEADERDOK["NOMOR_PENDAFTARAN_EDIT"];
                    }
                    if ($tgldaftarEdit != $tglDaftar) {
                        ($nodaftarEdit != $noDaftar) ? $koma = "," : $koma = "";
                        $tgldokHdr = "$koma TANGGAL_PENDAFTARAN = '" . $tgldaftarEdit . "' ";
                        $tgldokOut = "$koma TGL_DOK = '" . $tgldaftarEdit . "' ";
                        $HEADERDOK["TANGGAL_PENDAFTARAN"] = $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"];
                    }
                    if ($editHeader) {
                        $sqlLogOut = "SELECT LOGID FROM t_logbook_pengeluaran WHERE NO_DOK = '" . $noDaftar . "'
                                     AND TGL_DOK = '" . $tglDaftar . "' AND KODE_TRADER = '" . $kodeTrader . "'";
                        $logOut = $this->db->query($sqlLogOut);
                        $sqlHdr = "SELECT NOMOR_AJU FROM t_bc282_hdr WHERE NOMOR_AJU = '" . $HEADERDOK['NOMOR_AJU'] . "'
                                   AND NOMOR_PENDAFTARAN = '" . $noDaftar . "' AND TANGGAL_PENDAFTARAN = '" . $tglDaftar . "'
                                   AND KODE_TRADER = '" . $kodeTrader . "'";
                        $getHdr = $this->db->query($sqlHdr);
                        if ($getHdr->num_rows() > 0) {
                            $sqlUpdateHdr = "UPDATE t_bc282_hdr SET $nodokHdr $tgldokHdr WHERE NOMOR_AJU = '" . $HEADERDOK['NOMOR_AJU'] . "'
                                            AND NOMOR_PENDAFTARAN='" . $noDaftar . "' AND TANGGAL_PENDAFTARAN='" . $tglDaftar . "'
                                            AND KODE_TRADER = '" . $kodeTrader . "'";
                            $execHdr = $this->db->query($sqlUpdateHdr);
                        }
                        if ($logOut->num_rows() > 0) {
                            $sqlUpdateOut = "UPDATE t_logbook_pengeluaran SET $nodokOut $tgldokOut WHERE NO_DOK = '" . $noDaftar . "' 
                                           AND TGL_DOK = '" . $tglDaftar . "' AND KODE_TRADER = '" . $kodeTrader . "'";
                            $execOut = $this->db->query($sqlUpdateOut);
                        }
                    }
                }
                unset($HEADERDOK["NOMOR_PENDAFTARAN_EDIT"], $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"]);
                $CAR = $HEADER["NOMOR_AJU"];
                $BEBAN["NILAI_BEBAN"] = str_replace(",", "", $BEBAN["NILAI_BEBAN"]);
                $HEADER["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["UPDATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);

                if ($this->input->post("STATUS_DOK") == "LENGKAP" && $NODAF && $TGDAF) {
                    #CEK FLAGREVISI
                    ($this->input->post('flagrevisi') == "1") ? $HEADERDOK["STATUS"] = '19' : $HEADERDOK["STATUS"] = '07';
                    $this->db->where(array('NOMOR_AJU' => $CAR, 'KODE_BEBAN' => $BEBAN["KODE_BEBAN"]));
                    $exec = $this->db->update('t_bc282_pgt', $BEBAN);
                    $this->db->where(array('NOMOR_AJU' => $CAR));
                    $exec = $this->db->update('t_bc282_hdr', array_merge($HEADERDOK, $HEADER));
                } else {
                    $BEBAN["NOMOR_AJU"] = $CAR;
                    $JumDTBeban = (int) $func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS DATA_AJU FROM t_bc282_pgt WHERE NOMOR_AJU = '" . $CAR . "' AND KODE_BEBAN='" . $BEBAN["KODE_BEBAN"] . "'", "DATA_AJU");
                    if ($JumDTBeban > 0) {
                        $this->db->where(array('NOMOR_AJU' => $CAR, 'KODE_BEBAN' => $BEBAN["KODE_BEBAN"]));
                        $this->db->update('t_bc282_pgt', $BEBAN);
                    } elseif ($JumDTBeban == 0) {
                        $this->db->insert('t_bc282_pgt', $BEBAN);
                    }
                    $this->db->where(array('NOMOR_AJU' => $CAR));
                    $exec = $this->db->update('t_bc282_hdr', $HEADER);
                }
                $sql = "call hitungPungutanBC282('" . $CAR . "')";
                $this->db->query($sql);
                if ($exec) {
                    $func->main->activity_log('EDIT HEADER BC282', 'CAR=' . $CAR);
                    echo "MSG#OK#Proses header Berhasil#" . $CAR . "#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            }
        }
    }
}
?>
<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register_act extends CI_Model{	

	function get_data(){
			$conn =& get_instance();
			$conn->load->model("main", "main", true);				
			$arrdata = array(
							'act' => 'proses',
							'judul' => 'Registrasi Perusahaan',
							'kode_id_trader' => $conn->main->get_mtabel('KODE_ID'),
            				'kode_api_trader' => $conn->main->get_mtabel('API'),
            				'JENIS_TPB' => $conn->main->get_mtabel('JENIS_TPB'),
            				'TIPE_TRADER' => $conn->main->get_mtabel('JENIS_EKSPORTIR'),
							'PROPINSI_GB' => $conn->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'KOTA_GB' => $conn->main->get_combobox("SELECT KODE_DAERAH,URAIAN_DAERAH FROM M_DAERAH WHERE SUBSTRING(KODE_DAERAH,3,2)!='00' ORDER BY URAIAN_DAERAH ","KODE_DAERAH","URAIAN_DAERAH", TRUE),
							'JENIS_PLB' => $conn->main->get_combobox("SELECT KODE, URAIAN FROM M_TABEL WHERE JENIS='JENIS_PLB' AND KODE<>'01' ORDER BY KODE ","KODE","URAIAN", TRUE)
							);							 
			return $arrdata;
	}
	
	function setdata(){
		foreach($this->input->post('PENYELENGGARA') as $a => $b){
			$arrPenyelenggara[$a] = $b;
		}		
		$this->load->helper('email');
		if(!valid_email($arrPenyelenggara["EMAIL_USER"])){
			echo "MSG#ERR#Email tidak valid, periksa kembali email anda.";		
			die();			
		}
		//tambahan
		$NPWP = str_replace(".","",str_replace("-","",$arrPenyelenggara["ID"]));
		$SQL = "SELECT USERNAME FROM T_USER WHERE USERNAME='".$arrPenyelenggara["USERNAME"]."' UNION 
				SELECT ID FROM M_TRADER_TEMP WHERE USERNAME='".$arrPenyelenggara["USERNAME"]."'";
		$cek = $this->db->query($SQL);
		if($cek->num_rows() > 0){
			$ret = "MSG#ERR#Username sudah ada.";
		} else {		
			$KONFPASSWORD = $this->input->post('KONFPASSWORD');
			if($arrPenyelenggara["PASSWORD"]==$KONFPASSWORD){					
				$arrPenyelenggara["CREATED_TIME"] = date('Y-m-d H:i:s');		
				$arrPenyelenggara["PASS"] = $arrPenyelenggara["PASSWORD"];
				$arrPenyelenggara["PASSWORD"] = md5($arrPenyelenggara["PASSWORD"]);	
				$arrPenyelenggara["ID"]=$NPWP;
				
				$NOREGISTRASI="";
				for($i = 0; $i < 6; $i++) {
					$NOREGISTRASI .= rand(97, 122);
				}
				$arrPenyelenggara["NO_REGISTRASI"] = $NOREGISTRASI;
			
				$SQL1 = "SELECT MAX(SUBSTRING(KODE_TRADER,9,4))+1 MAXKODE FROM M_TRADER WHERE KODE_TRADER <> 'EDIINDONESIA'";
				$SQL2 = "SELECT MAX(SUBSTRING(KODE_TRADER,9,4))+1 MAXKODE FROM M_TRADER_TEMP";
				$rs1 = $this->db->query($SQL1);
				$rs2 = $this->db->query($SQL2);
				if($rs1->num_rows()>0) $MAXKODE_1 = (int)$rs1->row()->MAXKODE;
				if($rs2->num_rows()>0) $MAXKODE_2 = (int)$rs2->row()->MAXKODE;
				if($MAXKODE_1>=$MAXKODE_2) $NEXTID = $MAXKODE_1;
				else $NEXTID = $MAXKODE_2;
				$SISIP = substr(str_replace(array("PT","pt",".",","," "),"",$arrPenyelenggara["NAMA"]),0,4);
				$arrPenyelenggara["KODE_TRADER"] = "EDI".$SISIP.sprintf("%05d", $NEXTID);
						//echo $arrPenyelenggara;
						//die();						
				$exec = $this->db->insert('m_trader_temp', $arrPenyelenggara);
				if($exec){
					$isi = '<p>Selamat '.$arrPenyelenggara["NAMA"].' , Anda telah berhasil melakukan registrasi.<br />
								Email dan no registrasi anda :<br />
									Email : '.$arrPenyelenggara["EMAIL_USER"].'<br />
									No registrasi : '.$NOREGISTRASI.'
								</p>
								<p>Langkah selanjutnya adalah mempersiapkan dokumen pendukung dengan melengkapi:<br />
								1. NPWP<br />
								2. Ijin Domisili<br />
								3. SIUP<br />
								4. SK-Gudang Berikat<br />
								7. API<br />
								<p>Dokumen pendukung tersebut di atas dapat dikirimkan kepada :<br />PT EDI INDONESIA</p>
								<p>Wisma SMR 10th Fl<br />Jl Yos Sudarso Kav. 89<br />Jakarta Utara &ndash; 14350</p>
								<p>email : &nbsp;<a href="mailto:customs2@edi-indonesia.co.id">customs2@edi-indonesia.co.id</a></p>
								<p>telepon : 021 6505829</p>
								<p><br />Apabila Bapak/Ibu masih membutuhkan informasi lainnya terkait aplikasi GBInventory ini dapat menghubungi PIC kami sebagai berikut :</p>
								<ul><li>Rossa telp 021 650 5829 ext 8121 email : <a href="mailto:rossa@edi-indonesia.co.id">rossa@edi-indonesia.co.id</a></li><li>Ginanjar Anggi Wiratmoko telp 021 650 5829 ext 8211 email : <a href="mailto:ginanjar@edi-indonesia.co.id">ginanjar@edi-indonesia.co.id</a></li></ul>
								';											

					if($this->send_mail($arrPenyelenggara["EMAIL_USER"],'', 'Konfirmasi Registrasi PLBInventory', $isi,'')){
						}
					$ret = "MSG#OK#Register perusahaan Berhasil.<br>Silahkan cek email anda untuk proses lebih lanjut.<br>Terimakasih.#".site_url()."/#";
					}else{					
						$ret = "MSG#ERR#Register perusahaan Gagal#";
					}	 
				}else{
					$ret = "MSG#ERR#Password tidak sama.";
				}				
			}
		//}
		return $ret;
	}
	
	function send_mail($to, $nama, $subject, $isi, $flag){
		$this->load->library("phpmailer");
		$mailconfig = $this->config->config;
		$mailconfig = $mailconfig['mail_config'];
		$this->phpmailer->Config($mailconfig);
		$this->phpmailer->Subject = $subject;
		//$path = "D:\Web\aikbci\\uploads\\Formulir Belangganan Jasa EDI e-INKABER.xls";
		if($flag!='weare'){
			$path = "/home/inguber/file/Formulir Belangganan Jasa EDI GBInventory.xls";
			$this->phpmailer->AddAttachment($path, 'Formulir Belangganan Jasa EDI GBInventory.xls');
		}
		$email = explode(";", $to);
		foreach($email as $a){
			$this->phpmailer->AddAddress($a, '');
		}
		
		$body = '<html><body style="background: #ffffff; color: #000000; font-family: arial; font-size: 13px; margin: 20px; color: #363636;"><table style="margin-bottom: 2px"><tr style="font-size: 13px; color: #0b1d90; font-weight: 700; font-family: arial;"><td width="41" style="margin: 0 0 6px 0px;"><img src="'.base_url().'img/logo.png" style="vertical-align: middle;"/></td><td style="font-family: arial; vertical-align: middle; color: #153f6f;">'.$subject.'<br/><span style="color: #858585; font-size: 10px; text-decoration: none;">www.plbnventory.com</span></td></tr></table><div style="border-top: 1px solid #dcdcdc; margin-top: 4px; margin-bottom: 10px; padding: 5px; font-family: Verdana; font-size: 12px;  height:20px;text-align:justify;">'.$isi.'</div><div style="border-top: 1px solid #dcdcdc; clear: both; font-size: 11px; margin-top: 10px; padding-top: 5px;"><div style="font-family: arial; font-size: 10px; color: #a7aaab;">Bonded PLB Inventory Solution</div><a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="http://www.edi-indonesia.co.id/">Website EDII </a> | <a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="'.site_url().'">Website PLB Inventory</a></div></body></html>';
		$this->phpmailer->Body = $body;
		return $this->phpmailer->Send();
		
	}
}
?>
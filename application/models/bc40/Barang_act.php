<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Barang_act extends CI_Model{
	function set_perbaikan($TYPE="",$DATA="",$BARANG=""){
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($DATA){
			foreach($DATA as $x => $y){
				$PERBAIKAN[$x] = $y;
				$PERBAIKAN["KODE_TRADER"] = $kode_trader;
			}	
			$PERBAIKAN["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
			$PERBAIKAN["UPDATED_TIME"] = date("Y-m-d H:i:s");
				
			$AJU = $PERBAIKAN["AJU"];
			$SERI = $PERBAIKAN["SERI"];	
			
			$BARANG["SERI"] = $SERI;
			
			unset($PERBAIKAN["AJU"]);
			unset($PERBAIKAN["SERI"]);
			
			$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $AJU));
			$this->db->update('t_bc40_hdr', $PERBAIKAN);
			
			$query = "SELECT ID FROM t_bc40_perubahan WHERE NOMOR_AJU='".$AJU."' AND SERI='".$SERI."'";
			$data = $this->db->query($query);
			if($data->num_rows() > 0){
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $AJU, 'SERI' => $SERI));
				$this->db->update('t_bc40_perubahan', $BARANG);
			}else{
				$this->db->insert('t_bc40_perubahan', $BARANG);			
			}
			
			$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $AJU, 'SERI' => $SERI));
			$exec = $this->db->update('t_bc40_dtl', array("PERUBAHAN_FLAG"=>"Y"));
			if($exec){
				echo "MSG#OK#Simpan data Barang Berhasil#edit#";
			}else{					
				echo "MSG#ERR#Simpan data Barang Gagal#";
			}		
		}		
	}	
	
	function set_barang($type="", $isajax=""){
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($type=="save" || $type=="update"){
			foreach($this->input->post('BARANG') as $a => $b){
				$BARANG[$a] = $b;
			}
			$BARANG["KODE_TRADER"] = $kode_trader;
			$BARANG["NOMOR_AJU"] = $aju;
			if($type=="save"){				
				$status = "04";				
				$seri = (int)$func->main->get_uraian("SELECT MAX(SERI) AS MAXSERI FROM t_bc40_dtl WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."'", "MAXSERI") + 1;
				$BARANG["SERI"] = $seri;
				$BARANG["STATUS"] = $status;
				$BARANG["KODE_HS"] = str_replace(".","",$BARANG["KODE_HS"]);
				$exec = $this->db->insert('t_bc40_dtl', $BARANG);			
				if($exec){
					$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc40('".$aju."','".$kode_trader."'),0) AS JUM FROM DUAL", "JUM");     
					$this->db->where(array('NOMOR_AJU' => $aju,"KODE_TRADER"=>$kode_trader));
					$this->db->update('t_bc40_hdr', array("NETTO" => $netto));
					$func->main->activity_log('ADD DETIL BC40','CAR='.$aju.', SERI='.$seri);
					echo "MSG#OK#Simpan data Barang Berhasil#edit#".site_url()."/pemasukan/LoadHeader/bc40/".$aju."#";
				}else{					
					echo "MSG#ERR#Simpan data Barang Gagal#";
				}	
			}else{							
				$seri = $this->input->post('seri');
				$BARANG["STATUS"] = '04';			
				$BARANG["KODE_HS"] = str_replace(".","",$BARANG["KODE_HS"]);
				if($this->input->post('PERBAIKAN')){
					$ARRADD = array("AJU"=>$aju,"SERI"=>$seri);
					$this->set_perbaikan("update",array_merge($this->input->post('PERBAIKAN'),$ARRADD), $BARANG);	
				}
				else{
					$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri, 'KODE_TRADER'=>$kode_trader));
					$exec = $this->db->update('t_bc40_dtl', $BARANG);
					if($exec){
						$this->set_perbaikan("update",$this->input->post('PERBAIKAN'), $BARANG);
						$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc40('".$aju."','".$kode_trader."'),0) AS JUM FROM DUAL", "JUM");     
						$this->db->where(array('NOMOR_AJU' => $aju,'KODE_TRADER'=>$kode_trader));
						$this->db->update('t_bc40_hdr', array("NETTO" => $netto));
						$func->main->activity_log('EDIT DETIL BC40','CAR='.$aju.', SERI='.$seri);
						echo "MSG#OK#Update data Barang Berhasil#edit#".site_url()."/pemasukan/LoadHeader/bc40/".$aju."#";
					}else{					
						echo "MSG#ERR#Update data Barang Gagal#edit#";
					}	
				}
			}
		}else if($type=="delete"){	
			foreach($this->input->post('tb_chkfbarang') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$seri = $arrchk[1];
				$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri, 'KODE_TRADER'=>$kode_trader));
				$exec = $this->db->delete('t_bc40_dtl');
				$func->main->activity_log('DELETE DETIL BC40','CAR='.$aju.', SERI='.$seri);
			}
			if($exec){
				$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc40('".$aju."','".$kode_trader."'),0) AS JUM FROM DUAL", "JUM");     
				$this->db->where(array('NOMOR_AJU' => $aju,'KODE_TRADER'=>$kode_trader));
				$this->db->update('t_bc40_hdr', array("NETTO" => $netto));
				echo "MSG#OK#Hapus data Barang Berhasil#".site_url()."/pemasukan/detil/barang/bc40/".$aju."#".site_url()."/pemasukan/LoadHeader/bc40/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Barang Gagal#";die();
			}
		}
	}
		
	function get_barang($aju="",$seri=""){		
		$data = array();		
		$conn = get_instance();
		$conn->load->model("main","main", true);	
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');
		if($aju && $seri){
			$query = "SELECT A.*, f_satuan(A.KODE_SATUAN) 'URKODE_SATUAN', f_kemas(A.KODE_KEMASAN) 'URKODE_KEMASAN',
					  f_satuan(A.KODE_SATUAN) AS URAIAN_SATUAN,
					  B.KODE_SATUAN AS KD_SAT_BESAR, B.KODE_SATUAN_TERKECIL AS KD_SAT_KECIL
					  FROM t_bc40_dtl A
					  LEFT JOIN m_trader_barang B ON B.KODE_BARANG=A.KODE_BARANG 
					  AND B.JNS_BARANG=A.JNS_BARANG AND B.KODE_TRADER='".$KODETRADER."' 
					  WHERE A.NOMOR_AJU = '".$aju."' AND A.SERI = '".$seri."'";			
			$hasil = $conn->main->get_result($query);								
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess'=>$row, 'aju'=>$aju, 'seri'=>$seri);		
				}
			}
		}else{
			$data = array('act' => 'save','aju'=>$aju);
		}
			
		$data = array_merge($data, array('jenis_komoditi' => $conn->main->get_mtabel('JENIS_BARANG'),
										 'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_KIRIM'),
										 'penggunaan' => $conn->main->get_mtabel('PENGGUNAAN_BARANG'),										
										 'komoditi_cukai' => $conn->main->get_mtabel('KOMODITI_CUKAI'),
										 'jenis_tarif' => $conn->main->get_mtabel('JENIS_TARIF_BM',1,TRUE,"AND KODE IN ('1','2')"),
										 'kode_bm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppn' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppnbm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_pph' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_cukai' => $conn->main->get_mtabel('STATUS_BAYAR')));
		return $data;
			
	}
}
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Detil_act extends CI_Model{
	function list_detil($tipe="",$aju=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($tipe=="barang"){
			$SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
					f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
					JUMLAH_SATUAN AS 'JUM. SATUAN',CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', 
					JUMLAH_KEMASAN AS 'JUM. KEMASAN',CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
					f_ref('STATUS',STATUS) AS 'STATUS' FROM t_bc40_dtl WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
					
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), array('URAIAN_BARANG', 'URAIAN BARANG')));
			
		}else if($tipe=="kemasan"){
			$SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', 
					MERK_KEMASAN AS 'MEREK', NOMOR_AJU	FROM t_bc40_kms WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";	
					
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));	
			
		}else if($tipe=="dokumen"){			
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', 
					NOMOR AS 'NOMOR DOKUMEN', DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', NOMOR_AJU 
					FROM t_bc40_dok  WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";	
					
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));	
			
		}else{
			return "Failed";exit();
		}
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/listdetil/".$tipe."/".$aju."/bc40");
		$this->newtable->hiddens(array('NOMOR_AJU','SERI'));		
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("f".$tipe);
		$this->newtable->set_divid("div".$tipe);
		$this->newtable->orderby(2);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
		
		
	function list_tab($aju=""){
		$content='<div class="row-fluid">
				  <div class="tabbable">
					<ul class="nav nav-tabs" id="myTab">
	 <li class="active"> <a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
					  <li> <a data-toggle="tab" href="#tabKemasan">Data Kemasan</a></li>
					 <li> <a data-toggle="tab" href="#tabDokumen">Data Dokumen</a></li>
					</ul>
					<div class="tab-content">
					 <div id="tabBarang" class="tab-pane in active">'.$this->list_detil("barang",$aju).'</div>
					 <div id="tabKemasan" class="tab-pane">'.$this->list_detil("kemasan",$aju).'</div>
					 <div id="tabDokumen" class="tab-pane">'.$this->list_detil("dokumen",$aju).'</div>
					</div>
				  </div>
				</div>';
		return $content;			
	}		
	
		
	function detil($type="",$dokumen="",$aju="",$action="",$PERBAIKAN=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($type=="barang"){
			$judul = "Daftar Detil Barang";			
			$SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG', 
						f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG', f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
						JUMLAH_SATUAN AS 'JUM. SATUAN',CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', 
						JUMLAH_KEMASAN AS 'JUM. KEMASAN', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', 
						NETTO AS 'NETTO', f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU, SERI_HS AS SERITRP
					FROM t_bc40_dtl 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";
					
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERITRP","KODE BARANG"));
			$this->newtable->hiddens(array("NOMOR_AJU","SERI","SERITRP"));
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
												array('URAIAN_BARANG', 'URAIAN BARANG')));
		}else if($type=="kemasan"){
			$judul = "Daftar Detil Kemasan";	
			$SQL = "SELECT CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', 
						JUMLAH AS 'JUMLAH', MERK_KEMASAN AS 'MEREK',
			        	NOMOR_AJU, KODE_KEMASAN AS SERI, KODE_KEMASAN AS SERITRP
					FROM t_bc40_kms WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";	
					
			$this->newtable->hiddens(array("SERI","NOMOR_AJU","SERITRP"));
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERITRP"));
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));
			
		}else if($type=="dokumen"){
			$judul = "Daftar Detil Dokumen";	
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', 
						NOMOR AS 'NOMOR DOKUMEN', DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', 
						KODE_DOKUMEN AS 'SERI', NOMOR_AJU, NOMOR AS SERITRP 
					FROM t_bc40_dok 
						WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";				
			
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERITRP"));	
			$this->newtable->hiddens(array("NOMOR_AJU","SERI","SERITRP"));
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));	
		}else{
			return "Failed";exit();
		}		
		
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/detil/".$type."/bc40/".$aju);
		$this->newtable->cidb($this->db);
		$this->newtable->count_keys(array('NOMOR_AJU'));
		$this->newtable->ciuri($ciuri);
		$this->newtable->orderby(4);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(20);
		$this->newtable->clear();  
		$this->newtable->set_formid("f".$type);		
		$this->newtable->set_divid("div".$type);
		$this->newtable->tipe_proses('button');
				
		
		if($PERBAIKAN){			
			$FPERBAIKAN ='<form id="fperbaikan_">
			<input type="hidden" name="PERBAIKAN[KPBC_UBAH]" id="PERBAIKAN_KODE_KPBC" value="'.$PERBAIKAN["KPBC_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NO_SURAT_UBAH]" id="PERBAIKAN_NOMOR_SURAT" value="'.$PERBAIKAN["NO_SURAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[TGL_SURAT_UBAH]" id="PERBAIKAN_TANGGAL_SURAT" value="'.$PERBAIKAN["TGL_SURAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NM_PEJABAT_UBAH]" id="PERBAIKAN_NAMA_PEJABAT" value="'.$PERBAIKAN["NM_PEJABAT_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[JABATAN_UBAH]" id="PERBAIKAN_JABATAN" value="'.$PERBAIKAN["JABATAN_UBAH"].'"/>
			<input type="hidden" name="PERBAIKAN[NIP_UBAH]" id="PERBAIKAN_NIP" value="'.$PERBAIKAN["NIP_UBAH"].'"/>
			</form>';
			$FPERBAIKAN .= '<p id="notify" class="notify" style="display:none">Perubahan Data Barang Berhasil, Untuk melihat hasil perubahannya silahkan klik menu History perubahan</p>';
			echo $FPERBAIKAN;
			$process = array('Ubah '.$type => array('EDIT', site_url().'/pemasukan/'.$type.'/bc40', '1'));	
		}
		else{
			$process = array('Tambah ' => array('ADD', site_url().'/pemasukan/'.$type.'/bc40/'.$aju, '0','icon-plus'),
					     	 'Ubah ' => array('EDIT', site_url().'/pemasukan/'.$type.'/bc40', '1','icon-edit'),
						 	 'Hapus ' => array('DEL', site_url().'/pemasukan/'.$type.'/bc40', 'N','icon-remove'));	
		}		
		if($action=="perbaikan")$process = array('Ubah '.$type => array('EDIT', site_url().'/pemasukan/'.$type.'/bc40', '1'));
		
		$this->newtable->menu($process);
		$tabel .= $this->newtable->generate($SQL);			
		$arrdata = array("judul" => $judul,
						 "tabel" => $tabel);
		if($this->input->post("ajax")) return $tabel;				 
		else return $arrdata;	
	}  
	
	function list_detil_perubahan($aju=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');	
		
		$SQL = "SELECT KODE_BARANG 'KODE BARANG', URAIAN_BARANG 'URAIAN BARANG',f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
				f_formaths(KODE_HS) 'KODE HS', SERI 'SERI', 
				JUMLAH_SATUAN AS 'JUM. SATUAN',CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', 
				JUMLAH_KEMASAN AS 'JUM. KEMASAN',CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
				f_ref('STATUS',STATUS) AS 'STATUS' FROM t_bc40_perubahan WHERE NOMOR_AJU= '$aju'";
					
		$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
					array('URAIAN_BARANG', 'URAIAN BARANG'),
					array('JENIS_BARANG', 'JENIS BARANG','tag-select',$func->main->get_mtabel('ASAL_JENIS_BARANG'))));
											
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/list_detil_perubahan/".$aju."/bc40");
		$this->newtable->hiddens(array('NOMOR_AJU','SERI'));			
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("fpbarang");
		$this->newtable->set_divid("divfpbarang");
		$this->newtable->orderby(2);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
}
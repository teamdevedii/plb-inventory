<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kemasan_act extends CI_Model{
	function set_kemasan($type="", $isajax=""){	
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$aju = $this->input->post('NOMOR_AJU');
		if($type=="save" || $type=="update"){
			foreach($this->input->post('KEMASAN') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"]  = $kode_trader;
			}
			if($type=="save"){	
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS JUM FROM T_BC40_KMS WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' and KODE_KEMASAN = '".$arrinsert["KODE_KEMASAN"]."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;				
				if($countKode > 0){
					echo "MSG#ERR#Simpan data Kemasan Gagal. Kode Kemasan Sudah Pernah digunakan.#";
				}else{
					$exec = $this->db->insert('t_bc40_kms', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD KEMASAN BC40','CAR='.$aju.', KODE_KEMASAN='.$arrinsert["KODE_KEMASAN"]);
						echo "MSG#OK#Simpan data Kemasan Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Kemasan Gagal#";
					}
				}
			}else{
				$kode_kemas = $this->input->post('kode_kemas');	
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas,'KODE_TRADER'=>$kode_trader));
				$exec=$this->db->update('t_bc40_kms', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT KEMASAN BC40','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkemasan') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$kode_kemas = $arrchk[1];				
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas,'KODE_TRADER'=>$kode_trader));
				$exec = $this->db->delete('t_bc40_kms');	
				$func->main->activity_log('DELETE KEMASAN BC40','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
			}
			if($exec){
				echo "MSG#OK#Hapus data Kemasan Berhasil#".site_url()."/pemasukan/detil/kemasan/bc40/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Kemasan Gagal#del#";die();
			}
		}
	}
	
	
	 function get_kemasan($aju="", $kode_kemas=""){
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($aju && $kode_kemas){
			$query = "SELECT KODE_KEMASAN,JUMLAH,MERK_KEMASAN,f_kemas(KODE_KEMASAN) URAIAN_KEMASAN 
						FROM t_bc40_kms 
						WHERE NOMOR_AJU = '".$aju."' AND KODE_TRADER = '".$kode_trader."'
					  AND KODE_KEMASAN = '".$kode_kemas."'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save');
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,'kode_kemas' => $kode_kemas));
		return $data;
	}
}
?>
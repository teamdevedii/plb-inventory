<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Header_act extends CI_Model{
	
	function get_header($aju=""){
		$data = array();
		$func = get_instance();
		$func->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$user_id = $this->newsession->userdata("USER_ID");
		if($aju){	
			$query = "Select A.NOMOR_AJU, A.KODE_KPBC, A.JENIS_TPB, A.TUJUAN_KIRIM, A.KODE_ID_TRADER, A.ID_TRADER,
						A.NAMA_TRADER, A.ALAMAT_TRADER, A.NOMOR_IZIN_TPB, A.KODE_ID_PENGIRIM, A.ID_PENGIRIM, A.NAMA_PENGIRIM, 
						A.ALAMAT_PENGIRIM, A.JENIS_SARANA_ANGKUT, A.NOMOR_POLISI, A.KODE_VALUTA, A.NDPBM, A.HARGA_PENYERAHAN,
						A.BRUTO, A.NETTO, A.VOLUME, A.JUMLAH_DTL, A.TANGGAL_TTD, A.KOTA_TTD, A.NAMA_TTD, A.NAMA_PEJABAT_BC, 
						A.NIP_PEJABAT_BC, A.NOMOR_PENDAFTARAN, A.TANGGAL_PENDAFTARAN, A.NOMOR_DOK_PABEAN, A.TANGGAL_DOK_PABEAN, 
						A.NOMOR_DOK_INTERNAL, A.TANGGAL_DOK_INTERNAL, A.SNRF, A.TANGGAL_REALISASI, A.STATUS, A.KPBC_UBAH, 
						A.NO_SURAT_UBAH, A.TGL_SURAT_UBAH, A.NM_PEJABAT_UBAH, A.JABATAN_UBAH, A.NIP_UBAH, A.PARSIAL_FLAG, 
						A.STATUS_REALISASI, A.KODE_LOKASI ,f_kpbc(A.KODE_KPBC) URAIAN_KPBC,
						f_status_bc40(A.NOMOR_AJU,1,KODE_TRADER) STATUS_DOK ,
					  	IFNULL(f_jumnetto_bc40(A.NOMOR_AJU,A.KODE_TRADER),0) AS 'JUM_NETTO'
					  FROM t_bc40_hdr A 
					  	WHERE A.nomor_aju = '".$aju."' AND KODE_TRADER='".$kode_trader."'";
			$hasil = $func->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$dataarray = $row;
				}
			}
			
			$SQL = "SELECT ID, KODE_TRADER, NOMOR_AJU, NOMOR_DAFTAR, TANGGAL_DAFTAR
					FROM T_BC40_DOKASAL WHERE KODE_TRADER='".$kode_trader."' AND NOMOR_AJU='".$aju."'";		
			$hasil = $func->main->get_result($SQL);
			$asal = "";
			if($hasil){
				$no=1;	
				foreach($SQL->result_array() as $data){			
					$asal .= '<tr id="trnomor'.$no.'">
								<td width="180">Nomor</td>
								<td><input type="text" name="ASAL[NOMOR_DAFTAR][]" id="NOMOR_DAFTAR'.$no.'" maxlength="8" class="text" value="'.$data["NOMOR_DAFTAR"].'"/><input style="margin-left:5px" type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search(\'BC41ASAL\',\'NOMOR_DAFTAR'.$no.';TANGGAL_DAFTAR'.$no.'\',\'Dokumen BC 4.1 Asal\',this.form.id,650,400)" value="..."></td>
							  </tr>
							  <tr id="trtanggal'.$no.'">
							  <td>Tanggal</td>
							  <td><input type="text" name="ASAL[TANGGAL_DAFTAR][]" id="TANGGAL_DAFTAR'.$no.'" class="sstext" maxlength="10" onFocus="ShowDP(this.id);" value="'.$data["TANGGAL_DAFTAR"].'"/>&nbsp;&nbsp;YYYY-MM-DD&nbsp;';
					if($no!=1){		  
						$asal .='<a href="javascript:void(0)" onClick="hapusasal('.$no.')"><img width="16px" height="16px" align="texttop" style="border:none" src="'.base_url().'/img/tbl_delete.png">&nbsp;Hapus</a>';		  
					}
					$asal .='</td></tr>';	
					$no++;
				}
			}else{
				$asal = '<tr id="trnomor">
							<td width="180">Nomor</td>
							<td id="tdnomor"><input type="text" name="ASAL[NOMOR_DAFTAR][]" id="NOMOR_DAFTAR" maxlength="8" class="text" /><input style="margin-left:5px" type="button" name="cari" id="cari" class="btn btn-primary btn-xs" onclick="tb_search(\'BC41ASAL\',\'NOMOR_DAFTAR;TANGGAL_DAFTAR\',\'Dokumen BC 4.1 Asal\',this.form.id,650,400)" value="...">
						   </td>
						  </tr>
						  <tr id="trtanggal">
							<td>Tanggal</td>
							<td id="tdtanggal"><input type="text" name="ASAL[TANGGAL_DAFTAR][]" id="TANGGAL_DAFTAR" class="sstext" maxlength="10" onFocus="ShowDP(this.id);" />
							  &nbsp;&nbsp;YYYY-MM-DD</td>
						  </tr>';	
			}
			
			if(!$dataarray["KODE_ID_TRADER"]){
				$this->db->where('KODE_TRADER',$kode_trader);
				$this->db->select_max('TANGGAL_SKEP');
				$resultSkep = $this->db->get('m_trader_skep')->row();
				$maxTglSkep = $resultSkep->TANGGAL_SKEP;
				
				$query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID,  A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                        	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
							D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                        	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
                          FROM M_TRADER A 
                        	INNER JOIN T_USER C ON A.KODE_TRADER = C.KODE_TRADER 
                        	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER = B.KODE_TRADER AND B.TANGGAL_SKEP = '".$maxTglSkep."'
                        	LEFT JOIN M_SETTING D ON A.KODE_TRADER = D.KODE_TRADER
                          WHERE A.KODE_TRADER = '".$kode_trader."'
                          AND C.USER_ID = ".$user_id." LIMIT 0,1";
								
				$hasil = $func->main->get_result($query);
				if($hasil){
					foreach($query->result_array() as $row){
						$dt = $row;
					}
				}		
				$dataarray = array_merge($dataarray,$dt);	
			}
			$data = array('act' => 'update');
		}else{
			$this->db->where('KODE_TRADER',$kode_trader);
			$this->db->select_max('TANGGAL_SKEP');
			$resultSkep = $this->db->get('m_trader_skep')->row();
			$maxTglSkep = $resultSkep->TANGGAL_SKEP;
			
			$query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                      	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
						D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                      	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM', KODE_API, NOMOR_API
                      FROM M_TRADER A 
                      	INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                        LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER AND B.TANGGAL_SKEP = '".$maxTglSkep."'
                        LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
                       WHERE A.KODE_TRADER = '".$kode_trader."'
                        AND C.USER_ID = ".$user_id." LIMIT 0,1";
			$hasil = $func->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$dataarray = $row;
				}
			}	
			$data = array('act' => 'save', 'aju' => $aju);	
		}
		$data = array_merge($data, array('aju' => $aju, 'sess' => $dataarray,
										 'jenis_barang' => $func->main->get_mtabel('JENIS_BARANG'),
						  				 'tujuan'  => $func->main->get_mtabel('TUJUAN'),
										 'tujuan_kirim'  => $func->main->get_mtabel('TUJUAN_KIRIMBC40'),
										 'kode_id' => $func->main->get_mtabel('KODE_ID',1,false),
										 'jenis_tpb' => $func->main->get_mtabel('TUJUAN'),
										 'status' => $func->main->get_mtabel('JENIS_IMPORTIR'),
										 'kode_api' => $func->main->get_mtabel('API'),
										 'cara_angkut' => $func->main->get_mtabel('MODA'),
										 'kode_penutup' => $func->main->get_mtabel('KODE_PENUTUP'),
										 'dataasal'=>$asal));
		return $data;
	}
	
	function set_header($type="",$isajax=""){
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		if(strtolower($type)=="save" || strtolower($type)=="update"){			
			foreach($this->input->post('HEADER') as $a => $b){
				$HEADER[$a] = $b;
			}	
			if(strtolower($type)=="save"){
				$func->main->get_car($car,'BC40');
				$HEADER["NOMOR_AJU"] = $car;
				$HEADER["CREATED_BY"] = $this->newsession->userdata('USER_ID');
				$HEADER["CREATED_TIME"]=date('Y-m-d H:i:s',time()-60*60*1);	
				$HEADER['ID_TRADER'] = str_replace('.','',str_replace('-','',$HEADER['ID_TRADER']));
				$HEADER['ID_PENGIRIM'] = str_replace('.','',str_replace('-','',$HEADER['ID_PENGIRIM']));
				
				$SQL = "SELECT NOMOR_AJU FROM T_BC40_HDR WHERE NOMOR_AJU='".$car."' AND KODE_TRADER='".$KODE_TRADER."'";
				$rs = $func->main->get_result($SQL);
				if($rs){
					echo "MSG#ERR#Nomor Aju sudah digunakan, Silahkan Setting Nomor aju terlebih dahulu pada menu : Master Data >> Nomor Pengajuan#";
					die();
				}				
				$exec = $this->db->insert('t_bc40_hdr', $HEADER);					
				$this->db->where(array('NOMOR_AJU'=>$car,"KODE_TRADER"=>$KODE_TRADER));
				$this->db->delete('T_BC40_DOKASAL');
				$arrasal= $this->input->post('ASAL');
				$arrkeys = array_keys($arrasal);
				for($i=0;$i<count($arrasal[$arrkeys[0]]);$i++){			
					for($j=0;$j<count($arrkeys);$j++){
						$data[$arrkeys[$j]] = $arrasal[$arrkeys[$j]][$i];
					}		
					$data["KODE_TRADER"] = $KODE_TRADER;
					$data["NOMOR_AJU"] = $car;
					if($data["NOMOR_DAFTAR"]&&$data["TANGGAL_DAFTAR"]){
						$this->db->insert('T_BC40_DOKASAL',$data);
					}
				}
							
				if($exec){
					$func->main->set_car('BC40'); 
					$func->main->activity_log('ADD HEADER BC40','CAR='.$car);
					echo "MSG#OK#Proses header Berhasil#".$car."#".site_url()."/pemasukan/LoadHeader/bc40/".$car."#bc40#";
				}else{					
					echo "MSG#ERR#Proses data header Gagal#";
				}				
			}else if(strtolower($type)=="update"){
				foreach($this->input->post('HEADERDOK') as $a => $b){
					$HEADERDOK[$a] = $b;
				}
				$car = $HEADER["NOMOR_AJU"];
				$NODAF = $HEADERDOK["NOMOR_PENDAFTARAN"];
				$TGDAF = $HEADERDOK["TANGGAL_PENDAFTARAN"];	
				$NMDAF = $HEADERDOK["NAMA_PEJABAT_BC"];	
				$NIDAF = $HEADERDOK["NIP_PEJABAT_BC"];				
				$HEADERDOK["UPDATED_BY"]=$this->newsession->userdata('USER_ID');
				$HEADERDOK["UPDATED_TIME"]=date('Y-m-d H:i:s',time()-60*60*1);
				$HEADER['ID_TRADER'] = str_replace('.','',str_replace('-','',$HEADER['ID_TRADER']));
				$HEADER['ID_PENGIRIM'] = str_replace('.','',str_replace('-','',$HEADER['ID_PENGIRIM']));
				$this->db->where(array('NOMOR_AJU' => $HEADER["NOMOR_AJU"],'KODE_TRADER'=>$KODE_TRADER));
				if($this->input->post("STATUS_DOK")=="LENGKAP" && $NODAF && $TGDAF){
					$HEADERDOK["STATUS"] = '07';	
					$exec = $this->db->update('t_bc40_hdr', array_merge($HEADERDOK,$HEADER));
				}else{	
					$exec = $this->db->update('t_bc40_hdr', $HEADER);
				}	
				$this->db->where(array('NOMOR_AJU'=>$car,"KODE_TRADER"=>$KODE_TRADER));
				$this->db->delete('T_BC40_DOKASAL');
				$arrasal= $this->input->post('ASAL');
				$arrkeys = array_keys($arrasal);
				for($i=0;$i<count($arrasal[$arrkeys[0]]);$i++){			
					for($j=0;$j<count($arrkeys);$j++){
						$data[$arrkeys[$j]] = $arrasal[$arrkeys[$j]][$i];
					}		
					$data["KODE_TRADER"] = $KODE_TRADER;
					$data["NOMOR_AJU"] = $car;
					if($data["NOMOR_DAFTAR"]&&$data["TANGGAL_DAFTAR"]){
						$this->db->insert('T_BC40_DOKASAL',$data);
					}
				}			
				if($exec){
					$func->main->activity_log('EDIT HEADER BC40','CAR='.$car);
					if ($this->input->post('ALASAN')) {
						$alasan = $this->input->post('ALASAN');
						$func->main->revisi_log('REVISI DOKUMEN PABEAN BC40','EDIT HEADER BC40, CAR='.$CAR,$alasan);
					}
					echo "MSG#OK#Proses header Berhasil#".$car."#";
				}else{					
					echo "MSG#ERR#Proses data header Gagal#";
				}	
			}
		}
	}
	
}
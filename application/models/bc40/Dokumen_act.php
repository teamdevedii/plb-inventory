<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Dokumen_act extends CI_Model{
	function set_dokumen($type="", $isajax=""){	
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$kode_dokumen = $this->input->post("KODE_DOKUMEN");
		if($type=="save" || $type=="update"){
			foreach($this->input->post('DOKUMEN') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"]  = $kode_trader;
				$arrinsert["KODE_DOKUMEN"] = $kode_dokumen;
			}
			if($type=="save"){				
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS JUM FROM T_BC40_DOK WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU='".$aju."' AND KODE_DOKUMEN = '".$arrinsert["KODE_DOKUMEN"]."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;
				if($countKode > 0){
					echo "MSG#ERR#Simpan data Dokumen Gagal. Kode Dokumen Sudah Pernah digunakan.#";
				}else{
					$exec = $this->db->insert('t_bc40_dok', $arrinsert);
					if($exec){
						echo "MSG#OK#Simpan data Dokumen Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Dokumen Gagal#";
					}
				}
			}else{
				$kode_dok = $this->input->post('kode_dok');
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_DOKUMEN' => $kode_dok,'KODE_TRADER'=>$kode_trader));
				$exec = $this->db->update('t_bc40_dok', $arrinsert);
				if($exec){
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfdokumen') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$kode_dok = $arrchk[1];		
				$no_dok = $arrchk[2];			
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_DOKUMEN' => $kode_dok,'KODE_TRADER'=>$kode_trader,'NOMOR'=>$no_dok));
				$exec = $this->db->delete('t_bc40_dok');	
			}
			if($exec){
				echo "MSG#OK#Hapus data Dokumen Berhasil#".site_url()."/pemasukan/detil/dokumen/bc40/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Dokumen Gagal#del#";die();
			}
		}
	}
	
	 function get_dokumen($aju="", $kode_dok="", $nodok=""){		
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($aju && $kode_dok){
			$query = "SELECT KODE_DOKUMEN, NOMOR, TANGGAL 
						FROM t_bc40_dok 
					  WHERE NOMOR_AJU = '".$aju."' AND KODE_DOKUMEN = '".$kode_dok."' 
					  	AND NOMOR = '".$nodok."' AND KODE_TRADER = '".$kode_trader."'";	
			$hasil = $conn->main->get_result($query);			
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess' => $row,'kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
				}
			}
		}else{
			$data = array('act' => 'save','kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,'kode_dok' => $kode_dok));
		return $data;
	}
}
?>
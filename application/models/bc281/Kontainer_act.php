<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kontainer_act extends CI_Model{
	
	function set_kontainer($type="", $isajax=""){		
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($type=="save" || $type=="update"){	
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('KONTAINER') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"] = $kode_trader;
			}
			if($type=="save"){
				$arrinsert["NOMOR_AJU"] = $aju;
				$cont = "SELECT COUNT(*) AS JUM FROM T_BC23_CNT WHERE NOMOR_AJU='$aju' AND NOMOR = '".$arrinsert["NOMOR"]."'";
				$countKode = (int)$func->main->get_uraian($cont, "JUM");
				if($countKode > 0){
					echo "MSG#ERR#Simpan data Container Gagal<br><p style=\"margin-left:168px;\">Nomor Container Sudah Pernah digunakan</p>#";
				}else{				
					$exec = $this->db->insert('t_bc23_cnt', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD KONTAINER BC281','CAR='.$aju.', NOMOR='.$arrinsert["NOMOR"]);
						echo "MSG#OK#Simpan data Container Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Container Gagal#";
					}
				}
			}else{	
				$seri = $this->input->post('seri');					
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'NOMOR' => $seri));
				$exec = $this->db->update('t_bc23_cnt', $arrinsert);		
				if($exec){
					$func->main->activity_log('EDIT KONTAINER BC281','CAR='.$aju.', NOMOR='.$seri);
					echo "MSG#OK#Update data Barang Berhasil#edit#";
				}else{					
					echo "MSG#ERR#Update data Barang Gagal#edit#";
				}							
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkontainer') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$seri = $arrchk[1];				
				$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'NOMOR' => $seri));
				$exec = $this->db->delete('t_bc23_cnt');	
				$func->main->activity_log('DELETE KONTAINER BC281','CAR='.$aju.', NOMOR='.$seri);
			}
			if($exec){
				echo "MSG#OK#Hapus data kontainer Berhasil#".site_url()."/pemasukan/detil/kontainer/bc23/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data kontainer Gagal#del#";die();
			}
		}
	}
	
	function get_kontainer($aju="", $seri=0){
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		if($aju && $seri){
			$query = "Select * from t_bc23_cnt where nomor_aju = '$aju' AND NOMOR = '$seri'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update', 'sess' => $row,
								  'TIPE'=> $conn->main->get_mtabel('TIPE_CNT'),
								  'UKURAN'=> $conn->main->get_mtabel('UKURAN_CNT'));
				}
			}
		}else{
			$data = array('act' => 'save',
						  'TIPE'=> $conn->main->get_mtabel('TIPE_CNT'),
						  'UKURAN'=> $conn->main->get_mtabel('UKURAN_CNT'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju, 'seri' => $seri));
		return $data;
	}
}
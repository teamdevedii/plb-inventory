<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Header_act extends CI_Model {
	function get_header($aju = "") {
		$data = array();
		$func = get_instance();
		$func->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$user_id =  $this->newsession->userdata('USER_ID');
		if(!$aju){
			$query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                      	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
						D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                      	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM', KODE_API, NOMOR_API
                      FROM M_TRADER A 
                      	INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                        LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER 
                        LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
                        WHERE A.KODE_TRADER = '".$kode_trader."'
                        AND C.USER_ID = ".$user_id." LIMIT 0,1";
            $hasil = $func->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
			
			$this->db->where('KODE_TRADER', $kode_trader);
            $this->db->select_max('TANGGAL_SKEP');
            $resultSkep = $this->db->get('m_trader_skep')->row();
            $maxTglSkep = $resultSkep->TANGGAL_SKEP;
            $SQL = "SELECT KODE_SKEP, NOMOR_SKEP, TANGGAL_SKEP, KODE_TRADER, SERI 
					FROM m_trader_skep WHERE KODE_TRADER = '".$kode_trader."' AND TANGGAL_SKEP = '" . $maxTglSkep . "'";
            $hasil = $func->main->get_result($SQL);
            if ($hasil) {
                foreach ($SQL->result_array() as $row) {
                    $SKEP = $row;
                }
            }
			$data = array('act' => 'save', 'aju' => $aju);
		}else{
			$query = "SELECT A.NOMOR_AJU, A.KODE_KPBC, A.TUJUAN, A.JENIS_BARANG, A.TUJUAN_KIRIM, A.NAMA_PEMASOK, A.ALAMAT_PEMASOK, 
						A.NEGARA_PEMASOK, A.KODE_TRADER, A.KODE_ID_TRADER, A.ID_TRADER, A.NAMA_TRADER, A.ALAMAT_TRADER, A.STATUS_TRADER, 
						A.KODE_TPB, A.REGISTRASI, A.KODE_API, A.NOMOR_API, A.MODA, A.NAMA_ANGKUT, A.NOMOR_ANGKUT, A.BENDERA, 
						A.PELABUHAN_BONGKAR, A.PELABUHAN_MUAT, A.PELABUHAN_TRANSIT, A.NOMOR_PENDAFTARAN, A.TANGGAL_PENDAFTARAN,
						A.NOMOR_DOK_PABEAN, A.TANGGAL_DOK_PABEAN, A.NOMOR_DOK_INTERNAL, A.TANGGAL_DOK_INTERNAL, A.KODE_KPBC_BONGKAR, 
						A.KODE_KPBC_AWAS, A.KODE_PENUTUP, A.NOMOR_PENUTUP, A.TANGGAL_PENUTUP, A.NOMOR_POS, A.SUB_POS, A.KODE_TIMBUN, 
						A.KODE_VALUTA, A.NDPBM, A.NILAI_INVOICE, A.FREIGHT, A.TAMBAHAN, A.DISKON, A.KODE_ASURANSI, A.ASURANSI, A.KODE_HARGA, 
						A.FOB, A.CIF, A.CIFRP, A.BRUTO, A.NETTO, A.JUMLAH_CNT, A.JUMLAH_DTL, A.TANGGAL_TTD, A.KOTA_TTD, A.NAMA_TTD,
						A.STATUS, A.KPBC_UBAH, A.NO_SURAT_UBAH, A.TGL_SURAT_UBAH, A.NM_PEJABAT_UBAH, A.JABATAN_UBAH, A.NIP_UBAH,
						f_kpbc(A.KODE_KPBC) URAIAN_KPBC, f_kpbc(A.KODE_KPBC_BONGKAR) URAIAN_KPBC_BONGKAR, 
						f_kpbc(A.KODE_KPBC_AWAS) URAIAN_KPBC_AWAS,f_kpbc(A.NEGARA_PEMASOK) AS URAIAN_NEGARA_PEMASOK, 
						f_kpbc(A.BENDERA) URAIAN_BENDERA, f_kpbc(A.PELABUHAN_MUAT) URAIAN_MUAT,
						f_kpbc(A.PELABUHAN_TRANSIT) URAIAN_TRANSIT, f_kpbc(A.PELABUHAN_BONGKAR) URAIAN_BONGKAR,
						f_valuta(A.KODE_VALUTA) URAIAN_VALUTA,f_status_bc23(A.NOMOR_AJU,1) STATUS_DOK, 
						IFNULL(f_jumnetto_bc23(A.NOMOR_AJU),0) AS 'JUM_NETTO',  f_pelab(A.PELABUHAN_MUAT) AS URAIAN_MUAT, 
						f_pelab(A.PELABUHAN_TRANSIT) AS URAIAN_TRANSIT, f_pelab(A.PELABUHAN_BONGKAR) AS URAIAN_BONGKAR, 
						f_timbun(A.KODE_TIMBUN,A.KODE_KPBC_BONGKAR) AS URAIAN_TIMBUN, f_negara(A.BENDERA) AS URAIAN_BENDERA, 
						f_negara(A.NEGARA_PEMASOK) AS URAIAN_NEGARA_PEMASOK,
					(SELECT KODE_FASILITAS FROM T_BC23_PGT WHERE KODE_BEBAN='8' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS KODE_FASILITAS,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='1' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_BM,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='1' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_BM_DIT,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='2' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPN,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='2' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPN_DIT,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='3' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPNBM,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='3' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPNBM_DIT,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='4' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPH,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='4' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PPH_DIT,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='6' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_CUKAI,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='6' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_CUKAI_DIT,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='8' AND KODE_FASILITAS='0' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PNBP,
					(SELECT NILAI_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='8' AND KODE_FASILITAS='2' AND nomor_aju='".$aju."' AND KODE_TRADER = '".$kode_trader."') AS PGT_PNBP_DIT,
						C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP'			
					FROM t_bc23_hdr A 
						LEFT JOIN T_BC23_PGT B ON B.NOMOR_AJU = A.NOMOR_AJU AND A.KODE_TRADER = B.KODE_TRADER
						LEFT JOIN T_USER C ON A.CREATED_BY = C.USER_ID AND C.KODE_TRADER = A.KODE_TRADER
					WHERE A.nomor_aju = '".$aju."' AND A.KODE_TRADER = '".$kode_trader."'";
			$hasil = $func->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
			
			if (!$dataarray["KODE_ID_TRADER"]) {
                $query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID,  A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                        	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
							D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                        	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
                          FROM M_TRADER A 
                        	INNER JOIN T_USER C ON A.KODE_TRADER = C.KODE_TRADER 
                        	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER = B.KODE_TRADER 
                        	LEFT JOIN M_SETTING D ON A.KODE_TRADER = D.KODE_TRADER
                          WHERE A.KODE_TRADER = '".$kode_trader."'
                          AND C.USER_ID = ".$user_id." LIMIT 0,1";
                $hasil = $conn->main->get_result($query);
                if ($hasil) {
                    foreach ($query->result_array() as $row) {
                        $dt = $row;
                    }
                }
                $dataarray = array_merge($dataarray, $dt);
                $this->db->where('KODE_TRADER', $kode_trader);
                $this->db->select_max('TANGGAL_SKEP');
                $resultSkep = $this->db->get('m_trader_skep')->row();
                $maxTglSkep = $resultSkep->TANGGAL_SKEP;
                $SQL = "SELECT KODE_SKEP, NOMOR_SKEP, TANGGAL_SKEP, KODE_TRADER, SERI 
                        FROM m_trader_skep WHERE KODE_TRADER = '".$kode_trader."'
                        AND TANGGAL_SKEP='".$maxTglSkep."'";
                $hasil = $conn->main->get_result($SQL);
                if ($hasil) {
                    foreach ($SQL->result_array() as $row) {
                        $SKEP = $row;
                    }
                }
            }
            $data = array('act' => 'update');
		}
		$kode_pnbp = "SELECT KODE , CONCAT(KODE,' - ',URAIAN) AS URAIAN FROM M_TABEL WHERE JENIS='STATUS_BAYAR' AND KODE IN('0','2') ORDER BY KODE";
		$data = array_merge($data, array('aju' => $aju,
            				'sess' => array_merge(array("REGISTRASI" => $SKEP["NOMOR_SKEP"]), $dataarray),
            				'jenis_barang' => $func->main->get_mtabel('JENIS_BARANG'),
							'tujuan_kirim' => $func->main->get_mtabel('TUJUAN_PENGIRIMAN'),
							'tujuan' => $func->main->get_mtabel('TUJUAN'),
							'kode_id_trader' => $func->main->get_mtabel('KODE_ID', 1, FALSE),
							'jenis_tpb' => $func->main->get_mtabel('JENIS_TPB'),
							'status' => $func->main->get_mtabel('JENIS_EKSPORTIR'),
							'kode_api' => $func->main->get_mtabel('API'),
							'cara_angkut' => $func->main->get_mtabel('MODA'),
							'kode_pnbp' => $func->main->get_combobox($kode_pnbp, "KODE", "URAIAN", TRUE),
            				'kode_penutup' => $func->main->get_mtabel('KODE_PENUTUP')));
        return $data;
	}
	
	function set_header($type = "", $isajax = "") {
    	$func = get_instance();
        $func->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$user_id =  $this->newsession->userdata('USER_ID');
		if(strtolower($type) == "save" || strtolower($type) == "update"){
			foreach ($this->input->post('HEADER') as $a => $b) {
                $HEADER[$a] = $b;
				$HEADER["KODE_TRADER"] = $kode_trader;
            }
            foreach ($this->input->post('BEBAN') as $c => $d) {
                $BEBAN[$c] = $d;
				$BEBAN["KODE_TRADER"] = $kode_trader;
            }
            $HEADER['ID_TRADER'] = str_replace('.', '', str_replace('-', '', $HEADER['ID_TRADER']));
			if(strtolower($type) == "save"){
				$func->main->get_car($car, 'BC23');
                #insert table header
				$HEADER["NOMOR_AJU"] = $car;
                $HEADER["CREATED_BY"] = $user_id;
                $HEADER["CREATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $exec = $this->db->insert('t_bc23_hdr', $HEADER);
				
				#jika ada kode beban maka insert ke table t_bc23_pgt
				if($BEBAN["KODE_FASILITAS"]){
					$BEBAN["NOMOR_AJU"] = $car;
                	$BEBAN["NILAI_BEBAN"] = str_replace(",", "", $BEBAN["NILAI_BEBAN"]);
					$exec = $this->db->insert('t_bc23_pgt', $BEBAN);
				}
				
				#panggil SP(Stored Procs)
				$this->db->query("call hitungPungutanBC23('".$car."')");
				
				if($exec){
                    $func->main->set_car('BC23');
                    $func->main->activity_log('ADD HEADER BC281', 'CAR = '.$car);
                    echo "MSG#OK#Proses data header berhasil disimpan.#".$car."#".site_url()."/pemasukan/LoadHeader/bc281/".$car."#bc281#";
                }else{
                    echo "MSG#ERR#Proses data header header gagal disimpan.#";
                }
			}else if (strtolower($type) == "update") {
				unset($HEADERDOK["NOMOR_PENDAFTARAN_EDIT"],$HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"]);
				$CAR = $HEADER["NOMOR_AJU"];
                $HEADER["CIFRP"] = str_replace(",", "", $HEADER["CIFRP"]);
                $HEADER["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["UPDATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $BEBAN["NILAI_BEBAN"] = str_replace(",", "", $BEBAN["NILAI_BEBAN"]);
				
				$SQL = "SELECT KODE_BEBAN FROM T_BC23_PGT WHERE KODE_BEBAN='" . $BEBAN["KODE_BEBAN"] . "' 
						AND NOMOR_AJU='" . $CAR . "' AND KODE_TRADER = '".$kode_trader."'";
                $data = $this->db->query($SQL);
                if ($data->num_rows() > 0) {
					if($BEBAN["KODE_FASILITAS"]){
						$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $CAR, 'KODE_BEBAN' => $BEBAN["KODE_BEBAN"]));
						$exec = $this->db->update('T_BC23_PGT', $BEBAN);
					}
                } else {
					if($BEBAN["KODE_FASILITAS"]){
						$BEBAN["NOMOR_AJU"] = $CAR;
						$exec = $this->db->insert('T_BC23_PGT', $BEBAN);
					}
                }
				$TGLREALISASI = $func->main->get_uraian("SELECT TANGGAL_REALISASI FROM t_bc23_hdr WHERE NOMOR_AJU = '".$CAR."' 
								AND KODE_TRADER = '".$kode_trader."'", "TANGGAL_REALISASI");
				$this->db->where(array('NOMOR_AJU' => $CAR, 'KODE_TRADER' => $this->newsession->userdata("KODE_TRADER")));
                if ($this->input->post("STATUS_DOK") == "LENGKAP" && $TGLREALISASI == "" || $TGLREALISASI == "null") {
                    $HEADERDOK["STATUS"] = '07';
                    $exec = $this->db->update('T_BC23_HDR', array_merge($HEADERDOK, $HEADER));
                } else {
                    $exec = $this->db->update('T_BC23_HDR', $HEADER);
                }
				$sql = "call hitungPungutanBC23('" . $CAR . "')";
                $this->db->query($sql);
                if ($exec) {
                    $func->main->activity_log('EDIT HEADER BC23', 'CAR=' . $CAR);
                    echo "MSG#OK#Proses header Berhasil#" . $CAR . "#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
			}
		}
	}
}
?>
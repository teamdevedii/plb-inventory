<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Barang_act extends CI_Model {
	
	function get_barang($aju = "", $seri = "", $kdbarang = "") {
		$data = array();
        $func = get_instance();
        $func->load->model("main", "main", true);
        $kode_trader = $this->newsession->userdata('KODE_TRADER');
		if ($aju && $seri) {
            $query = "SELECT a.NOMOR_AJU, a.SERI, a.KODE_TRADER, a.JENIS_BARANG, a.TUJUAN_KIRIM, a.KODE_HS, a.SERI_HS, a.URAIAN_BARANG, 
						a.KODE_BARANG, a.MERK, a.TIPE, a.SPF, a.PENGGUNAAN, a.KODE_KEMASAN, a.JUMLAH_KEMASAN, a.KODE_FAS, a.NEGARA_ASAL,
						a.INVOICE, a.CIF, a.CIFRP, a.KODE_SATUAN, a.JUMLAH_SATUAN, a.NETTO, a.JUMLAH_BM, a.JUMLAH_CUKAI,
						f_kemas(a.KODE_KEMASAN) KODE_KEMASANUR,b.KODE_FAS_BM, b.FAS_BM, b.KODE_FAS_CUKAI, b.FAS_CUKAI, 
                		b.KODE_FAS_PPN, b.FAS_PPN, b.KODE_FAS_PPH, f_negara(a.NEGARA_ASAL) NEGARA_ASALUR, f_satuan(a.KODE_SATUAN) KODE_SATUANUR,
                		b.FAS_PPH, b.KODE_FAS_PPNBM, b.FAS_PPNBM, c.KODE_TARIF_BM, c.KODE_SATUAN_BM, c.TARIF_BM, c.KODE_TARIF_CUKAI,
                		c.KODE_SATUAN_CUKAI, c.TARIF_CUKAI, c.KODE_CUKAI, c.TARIF_PPN, c.TARIF_PPNBM, c.SERI_HS, d.FREIGHT, d.ASURANSI, 
						d.NOMOR_API, Z.KODE_SATUAN AS KD_SAT_BESAR, Z.KODE_SATUAN_TERKECIL AS KD_SAT_KECIL,a.SERI_HS SERI_HS_BARANG
                	  FROM t_bc23_dtl a 
                		LEFT JOIN t_bc23_fas b ON b.NOMOR_AJU=a.NOMOR_AJU AND a.SERI=b.SERI AND a.KODE_TRADER = b.KODE_TRADER
                		LEFT JOIN t_bc23_trf c ON c.NOMOR_AJU=a.NOMOR_AJU AND a.SERI_HS=c.SERI_HS AND a.KODE_HS=c.KODE_HS
                		LEFT JOIN t_bc23_hdr d ON d.NOMOR_AJU=a.NOMOR_AJU AND a.KODE_TRADER = d.KODE_TRADER
                		LEFT JOIN M_TRADER_BARANG Z ON Z.KODE_BARANG=A.KODE_BARANG AND Z.JNS_BARANG=A.JENIS_BARANG 
						AND a.KODE_TRADER = Z.KODE_TRADER AND Z.KODE_TRADER='".$kode_trader."'
                	  WHERE a.NOMOR_AJU = '".$aju."' AND a.SERI = '".$seri."' AND a.KODE_TRADER = '".$kode_trader."'";

            $hasil = $func->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $data = array('act' => 'update', 'sess' => $row, 'aju' => $aju, 'seri' => $seri);
                }
            }
        }else{
			$query = "SELECT NOMOR_API, 0 AS 'SERI_HS', 0 AS 'JUMLAH_KEMASAN', 0 AS 'NETTO', 0 AS 'INVOICE', 0 AS 'DISKON', 
						0 AS 'JUMLAH_SATUAN', 0 AS 'FOB', 0 AS 'CIF', 0 AS 'CIFRP', 0 AS 'TARIF_BM', 0 AS 'TARIF_PPN', 
						0 AS 'TARIF_PPNBM', 0 AS 'FAS_PPN', 0 AS 'FAS_PPNBM',	0 AS 'FAS_PPH', 0 AS 'FAS_BM', 
						0 AS 'KODE_SATUAN_BM', 0 AS 'JUMLAH_BM'
                      FROM t_bc23_hdr WHERE NOMOR_AJU='".$aju."' AND KODE_TRADER = '".$kode_trader."'";
            $hasil = $func->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'save', 'aju' => $aju, 'sess' => $dataarray);
		}
		$data = array_merge($data, array('jenis_komoditi' => $func->main->get_mtabel('JENIS_BARANG'),
            				'tujuan_kirim' => $func->main->get_mtabel('TUJUAN_PENGIRIMAN'),
							'penggunaan' => $func->main->get_mtabel('PENGGUNAAN_BARANG'),
							'komoditi_cukai' => $func->main->get_mtabel('KOMODITI_CUKAI'),
							'jenis_tarif' => $func->main->get_mtabel('JENIS_TARIF_BM', 1, false, "AND KODE IN ('1','2')"),
							'kode_bm' => $func->main->get_mtabel('STATUS_BAYAR'),
							'kode_ppn' => $func->main->get_mtabel('STATUS_BAYAR'),
							'kode_ppnbm' => $func->main->get_mtabel('STATUS_BAYAR'),
							'kode_pph' => $func->main->get_mtabel('STATUS_BAYAR'),
							'kode_cukai' => $func->main->get_mtabel('STATUS_BAYAR')));
        return $data;
	}
	
	function set_barang($type = "", $isajax = "") {
		$func = get_instance();
        $func->load->model("main", "main", true);
        $aju = $this->input->post('NOMOR_AJU');
        $kode_trader = $this->newsession->userdata("KODE_TRADER");
		if ($type == "save" || $type == "update") {
            foreach ($this->input->post('BARANG') as $a => $b) {
                $BARANG[$a] = $b;
                $BARANG["KODE_TRADER"] = $kode_trader;
            }
            foreach ($this->input->post('FASILITAS') as $a1 => $b1) {
                $FASILITAS[$a1] = $b1;
                $FASILITAS["KODE_TRADER"] = $kode_trader;
            }
            foreach ($this->input->post('TARIF') as $a2 => $b2) {
                $TARIF[$a2] = $b2;
                $TARIF["KODE_TRADER"] = $kode_trader;
            }
            if ($type == "save") {
                unset($BARANG["KODE_BARANG_HIDE"]);
                $status = "04";
                $seri = (int) $func->main->get_uraian("SELECT MAX(SERI) AS MAXSERI FROM t_bc23_dtl WHERE NOMOR_AJU = '" . $aju . "'", "MAXSERI") + 1;
                $BARANG["NOMOR_AJU"] = $aju;
                $BARANG["SERI"] = $seri;
                $BARANG["STATUS"] = $status;
                $BARANG["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);
				
                $FASILITAS["NOMOR_AJU"] = $aju;
                $FASILITAS["SERI"] = $seri;

                $TARIF["NOMOR_AJU"] = $aju;
                $TARIF["SERI_HS"] = $BARANG["SERI_HS"];
                $TARIF["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);

                $this->db->insert('t_bc23_dtl', $BARANG);
                $exec = $this->db->insert('t_bc23_fas', $FASILITAS);

                $query = "SELECT KODE_HS FROM t_bc23_trf WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU='" . $aju . "' 
							AND KODE_HS='" . $TARIF["KODE_HS"] . "' AND SERI_HS='" . $TARIF["SERI_HS"] . "'";
                $data = $this->db->query($query);
                if ($data->num_rows() > 0) {
                    $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'KODE_HS' => $TARIF["KODE_HS"], 
											'SERI_HS' => $TARIF["SERI_HS"]));
                    $exec = $this->db->update('t_bc23_trf', $TARIF);
                } else {
                    $exec = $this->db->insert('t_bc23_trf', $TARIF);
                }
                $this->db->query("call hitungPungutanBC23('" . $aju . "')");
                if ($exec) {
                    $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc23('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                    $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju));
                    $this->db->update('t_bc23_hdr', array("NETTO" => $netto));
                    $func->main->cekkodehs($BARANG["KODE_HS"], $BARANG["SERI_HS"], $this->input->post('TARIF'));
                    $func->main->activity_log('ADD DETIL BC281', 'CAR=' . $aju . ', SERI=' . $seri);
                    echo "MSG#OK#Simpan data Barang Berhasil.#edit#" . site_url() . "/pemasukan/LoadHeader/bc281/" . $aju . "#";
                } else {
                    echo "MSG#ERR#Simpan data Barang Gagal.#";
                }
            }else{
				$seri = $this->input->post('seri');
                $trf_serihs = $this->input->post('trf_serihs');
                $BARANG["STATUS"] = '04';
                $BARANG["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);
                $BARANG["NOMOR_AJU"] = $aju;
				
                $TARIF["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);
                $TARIF["SERI_HS"] = $BARANG["SERI_HS"];
                $TARIF["NOMOR_AJU"] = $aju;
				unset($BARANG["KODE_BARANG_HIDE"]);
                if($this->input->post('PERBAIKAN')){
                    $ARRADD = array("AJU" => $aju, "SERI" => $seri);
                    $this->set_perbaikan("update", array_merge($this->input->post('PERBAIKAN'), $ARRADD), $BARANG);
                } else {
					$this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                    $exec = $this->db->update('t_bc23_dtl', $BARANG);
                    $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                    $exec = $this->db->update('t_bc23_fas', $FASILITAS);

                    $query = "SELECT KODE_HS FROM t_bc23_trf WHERE KODE_TRADER = '".$kode_trader."' AND NOMOR_AJU = '".$aju."' 
								AND KODE_HS='" . $TARIF["KODE_HS"] . "' AND SERI_HS='" . $TARIF["SERI_HS"] . "'";
                    $data = $this->db->query($query);
                    if ($data->num_rows() > 0) {
                        $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'KODE_HS' => $TARIF["KODE_HS"], 
												'SERI_HS' => $TARIF["SERI_HS"]));
                        $exec = $this->db->update('t_bc23_trf', $TARIF);
                    } else {
                        $exec = $this->db->insert('t_bc23_trf', $TARIF);
                    }
                    $sql = "call hitungPungutanBC23('" . $aju . "')";
                    $this->db->query($sql);
                    if ($exec) {
                        $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc23('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                        $this->db->where(array('NOMOR_AJU' => $aju));
                        $this->db->update('t_bc23_hdr', array("NETTO" => $netto));
                        $func->main->cekkodehs($BARANG["KODE_HS"], $BARANG["SERI_HS"], $this->input->post('TARIF'));
                        $func->main->activity_log('EDIT DETIL BC281', 'CAR=' . $aju . ', SERI=' . $seri);
						echo "MSG#OK#Update data Barang Berhasil#edit#" . site_url() . "/pemasukan/LoadHeader/bc281/" . $aju . "#";
					}else {
                        echo "MSG#ERR#Update data Barang Gagal#edit#";
                    }
				}
			}
		}else if ($type == "delete") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $seri_hs = $arrchk[2];
                $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                $exec = $this->db->delete('t_bc23_dtl');
				
                $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI' => $seri));
                $exec = $this->db->delete('t_bc23_fas');
				
                $this->db->where(array('KODE_TRADER'=>$kode_trader,'NOMOR_AJU' => $aju, 'SERI_HS' => $seri_hs));
                $exec = $this->db->delete('t_bc23_trf');
				
                $func->main->activity_log('DELETE DETIL BC281', 'CAR=' . $aju . ', SERI=' . $seri);
            }
            $sql = "call hitungPungutanBC23('" . $aju . "')";
            $this->db->query($sql);
            if ($exec) {
                $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc23('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                $this->db->where(array('NOMOR_AJU' => $aju));
                $this->db->update('t_bc23_hdr', array("NETTO" => $netto));
                echo "MSG#OK#Hapus data Barang Berhasil#" . site_url() . "/pemasukan/detil/barang/bc281/" . $aju . "#" . site_url() . "/pemasukan/LoadHeader/bc281/" . $aju . "#";
                die();
            } else {
                echo "MSG#ERR#Hapus data Barang Gagal#";
                die();
            }
        }
	}
	
	function set_perbaikan($TYPE = "", $DATA = "", $BARANG = "") {
        $func = get_instance();
        $func->load->model("main", "main", true);
        if ($DATA) {
            foreach ($DATA as $x => $y) {
                $PERBAIKAN[$x] = $y;
            }
            $PERBAIKAN["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
            $PERBAIKAN["UPDATED_TIME"] = date("Y-m-d H:i:s");
            $AJU = $PERBAIKAN["AJU"];
            $SERI = $PERBAIKAN["SERI"];
            $BARANG["SERI"] = $SERI;
            unset($PERBAIKAN["AJU"]);
            unset($PERBAIKAN["SERI"]);
            $this->db->where(array('NOMOR_AJU' => $AJU));
            $this->db->update('t_bc23_hdr', $PERBAIKAN);

            $query = "SELECT ID FROM t_bc23_perubahan WHERE NOMOR_AJU='" . $AJU . "' AND SERI='" . $SERI . "'";
            $data = $this->db->query($query);
            if ($data->num_rows() > 0) {
                $this->db->where(array('NOMOR_AJU' => $AJU, 'SERI' => $SERI));
                $this->db->update('t_bc23_perubahan', $BARANG);
            } else {
                $this->db->insert('t_bc23_perubahan', $BARANG);
            }

            $this->db->where(array('NOMOR_AJU' => $AJU, 'SERI' => $SERI));
            $exec = $this->db->update('t_bc23_dtl', array("PERUBAHAN_FLAG" => "Y"));
            if ($exec) {
                $func->main->activity_log('EDIT PERBAIKAN DETIL BC281', 'CAR=' . $AJU . ', SERI=' . $SERI);
                echo "MSG#OK#Simpan data Barang Berhasil#edit#";
            } else {
                echo "MSG#ERR#Simpan data Barang Gagal#";
            }
        }
    }
}
?>
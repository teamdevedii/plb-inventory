<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Class Menu_act untuk generate menu setiap user
*/
class Menu_act extends CI_Model{

  function createMenu() {
      $SQL = "SELECT A.KODE_MENU, A.KODE_MENU_PARENT, A.JUDUL, A.SUB_JUDUL, 
              A.URL, A.URL_TIPE, A.INDEX, A.TAMPIL_FLAG, A.CONTROLLER, B.AKSES  
              FROM M_MENU A, M_MENU_USER B
              WHERE A.KODE_MENU = B.KODE_MENU AND A.TAMPIL_FLAG = 'Y'
              AND B.USER_ID = '" . $this->newsession->userdata("USER_ID") . "'
              ORDER BY A.KODE_MENU_PARENT, A.INDEX ASC";
      $rs = $this->db->query($SQL);
      if ($rs->num_rows() > 0) {
          foreach ($rs->result_array() as $row) {
              $data[$row["KODE_MENU_PARENT"]][] = array("KODE_MENU" => $row["KODE_MENU"],
                  "KODE_MENU_PARENT" => $row["KODE_MENU_PARENT"],
                  "JUDUL" => $row["JUDUL"],
                  "SUB_JUDUL" => $row["SUB_JUDUL"],
                  "URL" => $row["URL"],
                  "URL_TIPE" => $row["URL_TIPE"],
                  "INDEX" => $row["INDEX"],
                  "CONTROLLER" => $row["CONTROLLER"]
              );
          }
          $uri = $this->uri->segment(1);
          $addClass = "";
          if ($uri == "" || $uri == "home") {
            $addClass = " class='active' ";
          }
          $menu = '<li '.$addClass.'><a href="'.site_url().'/home">Home</a></li>';
          $getmenu = $this->drawMenu($data);
          $menu = $menu.$getmenu;
      }
      return $menu;
  }

  function drawMenu($data, $parent = 1, $inc = 0) {
    error_reporting(E_ALL ^ E_NOTICE);
    static $i = 1;
    if ($data[$parent]) {
      if ($parent != 1) {
          $addClass = ' class="dropdown" ';
          if ($this->uri->segment(1) == $data[1][$inc]["CONTROLLER"]) {
            $addClass = ' class="active" ';
          }
          $html .= '<li '.$addClass.'>';
          $html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$data[1][$inc]["JUDUL"].' <b class="caret"></b></a>';
          $html .= '<ul class="dropdown-menu">';
      }
      $i++;
      for ($c = 0; $c < count($data[$parent]); $c++) {
          $child = $this->drawMenu($data, $data[$parent][$c]["KODE_MENU"], $inc++);
          if ($parent != 1) {
              $html .= '<li>';
              if ($data[$parent][$c]["URL_TIPE"] == 'javascript') {
                  $html .= '<a href="javascript:void(0)" onClick="' . $data[$parent][$c]["URL"] . '">' . $data[$parent][$c]["JUDUL"] . '</a>';
              } elseif ($data[$parent][$c]["URL_TIPE"] == 'base_url') {
                  $html .= '<a href="' . base_url() . $data[$parent][$c]["URL"] . '">' . $data[$parent][$c]["JUDUL"] . '</a>';
              } else {
                  $html .= '<a href="' . site_url() . '/' . $data[$parent][$c]["URL"] . '">' . $data[$parent][$c]["JUDUL"] . '</a>';
              }
          }
          if ($child) {
              $i--;
              $html .= $child;
          }
          if ($parent == 1) {
              $html .= '</ul>';
          }
          $html .= '</li>';
      }
      return $html;
    } else {
      $html = "";
      return false;
    }
  }
  	function akses($kodemenu=""){		
		$func = get_instance();
		$func->load->model("main");
		$sql = "SELECT KODE_ROLE,KODE_MENU,USER_ID,AKSES FROM M_MENU_USER WHERE USER_ID='".$this->newsession->userdata('USER_ID')."'";
		if($func->main->get_result($sql)){
			foreach ($sql->result_array() as $row){
				$akses[$row['KODE_MENU']] = $row['AKSES'];
			}
			return $akses[$kodemenu];
		}
		return array();
	}

}
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Barang_act extends CI_Model{
	
	function set_perbaikan($TYPE="",$DATA="",$BARANG=""){		
		$func = get_instance();
		$func->load->model("main","main", true);
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');
		if($DATA){
			foreach($DATA as $x => $y){
				$PERBAIKAN[$x] = $y;
			}	
			$PERBAIKAN["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
			$PERBAIKAN["UPDATED_TIME"] = date("Y-m-d H:i:s");	
			$PERBAIKAN["KODE_TRADER"] = $KODETRADER;
			
			$AJU = $PERBAIKAN["AJU"];
			$SERI = $PERBAIKAN["SERIAL"];	
			
			$BARANG["SERIAL"] = $SERI;
			unset($PERBAIKAN["AJU"]);
			unset($PERBAIKAN["SERIAL"]);
			
			$this->db->where(array('NOMOR_AJU' => $AJU));
			$this->db->update('t_bc24_hdr', $PERBAIKAN);
			
			$query = "SELECT ID FROM t_bc24_perubahan WHERE NOMOR_AJU='".$AJU."' AND SERIAL='".$SERI."' AND KODE_TRADER = '".$KODETRADER."'";
			$data = $this->db->query($query);
			if($data->num_rows() > 0){
				$this->db->where(array('NOMOR_AJU' => $AJU, 'SERIAL' => $SERI));
				$this->db->update('t_bc24_perubahan', $BARANG);
			}else{
				$this->db->insert('t_bc24_perubahan', $BARANG);			
			}
			
			$this->db->where(array('NOMOR_AJU' => $AJU, 'SERIAL' => $SERI));
			$exec = $this->db->update('t_bc24_dtl', array("PERUBAHAN_FLAG"=>"Y"));
			if($exec){
				$func->main->activity_log('EDIT PERBAIKAN DETIL BC24','CAR='.$AJU.', SERIAL='.$SERI);
				echo "MSG#OK#Simpan data Barang Berhasil#edit#";
			}else{					
				echo "MSG#ERR#Simpan data Barang Gagal#";
			}		
		}		
	}	
	
	function set_barang($type="", $isajax=""){
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		if($type=="save" || $type=="update"){
			foreach($this->input->post('BARANG') as $a => $b){
				$BARANG[$a] = $b;
			}
			foreach($this->input->post('FASILITAS') as $a1 => $b1){
				$FASILITAS[$a1] = $b1;				
			}			
			foreach($this->input->post('TARIF') as $a2 => $b2){
				$TARIF[$a2] = $b2;
			}
			$BARANG["KODE_TRADER"] = $KODE_TRADER;
			
			$FASILITAS["KODE_TRADER"] = $KODE_TRADER;
			
			$TARIF["KODE_TRADER"] = $KODE_TRADER;
			
			if($type=="save"){				
				$status = "04";				
				$seri = (int)$func->main->get_uraian("SELECT MAX(SERIAL) AS MAXSERI FROM t_bc24_dtl WHERE NOMOR_AJU = '".$aju."' AND KODE_TRADER = '".$KODE_TRADER."'", "MAXSERI")+1;
				$BARANG["NOMOR_AJU"] = $aju;
				$BARANG["SERIAL"] = $seri;
				$BARANG["STATUS"] = $status;				
				$BARANG["NOHS"] = str_replace(".","",$BARANG["NOHS"]);
				
				$FASILITAS["NOMOR_AJU"] = $aju;
				$FASILITAS["SERIAL"] = $seri;
				
				$TARIF["NOMOR_AJU"] = $aju;
				$TARIF["SERITRP"] = $BARANG["SERITRP"];
				$TARIF["NOHS"] = str_replace(".","",$BARANG["NOHS"]);
				
				$exec = $this->db->insert('t_bc24_dtl', $BARANG);
				
				$query = "SELECT NOHS FROM t_bc24_trf WHERE NOHS='".$TARIF["NOHS"]."'
						  AND SERITRP='".$TARIF["SERITRP"]."' AND NOMOR_AJU='".$aju."' 
						  AND KODE_TRADER = '".$KODE_TRADER."'";
				$data = $this->db->query($query);	
				if($data->num_rows() > 0){		
					$this->db->where(array('NOMOR_AJU' => $aju, 'NOHS' => $TARIF["NOHS"], 'SERITRP' => $TARIF["SERITRP"], "KODE_TRADER"=>$KODE_TRADER));
					$exec = $this->db->update('t_bc24_trf', $TARIF);					
				}else{
					$exec = $this->db->insert('t_bc24_trf', $TARIF);		
				}
				#$sql="call hitungPungutanBC24('".$aju."')";		
				if($exec){
					$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc24('".$aju."'),0) AS JUM FROM DUAL", "JUM");     
					$this->db->where(array('NOMOR_AJU' => $aju, "KODE_TRADER"=>$KODE_TRADER));
					$this->db->update('t_bc24_hdr', array("NETTO" => $netto));
					$func->main->activity_log('ADD DETIL BC24','CAR='.$aju.', SERIAL='.$seri);
					echo "MSG#OK#Simpan data Barang Berhasil#edit#".site_url()."/pemasukan/LoadHeader/bc24/".$aju."#";
				}else{					
					echo "MSG#ERR#Simpan data Barang Gagal#";
				}					
			}else{			
				$seri = $this->input->post('seri');
				$trf_serihs = $this->input->post('trf_serihs');
				$BARANG["STATUS"] = '04';
				$BARANG["NOHS"] = str_replace(".","",$BARANG["NOHS"]);
				$TARIF["NOHS"] = str_replace(".","",$BARANG["NOHS"]);
				$TARIF["SERITRP"] = $BARANG["SERITRP"];			
				$TARIF["NOMOR_AJU"] = $aju;	
				$BARANG["NOMOR_AJU"] = $aju;		
				
				if($this->input->post('PERBAIKAN')){
					$ARRADD = array("AJU"=>$aju,"SERIAL"=>$seri);
					$this->set_perbaikan("update",array_merge($this->input->post('PERBAIKAN'),$ARRADD), $BARANG);	
				}
				else{
					$this->db->where(array('NOMOR_AJU' => $aju, 'SERIAL' => $seri, 'KODE_TRADER'=>$KODE_TRADER));
					$exec = $this->db->update('t_bc24_dtl', $BARANG);					
				
					$query = "SELECT NOHS FROM t_bc24_trf WHERE NOHS='".$TARIF["NOHS"]."'
							  AND SERITRP='".$TARIF["SERITRP"]."' AND NOMOR_AJU='".$aju."' AND KODE_TRADER = '".$KODE_TRADER."'";
					$data = $this->db->query($query);	
					if($data->num_rows() > 0){		
						$this->db->where(array('NOMOR_AJU' => $aju, 'NOHS' => $TARIF["NOHS"], 
												'SERITRP' => $TARIF["SERITRP"],
												'KODE_TRADER'=>$KODE_TRADER));
						$exec = $this->db->update('t_bc24_trf', $TARIF);					
					}else{
						$exec = $this->db->insert('t_bc24_trf', $TARIF);		
					}
					#$sql="call hitungPungutanBC24('".$aju."')";
					if($exec){
						$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc24('".$aju."'),0) AS JUM FROM DUAL", "JUM");     
						$this->db->where(array('NOMOR_AJU' => $aju, "KODE_TRADER"=>$KODE_TRADER));
						$this->db->update('t_bc24_hdr', array("NETTO" => $netto));
						$func->main->activity_log('EDIT DETIL BC24','CAR='.$aju.', SERIAL='.$seri);
						echo "MSG#OK#Update data Barang Berhasil#edit#".site_url()."/pemasukan/LoadHeader/bc24/".$aju."#";
					}else{					
						echo "MSG#ERR#Update data Barang Gagal#edit#";
					}	
				}
			}
		}else if($type=="delete"){	
			foreach($this->input->post('tb_chkfbarang') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$seri = $arrchk[1];
				$seri_hs = $arrchk[2];
				$this->db->where(array('NOMOR_AJU' => $aju, 'SERIAL' => $seri, "KODE_TRADER"=>$KODE_TRADER,"NIPER"=>$NIPER));
				$exec = $this->db->delete('t_bc24_dtl');
				$this->db->where(array('NOMOR_AJU' => $aju, 'SERIAL' => $seri,"KODE_TRADER"=>$KODE_TRADER,"NIPER"=>$NIPER));
				$exec = $this->db->delete('t_bc24_fas');
				$this->db->where(array('NOMOR_AJU' => $aju, 'SERITRP' => $seri_hs,"KODE_TRADER"=>$KODE_TRADER,"NIPER"=>$NIPER));
				$exec = $this->db->delete('t_bc24_trf');
				$func->main->activity_log('DELETE DETIL BC24','CAR='.$aju.', SERIAL='.$seri);
			}
			#$sql="call hitungPungutanBC24('".$aju."')";
			if($exec){
				$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc24('".$aju."'),0) AS JUM FROM DUAL", "JUM");     
				$this->db->where(array('NOMOR_AJU' => $aju));
				$this->db->update('t_bc24_hdr', array("NETTO" => $netto));
				echo "MSG#OK#Hapus data Barang Berhasil#".site_url()."/pemasukan/detil/barang/bc24/".$aju."#".site_url()."/pengeluaran/LoadHeader/bc24/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Barang Gagal#";die();
			}
		}
	}
		
	function get_barang($aju="",$seri=""){				
		$data = array();		
		$conn = get_instance();
		$conn->load->model("main","main", true);		
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');
		$NIPER = $this->newsession->userdata('NIPER');
		if($aju && $seri){
			$NDPBM = $conn->main->get_uraian("SELECT NDPBM FROM t_bc24_hdr WHERE NOMOR_AJU = '".$aju."'", "NDPBM");			
			$query = "SELECT KODE_BARANG,KODE_KEMASAN,URAIAN_BARANG,MERK,TIPE,UKURAN,SPFLAIN,NOHS, 
					  SERITRP,HARGA_CIF_BB_DTL,JUMLAH_SATUAN, NETTO_DTL, HARGA_PENYERAHAN_DTL,
					  IFNULL(HARGA_CIF_BB_DTL,0)*".$NDPBM." AS CIFRPBRG,
					  f_kemas(KODE_KEMASAN) KODE_KEMASANUR,JUMLAH_KEMASAN,
					  f_satuan(KODE_SATUAN) KODE_SATUANUR,KODE_SATUAN,
					  f_ref('STATUS',STATUS),NOMOR_AJU,JNS_BARANG 
					  FROM t_bc24_dtl				  
					  WHERE NOMOR_AJU = '".$aju."' AND SERIAL = '".$seri."' AND KODE_TRADER = '".$KODETRADER."' AND NIPER = '".$NIPER."'";
			$hasil = $conn->main->get_result($query);	
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess'=>$row, 'aju'=>$aju, 'seri'=>$seri);
							
				}
			}
		}else{	
			$query = "SELECT NOMOR_API, 0 AS 'SERITRP', 0 AS 'JUMLAH_KEMASAN', 0 AS 'NETTO', 0 AS 'INVOICE', 0 AS 'DISKON', 0 AS 'JUMLAH_SATUAN',
 					  0 AS 'FOB', 0 AS 'CIF', 0 AS 'CIFRP', 0 AS 'TARIF_BM', 0 AS 'TARIF_PPN', 0 AS 'TARIF_PPNBM', 0 AS 'FAS_PPN', 
					  0 AS 'FAS_PPNBM',	0 AS 'FAS_PPH', 0 AS 'FAS_BM', 0 AS 'KODE_SATUAN_BM', 0 AS 'JUMLAH_BM'
					  FROM t_bc24_hdr WHERE NOMOR_AJU='".$aju."'";	
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$dataarray = $row;
				}
			}		
			$data = array('act' => 'save','aju'=>$aju, 'sess'=>$dataarray);			
		}
		$data = array_merge($data, array('jenis_komoditi' => $conn->main->get_mtabel('JNS_BARANG'),
										 'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_PENGIRIMAN'),
										 'penggunaan' => $conn->main->get_mtabel('PENGGUNAAN_BARANG'),										
										 'komoditi_cukai' => $conn->main->get_mtabel('KOMODITI_CUKAI'),
										 'jenis_tarif' => $conn->main->get_mtabel('JENIS_TARIF_BM',1,false,"AND KODE IN ('1','2')"),
										 'kode_bm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppn' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppnbm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_pph' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_cukai' => $conn->main->get_mtabel('STATUS_BAYAR')));
		return $data;
			
	}
}
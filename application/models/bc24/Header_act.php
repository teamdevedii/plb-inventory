<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Header_act extends CI_Model{
	
	function get_header($aju=""){		
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		$user_id = $this->newsession->userdata("USER_ID");
		if($aju){	
			$query = "SELECT f_ref('API',KODE_API) URAPI, f_kpbc(A.KD_KPBC) URAIAN_KPBC,
						IFNULL(f_jumnetto_bc24(A.NOMOR_AJU),0) AS 'JUM_NETTO',IFNULL(A.NILAI_CIF_BB*A.NDPBM,0) AS CIFRP,
							(SELECT KDFASIL FROM T_BC24_PGT WHERE KDBEBAN='8' AND nomor_aju='$aju') AS KDFASIL,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='1' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_BM,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='1' AND KDFASIL='2' AND nomor_aju='$aju') AS PGT_BM_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='1' AND KDFASIL='4' AND nomor_aju='$aju') AS PGT_BM_DIB,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='2' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_PPN,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='2' AND KDFASIL='2' AND nomor_aju='$aju') AS PGT_PPN_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='2' AND KDFASIL='4' AND nomor_aju='$aju') AS PGT_PPN_DIB,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='3' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_PPNBM,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='3' AND KDFASIL='2' AND  nomor_aju='$aju') AS PGT_PPNBM_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='3' AND KDFASIL='4' AND  nomor_aju='$aju') AS PGT_PPNBM_DIB,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='4' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_PPH,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='4' AND KDFASIL='2' AND  nomor_aju='$aju') AS PGT_PPH_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='4' AND KDFASIL='4' AND  nomor_aju='$aju') AS PGT_PPH_DIB,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='6' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_CUKAI,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='6' AND KDFASIL='2' AND nomor_aju='$aju') AS PGT_CUKAI_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='6' AND KDFASIL='4' AND nomor_aju='$aju') AS PGT_CUKAI_DIB,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='8' AND KDFASIL='0' AND nomor_aju='$aju') AS PGT_PNBP,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='8' AND KDFASIL='2' AND nomor_aju='$aju') AS PGT_PNBP_DIT,
							(SELECT NILBEBAN FROM T_BC24_PGT WHERE KDBEBAN='8' AND KDFASIL='4' AND nomor_aju='$aju') AS PGT_PNBP_DIB,
						b.NAMA,b.ALAMAT,b.KODE_NPWP,b.NPWP,b.status_usaha, a.* 
					  FROM T_BC24_HDR a 
					  	LEFT OUTER JOIN m_trader b ON b.KODE_TRADER=A.KODE_TRADER
					  WHERE A.NOMOR_AJU = '$aju'";
			
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$dataarray = $row;
				}
			}
			if(!$dataarray["KODE_NPWP_PEMASOK"]){
				$query = "SELECT A.KODE_PERUSAHAAN KODE_TRADER, A.KODE_NPWP 'IMPID', A.NPWP 'IMPNPWP', A.NAMA 'IMPNAMA', 
						  A.ALAMAT 'IMPALMT', D.NAMA_TTD, D.KOTA_TTD,
						  C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',
						  0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 0 AS 'BRUTO', 0 AS 'NETTO', 
						  0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
						  FROM T_MST_PERUSAHAAN A, T_MST_USER C, t_mst_setting D
						  WHERE A.KODE_PERUSAHAAN=C.KODE_PERUSAHAAN 
						  AND A.KODE_PERUSAHAAN=D.KODE_PERUSAHAAN
						  AND A.KODE_PERUSAHAAN='".$kode_trader."'
						  AND C.USER_ID = '".$user_id."'";				
				$hasil = $conn->main->get_result($query);
				if($hasil){
					foreach($query->result_array() as $row){
						$dt = $row;
					}
				}		
			}
			$data = array('act' => 'update');
		}else{	
			$this->db->where('KODE_TRADER',$kode_trader);
			$this->db->select_max('TANGGAL_SKEP');
			$resultSkep = $this->db->get('m_trader_skep')->row();
			$maxTglSkep = $resultSkep->TANGGAL_SKEP;
			
			$query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
                      	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', 
						D.NAMA_TTD, D.KOTA_TTD, 0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
                      	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 0 AS 'NILAI_INVOICE', 0 AS 'NDPBM', KODE_API, NOMOR_API
                      FROM M_TRADER A 
                      	INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
                        LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER AND B.TANGGAL_SKEP = '".$maxTglSkep."'
                        LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
                       WHERE A.KODE_TRADER = '".$kode_trader."'
                        AND C.USER_ID = ".$user_id." LIMIT 0,1";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$dataarray = $row;
				}
			}
			$data = array('act' => 'save', 'aju' => $aju);	
		}			
		$data = array_merge($data, array('aju' => $aju,
										 'sess' => array_merge(array("REGISTRASI"=>$SKEP["NOMOR_SKEP"]),$dataarray),
										 'jenis_barang' => $conn->main->get_mtabel('JNS_BARANG'),
										 'kriteria' => $conn->main->get_mtabel('KRITERIA'),
										 'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_BC24'),
										 'jenis_pib' => $conn->main->get_mtabel('JENIS_PIB'),
										 'jenis_impor' => $conn->main->get_mtabel('JENIS_IMPOR'),
										 'cara_bayar' => $conn->main->get_mtabel('CARA_PEMBAYARAN'),
										 'kode_id_trader' => $conn->main->get_mtabel('KODE_ID',1,FALSE),
										 'jenis_tpb' => $conn->main->get_mtabel('JENIS_TPB'),
										 'status' => $conn->main->get_mtabel('JENIS_EKSPORTIR'),
										 'kode_api' => $conn->main->get_mtabel('API'),
										 'cara_angkut' => $conn->main->get_mtabel('MODA'),
										 'kode_pnbp' => $conn->main->get_combobox("SELECT KODE , CONCAT(KODE,' - ',URAIAN) AS URAIAN FROM M_TABEL 
							 											 		   WHERE JENIS='STATUS_BAYAR' AND KODE IN('0','2') 
																		 		   ORDER BY KODE","KODE", "URAIAN", TRUE),
										 'kode_penutup' => $conn->main->get_mtabel('KODE_PENUTUP'))
										 );
		return $data;
	}
	
	function set_header($type="",$isajax=""){
		$func = get_instance();
		$func->load->model("main");
		if(strtolower($type)=="save" || strtolower($type)=="update"){			
			foreach($this->input->post('HEADER') as $a => $b){
				$HEADER[$a] = $b;
			}
			foreach($this->input->post('BEBAN') as $c => $d){
				$BEBAN[$c] = $d;
			}
			//$HEADER['ID_TRADER'] = str_replace('.','',str_replace('-','',$HEADER['ID_TRADER']));
			$HEADER['KODE_TRADER'] = $this->newsession->userdata('KODE_TRADER');
			$HEADER['NIPER'] = $this->newsession->userdata('NIPER');
			$BEBAN['KODE_TRADER'] = $this->newsession->userdata('KODE_TRADER');
			$BEBAN['NIPER'] = $this->newsession->userdata('NIPER');
			if(strtolower($type)=="save"){
			$func->main->get_car($car,'bc24');
				$HEADER["STATUS"]       = '00';
				$HEADER["NOMOR_AJU"] 	= $car;
			//$HEADER["CIFRP"] 		= str_replace(",","",$HEADER["CIFRP"]);
				$HEADER['KODE_TRADER']	= $this->newsession->userdata('KODE_TRADER');
				$HEADER["CREATED_BY"]	= $this->newsession->userdata('USER_ID');
				$HEADER["CREATED_TIME"]	= date('Y-m-d H:i:s',time()-60*60*1);
				$BEBAN["NOMOR_AJU"]		= $car;
				$BEBAN["NILBEBAN"] 	= str_replace(",","",$BEBAN["NILBEBAN"]);
				$exec = $this->db->insert('T_bc24_HDR', $HEADER);	
				$exec = $this->db->insert('T_bc24_PGT', $BEBAN);
				//$sql="call hitungPungutanbc24('".$car."')";
				//$this->db->query($sql);
				if($exec){
					$func->main->set_car('bc24'); 
					$func->main->activity_log('ADD HEADER bc24','NOMOR_AJU='.$car);
					echo "MSG#OK#Proses header Berhasil#".$car."#".site_url()."/pengeluaran/LoadHeader/bc24/".$car."#bc24#";
				}else{					
					echo "MSG#ERR#Proses data header Gagal#";
				}				
			}else if(strtolower($type)=="update"){				
				foreach($this->input->post('HEADERDOK') as $x => $z){
					$HEADERDOK[$x] = $z;
				}
				$NODAF = $HEADERDOK["NOMOR_PENDAFTARAN"];
				$TGDAF = $HEADERDOK["TANGGAL_PENDAFTARAN"];
					
				$CAR = $HEADER["NOMOR_AJU"];
				//$HEADER["CIFRP"] 		= str_replace(",","",$HEADER["CIFRP"]);
				$HEADER["CREATED_BY"]	= $this->newsession->userdata('USER_ID');
				$HEADER["CREATED_TIME"]	= date('Y-m-d H:i:s',time()-60*60*1);
				//$HEADER["UPDATED_BY"]	= $this->newsession->userdata('USER_ID');
				//$HEADER["UPDATED_TIME"]	= date('Y-m-d H:i:s',time()-60*60*1);	
				$BEBAN["NILBEBAN"] 	= str_replace(",","",$BEBAN["NILBEBAN"]);	
				
				$SQL = "SELECT KDBEBAN FROM T_bc24_PGT WHERE KDBEBAN='".$BEBAN["KDBEBAN"]."' AND NOMOR_AJU='".$CAR."'";
				$data = $this->db->query($SQL);
				if($data->num_rows() > 0){
					$this->db->where(array('NOMOR_AJU' => $CAR, 'KDBEBAN' => $BEBAN["KDBEBAN"]));
					$exec = $this->db->update('T_bc24_PGT', $BEBAN);	
				}else{
					$BEBAN["NOMOR_AJU"] = $CAR;
					$exec = $this->db->insert('T_bc24_PGT', $BEBAN);	
				}
				
				$this->db->where(array('NOMOR_AJU' => $CAR));
				if($this->input->post("STATUS_DOK")=="LENGKAP" && $NODAF && $TGDAF){	
					$HEADERDOK["STATUS"] = '07';
					$exec = $this->db->update('T_bc24_HDR', array_merge($HEADERDOK,$HEADER));
				}else{
					$exec = $this->db->update('T_bc24_HDR', $HEADER);
				}
				//$sql="call hitungPungutanbc24('".$CAR."')";
				//$this->db->query($sql);
				if($exec){
					$func->main->activity_log('EDIT HEADER bc24','CAR='.$CAR);
					echo "MSG#OK#Proses header Berhasil#".$CAR."#";
				}else{					
					echo "MSG#ERR#Proses data header Gagal#";
				}	
			}
		}
	}
	
}
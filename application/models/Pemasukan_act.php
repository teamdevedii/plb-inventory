<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pemasukan_act extends CI_Model{
	function daftar($tipe="",$dokumen="",$aju=""){
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		$KODE_ROLE = $this->newsession->userdata('KODE_ROLE');
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$jenis = "pemasukan";
		$addSql="";
		if($tipe=="approved"){
			$title = "Daftar Permohonan Pemasukan [Disetujui]";
			$TGLPENDAFTARAN = "NOMOR_PENDAFTARAN 'NOMOR DOKUMEN',DATE_FORMAT(TANGGAL_PENDAFTARAN, '%d %M %Y') AS 'Tanggal Dokumen',";	
			$FIELD = ",DATE_FORMAT(TANGGAL_REALISASI, '%d %M %Y %H:%m:%s') AS 'Tanggal Realisasi/Gate', TANGGAL_REALISASI";
			$WHERE = " WHERE STATUS IN ('07','19') ";	
			if(!in_array($KODE_ROLE,array("3,4"))){ 
				$WHERE .= " AND KODE_TRADER ='".$KODE_TRADER."' ";					
			}else{
				$FIELD .= ", f_trader(KODE_TRADER) 'NAMA PERUSAHAAN'";
			}
			if(in_array($KODE_ROLE,array('5'))){
				$prosesnya = array(	'Preview' => array('GET2', site_url()."/pemasukan/priview", '1','icon-eye-open'),										
									'Cetak' => array('GETNEW', site_url()."/pemasukan/print_dok", '1','red icon-print'));	
			}else{
				$prosesnya = array(	'Preview' => array('GET2', site_url()."/pemasukan/priview", '1','icon-eye-open'),						
									'Cetak' => array('GETNEW', site_url()."/pemasukan/print_dok", '1','red icon-print'),																	
									'Copy' => array('COPY', site_url()."/pemasukan/copydata", '1','icon-copy'),
									'Ubah ke Draft' => array('PROCESS', site_url().'/pemasukan/set_pemasukan/approved','pemasukan','icon-reply'));					
			}
			$this->newtable->keys(array("Dokumen","Nomor Aju","TANGGAL_REALISASI"));
			$this->newtable->hiddens(array("CREATED_TIME","TANGGAL_REALISASI"));
		}elseif($tipe=="draft"){						
			$title = "Daftar Permohonan Pemasukan [Draft]";				
			$WHERE = " WHERE STATUS NOT IN ('07','19') ";
			if(!in_array($KODE_ROLE,array("3,4"))){  
				$WHERE .= " AND KODE_TRADER ='".$KODE_TRADER."' ";					
			}else{
				$FIELD = ", f_trader(KODE_TRADER) 'NAMA PERUSAHAAN'";
			}
			if(in_array($KODE_ROLE,array('5'))){
				$prosesnya = array( 'Preview' => array('GET2', site_url()."/pemasukan/priview", '1','icon-eye-open'),
									'Cetak' => array('GETNEW', site_url()."/pemasukan/print_dok", '1','red icon-print'));
			}else{
				$prosesnya = array( 'Tambah' => array('GET2', site_url()."/pemasukan/draft/priview", '1','icon-plus','','btn-group',
												array('BC 2.8.1'=>array('GET2', site_url()."/pemasukan/create/bc281", '0'),
													  'BC 2.7'=>array('GET2', site_url()."/pemasukan/create/bc27", '0'),
													  'BC 2.4'=>array('GET2', site_url()."/pemasukan/create/bc24", '0'),
													  'BC 3.0'=>array('GET2', site_url()."/pemasukan/create/bc30", '0'),
													  'BC 4.0'=>array('GET2', site_url()."/pemasukan/create/bc40", '0'))),	
									'Ubah' => array('EDITJS', site_url()."/pemasukan/edit", '1','icon-edit'),				
									'Hapus' => array('DELETE', site_url().'/pemasukan/set_pemasukan/'.$tipe, 'pemasukan','red icon-remove'),
									'Preview' => array('EDITJS', site_url()."/pemasukan/priview", '1','icon-eye-open'),					
									'Setujui' => array('DIALOG', site_url()."/pemasukan/setujui", '1','icon-check','600,250'),
									'Copy' => array('COPY', site_url()."/pemasukan/copydata", '1','icon-copy'),
									'Cetak' => array('GETNEW', site_url()."/pemasukan/print_dok", '1','red icon-print'));
			}
			$this->newtable->keys(array("Dokumen","Nomor Aju"));					
			$this->newtable->hiddens('CREATED_TIME');					
		}
		
		foreach($this->input->post('CARI') as $a=>$b){
			$CARI[$a] = $b;
		}
		
		if($dokumen=="bc281"){			
			$SQL = "SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC281' AS 'Dokumen', ".$TGLPENDAFTARAN." 
					NAMA_PEMASOK AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc23(NOMOR_AJU),0) AS 'Detil Barang', 
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc23(NOMOR_AJU,2))) 'Status' ".$FIELD." 
					FROM T_BC23_HDR";	
			$SQL .=	$WHERE;
			$this->newtable->search(array(array('NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'), 
										  array('NAMA_PEMASOK', 'PEMASOK/PENGIRIM'),
										  array('CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')));					
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_PEMASOK LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
		}elseif($dokumen=="bc27"){		
			$SQL = "SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC27' AS 'Dokumen', ".$TGLPENDAFTARAN."
					NAMA_TRADER_ASAL AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc27(NOMOR_AJU,KODE_TRADER),0) AS 'Detil Barang',
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc27(NOMOR_AJU,2,KODE_TRADER))) 'Status' ".$FIELD." 
					FROM T_BC27_HDR";
			$SQL .=	$WHERE."AND TIPE_DOK='MASUK' ";		
			$this->newtable->search(array(array('NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'), 
										  array('NAMA_TRADER_ASAL', 'PEMASOK/PENGIRIM'),
										  array('CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')));	
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_TRADER_ASAL LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}							  
		}elseif($dokumen=="bc40"){				
			$SQL = "SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC40' AS 'Dokumen', ".$TGLPENDAFTARAN."
					NAMA_TRADER AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc40(NOMOR_AJU,KODE_TRADER),0) AS 'Detil Barang',
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc40(NOMOR_AJU,2,KODE_TRADER))) 'Status' ".$FIELD." 
					FROM T_BC40_HDR";
			$SQL .=	$WHERE;
			$this->newtable->search(array(array('NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'), 
										  array('NAMA_TRADER', 'PEMASOK/PENGIRIM'),
										  array('CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')));	
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_TRADER LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
		}elseif($dokumen=="bc30"){
			$judul = "Dokumen BC 3.0";
			$SQL = "SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC30' AS Dokumen, ".$TGLPENDAFTARAN."
					NAMA_PEMBELI AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc30(NOMOR_AJU),0) AS 'Detil Barang' ,
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc30(NOMOR_AJU,2))) AS 'Status' 
					".$FIELD." FROM T_BC30_HDR ";	
			$SQL .=	$WHERE;	
			$SQL .= " AND TIPE_DOK ='MASUK'";
			
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_PEMBELI LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
		}else{
			$POSTURI = $this->input->post("uri");
			$judul = "Semua Dokumen";
			$SQL = "SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC281' AS 'Dokumen', ".$TGLPENDAFTARAN." 
					NAMA_PEMASOK AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc23(NOMOR_AJU),0) AS 'Detil Barang', 
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc23(NOMOR_AJU,2))) 'Status' ".$FIELD." 
					FROM T_BC23_HDR";
			$SQL .=	$WHERE;	
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_PEMASOK LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
			
			$SQL .=	"UNION ALL
					SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC27' AS 'Dokumen', ".$TGLPENDAFTARAN."
					NAMA_TRADER_ASAL AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc27(NOMOR_AJU,KODE_TRADER),0) AS 'Detil Barang',
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc27(NOMOR_AJU,2,KODE_TRADER))) 'Status' ".$FIELD."
					FROM T_BC27_HDR";
			$SQL .=	$WHERE."AND TIPE_DOK='MASUK' ";
			
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_TRADER_ASAL LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}		
			
			$SQL .=	"UNION ALL
					SELECT CREATED_TIME, NOMOR_AJU AS 'Nomor Aju', 'BC40' AS 'Dokumen', ".$TGLPENDAFTARAN."
					NAMA_TRADER AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc40(NOMOR_AJU,KODE_TRADER),0) AS 'Detil Barang',
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc40(NOMOR_AJU,2,KODE_TRADER))) 'Status' ".$FIELD."
					FROM T_BC40_HDR";
			$SQL .=	$WHERE;
			
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_TRADER LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
			
			$SQL .= "UNION ALL SELECT CREATED_TIME, NOMOR_AJU AS 'NOMOR AJU', 'BC30' AS DOKUMEN, ".$TGLPENDAFTARAN."
					NAMA_PEMBELI AS 'Pemasok/Pengirim', 
					IFNULL(f_jumbrg_bc30(NOMOR_AJU),0) AS 'Detil Barang' ,
					IF(STATUS='07','DISETUJUI',IF(STATUS='19','REALISASI',f_status_bc30(NOMOR_AJU,2))) AS 'Status' 
					".$FIELD." FROM T_BC30_HDR ";	
			$SQL .=	$WHERE;	
			$SQL .= " AND TIPE_DOK = 'MASUK'";
			
			if($CARI["NOMOR_AJU"]!=""){
				$SQL .= " AND NOMOR_AJU LIKE '%".$CARI["NOMOR_AJU"]."%'";
			}
			if($CARI["PEMASOK"]!=""){
				$SQL .= " AND NAMA_PEMBELI LIKE '%".$CARI["PEMASOK"]."%'";
			}
			if($CARI["LOKASI"]!=""){
				$SQL .= " AND KODE_TRADER = '".$CARI["LOKASI"]."'";
			}
			if($tipe=="approved"){
				if($CARI["NOMOR_PENDAFTARAN"]!=""){
					$SQL .= " AND NOMOR_PENDAFTARAN = '".$CARI["NOMOR_PENDAFTARAN"]."'";
				}
				if($CARI["TANGGAL_PENDAFTARAN_AWAL"]!="" && $CARI["TANGGAL_PENDAFTARAN_AKHIR"]!=""){
					$SQL .= " AND TANGGAL_PENDAFTARAN BETWEEN '".$CARI["TANGGAL_PENDAFTARAN_AWAL"]."' AND '".$CARI["TANGGAL_PENDAFTARAN_AKHIR"]."'";
				}
			}
									  
		}	
		
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/".$tipe."/".$dokumen."");
		#$this->newtable->detail(site_url()."/pemasukan/priview");
		#$this->newtable->detail_tipe("detil_priview_blank");
		$this->newtable->tipe_proses('button');
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->orderby(1,2);
		$this->newtable->sortby("DESC");
		$this->newtable->count_keys(array("CREATED_TIME"));
		$this->newtable->set_formid("f".$tipe);
		$this->newtable->set_divid("div".$tipe);
		$this->newtable->rowcount(20);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$this->newtable->show_search(false);
		
		if(!$this->input->post("ajax") || $dokumen!="view"){	
			$action = "pemasukan/".$tipe."/".$dokumen;
			$div = "div".$tipe;
			$SQ = "SELECT KODE_TRADER, NAMA FROM M_TRADER WHERE INDUK_PLB='".$KODE_TRADER."' ORDER BY NAMA";
			$LOKASI = $func->main->get_combobox($SQ, "KODE_TRADER", "NAMA", TRUE);
			$tabel .= '<form name="tblCari" id="tblCari" class="form-horizontal" method="post" action="'.$action.'" onsubmit="return frm_Cari(\''.$div.'\',\'tblCari\')">';
			$tabel .= ' <div class="form-group">
					    <label class="col-sm-2 control-label" style="width:15%">Nomor Aju</label>
						<div class="col-sm-2">
						<input id="NOMOR_AJU" class="form-control" type="text" name="CARI[NOMOR_AJU]">
						</div>';						
			if(in_array($KODE_ROLE,array('3','4'))){				
				$tabel .= '<label class="col-sm-2 control-label" style="width:15%">Lokasi Kantor</label>
						   <div class="col-sm-2">
						   '.form_dropdown('CARI[LOKASI]', $LOKASI, '', 'id="LOKASI" class="text"').'
						   </div>';				
			}else{
				if($tipe=="approved"){
					$tabel .= ' <label class="col-sm-2 control-label" style="width:15%">Nomor Pendaftaran</label>
								<div class="col-sm-2">
								<input type="text"  name="CARI[NOMOR_PENDAFTARAN]" id="NOMOR_PENDAFTARAN" class="text">
								</div>';
				}
			}
			$tabel .= '</div>';
			$tabel .= '<div class="form-group">
							<label class="col-sm-2 control-label" style="width:15%">Pemasok/Pengirim</label>
							<div class="col-sm-2">
							<input id="PEMASOK" class="form-control" type="text" name="CARI[PEMASOK]">
							</div>';
			
			if($tipe=="approved"){
				$tabel .= '<label class="col-sm-2 control-label" style="width:15%">Tanggal Pendaftaran</label>';
				$tabel .= '<div class="col-sm-2"><input type="text" class="stext date" name="CARI[TANGGAL_PENDAFTARAN_AWAL]" onclick="ShowDP(\'tgl-dftr-awal\');" onfocus="ShowDP(\'tgl-dftr-awal\');" id="tgl-dftr-awal">&nbsp;s/d&nbsp;<input type="text" class="stext date" name="CARI[TANGGAL_PENDAFTARAN_AKHIR]" onclick="ShowDP(\'tgl-dftr-akhir\');" onfocus="ShowDP(\'tgl-dftr-akhirakhir\');" id="tgl-dftr-akhir"></div>';
			}
			$tabel .= '</div>';
			$tabel .= '<input type="submit" style="display:none">';
			$tabel .= ' <div class="form-group"><label class="col-sm-2 control-label" style="width:15%">&nbsp;</label>';
			$tabel .= ' <div class="col-sm-2" style="text-align:left;"><a style="float:left;" onclick="frm_Cari(\''.$div.'\',\'tblCari\')" id="ok_" class="btn btn-primary btn-sm" href="javascript:void(0);"><i class="icon-search"></i>&nbsp;Search</a>&nbsp;';
			$tabel .= '	<a onclick="cancel(\'tblCari\')" id="ok_" class="btn btn-danger btn-sm" href="javascript:void(0);"><i class="icon-remove"></i>&nbsp;Clear</a></div>';
			$tabel .= ' </div></form><br />';					
		}
		$generate = $this->newtable->generate($SQL);
		$tabel .= $generate;					
		$arrdata = array("title" => $title,
						 "judul" => $judul,
						 "tabel" => $tabel,
						 "jenis" => $jenis,
						 "tipe" => $tipe);
		if($this->input->post("ajax")||$dokumen=="view") return $generate;
		else return $arrdata;
	}
	
	function get_pemasukan($type="",$id_1="",$id_2="",$id_3="",$aju=""){		
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');
		if($type=="ceksurat"){
			$SQL = "SELECT * FROM m_trader_permohonan WHERE NOMOR_AJU = '".$id_1."'";
			$arrdata = $func->main->get_result($SQL);
		}
		elseif($type=="Updatesurat"){
			$SQL = "SELECT NOMOR_AJU, KANTOR_TUJUAN, KODE_TRADER, NO_PERMOHONAN, TGL_PERMOHONAN, LAMPIRAN, PERIHAL, TUJUAN_PERMOHONAN, 
					URAIAN_SURAT, NAMA_PEMOHON, NOID_PEMOHON, NO_SRT_TUGAS, TELP_PEMOHON, EMAIL_PEMOHON FROM m_trader_permohonan 
					WHERE NOMOR_AJU = '".$aju."'";					
			$hasil = $func->main->get_result($SQL);
			if($hasil){
				foreach($SQL->result_array() as $row){
					$arrdata = $row;
				}
			}			
			if($arrdata["LAMPIRAN"]){
				$arrdata["LAMPIRAN"] = "'".str_replace(",","','",$arrdata["LAMPIRAN"])."'";		
				$SQL = "SELECT URAIAN FROM M_TABEL WHERE JENIS='LAMPIRAN' AND KODE IN (".$arrdata["LAMPIRAN"].") ORDER BY KODE ASC ";
				$res = $func->main->get_result($SQL);
				if($res){
					$LIST = '<ol type="1" style="margin:0px 0 1px 20px">';
					foreach($SQL->result_array() as $DATA){
						$LIST .= "<li>".ucwords(strtolower($DATA["URAIAN"]))."</li>";
					}
					$LIST .= '</ol>';
				}			
			}
			$SQL = "SELECT NOMOR_AJU, SERI, KODE_BARANG, f_barang(KODE_BARANG,JNS_BARANG,'".$KODETRADER."') URAIAN_BARANG , 
					JNS_BARANG, KONDISI, JUMLAH, KODE_SATUAN	
					FROM m_trader_permohonan_barang WHERE NOMOR_AJU = '".$aju."'";
			$dtl = $func->main->get_result($SQL);
			$KONDISI_BARANG = $func->main->get_mtabel('KONDISI_BARANG');			
			$JENIS_BRG = $func->main->get_mtabel('ASAL_JENIS_BARANG');
			if($dtl){				
				$a=1;
				foreach($SQL->result_array() as $row){				
					$TXT .= '<tr id="tr_barang">';
					$TXT .= "<td class=\"alt2 left bright\">
							<input type=\"text\" name=\"DTLSURAT[KODE_BARANG][]\" id=\"KODE_BARANG".$a."\" style=\"width:10%;\" class=\"text\"  url=\"".site_url()."/autocomplete/barang2\" urai=\"NM_BARANG;\" onfocus=\"Autocomp(this.id)\" value=\"".$row["KODE_BARANG"]."\"/>&nbsp;<input type=\"text\" name=\"NM_BARANG[]\" id=\"NM_BARANG".$a."\" style=\"width:80%;\" class=\"text\" value=\"".$row["URAIAN_BARANG"]."\"/>&nbsp;<input type=\"button\" name=\"cari\" id=\"cari\" class=\"button\" onclick=\"tb_search('kodebarang','KODE_BARANG".$a.";NM_BARANG".$a."','Kode Barang',this.form.id,600,400)\" value=\"...\"></td>
							<td id=\"tdJenisBarang\" class=\"alt2 bright\">".form_dropdown('DTLSURAT[JNS_BARANG][]', $JENIS_BRG, $row["JNS_BARANG"], 'id="JNS_BARANG'.$a.'" class="ltext" ')."</td>
							<td id=\"tdKondisiBarang\" class=\"alt2 bright\">".form_dropdown('DTLSURAT[KONDISI][]', $KONDISI_BARANG, $row["KONDISI"], 'id="KONDISI'.$a.'" class="sstext" ')."</td>
							<td class=\"alt2 bright\">
							<input type=\"text\" name=\"DTLSURAT[JUMLAH][]\" id=\"JUMLAH_BARANG".$a."\" class=\"stext\" value=\"".$row["JUMLAH"]."\"/>
							</td>
							<td class=\"alt2 bright\">
							<input type=\"text\" name=\"DTLSURAT[KODE_SATUAN][]\" id=\"KODE_SATUAN".$a."\" class=\"ssstext\" value=\"".$row["KODE_SATUAN"]."\" url=\"".site_url()."/autocomplete/satuan\" urai=\"\" onfocus=\"Autocomp(this.id)\"/>
							</td>
							<td class=\"alt2 right\">";
					if($a!=1){		
					$TXT .=	"<a href=\"javascript:void(0)\" onclick=\"removeRowBarang('tableBarang','tr_barang','')\" title=\"Hapus Barang\" class=\"del\"><span><span class=\"icon\"></span>&nbsp;</span></a>";
					}
					$TXT .=	"</td>";					
					$TXT .= '</tr>';
					$a++;
				}
			}
			$arrdata = array('LAMPIRAN'  => $LIST,
							 'KONDISI_BARANG'=> $func->main->get_mtabel('KONDISI_BARANG'),
							 'act' => 'update',
							 "DATA" => $arrdata,
							 "DETILBARANG" => $TXT,
							 'JENIS_BRG' => $func->main->get_mtabel('ASAL_JENIS_BARANG'),		  
							 );
		}elseif($type=="surat"){
			$dokumen = $id_1; $tmp_asal = $id_2; $jns_barang = $id_3;
			$arrdata = array('LAMPIRAN'  => $this->get_lampiran($tmp_asal,$jns_barang),
							 'KONDISI_BARANG'=> $func->main->get_mtabel('KONDISI_BARANG'),
							 'dokumen'=>$dokumen,
							 'tmp_asal'=>$tmp_asal,				
							 'jns_barang'=>$jns_barang,
							 'JENIS_BRG' => $func->main->get_mtabel('ASAL_JENIS_BARANG'),	
							 'act' => 'save'	  
							 );
			
		}else{
			$arrdata = array('act' => site_url().'/pemasukan/permohonan',
							'TEMPAT_ASAL' => $func->main->get_combobox("SELECT KODE , CONCAT(URAIAN,' ','(',KODE,')') AS URAIAN FROM M_TABEL 
							 											 WHERE JENIS='TEMPAT' AND KODE	IN('LDP','KB','TBB','GB','GB','KWBBS') 
																		 ORDER BY KODE","KODE", "URAIAN", TRUE),	
							 'JENIS_BARANG' => $func->main->get_mtabel('ASAL_JENIS_BARANG'),
							 'TUJUAN_PEMASUKAN' => $func->main->get_mtabel('TUJUAN_MASUK')
							 );
		}
		return $arrdata;	
	}
	
	function sstb($act=""){			
		$func = &get_instance();
		$func->load->model("main","main", true);
		$this->load->library('newtable');
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
				
		$SQL = "SELECT A.NOMOR_SURAT 'NOMOR DOKUMEN', DATE_FORMAT(A.TANGGAL_SURAT,'%d-%M-%Y') 'TANGGAL DOKUMEN', 
				A.NAMA_PERUSAHAAN_PENERIMA 'PENERIMA',(SELECT COUNT(*) FROM T_SSTB_DTL B WHERE B.IDSSTB=A.IDSSTB) AS 'DETIL BARANG',
				IDSSTB,
				IF(STATUS='07','DISETUJUI',f_status_sstb(IDSSTB,2)) 'Status', A.TANGGAL_SURAT 
				FROM T_SSTB_HDR A WHERE A.KODE_TRADER='".$KODE_TRADER."'"; 	
		
		$proses = array('Tambah' => array('GET2', site_url()."/pemasukan/sstb/save", '0','icon-plus'),
						'Ubah' => array('EDITJS', site_url()."/pemasukan/sstb/update", '1','icon-edit'),
						'Setujui' => array('PROCESS', site_url()."/pemasukan/set_sstb/setujui", 'sstb','icon-check'),	
						'Hapus' => array('DELETE', site_url()."/pemasukan/set_sstb/delete", 'sstb','red icon-remove'),
						'Cetak PDF' => array('GETNEW', site_url()."/pemasukan/print_dok/sstb", '1','red icon-print'),
						'Cetak Excel' => array('DIALOG-400;200', site_url()."/pemasukan/popupcetak/sstb", '0','green icon-print'));
							
		$this->newtable->hiddens(array("IDSSTB","TANGGAL_SURAT"));	
		$this->newtable->keys(array("IDSSTB"));
		$this->newtable->search(array(array('A.NOMOR_SURAT', 'NOMOR SURAT&nbsp;'),
									  array('A.TANGGAL_SURAT', 'TANGGAL SURAT', 'tag-tanggal'),
									  array('A.STATUS', 'STATUS','tag-select',array("0"=>"TIDAK LENGKAP","1"=>"LENGKAP","2"=>"DISETUJUI"))
									));
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/sstb");	
		$this->newtable->detail(site_url()."/pemasukan/sstb_view");
		$this->newtable->orderby('A.TANGGAL_SURAT');
		$this->newtable->count_keys(array('TANGGAL_SURAT'));
		$this->newtable->sortby("DESC");
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("fsstb");
		$this->newtable->set_divid("divsstb");
		$this->newtable->tipe_proses('button');	
		$this->newtable->rowcount(50);
		$this->newtable->clear(); 
		$this->newtable->menu($proses);
		$tabel .= $this->newtable->generate($SQL);			
		$arrdata = array("title" => "Daftar SSTB",
						 "tabel" => $tabel);
		if($this->input->post("ajax")||$act=="ajax") return $tabel;				 
		else return $arrdata;			
	}
	
	function get_sstb($act="",$id=""){
		$func = get_instance();
		$func->load->model("main");
		$KODE_TRADER = $this->newsession->userdata("KODE_TRADER");
		$DATA = array();
		if($act=='save'){			
			$SQL = "SELECT KODE_ID AS KODE_ID_PENGIRIM, ID AS NPWP_PERUSAHAAN_PENGIRIM,  NAMA AS NAMA_PERUSAHAAN_PENGIRIM, 
					ALAMAT AS ALAMAT_PERUSAHAAN_PENGIRIM FROM M_TRADER 
					WHERE KODE_TRADER='".$KODE_TRADER."'"; 
			$hasil = $func->main->get_result($SQL);
		}
		elseif($act=="update"){
			$SQL = "SELECT * FROM T_SSTB_HDR WHERE IDSSTB='".$id."'
					AND KODE_TRADER='".$KODE_TRADER."'";	
			$hasil = $func->main->get_result($SQL);				
		}		
		if($hasil){
			foreach($SQL->result_array() as $row){
				$DATA = $row;
			}
		}					
		return array("act" => $act,"DATA" => $DATA,"IDSSTB" => $id);	
	}
	
	function set_pemasukan($action="", $tipe=""){
		$func = get_instance();
		$func->load->model("main","main", true);
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		if($action=="delete"){ 
			$ret = "MSG#Hapus data Gagal";
			if($tipe=="draft"){
				$dataCheck = $this->input->post('tb_chkfdraft');
			}elseif($tipe=="approved"){	
				$dataCheck = $this->input->post('tb_chkfapproved');
			}
			foreach($dataCheck as $chkitem){
				$arrchk = explode("|", $chkitem);
				$DOK = strtolower($arrchk[0]);
				$AJU = $arrchk[1];
				if($DOK=="bc281"){					
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_cnt');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_dok');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_fas');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_kms');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_pgt');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_trf');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_dtl');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc23_hdr'); 	
				}elseif($DOK=="bc30"){
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_cnt');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_dok');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_pkb');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_pjt');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_kms');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_dtl');
					$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));	
					$this->db->delete('t_bc30_hdr');
				}elseif($DOK=="bc27"){	
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_bb');
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_dok');
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_kms');
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_dtl');
					$this->db->where(array('NOMOR_AJU' => $AJU));	
					$this->db->delete('t_bc27_cnt');
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_hdr'); 
					$this->db->where(array('NOMOR_AJU' => $AJU));
					$this->db->delete('t_bc27_dokasal');	
				}
				$this->db->where(array('NOMOR_AJU' => $AJU));
				$this->db->delete('m_trader_permohonan'); 	
				$this->db->where(array('NOMOR_AJU' => $AJU));
				$this->db->delete('m_trader_permohonan_barang'); 
				$func->main->activity_log('DELETE '.strtoupper($DOK),'CAR='.$AJU);	
				$ret = "MSG#OK#Hapus data Berhasil#".site_url()."/pemasukan/daftar_dok/".$tipe."/".$DOK."#";
			}
			echo $ret;
		}elseif($action=="process"){ 
			$ret = "MSG#Process data Gagal";
			$dataCheck = $this->input->post('tb_chkfapproved');
			foreach($dataCheck as $chkitem){
				$arrchk = explode("|", $chkitem);
				$DOK = strtolower($arrchk[0]);
				$AJU = $arrchk[1];	
				$this->db->where(array('NOMOR_AJU' => $AJU, "KODE_TRADER"=>$kode_trader));
				$this->db->update('t_'.$DOK.'_hdr',array('STATUS'=>'00'));
				$func->main->activity_log('EDIT '.strtoupper($DOK),'UBAH STATUS KE DRAFT, CAR='.$AJU);
				$ret = "MSG#OK#Proses data Berhasil#".site_url()."/pemasukan/daftar_dok/approved/".$DOK."#";
			}
			echo $ret;
		}
	}
	
	function daftar_breakdown($dokumen="",$aju=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$jenis = "pemasukan";
		$title = "Daftar History Breakdown Data Barang [Pemasukan]";	
		$WHERE = " AND A.KODE_TRADER ='".$this->newsession->userdata('KODE_TRADER')."' ";
		if($dokumen=="bc281"){				
			$judul = "Dokumen BC 2.8.1";					
			$SQL = "SELECT DISTINCT A.NOMOR_AJU AS 'Nomor Aju', 
					'BC281' AS 'Dokumen', DATE_FORMAT(A.CREATED_TIME, '%d %M %Y') AS 'Tanggal Dok.', A.NAMA_PEMASOK AS 'Pemasok/Pengirim', 
					f_jumbreak(A.NOMOR_AJU) AS 'DETIL BARANG BREAKDOWN'
					FROM T_BC23_HDR A, T_BREAKDOWN_PEMASUKAN B WHERE A.NOMOR_AJU=B.NOMOR_AJU";
			$SQL .=	$WHERE;
			$this->newtable->search(array(array('A.NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'), 
											  array('A.NAMA_PEMASOK', 'PEMASOK/PENGIRIM'),
											  array('CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')));
		}elseif($dokumen=="bc27"){
			$judul = "Dokumen BC 2.7";					
			$SQL = "SELECT DISTINCT A.NOMOR_AJU AS 'Nomor Aju', 
					'BC27' AS 'Dokumen', DATE_FORMAT(A.CREATED_TIME, '%d %M %Y') AS 'Tanggal Dok.', A.NAMA_TRADER_ASAL AS 'Pemasok/Pengirim',
					f_jumbreak(A.NOMOR_AJU) AS 'DETIL BARANG BREAKDOWN'
					FROM T_BC27_HDR A, T_BREAKDOWN_PEMASUKAN B WHERE A.NOMOR_AJU=B.NOMOR_AJU";
			$SQL .=	$WHERE."AND A.TIPE_DOK='MASUK' ";	
			$this->newtable->search(array(array('A.NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'), 
											  array('A.NAMA_TRADER_ASAL', 'PEMASOK/PENGIRIM'),
											  array('A.CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')));	
		}else{
			$POSTURI = $this->input->post("uri");
			$judul = "Semua Dokumen";
			$SQL = "SELECT DISTINCT A.NOMOR_AJU AS 'Nomor Aju', 
					'BC281' AS 'Dokumen', DATE_FORMAT(A.CREATED_TIME, '%d %M %Y') AS 'Tanggal Dok.', A.NAMA_PEMASOK AS 'Pemasok/Pengirim', 
					f_jumbreak(A.NOMOR_AJU) AS 'DETIL BARANG BREAKDOWN'
					FROM T_BC23_HDR A, T_BREAKDOWN_PEMASUKAN B WHERE A.NOMOR_AJU=B.NOMOR_AJU";
			$SQL .=	$WHERE;	
			$SQL .= $func->main->getWhere($SQL,$POSTURI,array("A.NOMOR_AJU","A.NAMA_PEMASOK","A.CREATED_TIME"));
			$SQL .=	"UNION ALL
					SELECT DISTINCT A.NOMOR_AJU AS 'Nomor Aju', 
					'BC27' AS 'Dokumen', DATE_FORMAT(A.CREATED_TIME, '%d %M %Y') AS 'Tanggal Dok.', A.NAMA_TRADER_ASAL AS 'Pemasok/Pengirim',
					f_jumbreak(A.NOMOR_AJU) AS 'DETIL BARANG BREAKDOWN'
					FROM T_BC27_HDR A, T_BREAKDOWN_PEMASUKAN B WHERE A.NOMOR_AJU=B.NOMOR_AJU";
			$SQL .=	$WHERE." AND A.TIPE_DOK='MASUK'";
					
			$this->newtable->search(array(array('A.NOMOR_AJU', 'NOMOR PENGAJUAN&nbsp;&nbsp;'),
										      array('A.NAMA_TRADER_ASAL', 'PEMASOK/PENGIRIM&nbsp;&nbsp;'),
											  array('A.CREATED_TIME', 'TANGGAL DOK', 'tag-tanggal')
											  ));			
									  
		}		
		
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/breakdown/".$dokumen."");
		$this->newtable->detail(site_url()."/pemasukan/breakdown/priview");
		$this->newtable->detail_tipe("detil_priview_bottom");		
		$this->newtable->keys(array("Nomor Aju","Dokumen"));
		$this->newtable->show_chk(false);
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->orderby(3);
		$this->newtable->sortby("DESC");
		$this->newtable->set_formid("fbreakdown");
		$this->newtable->set_divid("divbreakdown");
		$this->newtable->rowcount(20);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);			
		$arrdata = array("title" => $title, "tabel" => $tabel);
		if($this->input->post("ajax")) return $tabel;				 
		else return $arrdata;			
	}
	
	function detil_breakdown($noaju,$jnsdok){
		$func = get_instance();
		$func->load->model("main");
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		if($jnsdok=="BC281"){
			$JNS_BARANG = "JENIS_BARANG";
			$jnsdok = "BC23";
		}
		else $JNS_BARANG = "JNS_BARANG";
		$SQL = "SELECT KODE_BARANG,URAIAN_BARANG,f_ref('ASAL_JENIS_BARANG',".$JNS_BARANG.") JENIS_BARANG, SERI, NOMOR_AJU,
				f_formaths(KODE_HS) KODE_HS, JUMLAH_SATUAN, CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) KODE_SATUAN,
				f_jumbreakdetil(NOMOR_AJU,SERI) JUMBREAK 
				FROM T_".$jnsdok."_DTL WHERE NOMOR_AJU= '".$noaju."' AND BREAKDOWN_FLAG='Y'";
		if($func->main->get_result($SQL)){
			$first=1;
			$second=2;
			$html = '<table class="tabelPopUp" width="100%" id="treeView">';
			$html.= '<thead>';
			$html.= '<tr>';
			$html.= '	<th>KODE BARANG</th>';
			$html.= '	<th>SERI</th>';
			$html.= '	<th>URAIAN BARANG</th>';
			$html.= '	<th>JENIS BARANG</th>';
			$html.= '	<th>JUMLAH SATUAN</th>';
			$html.= '	<th>KODE SATUAN</th>';
			$html.= '	<th>JUMLAH BREAKDOWN</th>';
			$html.= '</tr>';
			$html.= '</thead>';
			$html.= '</tbody>';
			foreach($SQL->result_array() as $parent){
				$html .= '<tr id="node-'.$first.'">';
				$html .= '<td>'.$parent["KODE_BARANG"].'</td>';
				$html .= '<td>'.$parent["SERI"].'</td>';
				$html .= '<td>'.$parent["URAIAN_BARANG"].'</td>';
				$html .= '<td>'.$parent["JENIS_BARANG"].'</td>';
				$html .= '<td>'.$parent["JUMLAH_SATUAN"].'</td>';
				$html .= '<td>'.$parent["KODE_SATUAN"].'</td>';
				$html .= '<td>'.$parent["JUMBREAK"].'</td>';
				$html .= '</tr>';
				
				$query = "SELECT NOMOR_AJU, SERI, KODE_BARANG, f_ref('ASAL_JENIS_BARANG',JNS_BARANG) JENIS_BARANG, JUMLAH, 
						  CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) KODE_SATUAN,
					      f_barang(KODE_BARANG,JNS_BARANG,'".$KODE_TRADER."') URAIAN_BARANG
						  FROM t_breakdown_pemasukan WHERE NOMOR_AJU='".$parent["NOMOR_AJU"]."' 
						  AND SERI='".$parent["SERI"]."'"; 
				if($func->main->get_result($query)){
					foreach($query->result_array() as $child){
						if($first>1) $second = $second+1;
						$html .= '<tr id="node-'.$second.'" class="child-of-node-'.$first.'">';						
						$html .= '<td style="background-color:#FFFFCC"><span class="file">'.$child["KODE_BARANG"].'</span></td>';
						$html .= '<td style="background-color:#FFFFCC">'.$child["SERI"].'</td>';
						$html .= '<td style="background-color:#FFFFCC">'.$child["URAIAN_BARANG"].'</td>';
						$html .= '<td style="background-color:#FFFFCC">'.$child["JENIS_BARANG"].'</td>';
						$html .= '<td style="background-color:#FFFFCC">'.$child["JUMLAH"].'</td>';
						$html .= '<td style="background-color:#FFFFCC" colspan="2">'.$child["KODE_SATUAN"].'</td>';
						$html .= '</tr>';
						$second++;
					}
				}
				$first = $second-1;
				$first++;
			}			
			$html .= '</tbody>';
			$html .= '</table>';
		}
		return $html;
	}
	
	function getInformasiBC($dok="",$car=""){		
		$main = get_instance();
		$main->load->model("main");	
		if($dok=="bc281") $dok = "bc23";
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');
		$query = "SELECT NOMOR_PENDAFTARAN, TANGGAL_DOK_PABEAN FROM T_".$dok."_HDR WHERE NOMOR_AJU='".$car."'";  			
		$hasil = $main->main->get_result($query);
		if($hasil){
			foreach($query->result_array() as $row){
				$dataarray = $row;
			}
			return $dataarray;
		}else{
			return "failed";	
		}
	}
	
	function setujui(){
		$func = get_instance();
		$func->load->model("main","main", true);
		$DOK = $this->input->post('DOK'); 
		$CAR = $this->input->post('CAR'); 
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		if($this->input->post('act')=="process"){
			$DATA_TEMP = $this->input->post('tb_chkfdraft');
			foreach($DATA_TEMP as $a => $b){
				$POST[$a] = $b;
			}
			$DATA_TEMP1 = explode("|",$POST[0]);
			$DOK = $DATA_TEMP1[0];
			$CAR = $DATA_TEMP1[1];
		}		
		foreach($this->input->post('DATA') as $a => $b){  
			$DATA[$a] = $b;   
		}
		if($DOK=="bc281") $DOK = "bc23";
		$DATA["STATUS"] = '07';  
		if(in_array(strtoupper($DOK),array("BC27","BC40"))) $STATUS = "f_status_".$DOK."('".$CAR."', 1,'".$KODE_TRADER."')";		
		else $STATUS = "f_status_".$DOK."('".$CAR."', 1)";	
		$STAT=$func->main->get_uraian("SELECT ".$STATUS." STAT FROM T_".strtoupper($DOK)."_HDR WHERE NOMOR_AJU='".$CAR."' AND KODE_TRADER='".$KODE_TRADER."'","STAT");
		if(strtoupper($STAT)=="LENGKAP"){  					
			$this->db->where(array('NOMOR_AJU' => $CAR, "KODE_TRADER"=>$KODE_TRADER));
			$exec = $this->db->update('T_'.strtoupper($DOK).'_HDR', $DATA);
			if($exec){				
				$func->main->activity_log('EDIT '.strtoupper($DOK),'UBAH STATUS KE SETUJUI, CAR='.$CAR);	
				$ret = "MSG#OK#Proses data Berhasil#".site_url()."/pemasukan/draft";			
			}else{
				$ret = "MSG#ERR#Proses data gagal";		
			}
		}else{
			$ret = "MSG#ERR#Data Belum lengkap, tidak dapat diproses";	
		}
		echo $ret;		
	}
}
?>
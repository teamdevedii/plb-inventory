<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Bahan_baku_act extends Model{	
    function addBB()
	{
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju=$this->input->post('NOMOR_AJU');
		$seri=$this->input->post('SERI');
		$KodeBrg = $this->input->post('KODE_BARANG_BB');
		foreach($this->input->post('BB') as $a => $b){				
			$arrinsertBB[$a] = $b;
		}
		$seriBB = (int)$func->main->get_uraian("SELECT MAX(SERI_BB) AS MAXSERI FROM t_bc41_bb WHERE NOMOR_AJU = '".$aju."' AND SERI='".$seri."'", "MAXSERI") + 1;
		$jumDta = (int)$func->main->get_uraian("SELECT COUNT(*) AS JUMREC FROM t_bc41_bb WHERE NOMOR_AJU = '".$aju."' AND SERI='".$seri."' AND KODE_BARANG_BB='".$KodeBrg."'", "JUMREC"); 
		$SQL1="SELECT KODE_BARANG FROM T_BC41_DTL WHERE NOMOR_AJU='".$aju."' 
			   AND SERI='".$seri."'";
		$getKodeBrg = $this->db->query($SQL1)->row(); 
		$getKodeBrg = $getKodeBrg->KODE_BARANG;//echo $KodeBrg;exit;
		$arrinsertBB["STATUS"] = '04';
		$arrinsertBB["KODE_BARANG_BB"] = $KodeBrg;
		$arrinsertBB["SERI"] = $seri;
		$arrinsertBB["SERI_BB"] = $seriBB;
		$arrinsertBB["NOMOR_AJU"] = $aju; 
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		$arrinsertBB["KODE_TRADER"] = $KODE_TRADER;	
		if($KodeBrg==$getKodeBrg){
			echo "MSG#ERR#Simpan data Barang Gagal, Kode Barang Sudah Pernah Digunakan.#";die();
		}if($jumDta > 0){
			echo "MSG#ERR#Simpan data Barang Gagal, Kode Barang Sudah Pernah Digunakan.#";die();
		}else{
		$exec = $this->db->insert('t_bc41_bb', $arrinsertBB);//die('sini');	
		if($exec){
				$func->main->activity_log('ADD BB BC41','CAR='.$aju.', SERI='.$seri.', SERI_BB='.$seriBB);
				echo "MSG#OK#Simpan data Barang Berhasil#";
			}else{					
				echo "MSG#ERR#Simpan data Barang Gagal#";die();
			}
		}
	}
	function editBB()
	{
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju=$this->input->post('NOMOR_AJU');
		$seri=$this->input->post('SERI');
		$seri_bb=$this->input->post('SERI_BB');
		$KodeBrg = $this->input->post('KODE_BARANG_BB');
		foreach($this->input->post('BB') as $a => $b){				
			$arrinsertBB[$a] = $b;
		}
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		$arrinsertBB["KODE_TRADER"] = $KODE_TRADER;	
		$arrinsertBB["KODE_BARANG_BB"] = $KodeBrg;		
		$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri, 'SERI_BB' => $seri_bb));
		$exec = $this->db->update('t_bc41_bb', $arrinsertBB);	
		if($exec){
			$func->main->activity_log('EDIT BB BC41','CAR='.$aju.', SERI='.$seri.', SERI_BB='.$seri_bb);
			echo "MSG#OK#Ubah data Barang Berhasil#";
		}else{					
			echo "MSG#ERR#Ubah data Barang Gagal#";die();
		}
	}
	function deleteBB()
	{//$key2;die();
		$func = get_instance();
		$func->load->model("main","main", true);
		foreach($this->input->post('tb_chkfbahan_baku') as $chkitem){
			$arrchk = explode(".", $chkitem);//print_r($arrchk);exit;
			$aju  = $arrchk[0];
			$key1 = $arrchk[1];
			$key2 = $arrchk[2];				
			$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $key1,'SERI_BB' => $key2));
			$exec = $this->db->delete('t_bc41_bb');	
			$func->main->activity_log('DELETE BB BC41','CAR='.$aju.', SERI='.$key1.', SERI_BB='.$key2);
		}
			
		if($exec){
			echo "MSG#OK#Hapus data Barang Berhasil#".site_url()."/pengeluaran/load_list_BB/bahan_baku/bc41/".$aju."/".$key1."#";
			//echo "MSG#OK#Hapus data Barang Berhasil#";
		}else{					
			echo "MSG#ERR#Hapus data Barang Gagal#";die();
		}

	}
	function listDataBB($aju,$seri,$key){
		$func = get_instance();
		$func->load->model("main","main", true);
		$sql = "SELECT * FROM t_bc41_bb
				WHERE NOMOR_AJU='".$aju."' AND SERI='".$seri."' AND SERI_BB='".$key."'";//echo $sql;exit;
		$hasil = $func->main->get_result($sql);
		if($hasil){
			foreach($sql->result_array() as $row){
				$dataarray = $row;
			}
		}	
		return $dataarray;
	}
}
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Barang_act extends CI_Model{
	
	function set_perbaikan($TYPE="",$DATA="",$BARANG=""){		
		if($DATA){
			foreach($DATA as $x => $y){
				$PERBAIKAN[$x] = $y;
			}	
			$PERBAIKAN["DOKUMEN"] = "BC41";
			$PERBAIKAN["KODE_TRADER"] = $this->newsession->userdata('KODE_TRADER');
			$PERBAIKAN["CREATED_BY"] = $this->newsession->userdata('USER_ID');
			$PERBAIKAN["CREATED_TIME"] = date("Y_m-d H:i:s");	
			switch(strtolower($TYPE)){
				case "save";$PERBAIKAN["AKSI"]="TAMBAH";break;
				case "update";$PERBAIKAN["AKSI"]="UBAH";break;
				case "delete";$PERBAIKAN["AKSI"]="HAPUS";break;			
			}
			if(strtolower($TYPE)=="delete"){
				$select = $this->db->get_where('t_bc41_dtl', $BARANG);
				$DTL = $select->result_array();
				if($select->num_rows()){
					$this->db->insert('t_bc41_perubahan', array_merge($PERBAIKAN,$DTL[0]));
				}
			}else{				
				$this->db->insert('t_bc41_perubahan', array_merge($PERBAIKAN,$BARANG));		
			}
		}		
	}	
	
	function set_barang($type="", $isajax=""){
		#REVISI====================================		
		$flagrevisi=$this->input->post('flagrevisi');
		#==========================================	
		$func = get_instance();
		$func->load->model("main","main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$JUMLAH_SATUAN = $this->input->post('JUMLAH_SATUAN');
		$KODE_BARANG=$this->input->post('KODE_BARANG');
		$JNS_BARANG=$this->input->post('JNS_BARANG');
		
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		$BARANG["KODE_TRADER"] = $KODE_TRADER;	  
		$BARANG["NOMOR_AJU"] = $aju;
		if($type=="save" || $type=="update"){
			foreach($this->input->post('BARANG') as $a => $b){
				$BARANG[$a] = $b;
			}
			
			$SQLQUERY = $func->main->get_uraian("SELECT KODE_BARANG FROM m_trader_barang WHERE KODE_BARANG = '".$KODE_BARANG."' 
											  AND KODE_TRADER = '".$KODE_TRADER."' AND JNS_BARANG = '".$JNS_BARANG."'", "KODE_BARANG");
			if($SQLQUERY==""){
				echo "MSG#ERR#Kode Barang (".$KODE_BARANG.") tidak terdaftar.";die();
			}
			
			/*$SQL1="SELECT STOCK_AKHIR FROM M_TRADER_BARANG WHERE KODE_BARANG='$KODE_BARANG'
				   AND JNS_BARANG='$JNS_BARANG' AND KODE_TRADER=".$this->newsession->userdata('KODE_TRADER');
			$jumlahStock=$this->db->query($SQL1)->row(); $jumlahStock = $jumlahStock->STOCK_AKHIR;	*/
			if($type=="save"){				
				$status = "04";				
				$seri = (int)$func->main->get_uraian("SELECT MAX(SERI) AS MAX FROM t_bc41_dtl WHERE NOMOR_AJU = '".$aju."' AND KODE_TRADER='".$KODE_TRADER."'", "MAX") + 1;   
				$BARANG["JUMLAH_SATUAN"] = $JUMLAH_SATUAN;
				$BARANG["KODE_BARANG"] = $KODE_BARANG;
				$BARANG["JNS_BARANG"] = $JNS_BARANG;
				$BARANG["SERI"] = $seri;
				$BARANG["KODE_HS"] = str_replace(".","",$BARANG["KODE_HS"]);
				/*if($jumlahStock < $JUMLAH_SATUAN){
					echo "MSG#ERR#Simpan data Barang Gagal. Stock Barang anda tidak mencukupi, jumlah Stock yang ada adalah $jumlahStock#";
				}else{*/
					$BARANG["STATUS"] = $status;
					$exec = $this->db->insert('t_bc41_dtl', $BARANG);			
					if($exec){
						$this->set_perbaikan("save",$this->input->post('PERBAIKAN'), $BARANG);
						$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc41('".$aju."'),0) AS JUM FROM DUAL", "JUM");  
						$HARGA = (int)$func->main->get_uraian("SELECT IFNULL(f_jumcif_bc41('".$aju."'),0) AS HARGA FROM DUAL", "HARGA");        
						$this->db->where(array('NOMOR_AJU' => $aju));
						$this->db->update('t_bc41_hdr', array("NETTO" => $netto,"HARGA_PENYERAHAN" => $HARGA));
						$func->main->activity_log('ADD DETIL BC41','CAR='.$aju.', SERI='.$seri);
						#REVISI====================================================================
						if($flagrevisi){
							$func->load->model("tools_act");
							$alasan = $this->input->post('ALASAN');
							$BARANG['TIPE_REVISI'] = "ADD";
							$func->tools_act->eksekusirevisidokumen('databarang','BC41',$aju,$alasan,$BARANG);
						}
						#==========================================================================		
						echo "MSG#OK#Simpan data Barang Berhasil#edit#".site_url()."/pengeluaran/LoadHeader/bc41/".$aju."#";
					}else{					
						echo "MSG#ERR#Simpan data Barang Gagal#edit#";
					}
				//}
			}else{							
				$seri = $this->input->post('seri');
				$BARANG["STATUS"] = '04';
				$BARANG["NOMOR_AJU"] = $aju;
				$BARANG["JUMLAH_SATUAN"] = $JUMLAH_SATUAN;
				$BARANG["KODE_BARANG"] = $KODE_BARANG;
				$BARANG["JNS_BARANG"] = $JNS_BARANG;	
				$BARANG["KODE_HS"] = str_replace(".","",$BARANG["KODE_HS"]);
				#REVISI====================================================================
				if($flagrevisi){
					$SQL = "SELECT JUMLAH_SATUAN, KODE_BARANG, JNS_BARANG, SERI FROM T_BC41_DTL 
							WHERE NOMOR_AJU='".$aju."' AND SERI='".$seri."' AND KODE_TRADER='".$KODE_TRADER."'";  											
					$RD=$this->db->query($SQL);
					if($RD->num_rows()>0){
						$DATAREV = $RD->row();
						$BARANG_ASLI['KODE_BARANG_ASLI'] = $DATAREV->KODE_BARANG;
						$BARANG_ASLI['JENIS_BARANG_ASLI'] = $DATAREV->JNS_BARANG;
						$BARANG_ASLI['JUMLAH_SATUAN_ASLI'] = $DATAREV->JUMLAH_SATUAN;
						$BARANG_ASLI['SERI'] = $DATAREV->SERI;
					} 
				}
				#=========================================================================
				/*if($jumlahStock < $JUMLAH_SATUAN){
					echo "MSG#ERR#Update data Barang Gagal. Stock Barang anda tidak mencukupi, jumlah Stock yang ada adalah $jumlahStock#";
				}else{	*/				
					$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri, "KODE_TRADER"=>$KODE_TRADER));
					$exec = $this->db->update('t_bc41_dtl', $BARANG);
					if($exec){
						$this->set_perbaikan("update",$this->input->post('PERBAIKAN'), $BARANG);
						$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc41('".$aju."'),0) AS JUM FROM DUAL", "JUM");     
						$HARGA = (int)$func->main->get_uraian("SELECT IFNULL(f_jumcif_bc41('".$aju."'),0) AS HARGA FROM DUAL", "HARGA");        
						$this->db->where(array('NOMOR_AJU' => $aju));
						$this->db->update('t_bc41_hdr', array("NETTO" => $netto,"HARGA_PENYERAHAN" => $HARGA));
						$func->main->activity_log('EDIT DETIL BC41','CAR='.$aju.', SERI='.$seri);
						#REVISI====================================================================
						if($flagrevisi){
							$func->load->model("tools_act");
							$alasan = $this->input->post('ALASAN');
							$BARANG['TIPE_REVISI'] = "EDIT";
							$func->tools_act->eksekusirevisidokumen('databarang','BC41',$aju,$alasan,array_merge($BARANG_ASLI,$BARANG));
						}
						#=========================================================================
						echo "MSG#OK#Update data Barang Berhasil#edit#".site_url()."/pengeluaran/LoadHeader/bc41/".$aju."#";
					}else{					
						echo "MSG#ERR#Update data Barang Gagal#edit#";
					}
				//}
			}
		}else if($type=="delete"){	
			#REVISI====================================		
			$flagrevisi=$this->input->post('JENIS_REVISI');
			#==========================================			
			foreach($this->input->post('tb_chkfbarang') as $chkitem){
				$arrchk = explode(".", $chkitem);

				$aju  = $arrchk[0];
				$seri = $arrchk[1];	
				#REVISI====================================	
				if($flagrevisi){					
					$SQL = "SELECT JUMLAH_SATUAN, KODE_BARANG, JNS_BARANG, SERI FROM T_BC41_DTL 
							WHERE NOMOR_AJU='".$aju."' AND SERI='".$seri."' AND KODE_TRADER='".$KODE_TRADER."'";  											
					$RD=$this->db->query($SQL);
					if($RD->num_rows()>0){
						$DATAREV = $RD->row();
						$BARANG['KODE_BARANG'] = $DATAREV->KODE_BARANG;
						$BARANG['JNS_BARANG'] = $DATAREV->JNS_BARANG;
						$BARANG['JUMLAH_SATUAN'] = $DATAREV->JUMLAH_SATUAN;
						$BARANG['SERI'] = $DATAREV->SERI;
					}						
				}
				#==========================================	
				//$this->set_perbaikan("delete",$this->input->post('PERBAIKAN'),array('NOMOR_AJU'=>$aju,'SERI'=>$seri));		
				$this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri,'KODE_TRADER' => $KODE_TRADER));
				$exec = $this->db->delete('t_bc41_dtl');
				#REVISI====================================	
				if($flagrevisi){	
					if($exec){
						$func->load->model("tools_act");
						$alasan = $this->input->post('ALASAN');
						$BARANG['TIPE_REVISI'] = "DELETE";
						$func->tools_act->eksekusirevisidokumen('databarang','BC41',$aju,$alasan,$BARANG);
					}
				}
				#==========================================	
				$func->main->activity_log('DELETE DETIL BC41','CAR='.$aju.', SERI='.$seri);
			}
			if($exec){
				$netto = (int)$func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc41('".$aju."'),0) AS JUM FROM DUAL", "JUM");     
				$HARGA = (int)$func->main->get_uraian("SELECT IFNULL(f_jumcif_bc41('".$aju."'),0) AS HARGA FROM DUAL", "HARGA");        
				$this->db->where(array('NOMOR_AJU' => $aju));
				$this->db->update('t_bc41_hdr', array("NETTO" => $netto,"HARGA_PENYERAHAN" => $HARGA));
				echo "MSG#OK#Hapus data Barang Berhasil#".site_url()."/pengeluaran/detil/barang/bc41/".$aju."#".site_url()."/pemasukan/LoadHeader/bc41/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Barang Gagal#";die();
			}
		}
	}
		
	function get_barang($aju="",$seri=""){		
		$data = array();		
		$conn = get_instance();
		$conn->load->model("main","main", true);	
		$KODETRADER = $this->newsession->userdata('KODE_TRADER');	
		if($aju && $seri){
			$query = "SELECT A.*,f_Satuan(A.KODE_SATUAN) 'URKODE_SATUAN', IFNULL(f_jumbb_bc41(A.SERI,A.NOMOR_AJU),0)  AS 'JUM_BB', 
					  f_kemas(A.KODE_KEMASAN) 'URKODE_KEMASAN', f_satuan(A.KODE_SATUAN) AS URAIAN_SATUAN,
					  B.KODE_SATUAN AS KD_SAT_BESAR,
					  B.KODE_SATUAN_TERKECIL AS KD_SAT_KECIL, C.TANGGAL_REALISASI
					  from t_bc41_dtl A 
					  LEFT JOIN M_TRADER_BARANG B  ON A.KODE_BARANG=B.KODE_BARANG AND A.KODE_TRADER=B.KODE_TRADER
					  INNER JOIN t_bc41_hdr C ON A.KODE_TRADER = C.KODE_TRADER AND A.NOMOR_AJU = C.NOMOR_AJU
					  WHERE A.NOMOR_AJU = '".$aju."' AND A.SERI = '".$seri."'
					  AND A.KODE_TRADER=B.KODE_TRADER
					  AND B.KODE_TRADER='".$KODETRADER."'";			
			$hasil = $conn->main->get_result($query);								
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess'=>$row, 'aju'=>$aju, 'seri'=>$seri);		
				}
			}
		}else{
			$data = array('act' => 'save','aju'=>$aju);
		}
			
		$data = array_merge($data, array('jenis_komoditi' => $conn->main->get_mtabel('JENIS_BARANG'),
										 'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_KIRIM'),
										 'penggunaan' => $conn->main->get_mtabel('PENGGUNAAN_BARANG'),										
										 'komoditi_cukai' => $conn->main->get_mtabel('KOMODITI_CUKAI'),
										 'jenis_tarif' => $conn->main->get_mtabel('JENIS_TARIF_BM',1,TRUE,"AND KODE IN ('1','2')"),
										 'kode_bm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppn' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_ppnbm' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_pph' => $conn->main->get_mtabel('STATUS_BAYAR'),
										 'kode_cukai' => $conn->main->get_mtabel('STATUS_BAYAR')));
		return $data;
			
	}
}
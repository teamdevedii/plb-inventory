<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Print_act extends Model{
	
	function print_dok($aju=""){
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$this->load->library('fungsi');
		//surat
		$SQL = "SELECT NOMOR_AJU, f_kpbc(KANTOR_TUJUAN) KANTOR_TUJUAN, KODE_TRADER, NO_PERMOHONAN, TGL_PERMOHONAN, LAMPIRAN, 
				PERIHAL, TUJUAN_PERMOHONAN, URAIAN_SURAT, NAMA_PEMOHON, NOID_PEMOHON, NO_SRT_TUGAS, TELP_PEMOHON, EMAIL_PEMOHON 
				FROM m_trader_permohonan WHERE NOMOR_AJU='".$aju."'";
				//echo $SQL;die();
		$SURAT = $conn->main->get_result($SQL);
		if($SURAT){
			foreach($SQL->result_array() as $row){
				$dataSurat = $row;
			}
			if($dataSurat["LAMPIRAN"]){	
				$dataSurat["LAMPIRAN"] = "'".str_replace(",","','",$dataSurat["LAMPIRAN"])."'";		
				$SQL = "SELECT URAIAN FROM M_TABEL WHERE JENIS='LAMPIRAN' AND KODE IN (".$dataSurat["LAMPIRAN"].") ORDER BY KODE ASC ";
				$lam = $conn->main->get_result($SQL);
				if($lam){
					$LIST = '<ol type="1" style="margin:0px 0 1px 20px">';
					foreach($SQL->result_array() as $DATA){
						$LIST .= "<li>".ucwords(strtolower($DATA["URAIAN"]))."</li>";
					}
					$LIST .= '</ol>';
				}
			}	
			$SQL = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON, 
					A.KODE_API, A.NOMOR_API, A.EDINUMBER, A.PASSWORD, A.LAST_AJU, A.NAMA_TTD, A.KOTA_TTD, A.TIPE_TRADER 'STATUS_TRADER',
					A.FAX, A.JENIS_TPB 'KODE_TPB', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', C.EMAIL 'EMAIL_CP',ID AS 'ID_TRADER' 
					FROM M_TRADER A, T_USER C WHERE A.KODE_TRADER=C.KODE_TRADER
					AND A.KODE_TRADER='".$dataSurat["KODE_TRADER"]."'";
					//echo $SQL;die();
			$hasil = $conn->main->get_result($SQL);
			if($hasil){
				foreach($SQL->result_array() as $row){
					$TRADER = $row;
				}
			}	
			$JMLLAMPIRAN = count(explode(",",$dataSurat["LAMPIRAN"]))." Lembar";
			if($JMLLAMPIRAN !=''){
				$SQL ="SELECT A.KODE_BARANG, f_ref('KONDISI_BARANG', A.KONDISI) URKONDISI, URAIAN_BARANG, JUMLAH 
					   FROM m_trader_permohonan_barang A 
					   LEFT JOIN M_TRADER_BARANG B ON A.KODE_BARANG=B.KODE_BARANG
					   WHERE A.NOMOR_AJU='".$aju."'";	
				//echo $SQL;die();
				$hasilTotLampiran = $conn->main->get_result($SQL);
				if($hasilTotLampiran){
					$lampiranbarang = $SQL->result_array();	
				}
				
			}
		}
		
		//lampiran
		
		if($aju!=''){
			$queryHeader = "Select *,
								f_kpbc(KODE_KPBC) URAIAN_KPBC,
								f_valuta(KODE_VALUTA) URAIAN_VALUTA,
								f_ref('TUJUAN_KIRIMBC41',TUJUAN_KIRIM) URTUJUAN_KIRIM,
								f_ref('JENIS_TPB',JENIS_TPB) URJENIS_TPB,JENIS_SARANA_ANGKUT
							from t_bc41_hdr where nomor_aju = '$aju' AND KODE_TRADER='".$KODE_TRADER."' ";
			$queryDok = "select
								(SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN=217 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS NOMOR_PACKING_LIST,
								(SELECT TANGGAL FROM T_BC41_DOK WHERE KODE_DOKUMEN=217 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS TANGGAL_PACKING_LIST,
								(SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN=315 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS NOMOR_KONTRAK,
								(SELECT TANGGAL FROM T_BC41_DOK WHERE KODE_DOKUMEN=315 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS TANGGAL_KONTRAK,
								(SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN=911 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS NOMOR_SURAT_KEPUTUSAN,
								(SELECT TANGGAL FROM T_BC41_DOK WHERE KODE_DOKUMEN=911 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS TANGGAL_SURAT_KEPUTUSAN,
								(SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN=999 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS NOMOR_LAINNYA,
								(SELECT TANGGAL FROM T_BC41_DOK WHERE KODE_DOKUMEN=999 AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ) AS TANGGAL_LAINNYA															
						 from T_BC41_DOK  where nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."'  GROUP BY NOMOR_AJU";//echo $queryDok;die();
						 
			$queryDokLampiran = "SELECT 
									f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS JENIS_DOKUMEN, NOMOR AS NOMOR_DOKUMEN,
									DATE_FORMAT(TANGGAL,'%d %M %Y') AS TANGGAL_DOKUMEN 
								FROM t_bc41_dok	WHERE NOMOR_AJU= '$aju' AND KODE_TRADER='".$KODE_TRADER."'";
						 
		    $queryKms = "select *,
							f_kemas(KODE_KEMASAN) AS URKODE_KEMASAN
						from t_bc41_kms  where nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ";//echo $queryKms;die();
						
			$queryBrg = "select a.*,							
							f_ref('PENGGUNAAN_BARANG', PENGGUNAAN) URAIAN_PENGGUNAAN,  
							f_satuan(KODE_SATUAN) URAIAN_SATUAN, f_kemas(KODE_KEMASAN) URAIAN_KEMASAN,
							f_ref('ASAL_JENIS_BARANG',JNS_BARANG) JENIS_BARANG,
							f_satuan(KODE_SATUAN) URKODE_SATUAN
						from t_bc41_dtl a where a.nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ";//echo $queryBrg;die();
						
			$queryBhnBaku = "SELECT KODE_SATUAN_BB FROM t_bc41_bb WHERE NOMOR_AJU= '$aju' AND KODE_TRADER='".$KODE_TRADER."' ";
			
			$SQLPACKING_LIST = $this->db->query("SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN='217' AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ");	
			$SQLKONTRAK = $this->db->query("SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN='315' AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ");	
			$SQLSURAT_KEPUTUSAN = $this->db->query("SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN='911' AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ");	
			$SQLLAINNYA = $this->db->query("SELECT NOMOR FROM T_BC41_DOK WHERE KODE_DOKUMEN='999' AND nomor_aju='$aju' AND KODE_TRADER='".$KODE_TRADER."' ");	
			$SQLDOKASAL = $this->db->query("SELECT NOMOR_DAFTAR AS NOMOR_BC40_ASAL, DATE_FORMAT(TANGGAL_DAFTAR,'%d %M %Y') AS TANGGAL_BC40_ASAL FROM T_BC41_DOKASAL WHERE NOMOR_AJU= '$aju' AND KODE_TRADER='".$KODE_TRADER."'");	
			
			$arraytmp = array();			
			if($SQLPACKING_LIST->num_rows()>1){
				$arraytmp[] = 'Y';	
			}
			if($SQLKONTRAK->num_rows()>1){
				$arraytmp[] = 'Y';	
			}
			if($SQLSURAT_KEPUTUSAN->num_rows()>1){
				$arraytmp[] = 'Y';	
			}
			if($SQLLAINNYA->num_rows()>1){
				$arraytmp[] = 'Y';	
			}	
			
			
			$hasilHeader = $conn->main->get_result($queryHeader);
			if($hasilHeader){
				foreach($queryHeader->result_array() as $row){
					$dataHeader = $row;
				}
			}
			if($SQLDOKASAL->num_rows()>1){
				$lihatlampiran_dokasal = "==lihat lampiran dokumen==";	
				$queryDokLampiran .= "UNION SELECT 'DOKUMEN ASAL BC 4.0' AS JENIS_DOKUMEN, NOMOR_DAFTAR AS NOMOR_DOKUMEN, 
								DATE_FORMAT(TANGGAL_DAFTAR,'%d %M %Y') AS TANGGAL_DOKUMEN 
								FROM T_BC41_DOKASAL WHERE NOMOR_AJU= '$aju' AND KODE_TRADER='".$KODE_TRADER."'";
			}
			elseif($SQLDOKASAL->num_rows()==1){
				$DOKAS = $SQLDOKASAL->row();
				$dataHeader = array_merge($dataHeader,array("NOMOR_BC40_ASAL"=>$DOKAS->NOMOR_BC40_ASAL,"TANGGAL_BC40_ASAL"=>$DOKAS->TANGGAL_BC40_ASAL));
			}	
			$dataDok = array();
			if(!in_array('Y',$arraytmp)){
				$hasilDok = $conn->main->get_result($queryDok);
				if($hasilDok){
					foreach($queryDok->result_array() as $rowDok){
						$dataDok = $rowDok;
					}
				}	
			}else{
				$lihatlampiran = "==lihat lampiran==";	
			}
			
						
			$hasilDokLampiran = $conn->main->get_result($queryDokLampiran);
			$hasilKms = $conn->main->get_result($queryKms);
			$hasilBrg = $conn->main->get_result($queryBrg);
			$hasilBhnBaku = $conn->main->get_result($queryBhnBaku);
			
			/*if($hasilDok){
				foreach($queryDok->result_array() as $rowDok){
					$dataDok = $rowDok;
				}
			}*/
			if($hasilDokLampiran){
				$dokumen = $queryDokLampiran->result_array();
			}
			if($hasilKms){
				$dtkmss = $queryKms->result_array();
				foreach($queryKms->result_array() as $rowKms){
					$dataKms = $rowKms;
				}
			}
			if($hasilBrg){
				$barang = $queryBrg->result_array();
				foreach($queryBrg->result_array() as $rowBrg){
					$dataBrg = $rowBrg;
				}
			}
		}
		$data = array_merge($data,
								 array('DATA' => $dataHeader,
									   'DATADOK' => $dataDok,
									   'DOKUMEN' => $dokumen,
									   'DATAKMS' => $dataKms,
								       'KEMASAN' => $dtkmss,
									   'BARANG' => $barang,
									   'DATABRG' => $dataBrg,
									   'LAMPIRAN' => $LIST,
									   'TRADER' => $TRADER,
									   'SURAT' => $dataSurat,
									   'JMLLAMPIRAN' => $JMLLAMPIRAN,
									   'LAMPIRANBARANG' => $lampiranbarang,
									   'BAHANBAKU' => $hasilBhnBaku,
									   'lihatlampiran'=>$lihatlampiran,
									   'lihatlampiran_dokasal'=>$lihatlampiran_dokasal,
									   'TGL' => $this->fungsi->FormatDate($dataSurat["TGL_PERMOHONAN"])));
		return $data;				
	}	
}
?>

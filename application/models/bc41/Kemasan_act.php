<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kemasan_act extends CI_Model{
	function set_kemasan($type="", $isajax=""){
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		$arrinsert["KODE_TRADER"] = $KODE_TRADER;	
		if($type=="save" || $type=="update"){
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('KEMASAN') as $a => $b){
				$arrinsert[$a] = $b;
			}				
			if($type=="save"){
				$arrinsert["NOMOR_AJU"] = $aju;
				$SQL = "SELECT KODE_KEMASAN FROM t_bc23_kms WHERE KODE_KEMASAN='".$arrinsert["KODE_KEMASAN"]."' AND NOMOR_AJU='".$aju."'";
				$data = $this->db->query($SQL);
				if($data->num_rows() > 0){
					echo "MSG#ERR#Jenis Kemasan Sudah ada#";die();
				}
				$exec = $this->db->insert('t_bc41_kms', $arrinsert);
				if($exec){
					$func->main->activity_log('ADD KEMASAN BC41','CAR='.$aju.', KODE_KEMASAN='.$arrinsert["KODE_KEMASAN"]);
					if ($this->input->post('flagrevisi')) {
						$alasan = $this->input->post('ALASAN');
						$func->main->revisi_log('REVISI DOKUMEN PABEAN BC41','ADD KEMASAN BC41, CAR='.$aju,$alasan);
					}
					echo "MSG#OK#Simpan data Kemasan Berhasil#edit#";
				}else{					
					echo "MSG#ERR#Simpan data Kemasan Gagal#edit#";
				}	
			}else{
				$kode_kemas = $this->input->post('kode_kemas');	
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas));
				$exec=$this->db->update('t_bc41_kms', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT KEMASAN BC41','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
					if ($this->input->post('flagrevisi')) {
						$alasan = $this->input->post('ALASAN');
						$func->main->revisi_log('REVISI DOKUMEN PABEAN BC41','EDIT KEMASAN BC41, CAR='.$aju,$alasan);
					}
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkemasan') as $chkitem){
				$arrchk = explode(".", $chkitem);
				$aju  = $arrchk[0];
				$kode_kemas = $arrchk[1];				
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas));
				$exec = $this->db->delete('t_bc41_kms');	
				$func->main->activity_log('DELETE KEMASAN BC41','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
			}
			if($exec){
				echo "MSG#OK#Hapus data Kemasan Berhasil#".site_url()."/pengeluaran/detil/kemasan/bc41/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Kemasan Gagal#del#";die();
			}
		}
	}
	
	
	 function get_kemasan($aju="", $kode_kemas=""){
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		if($aju && $kode_kemas){
			$query = "Select *,f_kemas(kode_kemasan) URAIAN_KEMASAN from t_bc41_kms where nomor_aju = '$aju' AND kode_kemasan = '$kode_kemas'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update',
								  'sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save');
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,
						  				 'kode_kemas' => $kode_kemas));
		return $data;
	}
}
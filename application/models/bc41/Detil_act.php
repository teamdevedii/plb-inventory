<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Detil_act extends CI_Model{
	
	function list_detil($tipe="",$aju=""){
		$KODE_TRADER = $this->newsession->userdata("KODE_TRADER");
		$this->load->library('newtable');	
		if($tipe=="barang"){
			$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
					f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
					JUMLAH_SATUAN AS 'JUM. SATUAN',CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', 
					JUMLAH_KEMASAN AS 'JUM. KEMASAN',CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
					f_ref('STATUS',STATUS) AS 'STATUS' FROM t_bc41_dtl WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$KODE_TRADER."'";
					
			$this->newtable->search(array(array('KODE_HS', 'KODE HS'), array('URAIAN_BARANG', 'URAIAN BARANG')));
			
		}else if($tipe=="kemasan"){
			$SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', MERK_KEMASAN AS 'MEREK',
			        NOMOR_AJU FROM t_bc41_kms WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$KODE_TRADER."'";	
					
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));
			
		}else if($tipe=="dokumen"){
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', NOMOR AS 'NOMOR DOKUMEN',
					DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', NOMOR_AJU FROM t_bc41_dok  
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$KODE_TRADER."'";
					
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));	
							
		}else{
			return "Failed";exit();
		}
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");		
		$this->newtable->action(site_url()."/pemasukan/listdetil/".$tipe."/".$aju."/bc41");
		$this->newtable->hiddens('NOMOR_AJU','SERI');			
		$this->newtable->cidb($this->db);
		$this->newtable->ciuri($ciuri);
		$this->newtable->set_formid("f".$tipe);
		$this->newtable->set_divid("div".$tipe);
		$this->newtable->orderby(2);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(10);
		$this->newtable->show_chk(false);
		$this->newtable->clear(); 
		$this->newtable->menu($prosesnya);
		$tabel .= $this->newtable->generate($SQL);		
		return $tabel;
	}  
		
	function list_tab($aju=""){
		$content='<div id="tab">
					<ul class="tab">
					  <li><a href="#tab-1">Data Barang</a></li>
					  <li><a href="#tab-2">Data Kemasan</a></li>
					  <li><a href="#tab-4">Data Dokumen</a></li>
					</ul>
					<div id="tab-1">'.$this->list_detil("barang",$aju).'</div>
					<div id="tab-2">'.$this->list_detil("kemasan",$aju).'</div>
					<div id="tab-4">'.$this->list_detil("dokumen",$aju).'</div>
				</div>';	
		return $content;			
	}
	
	
	function detil($type="",$dokumen="",$aju="",$action="",$PERBAIKAN=""){
		$func = get_instance();
		$func->load->model("main");
		$this->load->library('newtable');
		$SERI=$this->uri->segment(6);
		if($type=="barang"){
			$judul = "Daftar Detil Barang";			
			$SQL = "SELECT KODE_BARANG 'KODE BARANG',URAIAN_BARANG 'URAIAN BARANG', f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',
					f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
					JUMLAH_SATUAN AS 'JUM. SATUAN',CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', 
					JUMLAH_KEMASAN AS 'JUM. KEMASAN', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
			        IFNULL(f_jumbb_bc41(SERI,NOMOR_AJU),0)  AS 'JUM.BAHAN BAKU',
					f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU FROM t_bc41_dtl WHERE NOMOR_AJU= '$aju'";
					
			$this->newtable->search(array(array('KODE_BARANG','KODE BARANG'), 
					array('URAIAN_BARANG', 'URAIAN BARANG')));		
					
		}else if($type=="bahan_baku"){
			$judul = "Daftar Detil Bahan Baku";	
			$SQL = "SELECT KODE_HS_BB AS 'KODE HS', KODE_BARANG_BB AS 'KODE BARANG', URAIAN_BARANG_BB AS'URAIAN BARANG', MERK_BB AS 'MERK', 
					KODE_SATUAN_BB AS 'KODE_SATUAN', JUMLAH_SATUAN_BB AS 'JUMLAH SATUAN',NOMOR_AJU,SERI,SERI_BB
					FROM t_bc41_bb WHERE NOMOR_AJU= '$aju' AND SERI='$SERI'";
					
			$this->newtable->search(array(array('KODE_BARANG_BB', 'KODE BARANG'), array('URAIAN_BARANG_BB', 'URAIAN BARANG')));			
					
		}else if($type=="kemasan"){
			$judul = "Daftar Detil Kemasan";	
			$SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', MERK_KEMASAN AS 'MEREK',
			        NOMOR_AJU, KODE_KEMASAN AS 'SERI' FROM t_bc41_kms WHERE NOMOR_AJU= '$aju'";	
					
			$this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));			
					
		}else if($type=="dokumen"){
			$judul = "Daftar Detil Dokumen";	
			$SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', NOMOR AS 'NOMOR DOKUMEN',
					DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN',
					REPLACE(REPLACE(NOMOR,'.','|T|'),'/','|S|') AS 'SERI', 
					KODE_DOKUMEN AS SERI_HS, 
					NOMOR_AJU FROM t_bc41_dok  
					WHERE NOMOR_AJU= '$aju'";	
			
			$this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));			
								
		}else{
			return "Failed";exit();
		}		
		$ciuri = (!$this->input->post("ajax"))?$this->uri->segment_array():$this->input->post("uri");
		$this->newtable->action(site_url()."/pemasukan/detil/".$type."/bc41/".$aju);
		$this->newtable->cidb($this->db);
		$this->newtable->tipe_proses('button');
		$this->newtable->ciuri($ciuri);
		$this->newtable->orderby(4);
		$this->newtable->sortby("DESC");
		$this->newtable->rowcount(20);
		$this->newtable->clear();  
		$this->newtable->set_formid("f".$type);		
		$this->newtable->set_divid("div".$type);
		
		if($PERBAIKAN){			
			$FPERBAIKAN ='<form id="fperbaikan_">
			<input type="hidden" name="PERBAIKAN[KODE_KPBC]" id="PERBAIKAN_KODE_KPBC" value="'.$PERBAIKAN["KODE_KPBC"].'"/>
			<input type="hidden" name="PERBAIKAN[NOMOR_SURAT]" id="PERBAIKAN_NOMOR_SURAT" value="'.$PERBAIKAN["NOMOR_SURAT"].'"/>
			<input type="hidden" name="PERBAIKAN[TANGGAL_SURAT]" id="PERBAIKAN_TANGGAL_SURAT" value="'.$PERBAIKAN["TANGGAL_SURAT"].'"/>
			<input type="hidden" name="PERBAIKAN[NAMA_PEJABAT]" id="PERBAIKAN_NAMA_PEJABAT" value="'.$PERBAIKAN["NAMA_PEJABAT"].'"/>
			<input type="hidden" name="PERBAIKAN[JABATAN]" id="PERBAIKAN_JABATAN" value="'.$PERBAIKAN["JABATAN"].'"/>
			<input type="hidden" name="PERBAIKAN[NIP]" id="PERBAIKAN_NIP" value="'.$PERBAIKAN["NIP"].'"/>
			</form>';
			echo $FPERBAIKAN;
		}
		
		if($type=='barang'){
			$this->newtable->keys(array("NOMOR_AJU","SERI"));
		}elseif($type=='bahan_baku'){
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERI_BB"));
			$this->newtable->hiddens(array("NOMOR_AJU","SERI","SERI_BB"));
		}elseif($type=='kemasan'){
			$this->newtable->hiddens('SERI');
			$this->newtable->keys(array("NOMOR_AJU","SERI"));
		}elseif($type=='kontainer'){
			$this->newtable->hiddens('SERI');
			$this->newtable->keys(array("NOMOR_AJU","SERI"));	
		}elseif($type=='dokumen'){
			$this->newtable->hiddens('SERI');
			$this->newtable->keys(array("NOMOR_AJU","SERI","SERI_HS"));
		}
		$this->newtable->hiddens('NOMOR_AJU','SERI');
		if($type=='bahan_baku'){
		$tipe='barang';
		$process = array('Tambah' => array('GET2POP', site_url().'/pengeluaran/'.$type.'/bc41/add/'.$aju.'/'.$SERI, '0', 'tbl_add.png'),
						 'Ubah' => array('GET2POP', site_url().'/pengeluaran/'.$type.'/bc41/edit', '1', 'tbl_edit.png'),
						 'Hapus' => array('DELETE', site_url().'/pengeluaran/'.$type.'/bc41/delete', 'bahan_baku_bc41', 'tbl_delete.png'));
		}else{		
		$process = array('Tambah' => array('ADD', site_url().'/pengeluaran/'.$type.'/bc41/'.$aju, '0', 'tbl_add.png'),
					     'Ubah' => array('EDIT', site_url().'/pengeluaran/'.$type.'/bc41', 'N', 'tbl_edit.png'),
						 'Hapus' => array('DEL', site_url().'/pengeluaran/'.$type.'/bc41', 'N', 'tbl_delete.png'));	
		}
		$this->newtable->menu($process);
		$tabel .= $this->newtable->generate($SQL);			
		$arrdata = array("judul" => $judul,
						 "tabel" => $tabel);
		if($this->input->post("ajax")) return $tabel;				 
		else return $arrdata;
	}  
}
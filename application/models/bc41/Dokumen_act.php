<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Dokumen_act extends CI_Model{
	function set_dokumen($type="", $isajax=""){
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$KODE_TRADER = $this->newsession->userdata('KODE_TRADER');	
		$arrinsert["KODE_TRADER"] = $KODE_TRADER;	
		if($type=="save" || $type=="update"){
			foreach($this->input->post('DOKUMEN') as $a => $b){
				$arrinsert[$a] = $b;
			}
			if($type=="save"){
				$kode_dokAct = $this->input->post('KODE_DOKUMEN');
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(*) AS JUM FROM T_BC41_DOK WHERE NOMOR_AJU='".$aju."' 
														   AND KODE_DOKUMEN = '".$kode_dokAct."' AND NOMOR='".$arrinsert["NOMOR"]."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;
				$arrinsert["KODE_DOKUMEN"] = $kode_dokAct;
				$NOMOR = $arrinsert["NOMOR"];
				if($countKode > 0){
					echo "MSG#ERR#Kode dan Nomor Dokumen sudah Pernah digunakan#";
				}else{
					$exec = $this->db->insert('t_bc41_dok', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD DOKUMEN BC30','CAR='.$aju.', KODE_DOKUMEN='.$kode_dokAct.', NOMOR='.$NOMOR);
						if ($this->input->post('flagrevisi')) {
							$alasan = $this->input->post('ALASAN');
							$func->main->revisi_log('REVISI DOKUMEN PABEAN BC30','ADD DOKUMEN BC30, CAR='.$aju,$alasan);
						}
						echo "MSG#OK#Simpan data Dokumen Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Dokumen Gagal#";
					}
				}
			}else{
				$NOMOR = $this->input->post('NOMOR');
				$kode_dok = $this->input->post('kode_dok');
				$this->db->where(array('NOMOR_AJU'=>$aju,'KODE_DOKUMEN'=>$kode_dok, "NOMOR"=>$NOMOR));
				$exec = $this->db->update('t_bc41_dok', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT DOKUMEN BC41','CAR='.$aju.', KODE_DOKUMEN='.$kode_dok.', NOMOR='.$NOMOR);
					if ($this->input->post('flagrevisi')) {
						$alasan = $this->input->post('ALASAN');
						$func->main->revisi_log('REVISI DOKUMEN PABEAN BC41','EDIT DOKUMEN BC41, CAR='.$aju,$alasan);
					}
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfdokumen') as $chkitem){
				$arrchk = explode(".", $chkitem);
				$aju  = $arrchk[0];
				$NOMOR = str_replace("|S|","/",str_replace("|T|",".",$arrchk[1]));
				$KDDOK = $arrchk[2];				
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_DOKUMEN' => $KDDOK, 'NOMOR' => $NOMOR));
				$exec = $this->db->delete('t_bc41_dok');	
				$func->main->activity_log('DELETE DOKUMEN BC41','CAR='.$aju.', KODE_DOKUMEN='.$KDDOK.', NOMOR='.$NOMOR);
			}
			if($exec){
				echo "MSG#OK#Hapus data Dokumen Berhasil#".site_url()."/pengeluaran/detil/dokumen/bc41/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Dokumen Gagal#del#";die();
			}
		}
	}
	
	function get_dokumen($aju="",$seri="",$kode_dok=""){	
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");		
		if($aju && $kode_dok){
			$query = "SELECT * FROM t_bc41_dok where nomor_aju = '$aju' AND KODE_DOKUMEN = '$kode_dok' AND NOMOR='$seri'";	
			$hasil = $conn->main->get_result($query);			
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update',
								  'sess' => $row,
								  'kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
				}
			}
		}else{
			$data = array('act' => 'save','kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju,
						  				 'kode_dok' => $kode_dok,
										 'NOMOR'=>$seri));
		return $data;
	}
}
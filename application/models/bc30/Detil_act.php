<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
error_reporting(E_ERROR);

class Detil_act extends CI_Model {

    function list_detil($tipe = "", $aju = "") {
        $this->load->library('newtable');
		$KODE_TRADER = $this->newsession->userdata("KODE_TRADER");
        if($tipe == "barang"){
            $SQL = "SELECT KODE_BARANG 'KODE BARANG', 
					CONCAT_WS(', ',URAIAN_BARANG1,URAIAN_BARANG2,URAIAN_BARANG3,URAIAN_BARANG4) AS 'URAIAN BARANG',
					f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
					CONCAT(NEGARA_ASAL,' - ',f_negara(NEGARA_ASAL)) AS 'ASAL BARANG', JUMLAH_SATUAN AS 'JUM. SATUAN', 
					CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
					CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
					f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU FROM t_bc30_dtl WHERE NOMOR_AJU= '$aju'";

            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'),
                						  array('URAIAN_BARANG1', 'URAIAN BARANG')));			
			$this->newtable->orderby("SERI");
			$this->newtable->sortby("ASC");
        }elseif($tipe == "kemasan"){
            $SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', MERK_KEMASAN AS 'MEREK',
			        NOMOR_AJU FROM t_bc30_kms WHERE NOMOR_AJU= '$aju'";

            $this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));			
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->sortby("ASC");
        }elseif($tipe == "kontainer"){
            $SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', f_ref('PART_OF',JNPARTOF) AS 'JNPART OF', 
					f_ref('TYPE_CNT',TIPE) AS 'TIPE', f_ref('TIPE_CNT',STUFF) AS 'STUFF',
					NOMOR_AJU FROM t_bc30_cnt WHERE NOMOR_AJU= '$aju'";

            $this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TYPE_CNT', 'TIPE')));			
			$this->newtable->orderby("NOMOR");
			$this->newtable->sortby("ASC");
        }elseif($tipe == "dokumen"){
            $SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', NOMOR AS 'NOMOR DOKUMEN',
					DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN', NOMOR_AJU FROM t_bc30_dok  
					WHERE NOMOR_AJU= '$aju'";

            $this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));			
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->sortby("ASC");
        }else{
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url() . "/pemasukan/listdetil/" . $tipe . "/" . $aju . "/bc30");
        $this->newtable->cidb($this->db);
        $this->newtable->ciuri($ciuri);
        $this->newtable->set_formid("f" . $tipe);
        $this->newtable->set_divid("div" . $tipe);
        $this->newtable->hiddens(array('NOMOR_AJU'));
        $this->newtable->rowcount(10);
        $this->newtable->show_chk(false);
        $this->newtable->clear();
        $this->newtable->menu($prosesnya);
        $tabel .= $this->newtable->generate($SQL);
        return $tabel;
    }

    function list_tab($aju = "") {
		$content = '<div class="row-fluid">
				  		<div class="tabbable">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active"> <a data-toggle="tab" href="#tabBarang">Data Barang</a></li>
	 							<li> <a data-toggle="tab" href="#tabKemasan">Data Kemasan</a></li>					  
	 							<li> <a data-toggle="tab" href="#tabKontainer">Data Kontainer</a></li>
	 							<li> <a data-toggle="tab" href="#tabDokumen">Data Dokumen</a></li>
							</ul>
							<div class="tab-content">
								<div id="tabBarang" class="tab-pane in active">'.$this->list_detil("barang",$aju).'</div>
							 	<div id="tabKemasan" class="tab-pane">'.$this->list_detil("kemasan",$aju).'</div>
							 	<div id="tabKontainer" class="tab-pane">'.$this->list_detil("kontainer",$aju).'</div>
							 	<div id="tabDokumen" class="tab-pane">'.$this->list_detil("dokumen",$aju).'</div>
							</div>
				  		</div>
					</div>';
        return $content;
    }

    function detil($type = "", $dokumen = "", $aju = "", $action = "", $PERBAIKAN = "") {
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
        $this->load->library('newtable');
        if($type == "barang"){
            $judul = "Daftar Detil Barang";
            $SQL = "SELECT KODE_BARANG 'KODE BARANG', 
                    CONCAT_WS(', ',URAIAN_BARANG1,URAIAN_BARANG2,URAIAN_BARANG3,URAIAN_BARANG4) AS 'URAIAN BARANG',
                    REPLACE(KODE_BARANG, '.', '^') 'KODE_BARANG',
                    f_ref('ASAL_JENIS_BARANG',JNS_BARANG) 'JENIS BARANG',f_formaths(KODE_HS) AS 'KODE HS', SERI 'SERI', 
                    CONCAT(NEGARA_ASAL,' - ',f_negara(NEGARA_ASAL)) AS 'ASAL BARANG', JUMLAH_SATUAN AS 'JUM. SATUAN', 
                    CONCAT(KODE_SATUAN,' - ',f_satuan(KODE_SATUAN)) AS 'KODE SATUAN', JUMLAH_KEMASAN AS 'JUM. KEMASAN',  
                    CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'JENIS KEMASAN', NETTO AS 'NETTO',
                    f_ref('STATUS',STATUS) AS 'STATUS',NOMOR_AJU ,NOMOR_AJU AS KEY1
					FROM t_bc30_dtl WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";

            $this->newtable->search(array(array('KODE_BARANG', 'KODE BARANG'),
                						  array('URAIAN_BARANG1', 'URAIAN BARANG')));
        }elseif($type == "kemasan"){
            $judul = "Daftar Detil Kemasan";
            $SQL = "SELECT JUMLAH AS 'JUMLAH', CONCAT(KODE_KEMASAN,' - ',f_kemas(KODE_KEMASAN)) AS 'KODE KEMASAN', MERK_KEMASAN AS 'MEREK',
			        NOMOR_AJU, KODE_KEMASAN AS 'SERI', KODE_KEMASAN AS KODE_BARANG, KODE_KEMASAN AS KEY1
					FROM t_bc30_kms WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";

            $this->newtable->search(array(array('KODE_KEMASAN', 'KODE KEMASAN'), array('MERK_KEMASAN', 'MEREK')));
        }elseif($type == "kontainer"){
            $judul = "Daftar Detil Kontainer";
            $SQL = "SELECT NOMOR AS 'NOMOR CONTAINER', f_ref('UKURAN_CNT',UKURAN) AS 'UKURAN', f_ref('PART_OF',JNPARTOF) AS 'JNPART OF', 
					f_ref('TYPE_CNT',TIPE) AS 'TIPE', f_ref('TIPE_CNT',STUFF) AS 'STUFF',
					NOMOR AS 'SERI', NOMOR_AJU, NOMOR AS KODE_BARANG, NOMOR AS KEY1
					FROM t_bc30_cnt WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";

            $this->newtable->search(array(array('NOMOR', 'NOMOR CONTAINER'), array('UKURAN_CNT', 'UKURAN'), array('TYPE_CNT', 'TIPE')));
        }elseif($type == "dokumen"){
            $judul = "Daftar Detil Dokumen";
            $SQL = "SELECT KODE_DOKUMEN AS 'KODE DOKUMEN', f_ref('DOKUMEN_UTAMA',KODE_DOKUMEN) AS 'JENIS DOKUMEN', NOMOR AS 'NOMOR DOKUMEN',
					DATE_FORMAT(TANGGAL,'%d %M %Y') AS 'TANGGAL DOKUMEN',
					REPLACE(NOMOR,'/','~') AS 'SERI', 
					KODE_DOKUMEN AS KODE_BARANG, KODE_DOKUMEN AS KEY1,
					NOMOR_AJU FROM t_bc30_dok  
					WHERE NOMOR_AJU= '".$aju."' AND KODE_TRADER = '".$kode_trader."'";

            $this->newtable->search(array(array('KODE_DOKUMEN', 'KODE DOKUMEN'), array('NOMOR', 'NOMOR DOKUMEN')));
        }else{
            return "Failed";
            exit();
        }
        $ciuri = (!$this->input->post("ajax")) ? $this->uri->segment_array() : $this->input->post("uri");
        $this->newtable->action(site_url() . "/pemasukan/detil/" . $type . "/bc30/" . $aju);
        $this->newtable->cidb($this->db);
        $this->newtable->tipe_proses('button');
        $this->newtable->ciuri($ciuri);
        $this->newtable->rowcount(20);
        $this->newtable->clear();
        $this->newtable->set_formid("f" . $type);
        $this->newtable->set_divid("div" . $type);

        if ($PERBAIKAN) {
            $FPERBAIKAN = '<form id="fperbaikan_">
			<input type="hidden" name="PERBAIKAN[KODE_KPBC]" id="PERBAIKAN_KODE_KPBC" value="' . $PERBAIKAN["KODE_KPBC"] . '"/>
			<input type="hidden" name="PERBAIKAN[NOMOR_SURAT]" id="PERBAIKAN_NOMOR_SURAT" value="' . $PERBAIKAN["NOMOR_SURAT"] . '"/>
			<input type="hidden" name="PERBAIKAN[TANGGAL_SURAT]" id="PERBAIKAN_TANGGAL_SURAT" value="' . $PERBAIKAN["TANGGAL_SURAT"] . '"/>
			<input type="hidden" name="PERBAIKAN[NAMA_PEJABAT]" id="PERBAIKAN_NAMA_PEJABAT" value="' . $PERBAIKAN["NAMA_PEJABAT"] . '"/>
			<input type="hidden" name="PERBAIKAN[JABATAN]" id="PERBAIKAN_JABATAN" value="' . $PERBAIKAN["JABATAN"] . '"/>
			<input type="hidden" name="PERBAIKAN[NIP]" id="PERBAIKAN_NIP" value="' . $PERBAIKAN["NIP"] . '"/>
			</form>';
            echo $FPERBAIKAN;
        }

        if($type == 'barang'){
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG", "KEY1"));
            $this->newtable->hiddens(array("NOMOR_AJU","KODE_BARANG","KEY1"));
			$this->newtable->orderby("SERI");
			$this->newtable->sortby("ASC");
        }elseif($type == 'kemasan'){
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG", "KEY1"));
            $this->newtable->hiddens(array('SERI', "KODE_BARANG", "KEY1", "NOMOR_AJU"));
			$this->newtable->orderby("KODE_KEMASAN");
			$this->newtable->sortby("ASC");
        }elseif($type == 'kontainer'){
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG", "KEY1"));
            $this->newtable->hiddens(array('SERI', "KODE_BARANG", "KEY1","NOMOR_AJU"));
			$this->newtable->orderby("NOMOR");
			$this->newtable->sortby("ASC");
        }elseif($type == 'dokumen'){
            $this->newtable->keys(array("NOMOR_AJU", "SERI", "KODE_BARANG", "KEY1"));
            $this->newtable->hiddens(array('SERI', 'SERI_HS', "KODE_BARANG", "KEY1","NOMOR_AJU"));
			$this->newtable->orderby("KODE_DOKUMEN");
			$this->newtable->sortby("ASC");
        }

        $process = array('Tambah' => array('ADD', site_url() . '/pemasukan/' . $type . '/bc30/' . $aju, '0', 'icon-plus'),
						 'Ubah' => array('EDIT', site_url() . '/pemasukan/' . $type . '/bc30', 'N', 'icon-pencil'),
						 'Hapus' => array('DEL', site_url() . '/pemasukan/' . $type . '/bc30', 'N', 'icon-remove red'));
        $this->newtable->menu($process);
        $tabel .= $this->newtable->generate($SQL);
        $arrdata = array("judul" => $judul,
            "tabel" => $tabel);
        if ($this->input->post("ajax"))
            return $tabel;
        else
            return $arrdata;
    }

}

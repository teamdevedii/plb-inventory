<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Barang_act extends CI_Model {

    function set_perbaikan($TYPE = "", $DATA = "", $BARANG = "") {
        if ($DATA) {
            foreach ($DATA as $x => $y) {
                $PERBAIKAN[$x] = $y;
            }
            $PERBAIKAN["DOKUMEN"] = "BC30";
            $PERBAIKAN["KODE_TRADER"] = $this->newsession->userdata('KODE_TRADER');
            $PERBAIKAN["CREATED_BY"] = $this->newsession->userdata('USER_ID');
            $PERBAIKAN["CREATED_TIME"] = date("Y_m-d H:i:s");
            switch (strtolower($TYPE)) {
                case "save";
                    $PERBAIKAN["AKSI"] = "TAMBAH";
                    break;
                case "update";
                    $PERBAIKAN["AKSI"] = "UBAH";
                    break;
                case "delete";
                    $PERBAIKAN["AKSI"] = "HAPUS";
                    break;
            }
            if (strtolower($TYPE) == "delete") {
                $select = $this->db->get_where('t_bc30_dtl', $BARANG);
                $DTL = $select->result_array();
                if ($select->num_rows()) {
                    $this->db->insert('t_bc30_perubahan', array_merge($PERBAIKAN, $DTL[0]));
                }
            } else {
                $this->db->insert('t_bc30_perubahan', array_merge($PERBAIKAN, $BARANG));
            }
        }
    }

    function set_barang($type = "", $isajax = "") {
        $func = & get_instance();
        $func->load->model("main", "main", true);
        $aju = $this->input->post('NOMOR_AJU');
        $JUMLAH_SATUAN = $this->input->post('JUMLAH_SATUAN');
        $KODE_BARANG = $this->input->post('KODE_BARANG');
        $JNS_BARANG = $this->input->post('JNS_BARANG');
        $SQL1 = "SELECT STOCK_AKHIR FROM M_TRADER_BARANG WHERE KODE_BARANG='" . $KODE_BARANG . "' 
			   AND JNS_BARANG='" . $JNS_BARANG . "' AND KODE_TRADER='" . $this->newsession->userdata('KODE_TRADER') . "'";
        $jm = $this->db->query($SQL1)->row();
        $jumlahStock = $jm->STOCK_AKHIR;
        $KODE_TRADER = $this->newsession->userdata('KODE_TRADER');
		
        $BARANG["NOMOR_AJU"] = $aju;
        $BARANG["KODE_TRADER"] = $this->newsession->userdata('KODE_TRADER');
        $PJT["KODE_TRADER"] = $this->newsession->userdata('KODE_TRADER');
        if ($type == "save" || $type == "update") {
            foreach ($this->input->post('BARANG') as $a => $b) {
                $BARANG[$a] = $b;
            }
            foreach ($this->input->post('PJT') as $x => $y) {
                $PJT[$x] = $y;
            }
            if ($type == "save") {
                $status = "04";
                $seri = (int) $func->main->get_uraian("SELECT MAX(SERI) AS MAX FROM t_bc30_dtl WHERE NOMOR_AJU = '" . $aju . "' AND KODE_TRADER = '".$KODE_TRADER."'", "MAX") + 1;
                $BARANG["SERI"] = $seri;
                $BARANG["STATUS"] = $status;
                $BARANG["JUMLAH_SATUAN"] = $JUMLAH_SATUAN;
                $BARANG["KODE_BARANG"] = $KODE_BARANG;
                $BARANG["JNS_BARANG"] = $JNS_BARANG;
                $BARANG["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);
                $PJT["NOMOR_AJU"] = $aju;
                $PJT["SERI"] = $seri;
                $PJT["NPWP_EKSPORTIR"] = str_replace("-", "", str_replace(".", "", $PJT["NPWP_EKSPORTIR"]));
                if ($jumlahStock == 'null') {
                    echo "MSG#ERR#Simpan data Barang Gagal. Data barang tidak terdefinisi. Stock tidak ada.#";
                    die();
                }
                if ($jumlahStock < $JUMLAH_SATUAN) {
                    echo "MSG#ERR#Simpan data Barang Gagal. Stock Barang anda tidak mencukupi, jumlah Stock yang ada adalah $jumlahStock#";
                } else {
                    $exec = $this->db->insert('t_bc30_dtl', $BARANG);
                    if ($PJT["ID_EKSPROTIR"])
                        $this->db->insert('t_bc30_pjt', $PJT);
                    if ($exec) {
                        $this->set_perbaikan("save", $this->input->post('PERBAIKAN'), $BARANG);
                        $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc30('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                        $this->db->where(array('NOMOR_AJU' => $aju));
                        $this->db->update('t_bc30_hdr', array("NETTO" => $netto));
                        $func->main->activity_log('ADD DETIL BC30', 'CAR=' . $aju . ', SERI=' . $seri);
                        echo "MSG#OK#Simpan data Barang Berhasil#edit#" . site_url() . "/pemasukan/LoadHeader/bc30/" . $aju . "#";
                    } else {
                        echo "MSG#ERR#Simpan data Barang Gagal#edit#";
                    }
                }
            } else {
                $status = "04";
                $dokin = $this->input->post('DOKIN');
                $seri = $this->input->post('seri');
                $BARANG["STATUS"] = $status;
                $BARANG["JUMLAH_SATUAN"] = $JUMLAH_SATUAN;
                $BARANG["KODE_BARANG"] = $KODE_BARANG;
                $BARANG["JNS_BARANG"] = $JNS_BARANG;
                $BARANG["KODE_HS"] = str_replace(".", "", $BARANG["KODE_HS"]);
                $PJT["NPWP_EKSPORTIR"] = str_replace("-", "", str_replace(".", "", $PJT["NPWP_EKSPORTIR"]));

                if ($jumlahStock == 'null' || $jumlahStock == '') {
                    echo "MSG#ERR#Simpan data Barang Gagal. Data barang tidak terdefinisi. Stock tidak ada.#";
                    die();
                }
                
                $this->db->where(array('NOMOR_AJU' => $aju,'KODE_TRADER'=>$KODE_TRADER, 'SERI' => $seri));
                $exec = $this->db->update('t_bc30_dtl', $BARANG);
                $this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri,'KODE_TRADER'=>$KODE_TRADER));
                $this->db->update('t_bc30_pjt', $PJT);
                if ($exec) {
                    $this->set_perbaikan("update", $this->input->post('PERBAIKAN'), $BARANG);
                    $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc30('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                    $this->db->where(array('NOMOR_AJU' => $aju,"KODE_TRADER"=>$KODE_TRADER));
                    $this->db->update('t_bc30_hdr', array("NETTO" => $netto));
                    $func->main->activity_log('EDIT DETIL BC30', 'CAR=' . $aju . ', SERI=' . $seri);
                    echo "MSG#OK#Update data Barang Berhasil#edit#" . site_url() . "/pemasukan/LoadHeader/bc30/" . $aju . "#";
                } else {
                    echo "MSG#ERR#Update data Barang Gagal#edit#";
                }
                //}
            }
        } else if ($type == "delete") {
            foreach ($this->input->post('tb_chkfbarang') as $chkitem) {
                $arrchk = explode("|", $chkitem);
                $aju = $arrchk[0];
                $seri = $arrchk[1];
                $this->set_perbaikan("delete", $this->input->post('PERBAIKAN'), array('NOMOR_AJU' => $aju, 'SERI' => $seri));
                $this->db->where(array('NOMOR_AJU' => $aju, 'SERI' => $seri, 'KODE_TRADER'=>$KODE_TRADER));
                $exec = $this->db->delete('t_bc30_dtl');
                $func->main->activity_log('DELETE DETIL BC30', 'CAR=' . $aju . ', SERI=' . $seri);
            }
            if ($exec) {
                $netto = (int) $func->main->get_uraian("SELECT IFNULL(f_jumnetto_bc30('" . $aju . "'),0) AS JUM FROM DUAL", "JUM");
                $this->db->where(array('NOMOR_AJU' => $aju,'KODE_TRADER'=>$KODE_TRADER));
                $this->db->update('t_bc30_hdr', array("NETTO" => $netto));
                echo "MSG#OK#Hapus data Barang Berhasil#" . site_url() . "/pemasukan/detil/barang/bc30/" . $aju . "#";
                die();
            } else {
                echo "MSG#ERR#Hapus data Barang Gagal#";
                die();
            }
        }
    }

    function get_barang($aju = "", $seri = "", $seriBB = "", $kdbarang = "") {
        $data = array();
        $conn = get_instance();
        $conn->load->model("main", "main", true);
        $kode_trader = $this->newsession->userdata('KODE_TRADER');
        if ($aju && $seri) {
            $getKode = $this->db->query("SELECT KODE_HARGA FROM T_BC30_HDR WHERE NOMOR_AJU='" . $aju . "' AND KODE_TRADER = '".$kode_trader."'")->row();
            $getKode = $getKode->KODE_HARGA;
            $query = "SELECT A.*,f_negara(A.NEGARA_ASAL) URAIAN_NEGARA,f_satuan(A.KODE_SATUAN) URAIAN_SATUAN,
                    	(INVOICE/JUMLAH_SATUAN) AS 'HARGA_SATUAN',A.INVOICE AS 'INVOICE',
						C.KODE_SATUAN AS KD_SAT_BESAR,
						C.KODE_SATUAN_TERKECIL AS KD_SAT_KECIL 
                    FROM t_bc30_dtl A
						INNER JOIN t_bc30_hdr B ON a.NOMOR_AJU=b.NOMOR_AJU
						LEFT JOIN M_TRADER_BARANG C ON C.KODE_BARANG=A.KODE_BARANG AND C.JNS_BARANG=A.JNS_BARANG 
						AND C.KODE_TRADER='" . $KODETRADER . "'
                    WHERE a.NOMOR_AJU = '" . $aju . "' AND a.SERI = '" . $seri . "'";
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $data = array('act' => 'update','sess' => $row,'aju' => $aju,'seri' => $seri,'kode_harga' => $getKode);
                }
                $QQ = "SELECT *, f_negara(NEGARA_PEMBELI) URNEGARA_PEMBELI FROM t_bc30_pjt WHERE NOMOR_AJU = '$aju' AND SERI = '$seri'";
                $pjt = $conn->main->get_result($QQ);
                if ($pjt) {
                    foreach ($QQ->result_array() as $rows) {
                        $data = array_merge($data, array('pjt' => $rows));
                    }
                }
            }
        } else {
            $data = array('act' => 'save', 'aju' => $aju);
        }
		 $data = array_merge($data, array('jenis_komoditi' => $conn->main->get_mtabel('KATEGORI_EKSPOR'),
                'izin_ekspor' => $conn->main->get_mtabel('IZIN_EKSPOR'),
                'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_KIRIM'),
                'penggunaan' => $conn->main->get_mtabel('PENGGUNAAN_BARANG'),
                'komoditi_cukai' => $conn->main->get_mtabel('KOMODITI_CUKAI'),
                'jenis_tarif' => $conn->main->get_mtabel('JENIS_TARIF_BM', 1, TRUE, "AND KODE IN ('1','2')"),
                'kode_bm' => $conn->main->get_mtabel('STATUS_BAYAR'),
                'kode_ppn' => $conn->main->get_mtabel('STATUS_BAYAR'),
                'kode_ppnbm' => $conn->main->get_mtabel('STATUS_BAYAR'),
                'kode_pph' => $conn->main->get_mtabel('STATUS_BAYAR'),
                'kode_id_trader' => $conn->main->get_mtabel('KODE_ID', 1, FALSE),
                'kode_cukai' => $conn->main->get_mtabel('STATUS_BAYAR')));
        return $data;
    }

    function getDokumenIn($aju) {
        if ($aju) {
            $sqlOut = "SELECT c.NO_DOK, c.NO_DOK_MASUK, c.TGL_DOK_MASUK, c.JUMLAH, c.LOGID_MASUK, c.SERI_BARANG_MASUK
                        FROM t_bc30_hdr a
                        INNER JOIN t_bc30_dtl b ON b.`NOMOR_AJU` = a.`NOMOR_AJU`
                        INNER JOIN t_logbook_pengeluaran c ON c.`NO_DOK` = a.`NOMOR_PENDAFTARAN` AND c.`TGL_DOK` = a.`TANGGAL_PENDAFTARAN`
                        AND c.`KODE_BARANG` = b.`KODE_BARANG`
                        WHERE a.`NOMOR_AJU` = '" . $aju . "' AND a.KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'";
            $dokOut = $this->db->query($sqlOut);
            if ($dokOut->num_rows() > 0) {
                foreach ($dokOut->result_array() as $rows) {
                    $no_dok = $rows['NO_DOK'];
                    $no_dok_in = $rows['NO_DOK_MASUK'];
                    $tgl_dok_in = $rows['TGL_DOK_MASUK'];
                    $jumlah = $rows['JUMLAH'];
                    $logid_masuk = $rows['LOGID_MASUK'];
                    if ($logid_masuk != "") {
                        "SELECT NO_DOK, TGL_DOK, " . $jumlah . " 'JUMLAH', JENIS_DOK, b.SATUAN, b.SALDO, b.SERI_BARANG
                                FROM t_logbook_pemasukan 
                                WHERE LOGID = '" . $logid_masuk . "'
                                AND KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'";
                    } else {
                        if ($rows['SERI_BARANG_MASUK'] != "")
                            $statement = "AND b.`SERI_BARANG` = a.`SERI_BARANG_MASUK`";
                        else
                            $statement = "";
                        $sql = "SELECT IF(a.LOGID_MASUK = '' OR a.LOGID_MASUK IS NULL, b.LOGID, a.LOGID_MASUK) 'LOGID', 
                                b.`NO_DOK`, b.`TGL_DOK`, a.`JUMLAH`, b.`JENIS_DOK`, b.`SATUAN`, b.SALDO, b.SERI_BARANG
                                FROM t_logbook_pengeluaran a 
                                INNER JOIN t_logbook_pemasukan b ON b.`NO_DOK` = a.`NO_DOK_MASUK` AND b.`TGL_DOK` = a.`TGL_DOK_MASUK`
                                AND b.`KODE_BARANG` = a.`KODE_BARANG` " . $statement . "
                                WHERE b.`NO_DOK` = '" . $no_dok_in . "' AND b.`TGL_DOK` = '" . $tgl_dok_in . "' AND a.`NO_DOK` = '$no_dok'
                                AND a.KODE_TRADER = '" . $this->newsession->userdata('KODE_TRADER') . "'
                                GROUP BY b.`NO_DOK`";
                    }
                    $getDokIn = $this->db->query($sql);
                    if ($getDokIn->num_rows() > 0) {
                        foreach ($getDokIn->result_array() as $rowIn) {
                            ++$i;
                            $data .= '<tr id="pemasukan' . $i . '">
                            <td style="text-align: center;">
                                <input type="hidden" id="logidIn' . $i . '" style="border-style: none;background-color: transparent;width:100px;" readonly="readonly" value="' . $rowIn['LOGID'] . '" name="DOKIN[' . $i . '][LOGID]" />
                                <input type="text" id="noDaftar' . $i . '" style="border-style: none;background-color: transparent;width:100px;" readonly="readonly" value="' . $rowIn['NO_DOK'] . '" name="DOKIN[' . $i . '][NO_DOK]" />
                            </td>
                            <td style="text-align: center;">
                                <input type="text" id="tglDaftar' . $i . '" style="border-style: none;background-color: transparent;width:80px;" readonly="readonly" value="' . $rowIn['TGL_DOK'] . '" name="DOKIN[' . $i . '][TGL_DOK]" />
                            </td>
                            <td style="text-align: center;">
                                <input id="saldoIn' . $i . '" class="stext" type="hidden" readonly="readonly" value="' . $rowIn['SALDO'] . '" name="DOKIN[' . $i . '][SALDO]" />
                                <input id="JUMLAHIN' . $i . '" class="stext" type="text" readonly="readonly" value="' . $rowIn['JUMLAH'] . '" onkeyup="cekJumlahIn(' . $i . ', this.value)" name="DOKIN[' . $i . '][JUMLAH]" />
                            </td>
                            <td style="text-align: center;">
                                <input type="text" id="satuanIn' . $i . '" style="border-style: none;background-color: transparent;width:40px;" readonly="readonly" value="' . $rowIn['SATUAN'] . '" name="DOKIN[' . $i . '][SATUAN]" />
                            </td>
                            <td style="text-align: center;">
                                <input type="text" id="dokumenIn' . $i . '" style="border-style: none;background-color: transparent;width:50px;" readonly="readonly" value="' . $rowIn['JENIS_DOK'] . '" name="DOKIN[' . $i . '][JENIS_DOK]" />
                                <input type="hidden" name="DOKIN[' . $i . '][SERI_BARANG]" value="' . $rowIn['SERI_BARANG'] . '" />
                            </td>
                            <td style="text-align: center;" id="act' . $i . '">
                                <input class="button" id="rowEdit' . $i . '" type="button" value="Edit" onclick="editRow(' . $i . ')">
                                <input class="button" id="rowDelete' . $i . '" type="button" value="Delete" onclick="delRow(' . $i . ')">
                            </td>
                        </tr>';
                        }
                    }
                }
            }
        } else {
            $data = "";
        }
        return $data;
    }

}

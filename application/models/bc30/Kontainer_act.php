<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kontainer_act extends CI_Model{
	function set_kontainer($type="", $isajax=""){		
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = 	$this->newsession->userdata('KODE_TRADER');
		if($type=="save" || $type=="update"){
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('KONTAINER') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"] = $kode_trader;
			}
			if($type=="save"){
				$kode_cntAct = $this->input->post('NOMOR');
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS JUM FROM T_BC30_CNT WHERE NOMOR_AJU='".$aju."' and NOMOR = '".$kode_cntAct."' AND KODE_TRADER = '".$kode_trader."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;
				$arrinsert["NOMOR"] = $kode_cntAct;
				if($countKode > 0){
					echo "MSG#ERR#Simpan data Container Gagal<br><p style=\"margin-left:166px;\">Nomor Container Sudah Pernah digunakan</p>#";
				}else{
					$exec = $this->db->insert('t_bc30_cnt', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD KONTAINER BC30','CAR='.$aju.', NOMOR='.$arrinsert["NOMOR"]);
						echo "MSG#OK#Simpan data Container Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Container Gagal#";
					}
				}
			}else{	
				$nomor = $this->input->post('nomor');			
				$this->db->where(array('NOMOR_AJU' => $aju, 'NOMOR' => $nomor, 'KODE_TRADER'=>$kode_trader));
				$exec = $this->db->update('t_bc30_cnt', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT KONTAINER BC30','CAR='.$aju.', NOMOR='.$nomor);
					echo "MSG#OK#Update data kontainer Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data kontainer Berhasil#edit#";
				}				
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkontainer') as $chkitem){
					$arrchk = explode("|", $chkitem);
					$aju  = $arrchk[0];
					$nomor = $arrchk[1];				
					$this->db->where(array('NOMOR_AJU' => $aju, 'NOMOR' => $nomor, 'KODE_TRADER'=>$kode_trader));
					$exec = $this->db->delete('t_bc30_cnt');	
					$func->main->activity_log('DELETE KONTAINER BC30','CAR='.$aju.', NOMOR='.$nomor);	
				}
				if($exec){
					echo "MSG#OK#Hapus data kontainer Berhasil#".site_url()."/pemasukan/detil/kontainer/bc30/".$aju."#";die();
				}else{					
					echo "MSG#ERR#Hapus data kontainer Gagal#del#";die();
				}
		}
	}

	 function get_kontainer($aju="", $nomor=""){
		$kode_trader = 	$this->newsession->userdata('KODE_TRADER');
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		if($aju && $nomor){
			$query = "Select * from t_bc30_cnt where nomor_aju = '".$aju."' AND nomor = '".$nomor."'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update', 'sess' => $row,
								  'TIPE'=>  $conn->main->get_mtabel('TYPE_CNT'),
								  'UKURAN'=>  $conn->main->get_mtabel('UKURAN_CNT'),
								  'STUFF'=>  $conn->main->get_mtabel('TIPE_CNT'),
								  'JNPARTOF'=>  $conn->main->get_mtabel('PART_OF'));
				}
			}
		}else{
			$data = array('act' => 'save',
						  'TIPE'=>  $conn->main->get_mtabel('TYPE_CNT'),
						  'UKURAN'=>  $conn->main->get_mtabel('UKURAN_CNT'),
						  'STUFF'=>  $conn->main->get_mtabel('TIPE_CNT'),
						  'JNPARTOF'=>  $conn->main->get_mtabel('PART_OF'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju, 'nomor' => $nomor));
		return $data;
	}
}
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Kemasan_act extends CI_Model{
	function set_kemasan($type="", $isajax=""){	
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($type=="save" || $type=="update"){
			$aju = $this->input->post('NOMOR_AJU');
			foreach($this->input->post('KEMASAN') as $a => $b){
				$arrinsert[$a] = $b;
			}		
			$arrinsert["KODE_TRADER"] = $kode_trader;
			if($type=="save"){
				$kode_kemasAct = $this->input->post('KODE_KEMASAN');
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(*) AS JUM FROM T_BC30_KMS WHERE NOMOR_AJU='".$aju."' and KODE_KEMASAN = '".$kode_kemasAct."' AND KODE_TRADER = '".$kode_trader."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;
				$arrinsert["KODE_KEMASAN"] = $kode_kemasAct;
				if($countKode > 0){
					echo "MSG#ERR#Jenis Kemasan Sudah ada.#";die();
				}else{
					$exec = $this->db->insert('t_bc30_kms', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD KEMASAN BC30','CAR='.$aju.', KODE_KEMASAN='.$arrinsert["KODE_KEMASAN"]);
						echo "MSG#OK#Simpan data Kemasan Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Kemasan Gagal#";
					}
				}
			}else{
				$kode_kemas = $this->input->post('kode_kemas');	
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas, 'KODE_TRADER'=>$kode_trader));
				$exec=$this->db->update('t_bc30_kms', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT KEMASAN BC30','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Kemasan Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfkemasan') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$kode_kemas = $arrchk[1];				
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_KEMASAN' => $kode_kemas, 'KODE_TRADER'=>$kode_trader));
				$exec = $this->db->delete('t_bc30_kms');	
				$func->main->activity_log('DELETE KEMASAN BC262','CAR='.$aju.', KODE_KEMASAN='.$kode_kemas);
			}
			if($exec){
				echo "MSG#OK#Hapus data Kemasan Berhasil#".site_url()."/pemasukan/detil/kemasan/bc30/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Kemasan Gagal#del#";die();
			}
		}
	}
	
	
	 function get_kemasan($aju="", $kode_kemas=""){
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($aju && $kode_kemas){
			$query = "Select JUMLAH, MERK_KEMASAN, KODE_KEMASAN,f_kemas(kode_kemasan) URAIAN_KEMASAN 
					  from t_bc30_kms where nomor_aju = '".$aju."' 
					  AND kode_kemasan = '".$kode_kemas."' AND KODE_TRADER = '".$kode_trader."'";
			$hasil = $conn->main->get_result($query);
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess' => $row);
				}
			}
		}else{
			$data = array('act' => 'save');
		}
		$data['aju'] = $aju;
		$data = array_merge($data, array('aju' => $aju, 'kode_kemas' => $kode_kemas));
		return $data;
	}
}
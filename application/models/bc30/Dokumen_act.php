<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);
class Dokumen_act extends CI_Model{
	function set_dokumen($type="", $isajax=""){	
		$func =& get_instance();
		$func->load->model("main", "main", true);
		$aju = $this->input->post('NOMOR_AJU');
		$kode_trader = $this->newsession->userdata('KODE_TRADER');	
		if($type=="save" || $type=="update"){
			foreach($this->input->post('DOKUMEN') as $a => $b){
				$arrinsert[$a] = $b;
				$arrinsert["KODE_TRADER"] = $kode_trader;	
			}
			if($type=="save"){
				$kode_dokAct = $this->input->post('KODE_DOKUMEN');
				$countKode = (int)$func->main->get_uraian("SELECT COUNT(NOMOR_AJU) AS JUM FROM T_bc30_DOK WHERE NOMOR_AJU='".$aju."' 
														   AND KODE_DOKUMEN = '".$kode_dokAct."' AND NOMOR='".$arrinsert["NOMOR"]."'
														   AND KODE_TRADER = '".$kode_trader."'", "JUM");
				$arrinsert["NOMOR_AJU"] = $aju;
				$arrinsert["KODE_DOKUMEN"] = $kode_dokAct;
				$NOMOR = $arrinsert["NOMOR"];
				if($countKode > 0){
					echo "MSG#ERR#Kode dan Nomor Dokumen sudah Pernah digunakan#";
				}else{
					$exec = $this->db->insert('t_bc30_dok', $arrinsert);
					if($exec){
						$func->main->activity_log('ADD DOKUMEN BC30','CAR='.$aju.', KODE_DOKUMEN='.$kode_dokAct.', NOMOR='.$NOMOR);
						echo "MSG#OK#Simpan data Dokumen Berhasil#";
					}else{					
						echo "MSG#ERR#Simpan data Dokumen Gagal#";
					}
				}
			}else{
				$NOMOR = $this->input->post('NOMOR');
				$kode_dok = $this->input->post('kode_dok');
				$this->db->where(array('NOMOR_AJU'=>$aju,'KODE_DOKUMEN'=>$kode_dok, "NOMOR"=>$NOMOR, "KODE_TRADER"=>$kode_trader));
				$exec = $this->db->update('t_bc30_dok', $arrinsert);
				if($exec){
					$func->main->activity_log('EDIT DOKUMEN BC30','CAR='.$aju.', KODE_DOKUMEN='.$kode_dokAct.', NOMOR='.$NOMOR);
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}else{					
					echo "MSG#OK#Update data Dokumen Berhasil#edit#";
				}
			}
		}else if($type=="delete"){
			foreach($this->input->post('tb_chkfdokumen') as $chkitem){
				$arrchk = explode("|", $chkitem);
				$aju  = $arrchk[0];
				$NOMOR = str_replace("~","/",$arrchk[1]);
				$KDDOK = $arrchk[2];				
				$this->db->where(array('NOMOR_AJU' => $aju, 'KODE_DOKUMEN' => $KDDOK, 'NOMOR' => $NOMOR, "KODE_TRADER"=>$kode_trader));
				$exec = $this->db->delete('t_bc30_dok');	
				$func->main->activity_log('DELETE DOKUMEN BC30','CAR='.$aju.', KODE_DOKUMEN='.$KDDOK.', NOMOR='.$NOMOR);
			}
			if($exec){
				echo "MSG#OK#Hapus data Dokumen Berhasil#".site_url()."/pemasukan/detil/dokumen/bc30/".$aju."#";die();
			}else{					
				echo "MSG#ERR#Hapus data Dokumen Gagal#del#";die();
			}
		}
	}
	
	function get_dokumen($aju="",$seri="",$kode_dok=""){	
		$data = array();
		$conn = get_instance();
		$conn->load->model("main");
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		if($aju && $seri){
			$query = "SELECT * FROM t_bc30_dok where nomor_aju = '".$aju."' AND KODE_DOKUMEN = '".$kode_dok."' AND NOMOR='".$seri."'";	
			$hasil = $conn->main->get_result($query);			
			if($hasil){
				foreach($query->result_array() as $row){
					$data = array('act' => 'update','sess' => $row,
								  'kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
				}
			}
		}else{
			$data = array('act' => 'save','kode_dokumen'=> $conn->main->get_mtabel('DOKUMEN_UTAMA'));
		}
		$data['aju'] = $aju;
		$data = array_merge($data,array('aju' => $aju,'kode_dok' => $kode_dok,'NOMOR'=>$seri));
		return $data;
	}
}
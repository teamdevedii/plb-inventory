<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ERROR);

class Header_act extends CI_Model {

    function get_header($aju = "") {
        $data = array();
        $conn = get_instance();
        $conn->load->model("main");
		$kode_trader = $this->newsession->userdata("KODE_TRADER");
		$user_id = $this->newsession->userdata("USER_ID");
        if ($aju) {
            $query = "Select A.*,f_kpbc(A.KODE_KPBC) URAIAN_KPBC, f_daerah(A.PROPINSI_BARANG) URAIAN_DAERAH_ASL,
                                f_kpbc(A.KPBC_PERIKSA) URAIAN_KPBC_AWAS,f_negara(A.NEGARA_PEMBELI) as URAIAN_NEGARA_PARTNER, 
                                f_negara(A.NEGARA_PEMBELI) URAIAN_BENDERA, f_daerah(A.PROPINSI_BARANG) URAIAN_DAERAH_ASL, 
                                f_pelab(A.PELABUHAN_MUAT) URAIAN_MUAT, f_negara(A.BENDERA) BENDERAUR, 
                                f_negara(A.NEGARA_TUJUAN) URAIAN_NEG_TUJUAN,f_pelab(A.PELABUHAN_TRANSIT) URAIAN_TRANSIT,
                                f_pelab(A.PELABUHAN_BONGKAR) URAIAN_BONGKAR,f_pelab(A.PELABUHAN_MUAT_EKSPOR) URAIAN_MUAT_EKS,
                                f_valuta(A.KODE_VALUTA) URAIAN_VALUTA,
                                (SELECT URAIAN_HANGGAR FROM M_HANGGAR WHERE KODE_HANGGAR=A.KODE_HANGGAR) URAIAN_HANGGAR,
                                (SELECT SUM(NILAI_PE) FROM t_bc30_DTL WHERE NOMOR_AJU='$aju') JUM_BK,
                                f_status_bc30(A.NOMOR_AJU,1) STATUS_DOK from t_bc30_hdr A where A.nomor_aju = '$aju'";
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            if (!$dataarray["KODE_ID_TRADER"]) {
                $query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, 
					  A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
					  B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', 
					  C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', D.NAMA_TTD, D.KOTA_TTD,
					  0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
					  0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 
					  0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
					  FROM M_TRADER A 
					  INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
					  LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER 
					  LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
					  WHERE A.KODE_TRADER='" . $this->newsession->userdata('KODE_TRADER') . "'
					  AND C.USER_ID=" . $this->newsession->userdata('USER_ID') . " ";

                $hasil = $conn->main->get_result($query);
                if ($hasil) {
                    foreach ($query->result_array() as $row) {
                        $dt = $row;
                    }
                }
                $dataarray = array_merge($dataarray, $dt);
            }
            $data = array('act' => 'update');
        } else {
            $query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, 
					  	A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
					  	B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', 
					  	C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', D.NAMA_TTD, D.KOTA_TTD,
					  	0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
					  	0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 
					  	0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
					  FROM M_TRADER A 
					  	INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
					  	LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER 
					  	LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
					  WHERE A.KODE_TRADER='".$kode_trader."' AND C.USER_ID=".$user_id." LIMIT 0,1";

            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'save', 'aju' => $aju);
        }
        $data = array_merge($data, array('aju' => $aju,
            'sess' => $dataarray,
            'tujuan_kirim' => $conn->main->get_mtabel('TUJUAN_KIRIM'),
            'status' => $conn->main->get_mtabel('JENIS_EKSPORTIR'),
            'kode_id' => $conn->main->get_mtabel('KODE_ID'),
            'cara_angkut' => $conn->main->get_mtabel('MODA'),
            'tempat_asal' => $conn->main->get_mtabel('KBINOUT'),
            'jenis_tpb' => $conn->main->get_mtabel('JENIS_TPB'),
            'jenis_ekspor' => $conn->main->get_mtabel('JENIS_EKSPOR'),
            'kategori_ekspor' => $conn->main->get_mtabel('KATEGORI_EKSPOR'),
            'lokasi_pemeriksaan' => $conn->main->get_mtabel('LOKASI_PEMERIKSAAN'),
            'cara_penyerahan' => $conn->main->get_mtabel('PENYERAHAN_BARANG'),
            'cara_dagang' => $conn->main->get_mtabel('CARA_DAGANG'),
            'cara_bayar' => $conn->main->get_mtabel('CARA_BAYAR'),
            'jenis_komoditi' => $conn->main->get_mtabel('JENIS_KOMODITI'),
            'jenis_asuransi' => $conn->main->get_mtabel('KODE_ASURANSI_EKSPOR'))
        );
        return $data;
    }

    function set_header($type = "", $isajax = "") {
        $func = get_instance();
        $func->load->model("main", "main", true);
		$kode_trader = $this->newsession->userdata('KODE_TRADER');
		$user_id = $this->newsession->userdata('USER_ID');
        if (strtolower($type) == "save" || strtolower($type) == "update") {
            foreach ($this->input->post('HEADER') as $a => $b) {
                $HEADER[$a] = $b;
            }
            $ID_TRADER = str_replace('-', '', $HEADER['ID_TRADER']);
            $HEADER['ID_TRADER'] = str_replace('.', '', $ID_TRADER);
            if (strtolower($type) == "save") {
				if($this->input->post("NOMOR_AJU")){
					$car = $this->input->post("NOMOR_AJU");
				}else{
					$func->main->get_car($car, 'BC30');
				}
                $HEADER["NOMOR_AJU"] = $car;
                $HEADER["CREATED_BY"] = $user_id;
                $HEADER["CREATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $exec = $this->db->insert('t_bc30_hdr', $HEADER);
                $func->main->cekhanggar($HEADER["KODE_HANGGAR"], $this->input->post('URAIAN_HANGGAR'));
                if ($exec) {
					if($this->input->post("NOMOR_AJU")){
                    	$func->main->set_car('BC30');
					}
                    $func->main->activity_log('ADD HEADER BC30', 'CAR=' . $car);
                    echo "MSG#OK#Proses header Berhasil#" . $car . "#" . site_url() . "/pemasukan/LoadHeader/bc30/" . $car . "#bc30#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            } else if (strtolower($type) == "update") {
                foreach ($this->input->post('HEADERDOK') as $a => $b) {
                    $HEADERDOK[$a] = $b;
                }
                $NODAF = $HEADERDOK["NOMOR_PENDAFTARAN"];
                $TGDAF = $HEADERDOK["TANGGAL_PENDAFTARAN"];
                $nodaftarEdit = $HEADERDOK["NOMOR_PENDAFTARAN_EDIT"];
                $tgldaftarEdit = $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"];
                $noDaftar = $HEADERDOK["NOMOR_PENDAFTARAN"];
                $tglDaftar = $HEADERDOK["TANGGAL_PENDAFTARAN"];
                $editHeader = false;
                if ($nodaftarEdit != $noDaftar || $tgldaftarEdit != $tglDaftar) {
                    $editHeader = true;
                    $kodeTrader = $this->newsession->userdata('KODE_TRADER');
                    $nodokOut = "";$nodokHdr="";
                    $tgldokOut = "";$tgldokHdr="";
                    if ($nodaftarEdit != $noDaftar) {
                        $nodokHdr = " NOMOR_PENDAFTARAN = '" . $nodaftarEdit . "' ";
                        $nodokOut = " NO_DOK = '" . $nodaftarEdit . "' ";
                        $HEADERDOK["NOMOR_PENDAFTARAN"] = $HEADERDOK["NOMOR_PENDAFTARAN_EDIT"];
                    }
                    if ($tgldaftarEdit != $tglDaftar) {
                        ($nodaftarEdit != $noDaftar) ? $koma = "," : $koma = "";
                        $tgldokHdr = "$koma TANGGAL_PENDAFTARAN = '" . $tgldaftarEdit . "' ";
                        $tgldokOut = "$koma TGL_DOK = '" . $tgldaftarEdit . "' ";
                        $HEADERDOK["TANGGAL_PENDAFTARAN"] = $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"];
                    }
                    if ($editHeader) {
                        $sqlLogOut = "SELECT LOGID FROM t_logbook_pengeluaran WHERE NO_DOK = '" . $noDaftar . "'
                                     AND TGL_DOK = '" . $tglDaftar . "' AND KODE_TRADER = '" . $kodeTrader . "'";
                        $logOut = $this->db->query($sqlLogOut);
                        $sqlHdr = "SELECT NOMOR_AJU FROM t_bc30_hdr WHERE NOMOR_AJU = '" . $HEADERDOK['NOMOR_AJU'] . "'
                                   AND NOMOR_PENDAFTARAN = '" . $noDaftar . "' AND TANGGAL_PENDAFTARAN = '" . $tglDaftar . "'
                                   AND KODE_TRADER = '" . $kodeTrader . "'";
                        $getHdr = $this->db->query($sqlHdr);
                        if ($getHdr->num_rows() > 0) {
                            $sqlUpdateHdr = "UPDATE t_bc30_hdr SET $nodokHdr $tgldokHdr WHERE NOMOR_AJU = '" . $HEADERDOK['NOMOR_AJU'] . "'
                                            AND NOMOR_PENDAFTARAN='" . $noDaftar . "' AND TANGGAL_PENDAFTARAN='" . $tglDaftar . "'
                                            AND KODE_TRADER = '" . $kodeTrader . "'";
                            $execHdr = $this->db->query($sqlUpdateHdr);
                        }
                        if ($logOut->num_rows() > 0) {
                            $sqlUpdateOut = "UPDATE t_logbook_pengeluaran SET $nodokOut $tgldokOut WHERE NO_DOK = '" . $noDaftar . "' 
                                           AND TGL_DOK = '" . $tglDaftar . "' AND KODE_TRADER = '" . $kodeTrader . "'";
                            $execOut = $this->db->query($sqlUpdateOut);
                        }
                    }
                }
                unset($HEADERDOK["NOMOR_PENDAFTARAN_EDIT"], $HEADERDOK["TANGGAL_PENDAFTARAN_EDIT"]);
                $car = $HEADERDOK["NOMOR_AJU"];
                $HEADER["UPDATED_BY"] = $this->newsession->userdata('USER_ID');
                $HEADER["UPDATED_TIME"] = date('Y-m-d H:i:s', time() - 60 * 60 * 1);
                $this->db->where(array('NOMOR_AJU' => $HEADER["NOMOR_AJU"]));
                if ($this->input->post("STATUS_DOK") == "LENGKAP" && $NODAF && $TGDAF && $this->input->post('flagrevisi') == "") {
                    $HEADERDOK["STATUS"] = '07';
                    $exec = $this->db->update('t_bc30_hdr', array_merge($HEADERDOK, $HEADER));
                } elseif ($this->input->post("STATUS_DOK") == "LENGKAP" && $NODAF && $TGDAF && $this->input->post('flagrevisi') == "1") {
                    $HEADERDOK["STATUS"] = '19';
                    $exec = $this->db->update('t_bc30_hdr', array_merge($HEADERDOK, $HEADER));
                } else {
                    $exec = $this->db->update('t_bc30_hdr', $HEADER);
                }
                $func->main->cekhanggar($HEADER["KODE_HANGGAR"], $this->input->post('URAIAN_HANGGAR'));
                if ($exec) {
                    $func->main->activity_log('EDIT HEADER BC30', 'CAR=' . $car);
                    echo "MSG#OK#Proses header Berhasil#" . $car . "#";
                } else {
                    echo "MSG#ERR#Proses data header Gagal#";
                }
            }
        }
    }

    function get_pkb() {
        $kode_pkb = $this->uri->segment(5);
        $id_pkb = $this->uri->segment(6);
        $aju_pkb = $this->uri->segment(7);
        $data = array();
        $conn = get_instance();
        $conn->load->model("main");
        $SQL = "SELECT URAIAN FROM M_TABEL WHERE JENIS='KATEGORI_EKSPOR' AND KODE='" . $id_pkb . "'";
        $resultPKB = $this->db->query($SQL)->row(); 
        $urPKB = $resultPKB->URAIAN;
        $this->db->where('NOMOR_AJU', $aju_pkb);
        $this->db->from('T_BC30_PKB');
        $jumDt = $this->db->count_all_results();
        if ($jumDt > 0) {
            $SQL = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID, 
					  A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER', A.TELEPON,
					  B.NOMOR_SKEP 'REGISTRASI', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', 
					  C.EMAIL 'EMAIL_CP', ID AS 'ID_TRADER', D.NAMA_TTD, D.KOTA_TTD,
					  0 AS 'FOB', 0 AS 'FREIGHT', 0 AS 'ASURANSI', 0 AS 'CIF', 0 AS 'CIFRP', 
					  0 AS 'BRUTO', 0 AS 'NETTO', 0 AS 'TAMBAHAN', 0 AS 'DISKON', 
					  0 AS 'NILAI_INVOICE', 0 AS 'NDPBM'
					  FROM M_TRADER A 
					  INNER JOIN T_USER C ON A.KODE_TRADER=C.KODE_TRADER 
					  LEFT JOIN M_TRADER_SKEP B ON A.KODE_TRADER=B.KODE_TRADER 
					  LEFT JOIN M_SETTING D ON A.KODE_TRADER=D.KODE_TRADER
					  WHERE A.KODE_TRADER='" . $this->newsession->userdata('KODE_TRADER') . "'
					  AND C.USER_ID=" . $this->newsession->userdata('USER_ID') . " ";
            $resultHeader = $this->db->query($SQL)->row();
            $query = "SELECT *,f_ref('ZONING_KITE',ZONING_KITE) AS 'URAI_ZONING',f_ref('JNS_BRG_PKB',JNS_BARANG) AS 'URAI_JNS_BRG'
			         ,f_ref('TEMPAT_PKB',GUDANG) AS 'URAI_GUDANG',f_ref('JENIS_STUFF',CARA_STUFFING) AS 'URAI_STUFF'
					 ,f_ref('PART_OF',JNPARTOF) AS 'URAI_JNPARTOF'
			         FROM T_BC30_PKB WHERE NOMOR_AJU='" . $aju_pkb . "'";
            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'update', 'aju' => $aju_pkb, 'resultHeader' => $resultHeader, 'uraian_pkb' => $urPKB, 'kode_pkb' => $kode_pkb);
        } else {

            $query = "SELECT A.KODE_TRADER, A.KODE_ID 'KODE_ID_TRADER', A.ID 'ID_TRADER', A.NAMA 'NAMA_TRADER', A.ALAMAT 'ALAMAT_TRADER',
					  A.TELEPON, A.KODE_API, A.NOMOR_API, C.PASSWORD, A.NAMA_PENANGGUNGJAWAB AS NAMA_TTD, A.ALAMAT_PENANGGUNGJAWAB AS KOTA_TTD, 
					  A.STATUS_TRADER 'STATUS_TRADER',B.NOMOR_SKEP 'REGISTRASI', A.JENIS_TRADER 'KODE_TPB', C.NAMA 'NAMA_CP', C.TELEPON 'TELP_CP', 
					  C.EMAIL 'EMAIL_CP' FROM M_TRADER A, M_TRADER_SKEP B, T_USER C WHERE A.KODE_TRADER=B.KODE_TRADER AND 
					  A.KODE_TRADER=C.KODE_TRADER  AND A.KODE_TRADER='" . $this->newsession->userdata('KODE_TRADER') . "'
					  AND C.USER_ID='" . $this->newsession->userdata('USER_ID') . "'";

            $hasil = $conn->main->get_result($query);
            if ($hasil) {
                foreach ($query->result_array() as $row) {
                    $dataarray = $row;
                }
            }
            $data = array('act' => 'save',
                'aju' => $aju_pkb,
                'uraian_pkb' => $urPKB,
                'kode_pkb' => $kode_pkb);
        }
        $data = array_merge($data, array('aju' => $aju_pkb,
            'sess' => $dataarray
                )
        );
        return $data;
    }

    function set_pkb($type = "") {
        if (strtolower($type) == "save" || strtolower($type) == "update") {
            foreach ($this->input->post('PKB') as $a => $b) {
                $insertPKB[$a] = $b;
            }
            if (strtolower($type) == "save") {
                if ($insertPKB["NOMOR_AJU"] != "") {
                    $func = get_instance();
                    $func->load->model("main", "main", true);
                    $insertPKB["STATUS"] = '04';
                    $exec = $this->db->insert('t_bc30_pkb', $insertPKB);
                    if ($exec) {
                        echo "MSG#OK#Penambahan Data PKB Berhasil#";
                    } else {
                        echo "MSG#ERR#Penambahan Data PKB Gagal#";
                    }
                } else {
                    echo "MSG#ERR#Proses data gagal, Simpan dahulu perekaman Data Header!#";
                }
            } else if (strtolower($type) == "update") {
                if ($insertPKB["NOMOR_AJU"] != "") {
                    $this->db->where(array('NOMOR_AJU' => $insertPKB["NOMOR_AJU"]));
                    $exec = $this->db->update('t_bc30_pkb', $insertPKB);
                    if ($exec) {
                        echo "MSG#OK#Perubahan Data PKB Berhasil#";
                    } else {
                        echo "MSG#ERR#Perubahan Data PKB Gagal#";
                    }
                } else {
                    echo "MSG#ERR#Proses data gagal, Simpan dahulu perekaman Data Header!#";
                }
            }
        }
    }

}
